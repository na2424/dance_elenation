﻿Dance Elenation
===============
矢印が判定エリアに重なるタイミングで踏むゲームと、  
足でプレイするj◇beatをオマージュした音楽ゲームです。  
4Keys,9Keys,16Keysモードがあります。

Version
-------
ver1.65 (Update:2017/06)

Usage
-----
インストールしたDance Elenationフォルダ直下にある  
「Dance Elenation.exe」またはショートカットから起動します。
```
基本キー操作
移動…カーソルキー
決定…Enterキー
戻る…Escapeキー
(キーコンフィグで設定できます)
```

Simfiles → [Eternal∞Tune](http://na24ddr.html.xdomain.jp/)
ELEBEATとEternal∞Tuneパッケージを同梱しています。