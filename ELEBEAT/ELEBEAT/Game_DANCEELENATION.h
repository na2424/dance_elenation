﻿#ifndef GAME_DANCEELENATION_H
#define GAME_DANCEELENATION_H

#include <array>       // for array
#include <deque>
#include <unordered_map>

#include "GameBase.h"
#include "define.h"    // for Image, Sound, tstring
#include "memory"      // for shared_ptr

class NoteDanceElenation;
class KeyInput4Key;
class LongNotePic;
class LifeSt;
class Timing;
class ScoreSt;
class Combo;



class GameDanceElenation : public GameBase
{
private:
	std::shared_ptr<NoteDanceElenation> m_note4Key;
	std::shared_ptr<KeyInput4Key> m_keyinput4Key;
	std::shared_ptr<LongNotePic> m_longnoteImage;
	std::shared_ptr<ScoreSt> m_score;
	std::shared_ptr<Combo> m_combo;
	std::shared_ptr<LifeSt> m_life;
	std::shared_ptr<Timing> m_timing;

	tstring m_musicName;
	tstring m_musicSubName;
	tstring m_musicArtist;

	int m_BpmIndex;
	int m_StopIndex;

	double m_RatioImageSize;
	int m_SoftSoundHandle;
	int m_StartTime;                //開始時間を入れる変数
	int m_CurrentTime;                //現在時間を入れる変数
	bool isDead;
	bool isShowJudge;
	int readyState;

	int tempClapedTime = 0;
	double m_scrollSpeed = 1;            //譜面スクロールの倍率を入れるフラグ変数

	int m_EndTime;
	int m_EndNow;

	int m_ScoreAnimationFrame;
	int m_ShutterCount;

	int m_judgeDrawFlag;
	int m_judgeDrawTime;

	std::array<int, 4> m_longCount;
	std::array<bool, 2> m_isDrawReadyGo;

	std::vector <std::vector<bool> > m_hasCrap;

	std::array<int, 3> m_stringWidth;
	std::array<int, 3> m_musicStringX;
	std::array<int, 3> m_moveX;
	std::array<int, 3> m_stopTime;

	//-----------------------
	// 定数.
	//-----------------------
	static const int BUFFER_LENGTH = 4096;

	const std::unordered_map<NoteSkinType, tstring> noteSkinNames
	{
		{ NoteSkinType::RAINBOW,"RAINBOW"},
		{ NoteSkinType::NOTE,"NOTE"},
		{ NoteSkinType::FLAT,"FLAT" }
	};

	//-----------------------
	// フォント.
	//-----------------------
	std::array<int, 2> m_FontHandle;

	//-----------------------
	// 画像.
	//-----------------------
	std::array<Image, 2> m_lifeGageImage;                //ゲージ画像を入れるための配列変数
	std::array<Image, 2> m_shaterImage;
	std::array<Image, 2> m_filterImage;
	std::array<std::array<Image, 10>, 5> m_difnumberColorImage;

	Image m_backUnderImage;

	Image m_comboImage;            //combo!!画像を入れるための変数
	Image m_startImage;                    //「ここからスタート」画像を入れるための変数(9keys,16keys)
	Image m_readyImage;
	Image m_goImage;

	Image m_lifeMaxImage;
	Image m_lifeRedImage;
	Image m_lifeGreenImage;

	Image m_failedImage;
	Image m_backgroundImage;

	Image m_cursorImage;
	Image m_bpmImage;
	Image m_elebeatImage;

	Image m_explosionImage;

	//------------------------
	// 音声.
	//------------------------
	Sound m_readyGoSound;
	Sound m_failedSound;
	Sound m_handClapSound;
	Sound m_backSound;
	Sound m_MusicSound;

	//------------------------
	// キャッシュ.
	//------------------------
	std::array<std::deque<float>, BUFFER_LENGTH> m_cacheFftVibration;

	//------------------------
	// 関数.
	//------------------------
	void ReadLua();
	void PlayMusic(long &now, int nowTime, int &stFlag, bool flagDf[]) const;
	void GetMovingAverage(float paramList[], unsigned int cacheSize);
	void DrawSpectrum(int softSoundHandle, int musicSound);
	void DrawLongNoteJudge(int m, int i);
	void FadeInGame(double ratio) const;
	static void CalculateScrollTextPosition(int moveX[], int stoptime[], const int stringWidth[]);
	bool OnDead(int &count);
	void DrawCombo(const std::shared_ptr<Combo>& fCombo) override;
	void LoadNote(const std::shared_ptr<Music> &musicP) const;

public:
	GameDanceElenation();
	void Initialize(const std::shared_ptr<Music> &musicP) override;
	void Update(const std::shared_ptr<Music>& musicP) override;
	void Finalize(const std::shared_ptr<Music> &musicP) override;
};

#endif