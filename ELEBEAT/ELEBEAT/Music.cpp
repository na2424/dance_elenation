#include <tchar.h>
#include <vector>
#include "Music.h"



Music::Music()
{
	elebeatSong = std::make_unique<ElebeatSong>();
	danceSong = std::make_unique<DanceSong>();
}

ElebeatSong::ElebeatSong(void) :
	//charになってたものをintに変換して保存しとく変数
	m_FolderNum(0),                                //音楽種類(jubeat ddr+append)などの種類を保存
	m_TotalMusicNum(0)                               //全体音楽数の総計
{
	m_Name.resize(0);
	m_SubName.resize(0);
	m_Artist.resize(0);
	m_AlbumArtPath.resize(0);
	m_MusicPath.resize(0);
	m_Offset.resize(0);
	m_Bpm.resize(0);
	m_BpmPositions.resize(0);
	m_MusicNumAt.resize(0);
	m_FolderName.resize(0);
	m_SequencePath.resize(0);
}

void ElebeatSong::PushBackEmpty()
{
	std::vector <tstring> temp(0);
	m_AlbumArtPath.emplace_back(_T("\0"));
	m_MusicPath.emplace_back(_T("\0"));
	m_Name.emplace_back(_T("\0"));
	m_SubName.emplace_back(_T("\0"));
	m_Artist.emplace_back(_T("\0"));
	m_Offset.emplace_back(_T("\0"));
	m_SequencePath.emplace_back(_T("\0"));
	m_Bpm.emplace_back(temp);
	m_BpmPositions.emplace_back(temp);

	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			m_ElebeatDifficulty[i][j].emplace_back(0);
		}
	}
}

DanceSong::DanceSong() :
	m_FolderNum(0),
	m_TotalMusicNum(0)
{
	m_MusicNumAt.resize(0);

	m_Name.resize(0);
	m_SubName.resize(0);
	m_Artist.resize(0);
	m_BannerPath.resize(0);
	m_BackGroundPath.resize(0);
	m_MusicPath.resize(0);
	m_Offset.resize(0);
	m_FolderName.resize(0);
	m_SequencePath.resize(0);
	m_Bpm.resize(0);
	m_BpmPositions.resize(0);
	m_Stop.resize(0);
	m_StopPositions.resize(0);
}

void DanceSong::Reserve(int num)
{
	this->m_BannerPath.reserve(num);
	this->m_BackGroundPath.reserve(num);
	this->m_MusicPath.reserve(num);
	this->m_Name.reserve(num);
	this->m_SubName.reserve(num);
	this->m_Artist.reserve(num);
	this->m_Offset.reserve(num);
	this->m_Bpm.reserve(num);
	this->m_BpmPositions.reserve(num);
	this->m_Stop.reserve(num);
	this->m_StopPositions.reserve(num);
	this->m_SequencePath.reserve(num);
}

void DanceSong::PushBackEmpty()
{
	std::vector<tstring> temp(0);
	std::array<std::array<double, 5>, 5> chartTemps;

	m_BannerPath.emplace_back(_T("\0"));
	m_BackGroundPath.emplace_back(_T("\0"));
	m_MusicPath.emplace_back(_T("\0"));
	m_Name.emplace_back(_T("\0"));
	m_SubName.emplace_back(_T("\0"));
	m_Artist.emplace_back(_T("\0"));
	m_Offset.emplace_back(_T("\0"));
	m_SequencePath.emplace_back(_T("\0"));
	m_Bpm.emplace_back(temp);
	m_BpmPositions.emplace_back(temp);
	m_Stop.emplace_back(temp);
	m_StopPositions.emplace_back(temp);

	for (int di = 0; di < 5; di++)
	{
		m_Difficulty[di].emplace_back(0);
	}
	m_RadarChart.emplace_back(chartTemps);
}