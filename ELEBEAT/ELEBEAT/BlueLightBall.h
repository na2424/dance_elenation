#ifndef BLUE_LIGHT_BALL_H
#define BLUE_LIGHT_BALL_H

#include <vector>
#include <functional>
#include "define.h"
#include "IParticle.h"



class BlueLightBall
{
private:
	static int m_image;

public:
	double x;
	double y;
	double vx;
	double vy;
	double size;

	BlueLightBall() :x(0), y(0), vx(0), vy(0), size(0) {}
	void Init(std::function<void(BlueLightBall*)> f, tstring imagePath);
	void Draw() const;
	void Update(std::function<void(BlueLightBall*)> f);
};

class BlueLightBallList : IParticle
{
private:
	static const int BALL_MAX = 30;
	std::vector<BlueLightBall> m_blueLightBall;

public:
	BlueLightBallList();
	void Init(std::function<void(BlueLightBall*)> f, tstring imagePath = "Graphics/Common/particle.png");
	void Init(std::function<void(BlueLightBall*)> f);
	void Update(std::function<void(BlueLightBall*)> f);
	void Update() override;
	void Draw() override;
};

#endif