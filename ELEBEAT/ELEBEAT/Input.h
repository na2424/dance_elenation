﻿#ifndef INPUT_CLASS_H
#define INPUT_CLASS_H
#include <array>
#include "Singleton.h"

//Key入力クラス
class Input : public Singleton<Input>
{
public:
	friend class Singleton < Input >;

	static const int KEY_LENGTH = 4;

	//キーの状態を保存する変数(static)
	std::array<std::array<int, KEY_LENGTH>, KEY_LENGTH> m_Key;

	//KeyBoard.
	int keyUp;
	int keyDown;
	int keyRight;
	int keyLeft;
	int keyEsc;
	int keyEnter;
	int keyShift;
	int F7;
	int F8;
	int keyPrtsc;
	int casolPic;

	//Mouse.
	int mouseX, mouseY;
	int mouseClick;
	int mouseWheel;
	int Mouse_input;

	//function.
	void Init(void);
	void GetKey(void);
	void GetMouse(void);
protected:
	Input(void) : keyUp(0), keyDown(0)
	{
	}
};

#endif