#include "Result.h"
#include "header.h"
#include "extern.h"
#include "Foreach.h"
#include "Screen.h"
#include "DxLib.h"
#include "DxHelper.h"
#include "GamePlayOption.h"
#include "BlueLightBall.h"
#include "savedata_class.h"
#include "Color.h"
#include "Input.h"
#include "NumberDraw.h"
#include "JudgeCount.h"
#include "GameBase.h"
#include "SpecialFiles.h"

using namespace std;
using namespace DxLib;

Result::Result() :
m_Time(20),
m_Rank(0),
m_Percent(0),
m_ScoreNum(0), m_PlaceX(0), m_PlaceY(0), m_Spacing(0),
m_StartTime(0),
m_AspectRatio(0),
clearImage(0),
failedImage(0),
resultOverImage(0),
resultImage(0),
dotImage(0),
rankImages(),
fullcomboImages(),
scoreNumImages(),
startSound(0),
gameoverSound(0),
loopSound(0)
{
	numberDraw = std::make_shared<TimerDrawer>();
	m_JacketRect = std::make_shared<Rect>(591, 240, 128, 128);
}

void Result::Initialize(const std::shared_ptr<Music>& musicP)
{
	InitLua();

	LoadSceneImage();
	lightball.reserve(BALL_MAX);

	//FOREACH(BlueLightBall, lightball, lb)
	//{
	//	lb->Init();
	//}

	//Lua読み込み
	ReadLua();

	if (JudgeCount::GetInstance().score > 0)
	{
		SavaScore(musicP);
	}

	m_Time = 20;
	int aaSizeX = 0;
	int aaSizeY = 0;
	DxLib::GetGraphSize(musicAAImage, &aaSizeX, &aaSizeY);
	m_AspectRatio = static_cast<double>(aaSizeY) / static_cast<double>(aaSizeX);

	//光度をもとに戻す
	DxLib::SetDrawBright(255, 255, 255);
	//音の再生
	DxLib::PlaySoundMem(startSound, DX_PLAYTYPE_BACK);
	//音の再生
	DxLib::PlaySoundMem(loopSound, DX_PLAYTYPE_LOOP);
	//開始時間の取得
	m_StartTime = GetNowCount();
	SetDrawScreen(DX_SCREEN_BACK);
}

void Result::LoadSceneImage()
{
	//(リザルトの)スコア数字の画像
	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/score_result_num.png"), 10, 10, 1, 80, 96, this->scoreNumImages.data());
	//リザルトの判定カウントの数字の画像
	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/hanteicount_num.png"), 10, 10, 1, 30, 40, this->judgeNumImages.data());
	//うまく押せた率(?)の数字画像
	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/per_result_num.png"), 10, 10, 1, 40, 40, this->perNumImages.data());

	g_BackImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/back.jpg"));
}

void Result::Update(const std::shared_ptr<Music>& musicP)
{
	//シングルトン参照
	auto& input = Input::GetInstance();
	//auto& gameOption = GamePlayOption::GetInstance();
	auto& color = Color::GetInstance();
	auto& judge = JudgeCount::GetInstance();

	//FPS更新
	m_fps->Update();
	// キー入力の取得
	input.GetKey();

	//タイマー処理
	if (OptionSaveData::g_IsEventMode)
		m_Time = 99;
	else
		m_Time = 30 - (GetNowCount() - m_StartTime) / 1000;

	m_count++;

	if (m_count <= ANIMATE_SCORE_COUNT)
	{
		m_ScoreNum += static_cast<int>(judge.score / ANIMATE_SCORE_COUNT);
		if (m_count == ANIMATE_SCORE_COUNT)
			m_ScoreNum = static_cast<int>(judge.score);
	}

	// timeが0になるか決定キーまたはESCキーが押されたら
	if (m_Time <= -1 || input.keyEnter == 1 || input.keyEsc == 1)
	{
		SetNextScene();
		return;
	}

	/*ここから描画関係*/
	// 画面の初期化
	ClearDrawScreen();

	DxLib::DrawGraph(0, 0, g_BackImage, TRUE);

	//パーティクル位置を更新と描画
	FOREACH(BlueLightBall, lightball, lb)
	{
		lb->Update(nullptr);
		lb->Draw();
	}

	DxLib::DrawGraph(0, 0, resultImage, TRUE);

	//フレームの画像表示
	if (m_count < 20)
	{
		DxLib::DrawGraph(0, -200 + (10 * m_count), resultOverImage, TRUE);
		DxLib::DrawGraph(0, WIN_HEIGHT - 80 + 200 - (10 * m_count), g_underBarImage, TRUE);
	}
	else
	{
		DxLib::DrawGraph(0, 0, resultOverImage, TRUE);
		DxLib::DrawGraph(0, WIN_HEIGHT - 80, g_underBarImage, TRUE);
		//時間の描画
		numberDraw->DrawMenuTimer(m_Time);
	}

	if (judge.isDoneFullcombo == 1)
		DxLib::DrawRotaGraph(360, 290, 0.6, 0, fullcomboImages[m_count / 3 % 5], TRUE);

	DrawScoreResult(m_ScoreNum);
	DrawScorePercent(static_cast<int>(judge.percent * 100));

	if (m_AspectRatio != 1.0)
		DrawBox(591 - 128, 240 - 128, 591 + 128, 240 + 128, color.Black(), TRUE);

	//アルバムアート画像の描画
	DxLib::DrawModiGraph(
		591 - 128, static_cast<int>(240 - 128 * m_AspectRatio), /*左上*/
		591 + 128, static_cast<int>(240 - 128 * m_AspectRatio), /*右上*/
		591 + 128, static_cast<int>(240 + 128 * m_AspectRatio), /*右下*/
		591 - 128, static_cast<int>(240 + 128 * m_AspectRatio), /*左下*/
		musicAAImage, /*画像*/
		FALSE   /*透明オフ*/
	);

	//ドット(.)描画
	if (judge.percent != 0)
		DxLib::DrawRotaGraph2(200, 320, 25, 25, 1.0, 0, dotImage, TRUE);
	//パーセント(%)描画
	DxLib::DrawRotaGraph2(300, 310, 25, 25, 1.0, 0, m_Percent, TRUE);

	//各判定のカウントを描画
	DrawJudgeCount(judge.parfect, PLACE_Y + BETWEEN * 0);
	DrawJudgeCount(judge.grate, PLACE_Y + BETWEEN * 1);
	DrawJudgeCount(judge.good, PLACE_Y + BETWEEN * 2);
	DrawJudgeCount(judge.missed, PLACE_Y + BETWEEN * 3);
	DrawJudgeCount(judge.OK, PLACE_Y + BETWEEN * 4);
	DrawJudgeCount(judge.NG, PLACE_Y + BETWEEN * 5);
	DrawJudgeCount(judge.maxCombo, PLACE_Y + BETWEEN * 6);

	//評価画像の描画
	DxLib::DrawGraph(GRADE_1P_X, GRADE_1P_Y, rankImages[m_Rank], TRUE);

	//クリア画像の描画
	if (120 <= m_count && m_count <= 250 && 60 <= judge.percent)
		DxLib::DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT / 2, 597 / 2, 45, 1.0, 0, clearImage, TRUE);
	//failed画像の描画
	if (120 <= m_count && m_count <= 250 && judge.percent < 60)
		DxLib::DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT / 2, 237, 87 / 2, 1.0, 0, failedImage, TRUE);

	// 描画した画面の反映
	m_fps->Draw();        //描画
	DxLib::ScreenFlip();
	if (input.keyPrtsc == 1)
		m_ssc->CaptureScreenshot();
	m_fps->Wait();        //待機
}

void Result::Finalize(const std::shared_ptr<Music> &musicP)
{
	StopSoundMem(startSound);
	StopSoundMem(loopSound);
	//delete
	DxLib::InitSoundMem();
	if (SceneManager::g_CurrentScene != Scenes::FINISH)
	{
		Screen::TransitionFromResult();
		Screen::FadeOut();

		//ステージカウントを増やす
		OptionSaveData::g_SongCount++;

		//初期値に戻す
		JudgeCount::GetInstance().ResetCount();
	}
}

void Result::SetNextScene()
{
	DxLib::DrawGraph(GRADE_1P_X, GRADE_1P_Y, rankImages[m_Rank], TRUE);
	DrawScoreResult(static_cast<int>(JudgeCount::GetInstance().score));
	StopSoundMem(startSound);
	StopSoundMem(loopSound);
	DxLib::InitSoundMem();

	if (OptionSaveData::g_SongCount <= OptionSaveData::g_SongCountSetting && JudgeCount::GetInstance().percent > 60)
		SceneManager::SetNextScene(Scenes::MUSIC_SELECT);
	else if (OptionSaveData::g_SongCount <= OptionSaveData::g_SongCountSetting && GamePlayOption::GetInstance().isAuto)
		SceneManager::SetNextScene(Scenes::MUSIC_SELECT);
	else if (OptionSaveData::g_IsEventMode)    //EVENTモードに設定している場合
		SceneManager::SetNextScene(Scenes::MUSIC_SELECT);
	else
		SceneManager::SetNextScene(Scenes::TITLE);
}

void Result::SavaScore(const std::shared_ptr<Music> &musicP) const
{
	auto& judge = JudgeCount::GetInstance();

	auto str = SceneManager::g_PlayMode == SelectKeys::KEYS_4 ?
		musicP->danceSong->m_Name[musicNumber4] :
		musicP->elebeatSong->m_Name[musicNumber];

	ScoreData::GetInstance().TranslateToSafeFileName(str);

	tstring pass = _T("Data\\") + str + _T(".dat");

	FILE *fp1;
	errno_t error;
	auto scoreSaveData = std::make_shared<ScoreSaveData>();

	/*ファイルの内容からデータをロード*/
	//ファイルが無かったら.datを新規作成
	if ((error = _tfopen_s(&fp1, pass.c_str(), _T("rb"))) != 0)
	{
		_tfopen_s(&fp1, pass.c_str(), _T("wb"));
		for (auto i = 0; i < 3; i++)
		{
			for (auto j = 0; j < 5; j++)
			{
				scoreSaveData->m_IsFullCombo[i][j] = false;
				scoreSaveData->m_Rank[i][j] = 10;
				scoreSaveData->m_HighScore[i][j] = 0;
			}
		}
		fwrite(scoreSaveData.get(), sizeof(ScoreSaveData), 1, fp1);
		fclose(fp1);
	}
	//ファイルがあればデータを読み込む
	else
	{
		fread(scoreSaveData.get(), sizeof(ScoreSaveData), 1, fp1);
		fclose(fp1);//解放
	}

	auto playModeNumber = static_cast<int>(SceneManager::g_PlayMode);

	//フルコンボしたかどうか保存
	if (!scoreSaveData->m_IsFullCombo[playModeNumber][GameBase::g_MusicDif])
		scoreSaveData->m_IsFullCombo[playModeNumber][GameBase::g_MusicDif] = judge.isDoneFullcombo;

	//評価を保存
	if (m_Rank < scoreSaveData->m_Rank[playModeNumber][GameBase::g_MusicDif])
		scoreSaveData->m_Rank[playModeNumber][GameBase::g_MusicDif] = m_Rank;

	//スコアを保存
	if (scoreSaveData->m_HighScore[playModeNumber][GameBase::g_MusicDif] < judge.score)
		scoreSaveData->m_HighScore[playModeNumber][GameBase::g_MusicDif] = static_cast<long>(judge.score);

	/*データの内容をファイルにセーブ*/
	if ((error = _tfopen_s(&fp1, pass.c_str(), _T("wb"))) != 0)
	{
		MessageBox(nullptr, _T("std::exception"), _T("Save data not found."), MB_OK);
	}
	else
	{
		fwrite(scoreSaveData.get(), sizeof(ScoreSaveData), 1, fp1);
		fclose(fp1);
	}
}

//Lua初期化
void Result::InitLua()
{
	// LuaのVMを生成する
	L = luaL_newstate();

	// Luaの標準ライブラリを開く
	luaL_openlibs(L);

	luaHelper.SetLua(L);
}

//lua読み込み
void Result::ReadLua()
{
	LuaFuncParam params;
	LuaFuncParam results;

	while (!luaHelper.DoFile("Themes/DANCE ELENATION/lua/_file_result.lua", &results, 1, &params))
	{
		//エラー
		if (OptionSaveData::g_IsFullScreen != 0) exit(0);
		if (MessageBoxA(nullptr, _T("result.lua が読み込めませんでした。\nLuaファイルを読み直しますか？"), _T("読み込みエラー"), MB_RETRYCANCEL) != IDRETRY) exit(0);
	}

	// 画像
	std::vector<tstring>luaStackImageStr =
	{
		_T("gradeImage"),
		_T("fullcomboImage"),
		_T("dotImage"),
		_T("persentImage"),
		_T("resultTopImage"),
		//_T("resultBottomImage"),
		_T("frameImage"),
		_T("clearedImage"),
		_T("failedImage"),
	};

	// 音声
	std::vector<tstring>luaStackSoundStr =
	{
		_T("resultInSound"),
		_T("resultLoopSound")
	};

	//ファイル名をスタックに積む
	for each(tstring str in luaStackImageStr)
	{
		lua_getglobal(L, str.c_str());
	}
	for each(tstring str in luaStackSoundStr)
	{
		lua_getglobal(L, str.c_str());
	}

	const int num = lua_gettop(L);

	if (num == 0)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("エラー"), _T("No stack"), MB_OK);
	}

	std::vector<tstring> cg_name;
	for (int N = num; N > 0; N--)
	{
		if (lua_type(L, N) == LUA_TSTRING)
			cg_name.emplace_back(lua_tostring(L, N));
	}

	int nameNum = static_cast<int>(cg_name.size()) - 1;

	DxHelper::SetObject(rankImages.data(), DIV_IMAGE_TYPE, (SpecialFiles::DEFAULT_THEMES_DIR + tstring("Graphics/Result/%s")).c_str(), cg_name[nameNum--].c_str(), 8, 1, 8, 300, 200);
	DxHelper::SetObject(fullcomboImages.data(), DIV_IMAGE_TYPE, (SpecialFiles::DEFAULT_THEMES_DIR + tstring("Graphics/Result/%s")).c_str(), cg_name[nameNum--].c_str(), 5, 5, 1, 192, 192);
	DxHelper::SetObject(dotImage, IMAGE_TYPE, (SpecialFiles::DEFAULT_THEMES_DIR + tstring("Graphics/Result/%s")).c_str(), cg_name[nameNum--].c_str());
	DxHelper::SetObject(m_Percent, IMAGE_TYPE, (SpecialFiles::DEFAULT_THEMES_DIR + tstring("Graphics/Result/%s")).c_str(), cg_name[nameNum--].c_str());
	DxHelper::SetObject(resultOverImage, IMAGE_TYPE, (SpecialFiles::DEFAULT_THEMES_DIR + tstring("Graphics/Result/%s")).c_str(), cg_name[nameNum--].c_str());
	DxHelper::SetObject(resultImage, IMAGE_TYPE, (SpecialFiles::DEFAULT_THEMES_DIR + tstring("Graphics/Result/%s")).c_str(), cg_name[nameNum--].c_str());
	DxHelper::SetObject(clearImage, IMAGE_TYPE, (SpecialFiles::DEFAULT_THEMES_DIR + tstring("Graphics/Result/%s")).c_str(), cg_name[nameNum--].c_str());
	DxHelper::SetObject(failedImage, IMAGE_TYPE, (SpecialFiles::DEFAULT_THEMES_DIR + tstring("Graphics/Result/%s")).c_str(), cg_name[nameNum--].c_str());
	DxHelper::SetObject(startSound, SOUND_TYPE, (SpecialFiles::DEFAULT_THEMES_DIR + tstring("Sounds/Result/%s")).c_str(), cg_name[nameNum--].c_str());
	DxHelper::SetObject(loopSound, SOUND_TYPE, (SpecialFiles::DEFAULT_THEMES_DIR + tstring("Sounds/Result/%s")).c_str(), cg_name[nameNum--].c_str());


	//ランクの取得
	params.Number(JudgeCount::GetInstance().percent).Number(GameBase::g_MusicDif);
	if (!luaHelper.CallFunc("GetRank", &results, 1, &params))
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("GetRank Lua関数の呼び出しに失敗"), _T("error"), MB_OK);
		exit(1);
	}
	else
	{
		//成功→返り値の取得
		m_Rank = static_cast<int>(results.GetNumber(0));
	}

	//スタックを破棄
	lua_settop(L, 0);
}

void Result::DrawScoreResult(int num)
{
	// numが十進数で何桁になるか調べる
	auto beamWidth = 0;
	for (auto i = 1; num >= i; i *= 10) beamWidth++;

	auto x = 870;
	for (auto i = 0; i < beamWidth; i++)
	{
		DrawGraph(x, 600, scoreNumImages[num % 10], TRUE);
		num /= 10;
		x -= 70;
	}
}

void Result::DrawScorePercent(int Num)
{
	auto beamWidth = 0;
	for (auto i = 10; Num >= i; i *= 10) beamWidth++;

	auto x = 240;
	for (auto i = 0; i <= beamWidth; i++)
	{
		DrawGraph(x, 290, perNumImages[Num % 10], TRUE);
		x -= 40;
		Num /= 10;
	}
}

void Result::DrawJudgeCount(int num, int y)
{
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 60);
	if (num < 1000)
		DrawGraph(350 - (3 * 25), y, judgeNumImages[0], TRUE);
	if (num < 100)
		DrawGraph(350 - (2 * 25), y, judgeNumImages[0], TRUE);
	if (num < 10)
		DrawGraph(350 - (1 * 25), y, judgeNumImages[0], TRUE);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);       //ブレンドモードをオフ

	auto beamWidth = 0;
	for (auto i = 10; num >= i; i *= 10) beamWidth++;

	auto x = 350;
	for (auto i = 0; i <= beamWidth; i++)
	{
		DrawGraph(x, y, judgeNumImages[num % 10], TRUE);
		x -= 25;
		num /= 10;
	}
}