#include "DxHelper.h"
#include "DxLib.h"
#include "header.h"
#include "Enums.h"

void DxHelper::SetObject(int& setObject, const int setType, const tstring& path, const char* cgName)
{
	TCHAR objName[100] = {};
	_stprintf_s(objName, sizeof(objName), path.c_str(), cgName);
	if (setType == IMAGE_TYPE)
		setObject = LoadGraph(objName);
	if (setType == SOUND_TYPE)
		setObject = LoadSoundMem(objName);
}

void DxHelper::SetObject(int setObject[], const int setType, const tstring& path, const char* cgName, int allNum, int xNum, int yNum, int xSize, int ySize)
{
	TCHAR objName[100] = {};
	_stprintf_s(objName, sizeof(objName), path.c_str(), cgName);
	if (setType == DIV_IMAGE_TYPE)
		LoadDivGraph(objName, allNum, xNum, yNum, xSize, ySize, setObject);
}

double DxHelper::GetRatioImage(const int image)
{
	int x = 0;
	int y = 0;

	GetGraphSize(image, &x, &y);
	return (double)y / (double)x;
}

void DxHelper::DrawOutlineString(const int x, const int y, const int textColor, const int outlineColor, const tstring& text, const tstring& t)
{
	DxLib::DrawFormatString(x - 1, y - 1, outlineColor, text.c_str(), t.c_str());
	DxLib::DrawFormatString(x - 1, y + 1, outlineColor, text.c_str(), t.c_str());
	DxLib::DrawFormatString(x + 1, y - 1, outlineColor, text.c_str(), t.c_str());
	DxLib::DrawFormatString(x + 1, y + 1, outlineColor, text.c_str(), t.c_str());
	DxLib::DrawFormatString(x, y, textColor, text.c_str(), t.c_str());
}

void DxHelper::DrawOutlineString(const int x, const int y, const int textColor, const int outlineColor, const tstring& text, int num)
{
	DxLib::DrawFormatString(x - 1, y - 1, outlineColor, text.c_str(), num);
	DxLib::DrawFormatString(x - 1, y + 1, outlineColor, text.c_str(), num);
	DxLib::DrawFormatString(x + 1, y - 1, outlineColor, text.c_str(), num);
	DxLib::DrawFormatString(x + 1, y + 1, outlineColor, text.c_str(), num);
	DxLib::DrawFormatString(x, y, textColor, text.c_str(), num);
}

void DxHelper::DrawOutlineString(const int x, const int y, const int textColor, const int outlineColor, const tstring& text, double num)
{
	DxLib::DrawFormatString(x - 1, y - 1, outlineColor, text.c_str(), num);
	DxLib::DrawFormatString(x - 1, y + 1, outlineColor, text.c_str(), num);
	DxLib::DrawFormatString(x + 1, y - 1, outlineColor, text.c_str(), num);
	DxLib::DrawFormatString(x + 1, y + 1, outlineColor, text.c_str(), num);
	DxLib::DrawFormatString(x, y, textColor, text.c_str(), num);
}

void DxHelper::DrawOutlineStringToHandle(const int x, const int y, const int textColor, const int outlineColor, const int fontHandle, const tstring& text, const int value)
{
	DxLib::DrawFormatStringToHandle(x - 1, y - 1, outlineColor, fontHandle, text.c_str(), value);
	DxLib::DrawFormatStringToHandle(x - 1, y + 1, outlineColor, fontHandle, text.c_str(), value);
	DxLib::DrawFormatStringToHandle(x + 1, y - 1, outlineColor, fontHandle, text.c_str(), value);
	DxLib::DrawFormatStringToHandle(x + 1, y + 1, outlineColor, fontHandle, text.c_str(), value);
	DxLib::DrawFormatStringToHandle(x, y, textColor, fontHandle, text.c_str(), value);
}

void DxHelper::DrawOutlineStringToHandle(const int x, const int y, const int textColor, const int outlineColor, const int fontHandle, const tstring& text, const double value)
{
	DxLib::DrawFormatStringToHandle(x - 1, y - 1, outlineColor, fontHandle, text.c_str(), value);
	DxLib::DrawFormatStringToHandle(x - 1, y + 1, outlineColor, fontHandle, text.c_str(), value);
	DxLib::DrawFormatStringToHandle(x + 1, y - 1, outlineColor, fontHandle, text.c_str(), value);
	DxLib::DrawFormatStringToHandle(x + 1, y + 1, outlineColor, fontHandle, text.c_str(), value);
	DxLib::DrawFormatStringToHandle(x, y, textColor, fontHandle, text.c_str(), value);
}