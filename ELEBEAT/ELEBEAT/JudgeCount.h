﻿#ifndef JUDGECOUNT_CLASS_H
#define JUDGECOUNT_CLASS_H
#include "Singleton.h"

/// <summary>
/// 判定カウンタクラス
/// </summary>
class JudgeCount : public Singleton<JudgeCount>
{
public:
	friend class Singleton < JudgeCount >;

	void Init(void);

	JudgeCount();
	//各判定
	int parfect;            //PARFECT
	int grate;              //GRATE
	int good;               //GOOD
	int missed;             //MISSED
	int OK;                 //OK
	int NG;                 //NG

	int maxCombo;            //最大コンボ数を入れるための変数
	double percent;                //評価の基準になるパーセンテージを入れるための変数
	double score;                //スコアを入れるための変数
	int HiScore;                //ハイスコアを入れるための変数
	bool isDoneFullcombo;        //フルコンしたかどうかを入れるフラグ
	void ResetCount(void);

	/// <summary>
	/// 最大コンボを更新
	/// </summary>
	/// <param name="combo">現在のコンボ</param>
	inline void UpdateMaxComboOrNone(int combo)
	{
		if (maxCombo < combo)
			maxCombo = combo;
	}
};

#endif