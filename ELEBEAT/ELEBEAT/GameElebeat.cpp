﻿#include "GameElebeat.h"
#include "Color.h"
#include "Combo.h"
#include "define.h"
#include "extern.h"
#include "header.h"
#include "KeyInput.h"
#include "Rect.h"
#include "Screen.h"
#include "Input.h"
#include "savedata_class.h"
#include <boost/lexical_cast.hpp>
#include <fstream>
#include "JudgeCount.h"
#include <boost/algorithm/string/trim.hpp>
using namespace DxLib;

GameElebeat::GameElebeat() :
	m_enumKeys(),
	startImage(0),
	comboImage(0),
	elebeatImage(0),
	readyImage(0),
	goImage(0),
	readyGoSound(0),
	backSound(0),
	musicSound(0),
	handCrapSound(0), m_matrix(0),
	m_fontHandle(0), m_musicEndTime(0), m_scoreFrame(0),
	tempClapedTime(0),
	m1(0), m2(0), m3(0), m4(0), keysPosition(0),
	st(0), m_offset(0)
{
	m_judgeAreaImages.fill(0);
	m_difColors[0] = Color::GetInstance().Green();
	m_difColors[1] = Color::GetInstance().Yellow();
	m_difColors[2] = Color::GetInstance().Red();

	m_note = std::make_shared<NoteElebeat>();
	m_sScore = std::make_shared<ScoreSt>();
	m_sCombo = std::make_shared<Combo>();
	m_sKeyinput = std::make_shared<KeyInput>();
}

//画像のロード
void GameElebeat::LoadGameImage()
{
	m_judgeAreaImages[0] = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Game/waku1.png"));
	m_judgeAreaImages[1] = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Game/waku2.png"));
	m_judgeAreaImages[2] = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Game/waku3.png"));
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Numbers\\selectmusic_level01_num.png"), 10, 10, 1, 25, 50, m_difficultyNumberImages[0].data());        //番号画像 緑
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Numbers\\selectmusic_level02_num.png"), 10, 10, 1, 25, 50, m_difficultyNumberImages[1].data());        //番号画像 黄色
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Numbers\\selectmusic_level03_num.png"), 10, 10, 1, 25, 50, m_difficultyNumberImages[2].data());        //番号画像 赤
	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/combo_num.png"), 10, 10, 1, 192, 384, this->g_ComboElebeatImages.data());
	startImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Game/start.png"));
	comboImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Game/combo.png"));
	readyImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Game/ready.png"));
	goImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Game/go.png"));
}

//音声のロード
void GameElebeat::LoadGameSound()
{
	readyGoSound = LoadSoundMem(_T("Themes/DANCE ELENATION/Sounds/Game/readyGo.mp3"));
	backSound = LoadSoundMem(_T("Themes/DANCE ELENATION/Sounds/Common back.ogg"));
	handCrapSound = LoadSoundMem(_T("Themes/DANCE ELENATION/Sounds/Game/assist tick.ogg"));
}

void GameElebeat::SetBpmList(const std::shared_ptr<Music>& musicP)
{
	//BPM
	for (unsigned i = 0; i < musicP->elebeatSong->m_Bpm[musicNumber].size(); i++)
	{
		m_bpm.emplace_back(boost::lexical_cast<double>(musicP->elebeatSong->m_Bpm[musicNumber][i].c_str()));
	}
	//BPM POSITION
	for (unsigned i = 0; i < musicP->elebeatSong->m_BpmPositions[musicNumber].size(); i++)
	{
		m_bpmPosition.emplace_back(boost::lexical_cast<double>(musicP->elebeatSong->m_BpmPositions[musicNumber][i].c_str()) / 8.0);
	}

	m_bpm.emplace_back(0);
	m_bpmPosition.emplace_back(0);
}

void GameElebeat::SetOffset(const std::shared_ptr<Music>& musicP)
{
	m_offset = _ttoi(musicP->elebeatSong->m_Offset[musicNumber].c_str()) + 124 + Setting.GamePlaySetting.GlobalOffset;
}

void GameElebeat::SetBpmTimeSpan()
{
	using namespace std;
	vector<double>bn(0);
	vector<double>dn(0);

	//BPM変化で使う計算
	//各BPM変化ポジションの差の数列を求める
	for (unsigned n = 0; n < m_bpmPosition.size() - 1; n++)
	{
		bn.emplace_back(m_bpmPosition[n + 1] - m_bpmPosition[n]);
	}

	//各BPM変化ポジション間の時間を求める
	for (unsigned n = 0; n < m_bpm.size() - 1; n++)
	{
		if (m_bpm[n] != 0)
			dn.emplace_back(bn.at(n) * 60000.0 / m_bpm[n]);//+ m_TimeS;
	}

	m_bpmTimeSpan = dn;
}

bool GameElebeat::LoadNote(const tstring& lineString)
{
	auto x = 0,
		y = 0,
		z = 0;

	const auto num = sscanf_s(lineString.data(), "%d,%d,%d", &x, &y, &z);

	if (num != 3)
		return false;

	double bpmTimeSpanTotal = 0;

	//ロングノート終点
	if (z < 0)
	{
		auto z1 = -1 * z;
		const auto w = (z1 - 1) / m_matrix;
		z1 = (z1 + (m_matrix - 1)) % m_matrix;

		for (auto m = 0; m < m_matrix; m++)
		{
			if (m != z1) continue;

			for (auto n = 0; n < m_matrix; n++)
			{
				if (n != w) continue;

				for (auto i = 0; i < MAXX; i++)
				{
					if (m_note->note[m][n][i] != -1000) continue;

					m_note->note[m][n][i] = 32 * x + y;

					for (unsigned k = 0; k < m_bpmPosition.size() - 1; ++k)
					{
						if (m_bpmPosition[k + 1] > 4 * x + y / 8.0 || m_bpmPosition[k + 1] == 0)
						{
							//各ノートの押されるべき時間はここで決まる。
							m_note->note_time[m][n][i] = static_cast<int>(bpmTimeSpanTotal + 60000.0 / m_bpm[k] * ((4 * x + y / 8.0) - m_bpmPosition[k]) + m_offset);
							if (m_musicEndTime < m_note->note_time[m][n][i] + 2000)
								m_musicEndTime = m_note->note_time[m][n][i] + 2000;
							bpmTimeSpanTotal = 0;
							break;
						}
						bpmTimeSpanTotal += m_bpmTimeSpan.at(k);
					}
					m_note->m_NoteKind[m][n][i - 1] = -2;    //longnote start
					m_note->m_NoteKind[m][n][i] = -3;    //longnote end
					break;
				}
			}
		}
	}
	else
	{
		const auto w = (z - 1) / m_matrix;
		const auto z1 = (z + (m_matrix - 1)) % m_matrix;
		for (auto m = 0; m < m_matrix; ++m)
		{
			if (m != z1) continue;

			for (auto n = 0; n < m_matrix; ++n)
			{
				if (n != w) continue;

				for (auto i = 0; i < MAXX; ++i)
				{
					if (m_note->note[m][n][i] != -1000) continue;

					m_note->note[m][n][i] = 32 * x + y;

					for (unsigned k = 0; k < m_bpmPosition.size() - 1; ++k)
					{
						if (m_bpmPosition[k + 1] > 4 * x + y / 8.0 || m_bpmPosition[k + 1] == 0)
						{
							//各ノートの押されるべき時間はここで決まる。
							m_note->note_time[m][n][i] = static_cast<int>(bpmTimeSpanTotal + 60000.0 / m_bpm[k] * ((4 * x + y / 8.0) - m_bpmPosition[k]) + m_offset);
							if (m_musicEndTime < m_note->note_time[m][n][i] + 2000)
								m_musicEndTime = m_note->note_time[m][n][i] + 2000;
							bpmTimeSpanTotal = 0;
							break;
						}
						bpmTimeSpanTotal += m_bpmTimeSpan.at(k);
					}
					m_note->m_NoteKind[m][n][i] = -1;
					break;
				}
			}
		}
	}

	return true;
}

void GameElebeat::DrawCombo(const std::shared_ptr<Combo>& f_combo)
{
	int x;
	auto combo = f_combo->m_ComboNum;
	// numが十進数で何桁になるか調べる
	auto beamWidth = 0;
	for (auto i = 1; combo >= i; i *= 10) beamWidth++;

	// 画面右上に右詰で表示
	// xは描く数字(一桁一桁各々)の左端のX座標
	if (combo < 100)
		x = 390;
	else if (combo < 1000)
		x = 590;
	else
		x = 590;
	for (auto i = 0; i < beamWidth; i++)
	{
		DrawGraph(x, static_cast<int>(180 + 30 * sin(static_cast<double>(f_combo->m_AnimationFrame / 50.0*PI))), g_ComboElebeatImages[combo % 10], TRUE);
		combo /= 10;
		x -= 200;
	}
}

void GameElebeat::DrawDifficulty(const std::shared_ptr<Music>& musicP, int dif) const
{
	const auto keysType = (m_enumKeys == SelectKeys::KEYS_9) ? 0 : 1;
	DrawBox(880 - 30, 66 - 30, 880 + 30, 66 + 30, m_difColors[dif], TRUE);
	DrawDifficultyNum(musicP->elebeatSong->m_ElebeatDifficulty[keysType][dif][musicNumber], m_difficultyNumberImages[dif]);
}

void GameElebeat::DrawReadyGo(const int nowTime) const
{
	if (0 < nowTime && nowTime < 900)
		DrawRotaGraph(400, 400, 1, 0, readyImage, TRUE);
	if (900 < nowTime && nowTime < 1500)
		DrawRotaGraph(400, 400, 1, 0, goImage, TRUE);
}

void GameElebeat::InitMatrix()
{
	m_enumKeys = SceneManager::g_PlayMode;
	m_matrix = (m_enumKeys == SelectKeys::KEYS_9) ? 3 : 4;
}

void GameElebeat::FadeInScene() const
{
	Vector2D albumArtGrid;
	albumArtGrid.x = 874;
	albumArtGrid.y = 522;

	//黒から画像表示へ
	for (auto i = 0; i <= 255; i = i + 4)
	{
		ClearDrawScreen();
		//輝度をだんだんあげていく
		SetDrawBright(i, i, i);
		//背景描画
		DrawGraph(0, 0, g_BackImage, TRUE);
		//アルバムアート画像の描画
		DxLib::DrawModiGraph(albumArtGrid.x - 64, albumArtGrid.y - 64, albumArtGrid.x + 64, albumArtGrid.y - 64, albumArtGrid.x + 64, albumArtGrid.y + 64, albumArtGrid.x - 64, albumArtGrid.y + 64, musicAAImage, FALSE);
		//経過時間と判定カウントを表示させる関数の呼び出し
		DrawFormatStrings(0);
		DxLib::ScreenFlip();
	}
	SetDrawBright(255, 255, 255);
}

//9keys選択時のゲーム関数
void GameElebeat::Initialize(const std::shared_ptr<Music> &musicP)
{
	//シングルトン参照
	Color& color = Color::GetInstance();
	JudgeCount& judge = JudgeCount::GetInstance();

	InitMatrix();
	//初期値に戻す
	judge.ResetCount();

	SetFontSize(16);

	elebeatImage = DxLib::LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Game/elebeat.png"));

	//ステージ表示
	Screen::TransitionToGame();
	Screen::FadeOut();

	//ゲーム画面のフェードイン
	g_BackImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Game/game_back.png"));
	musicAAImage = LoadGraph(musicP->elebeatSong->m_AlbumArtPath[musicNumber].c_str());

	FadeInScene();

	///////////////初期化が必要な変数に値を代入する///////////////

	m_sScore->m_ScoreNum = 0;
	m_sScore->m_ScoreSa = 0;

	//フォント
	m_fontHandle = CreateFontToHandle(nullptr, 40, 3, DX_FONTTYPE_EDGE);

	m1 = marker_numb[markernum];
	m4 = marker_time[markernum];
	m3 = marker_just[markernum];
	m2 = marker_numj[markernum];

	DrawString(250, 240, _T("初期化完了"), color.White());
	DxLib::ScreenFlip();

	////////////////////////////////////////////////////////////
	DrawString(250, 240 + 32, _T("画像と音源を読み込み中...."), color.White());
	LoadGameImage();
	LoadGameSound();
	DxLib::ScreenFlip();
	DrawString(250, 240 + 64, _T("画像と音源の読み込み完了"), color.White());
	DrawString(250, 240 + 96, _T("選曲した曲情報を読み込み中....."), color.White());
	DxLib::ScreenFlip();

	//前の音楽番号を保持しておく
	beforeSelectMusicNum = musicNumber;

	//音源
	musicSound = LoadSoundMem(musicP->elebeatSong->m_MusicPath[musicNumber].c_str());

	//BPM,Offset設定
	SetBpmList(musicP);
	SetOffset(musicP);
	SetBpmTimeSpan();

	tstring selectKeyString = (m_enumKeys == SelectKeys::KEYS_9) ? _T("keys:9") : _T("keys:16");
	tstring difficultName;

	if (g_MusicDif == 0)
		difficultName = _T("BASIC");
	if (g_MusicDif == 1)
		difficultName = _T("NORMAL");
	if (g_MusicDif == 2)
		difficultName = _T("ADVANCED");

	std::ifstream ifs(musicP->elebeatSong->m_SequencePath[musicNumber].c_str());
	tstring lineStr;

	if (ifs.fail())
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("エラー"), _T("譜面ファイルを開けませんでした。"), MB_OK);
		return;
	}

	//データの読み込み(16keys) #NOTEのあと選択した難易度の譜面を読み込む
	int noteIndexNum = 0;
	int noteIndexMax = 0;
	bool isChecked[2] = {};
	bool isNoteLoading = false;

	while (getline(ifs, lineStr))
	{
		boost::algorithm::trim(lineStr);

		if (isNoteLoading)
		{
			if (!LoadNote(lineStr)) break;

			++noteIndexNum;
			++m_note->m_Maxnote;
			noteIndexMax = noteIndexNum;
		}
		else
		{
			if (lineStr.find(_T("#NOTE:")) != std::string::npos || isChecked[0])
			{
				isChecked[0] = true;
				if (lineStr.find(selectKeyString.c_str()) != std::string::npos || isChecked[1])
				{
					isChecked[1] = true;

					if (lineStr.find(difficultName.c_str()) != std::string::npos)
						isNoteLoading = true;
				}
			}
		}
	}

	m_note->note_time[m_matrix - 1][m_matrix - 1][noteIndexMax] = -10000;
	--m_note->m_Maxnote;

	//////「ここからスタート」を描画させるための代入//////
	st = static_cast<int>(m_note->note_time[0][0][0]);
	for (auto m = 0; m < m_matrix; ++m)
	{
		for (auto n = 0; n < m_matrix; ++n)
		{
			if (m_note->note_time[m][n][0] <= st && m_note->note_time[m][n][0] != -10000.0)
				st = static_cast<int>(m_note->note_time[m][n][0]);
		}
	}
	/////////////////////////////////////////////////////
	DrawString(250, 240 + 128, _T("選曲した曲情報の読み込み完了"), color.White());
	//反映
	DxLib::ScreenFlip();
	//ハンドクラップフラグを作成
	const std::vector<std::vector<bool> > tempHasCraps(m_matrix);
	m_isCraped = std::vector<std::vector<std::vector<bool> > >(m_matrix, tempHasCraps);

	for (auto i = 0; i < m_matrix; i++)
	{
		for (auto j = 0; j < m_matrix; j++)
		{
			for (int k = ARRAY_LENGTH(m_note->note_time[i][j]) - 1; k >= 0; --k)
			{
				m_isCraped[i][j].emplace_back(false);
			}
		}
	}

	keysPosition = (m_enumKeys == SelectKeys::KEYS_9) ? 200 : 0;

	//裏画面に設定
	DxLib::SetDrawScreen(DX_SCREEN_BACK);
	//READY・GOを鳴らす
	DxLib::PlaySoundMem(readyGoSound, DX_PLAYTYPE_BACK);
	//読み込んだ曲を鳴らす
	DxLib::PlaySoundMem(musicSound, DX_PLAYTYPE_BACK);
}

void GameElebeat::Update(const std::shared_ptr<Music>& musicP)
{
	auto& judge = JudgeCount::GetInstance();
	auto& input = Input::GetInstance();

	// キー入力の取得
	input.GetKey();

	//ESCキーが押されたら曲選択画面に戻る
	if (input.keyEsc > 1)
	{
		SceneManager::SetNextScene(Scenes::MUSIC_SELECT);
		DxLib::PlaySoundMem(backSound, DX_PLAYTYPE_BACK);
		return;
	}

	//曲の再生時間を取得
	if (CheckSoundMem(musicSound) == 1)
		g_Now = GetSoundCurrentTime(musicSound);

	//終了時間になったら or 曲再生が終了したらゲームループから抜ける
	if (g_Now >= m_musicEndTime || CheckSoundMem(musicSound) == 0)
	{
		SceneManager::SetNextScene(Scenes::RESULT);
		return;
	}

	//Auto反転
	if (Input::GetInstance().F8 == 1)
		m_isAuto = !m_isAuto;

	//HandCrap反転
	if (Input::GetInstance().F7 == 1)
		m_isCrap = !m_isCrap;

	//スコア加算時の20フレーム処理
	if (m_scoreFrame < 20)
	{
		m_sScore->m_ScoreNum += static_cast<int>(m_sScore->m_ScoreSa / 20.0);
		if (m_scoreFrame == 19)
			m_sScore->m_ScoreNum = static_cast<int>(judge.score);
		m_scoreFrame++;
	}

	//うまく押せた率を計算(ぇ
	if (m_note->m_NoteTotle != 0)
		judge.percent = 100 * (judge.parfect + (judge.grate*0.5) + (judge.good*0.2)) / m_note->m_NoteTotle;
	else
		judge.percent = 0;

	//エクセなら満点にする(ぇ
	if ((m_note->m_Maxnote == judge.parfect + judge.OK) && (!m_isAuto))
	{
		judge.score = 1000000;
	}

	//フルコンしたかどうか判定
	judge.isDoneFullcombo = (m_note->m_NoteTotle == m_note->m_NoteCount1);

	//maxCombo処理
	judge.UpdateMaxComboOrNone(m_sCombo->m_ComboNum);

	//OK、NG描画の際のアニメーションフラグ
	for (auto m = 0; m < 4; m++)
	{
		for (auto n = 0; n < 4; n++)
		{
			if (m_note->m_U[m][n] < 20)
				m_note->m_U[m][n]++;
		}
	}

	//コンボが増える際のアニメーションフラグ
	if (m_sCombo->m_AnimationFrame < 20)
	{
		m_sCombo->m_AnimationFrame++;
	}

	// 画面の初期化
	ClearDrawScreen();

	//コンボ数の描画
	if (m_sCombo->m_ComboNum > 3)
	{
		DrawCombo(m_sCombo);
		DrawRotaGraph(MASU_X + SQUARE_SIZE * 2, 550, 1.0, 0, comboImage, TRUE);
	}

	//AUTO時
	if (m_isAuto == true)
	{
		m_note->GetKeyTimeAuto(m_sKeyinput, &g_Now, m_matrix, m_note);
		m_note->GetKeyNoteAuto(m_sKeyinput, m_note, m_matrix);
	}
	else
	{
		//キーを押した時の時間を取得
		m_note->GetKeyTime(m_sKeyinput, &g_Now, m_matrix);

		//キーを押したとき最も近いノートが各方向の上から何番目かを求める。(「basyo[横][縦]」に結果が入る。)
		m_note->GetKeyNote(m_sKeyinput, m_matrix);
	}

	//判定
	for (auto m = 0; m < m_matrix; m++)
	{
		for (auto n = 0; n < m_matrix; n++)
		{
			//ノートのカウント処理(現在何個目か)
			m_note->m_NoteCount[m][n] = 0;
			for (auto i = 0; i < ARRAY_LENGTH(m_note->note[m][n]); i++)
			{
				if (static_cast<int>(g_Now) > m_note->note_time[m][n][i])
					m_note->m_NoteCount[m][n]++;
				else
					break;
			}

			//初期値(後方)
			auto start = 0;
			if (m_note->m_NoteCount[m][n] + 3 >= ARRAY_LENGTH(m_note->note[m][n]) - 1)
				start = ARRAY_LENGTH(m_note->note[m][n]) - 1;
			else
				start = m_note->m_NoteCount[m][n] + 3;

			//終了条件(前方)
			auto term = 0;
			if (m_note->m_NoteCount[m][n] - 1 >= 0)
				term = m_note->m_NoteCount[m][n] - 1;

			for (auto i = start; i - term >= 0; i--)
			{
				//ノートの判定処理
				m_note->NoteJudgement(m_sScore, m_sCombo, m_sKeyinput, m, n, &m_scoreFrame, g_Now, i, m_isAuto);
				////////////ハンクラ////////////
				if (m_isCrap && m_isCraped[m][n][i] == false && static_cast<int>(m_note->note_time[m][n][MAXX - i - 1]) <= g_Now + CRAP_OFFSET && (0 < m_note->note_time[m][n][MAXX - i - 1]))
				{
					//多重ハンクラ再生を避ける
					m_isCraped[m][n][i] = true;
					if (tempClapedTime != static_cast<int>(m_note->note_time[m][n][i]) && m_note->m_NoteKind[m][n][i] != -3)
					{
						tempClapedTime = static_cast<int>(m_note->note_time[m][n][i]);
						DxLib::PlaySoundMem(handCrapSound, DX_PLAYTYPE_BACK);
					}
				}
				//////////////描画//////////////

				//判定画像のアニメーション描画
				for (auto judgeKind_i = 0; judgeKind_i < 3; judgeKind_i++)
				{
					if (m_note->m_JudgmentResult == judgeKind_i + 1 && m_note->m_NoteKind[m][n][i] == 1)
					{
						for (auto j = 0; j < m2 - 1; j++)
						{
							if (m4 * j <= -m_note->m_NoteHitTime[m][n][i] + g_Now && -m_note->m_NoteHitTime[m][n][i] + g_Now < m4 * (j + 1))
							{
								DrawExtendGraph(keysPosition + SQUARE_SIZE*m, keysPosition + SQUARE_SIZE*n, 177 + keysPosition + SQUARE_SIZE*m, 177 + keysPosition + SQUARE_SIZE*n, noteJudgeImage[markernum][judgeKind_i][j], TRUE);
								break;
							}
						}
					}
				}

				/*(1P側)ノートの描画処理は各フラグが１以外のとき*/
				if (m_note->m_NoteKind[m][n][i] == -1)//通常ノート(アニメーション)
				{
					for (auto j = -m3; j < m1 - m3; j++)
					{
						if (m4 * j <= g_Now - m_note->note_time[m][n][i] && g_Now - m_note->note_time[m][n][i] < m4 * (j + 1))
						{
							DrawExtendGraph(keysPosition + SQUARE_SIZE*m, keysPosition + SQUARE_SIZE*n, 177 + keysPosition + SQUARE_SIZE*m, 177 + keysPosition + SQUARE_SIZE*n, noteMarkerImage[markernum][j + m3], TRUE);
							break;
						}
					}
				}
			}
		}
	}//判定のループはここまで

	//「ここからスタート」を描画
	for (auto m = 0; m < m_matrix; m++)
	{
		for (auto n = 0; n < m_matrix; n++)
		{
			if (st == m_note->note_time[m][n][0] && g_Now < m_note->note_time[m][n][0] - m4 * m1)
				DrawRotaGraph(MASU_X + keysPosition + SQUARE_SIZE * m, MASU_Y + keysPosition + SQUARE_SIZE * n, 1.0, 0, startImage, TRUE);
		}
	}

	// キーを押した時の描画
	for (auto m = 0; m < m_matrix; m++)
	{
		for (auto n = 0; n < m_matrix; n++)
		{
			if (Input::GetInstance().m_Key[m + (4 - m_matrix)][n + (4 - m_matrix)] >= 1)
				DrawRotaGraph(MASU_X + keysPosition + SQUARE_SIZE * m, MASU_Y + keysPosition + SQUARE_SIZE * n, 1.0, 0, m_judgeAreaImages[1], TRUE);
		}
	}
	//枠の描画
	for (int m = 0; m < m_matrix; m++)
	{
		for (int n = 0; n < m_matrix; n++)
		{
			DrawRotaGraph(MASU_X + keysPosition + SQUARE_SIZE * m, MASU_Y + keysPosition + SQUARE_SIZE * n, 1.0, 0, m_judgeAreaImages[0], TRUE);
		}
	}

	//背景フレームの描画
	DrawGraph(0, 0, g_BackImage, TRUE);
	//スコアの描画
	DrawScore(m_sScore->m_ScoreNum);
	//アルバムアート画像の描画
	DxLib::DrawModiGraph(874 - 64, 522 - 64, 874 + 64, 522 - 64, 874 + 64, 522 + 64, 874 - 64, 522 + 64, musicAAImage, FALSE);
	//ELEBEAT画像
	DxLib::DrawRotaGraph2(JUDGE_X_PX + 55, JUDGE_Y_PX - 55, 235 / 2, 53 / 2, 0.55, 0, elebeatImage, TRUE);
	//経過時間と判定カウントを表示させる関数の呼び出し
	DrawFormatStrings(g_Now);
	//難易度を表示
	DrawDifficulty(musicP, g_MusicDif);
	//RedyGoを描画
	DrawReadyGo(g_Now);
	//Auto Play
	if (m_isAuto)
		DrawStringToHandle(WIN_WIDTH / 2 - 100, WIN_HEIGHT / 2, _T("Auto Play"), Color::GetInstance().White(), m_fontHandle);
	// 描画した画面の反映
	DxLib::ScreenFlip();

	// スクリーンショットを撮る
	if(input.keyPrtsc == 1)
		m_ssc->CaptureScreenshot();
}

void GameElebeat::Finalize(const std::shared_ptr<Music> &musicP)
{
	StopSoundMem(musicSound);
	DxLib::InitSoundMem();
	// 作成したフォントデータを削除する
	DxLib::DeleteFontToHandle(m_fontHandle);

	if (SceneManager::g_CurrentScene != Scenes::FINISH)
		Screen::FadeOut();
}