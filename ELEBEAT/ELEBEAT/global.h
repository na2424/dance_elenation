﻿#ifndef GLOBAL_H
#define GLOBAL_H
#include "define.h"
#include "MarkerIni.h"
#include "SettingIni.h"
#include "struct.h"
#include <vector>

//画像のglobal
Image g_BackImage = 0;                       //背景を入れるための変数

int musicNumber = 0;                //一般化の音楽変数受け渡し
				  //選択音楽の難易度

int selectMerker = 0;               //選択したマーカー番号を渡す
int beforeSelectMusicNum = -1;      //ゲーム終了後その音楽を選択するため使用 初回は-1
int beforeSelectMusicNum_16 = -1;   //ゲーム終了後その音楽を選択するため使用 初回は-1
int musicNumber4 = 0;               //一般化の音楽変数受け渡し
int beforeSelectMusicNum4 = -1;

//マーカー変数
std::vector<std::vector<tstring> > marker_handle_normal(0, std::vector<tstring>(0));
std::vector<std::vector<tstring> > marker_handle_perfect(0, std::vector<tstring>(0));
std::vector<std::vector<tstring> > marker_handle_grate(0, std::vector<tstring>(0));
std::vector<std::vector<tstring> > marker_handle_good(0, std::vector<tstring>(0));

//RandAAReplace
std::vector<int> RandAAReplace(0);
std::vector<int> RandAA4Replace(0);

/*以下適当にglobal化*/
/***1P用***/
int sc = 0;
int judgeDiv = -1;        //各判定結果を入れるための変数

std::vector <std::vector <Image> > noteMarkerImage;
std::vector <int> row;
std::vector <std::vector <std::vector <Image> > > noteJudgeImage;

//黒画面用画像
Image blackImage = 0;
Image g_underBarImage = 0;

Image timerImage[10] = {};        //右上のタイマー画像を入れるための配列、各画面共通で使う。
Image bpmImage[10];            //BPMの数字画像を入れるための配列
Image musicAAImage = 0;            //選曲したAAのパスを入れるための変数。選曲した時にそれだけ入れればその後のゲーム画面やリザルト画面で使える。

int difficult = 0;                //今の難易度を入れる変数(0→BASIC、1→NORMAL、2→ADVANCED)
int AAnum = 0;                    //今のAAの番号を入れる変数

std::vector <int>marker_numb(0);//m1
std::vector <int>marker_just(0);//m3
std::vector <int>marker_time(0);//m4
std::vector <int>marker_numj(0);//m2

//MMDデータ関連
int ModelHandle = 0;
int markernum = 0;
SettingIni Setting;

#endif