#include "SceneManager.h"
#include "Logo.h"
#include "Title.h"
#include "Option.h"
#include "KeysSelect.h"
#include "SelectMusic9Keys.h"
#include "SelectMusic16Keys.h"
#include "SelectMusic4Keys.h"
#include "GameElebeat.h"
#include "Game_DANCEELENATION.h"
#include "Result.h"

#include "savedata_class.h"
#include "Enums.h"

Scenes SceneManager::g_CurrentScene = Scenes::LOGO;
SelectKeys SceneManager::g_PlayMode = SelectKeys::KEYS_4;

std::unique_ptr<Scene> SceneManager::CreateScene()
{
	switch (g_CurrentScene)
	{
	case Scenes::LOGO:
		return std::unique_ptr<Scene>(std::make_unique<Logo>());

	case Scenes::TITLE:
		return std::unique_ptr<Scene>(std::make_unique<Title>());

	case Scenes::OPTIONS:
		return std::unique_ptr<Scene>(std::make_unique<Option>());

	case Scenes::KEYS_SELECT:
		return std::unique_ptr<Scene>(std::make_unique<KeysSelect>());

	case Scenes::MUSIC_SELECT:
		switch (g_PlayMode)
		{
		case SelectKeys::KEYS_9:
			return std::unique_ptr<Scene>(std::make_unique<SelectMusic9keys>());
		case SelectKeys::KEYS_16:
			return std::unique_ptr<Scene>(std::make_unique<SelectMusic16keys>());
		case SelectKeys::KEYS_4:
			return std::unique_ptr<Scene>(std::make_unique<SelectMusic4Keys>());
		default:
			g_CurrentScene = Scenes::FINISH; break;
		}
		break;

	case Scenes::GAME:
		switch (g_PlayMode)
		{
		case SelectKeys::KEYS_9:
		case SelectKeys::KEYS_16:
			return std::unique_ptr<Scene>(std::make_unique<GameElebeat>());
		case SelectKeys::KEYS_4:
			return std::unique_ptr<Scene>(std::make_unique<GameDanceElenation>());
		default:
			g_CurrentScene = Scenes::FINISH; break;
		}
		break;

	case Scenes::RESULT:
		return std::unique_ptr<Scene>(std::make_unique<Result>());

	default:
		g_CurrentScene = Scenes::FINISH; break;
	}

	if (g_CurrentScene == Scenes::FINISH)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("error"), _T("No Scene"), MB_OK);
	}
	return nullptr;
}

void SceneManager::SetNextScene(Scenes nextScene)
{
	g_CurrentScene = nextScene;
}