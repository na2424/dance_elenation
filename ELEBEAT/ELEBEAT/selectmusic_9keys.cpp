#include <boost/lexical_cast.hpp>
#include "SelectMusic9keys.h"
#include "BlueLightBall.h"
#include "extern.h"
#include "Foreach.h"
#include "Input.h"
#include "Marker.h"
#include "NumberDraw.h"
#include "Screen.h"

std::vector<Image> SelectMusic9keys::g_AA;			//アルバムアート    5000から代入していく
std::vector<Image> SelectMusic9keys::g_TempAA;		//読み込んだAAをハンドルのみ保持する変数
std::vector<Image> SelectMusic9keys::g_KindFolder;	//folder画像

SelectMusic9keys::SelectMusic9keys() :
	m_MusicKind(0), m_MusicNum(0), m_SelectFolderNum(0), m_CurrentDisplayTime(0), m_time(0), m_StartTime(0), m_Key(0),
	m_SelecterCounter(0), m_LRfrag(0), m_Counter(0),
	m_MarkerOptionPositionY(0), isBreak(false), m_GraphCache(0), isLoadScore(false), isBack(false), setumei1(0), setumei2(0),
	player_option(0)
{
	m_NumberDraw = std::make_unique<TimerDrawer>();
	m_mouseClickObjectList = std::make_unique<MouseClickObjectList>();
	m_LightBalls.resize(BALL_MAX);
}

void SelectMusic9keys::Initialize(const std::shared_ptr<Music>& musicP)
{
	// 描画可能領域セット
	SetDrawArea(0, 0, WIN_WIDTH, WIN_HEIGHT);
	setlocale(LC_ALL, "ja_JP.UTF-8");

	m_CurrentDisplayTime = 99;                          //現在のタイムを入れるための変数
	m_time = 0;
	m_StartTime = 0;
	m_Key = 0;
	m_SelecterCounter = 0;                                //セレクター回転カウンタ
	ClearDifNum();
	m_LRfrag = 0;
	m_Counter = 0;
	m_fontHandle.fill(0);
	m_MarkerOptionPositionY = 420 + 42 * 6;

	isLoadScore = false;
	isBack = false;

	m_GraphCache = MakeGraph(WIN_WIDTH, WIN_HEIGHT);

	this->backSound = LoadSoundMem(_T("Themes\\DANCE ELENATION\\Sounds\\Common back.ogg"));

	if (rand() % 2 == 1)
		roop_music = LoadSoundMem(_T("Themes\\DANCE ELENATION\\Sounds\\music_select\\roop.ogg"));
	else
		roop_music = LoadSoundMem(_T("Themes\\DANCE ELENATION\\Sounds\\music_select\\roop.mp3"));

	//Lua読み込み
	InitLua();
	ReadLua();

	//画像ロード
	if ( !g_IsLoaded ) //先ロード終わってるのなら関数とばす
	{
		LoadGraphAll(musicP);
		g_NowCenter = CENTER_DEFAULT;
		g_BeforeCenter = CENTER_DEFAULT;
		this->selectmode = SelectMode::FOLDER;
		g_DifNowCenter = 0;
	}
	else if (beforeSelectMusicNum != -1) //一回でもゲームしたならばそのAAを選択するようにする
	{
		this->m_MusicKind = musicP->elebeatSong->m_FolderNum;
		this->m_MusicNum = musicP->elebeatSong->m_TotalMusicNum;
		std::copy(musicP->elebeatSong->m_MusicNumAt.begin(), musicP->elebeatSong->m_MusicNumAt.end(), back_inserter(this->m_FolderMusicNum));

		TCHAR foldername[260] = {};
		auto count = 0;
		auto beforefoldernumber = 0;
		m_InputFrameCount = 6;      //最初のsongsを抜く

		while (true)
		{
			if (musicP->elebeatSong->m_AlbumArtPath[beforeSelectMusicNum].at(m_InputFrameCount) == '\\')
				break;

			foldername[count] += musicP->elebeatSong->m_AlbumArtPath[beforeSelectMusicNum].at(m_InputFrameCount);
			count++;
			m_InputFrameCount++;
		}

		m_InputFrameCount = 0;

		while (true)
		{
			if (_tcscmp(foldername, musicP->elebeatSong->m_FolderName[m_InputFrameCount].c_str()) == 0)
			{
				beforefoldernumber = m_InputFrameCount;
				break;
			}
			else
			{
				m_InputFrameCount++;
			}
		}
		SetMode(beforefoldernumber, musicP);
		DifNumberCalculate(m_difNumberTemp, g_NowCenter, musicP);
		CutBPM(GetNowAANumber(g_NowCenter), g_NowCenter, musicP);
		this->selectmode = SelectMode::AA;
	}
	else
	{
		this->m_MusicKind = musicP->elebeatSong->m_FolderNum;
		this->m_MusicNum = musicP->elebeatSong->m_TotalMusicNum;
		copy(musicP->elebeatSong->m_MusicNumAt.begin(), musicP->elebeatSong->m_MusicNumAt.end(), back_inserter(this->m_FolderMusicNum));
		this->m_SelectFolderNum = 0;
		g_NowCenter = CENTER_DEFAULT;
		g_BeforeCenter = CENTER_DEFAULT;
		this->selectmode = SelectMode::FOLDER;
		g_DifNowCenter = 0;
	}

	m_InputFrameCount = 0;

	//フォント
	m_FontHandle[0] = CreateFontToHandle(nullptr, 42, 6, DX_FONTTYPE_ANTIALIASING_EDGE);
	m_FontHandle[1] = CreateFontToHandle(nullptr, 29, 6, DX_FONTTYPE_ANTIALIASING_EDGE);
	m_FontHandle[2] = CreateFontToHandle(nullptr, 18, 6, DX_FONTTYPE_ANTIALIASING_EDGE);
	m_FontHandle[3] = CreateFontToHandle(nullptr, 20, 4, DX_FONTTYPE_ANTIALIASING_EDGE);

	// 音声をループ再生する
	PlaySoundMem(roop_music, DX_PLAYTYPE_LOOP);
	//座標設定
	GraphCoordinatesSet();
	//マーカー座標
	MarkerCoordinatesSet();

	Screen::FadeIn();
	m_StartTime = GetNowCount();
	isLoadScore = TRUE;
	SetDrawScreen(DX_SCREEN_BACK);
}

void SelectMusic9keys::LoadSceneImage()
{
	g_BackImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\back.jpg"));

	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\selectmusic-asi.png"), 4, 4, 1, 100, 100, footImage.data());    //足画像
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Numbers\\selectmusic_level00_num.png"), 10, 10, 1, 25, 50, difnumberColorImage[0]);        //番号画像 灰色
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Numbers\\selectmusic_level01_num.png"), 10, 10, 1, 25, 50, difnumberColorImage[1]);        //番号画像 緑
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Numbers\\selectmusic_level02_num.png"), 10, 10, 1, 25, 50, difnumberColorImage[2]);        //番号画像 黄色
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Numbers\\selectmusic_level03_num.png"), 10, 10, 1, 25, 50, difnumberColorImage[3]);        //番号画像 赤
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\selectmusic_hyouka60x50.png"), 8, 1, 8, 60, 50, hyouka);
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\rotation48x48.png"), 5, 5, 1, 48, 48, fullcombo);

	if (OptionSaveData::g_IsEventMode)
	{
		stageNumImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/MusicSelect/eventmode.png"));
	}
	else
	{
		if (OptionSaveData::g_SongCount == OptionSaveData::g_SongCountSetting + 1)
			stageNumImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/MusicSelect/finalstage.png"));
		else
			stageNumImage = LoadGraph((_T("Themes/DANCE ELENATION/Graphics/MusicSelect/stage") + boost::lexical_cast<tstring>(OptionSaveData::g_SongCount) + _T(".png")).c_str());
	}
}

void SelectMusic9keys::LoadSceneSound() const
{
}

void SelectMusic9keys::Update(const std::shared_ptr<Music>& musicP)
{
	//シングルトン参照
	auto& input = Input::GetInstance();
	auto& color = Color::GetInstance();

	//カウンター
	count++;
	//キー操作
	input.GetKey();
	//マウスの座標を取得
	input.GetMouse();

	m_mouseClickObjectList->Update();

	//画面消去
	ClearDrawScreen();

	//背景画像
	DrawGraph(0, 0, g_BackImage, TRUE);

	//パーティクル位置を更新と描画
	FOREACH(BlueLightBall, m_LightBalls, lb)
	{
		lb->Update(nullptr);
		lb->Draw();
	}

	//AA描画
	DrawGraphAll(g_NowCenter);

	//その一個上におく画像
	DrawGraph(0, 0, backFrontImage, TRUE);

	//タイマー処理
	m_time = GetNowCount() - m_StartTime;

	if (OptionSaveData::g_IsEventMode)
		m_CurrentDisplayTime = 99;
	else
		m_CurrentDisplayTime = static_cast<int>(START_SELECT_TIME - m_time / 1000);

	//キー操作受け取り
	const auto key = CatchKey(g_NowCenter, selectdifsound);

	//難易度描画
	//難易度選択DrawBox
	DrawBox((WIN_WIDTH / 2) - 150, 554 + 45 * g_DifNowCenter, (WIN_WIDTH / 2) + 290, 595 + 45 * g_DifNowCenter, DIFFICULT_BOX_COLOR[g_DifNowCenter], TRUE);
	DrawBox((WIN_WIDTH / 2) - 150, 554 + 45 * g_DifNowCenter, (WIN_WIDTH / 2) + 290, 595 + 45 * g_DifNowCenter, color.Black(), FALSE);

	//足
	//灰色足描画
	for (int i = 0; i < 3; ++i)
	{
		DrawGraph(515, 526 + 45 * i, footImage[0], TRUE);
	}

	//選択足
	DrawGraph(515, 526 + 45 * g_DifNowCenter, footImage[g_DifNowCenter + 1], TRUE);

	//数字色つき
	for (int i = 0; i < 3; ++i)
	{
		DrawGraph(580, 550 + 45 * i, (i == g_DifNowCenter) ? difnumberColorImage[i + 1][m_difNumberTemp[i][0]] : difnumberColorImage[0][m_difNumberTemp[i][0]], TRUE);    //数字色つき
		DrawGraph(599, 550 + 45 * i, (i == g_DifNowCenter) ? difnumberColorImage[i + 1][m_difNumberTemp[i][1]] : difnumberColorImage[0][m_difNumberTemp[i][1]], TRUE);    //緑
	}

	//くるくる回るアレ
	DrawRotaGraph(360, 575 + 45 * g_DifNowCenter, 1.0, (PI / 20) * m_SelecterCounter, selecterImage, TRUE, FALSE);
	//回転処理
	m_SelecterCounter++;

	DrawGraph(-42, 631, setumei1, TRUE);
	DrawGraph(742, 631, setumei2, TRUE);

	if (m_SelecterCounter == 15)
		m_SelecterCounter = 1;

	//難易度文字
	DrawGraph(400, 545, difficultyImage, TRUE);

	//左右画像
	DrawLR(&right_pic, &left_pic);

	//ステージ数画像
	if (count < 20)
		DrawGraph((count - 20) * 13 + 20, 80, stageNumImage, TRUE);
	else
		DrawGraph(20, 80, stageNumImage, TRUE);

	/************************************こっから処理系統/************************************/
	//folderからAAへ
	if (key == 75)
	{
		PlaySoundMem(expandSound, DX_PLAYTYPE_BACK);
		g_NowCenter = ChangeMode(g_NowCenter, musicP);
		if (g_NowCenter == -1)
		{
			if (!OptionSaveData::g_IsFullScreen)
				MessageBox(nullptr, _T("エラー項目C_M"), _T("エラー"), MB_OK);
		}
	}
	else if (key == 100) //AAでenter
	{
		//譜面が存在するか否か
		if (m_difNumberTemp[g_DifNowCenter][0] != 0 || m_difNumberTemp[g_DifNowCenter][1] != 0)
		{
			PlaySoundMem(decideSound, DX_PLAYTYPE_BACK);
			//1ならげーむへ
			EscapeSelect(EscapeState::GAME, g_NowCenter);

			isBreak = true;
		}
	}
	else if (key == 125) //folderでrandを選択
	{
		//完全ランダム選択
		randomcenternum = GetRandom(1, musicP->elebeatSong->m_TotalMusicNum - 1, musicP);
		DxLib::PlaySoundMem(decideSound, DX_PLAYTYPE_BACK);
		//ランダムな数を渡す
		EscapeSelect(EscapeState::RANDOM_FOLDER, randomcenternum);
		isBreak = true;
	}
	else if (key == 150) //AAからfolderに
	{
		g_NowCenter = ChangeMode(g_NowCenter, musicP);
		//難易度数字をゼロに
		ClearDifNum();
		PlaySoundMem(closeSound, DX_PLAYTYPE_BACK);
		if (g_NowCenter == -1)
		{
			if (!OptionSaveData::g_IsFullScreen)
				MessageBox(nullptr, _T("ごめんなさいエラーです。作者に文句を言ってください。 エラー項目C_M"), _T("エラー"), MB_OK);
		}
	}
	else if (key == 175)
	{
		//AAでrandを選択した
		g_NowCenter = ChangeMode(g_NowCenter, musicP);
		//難易度数字をゼロに
		ClearDifNum();
		PlaySoundMem(closeSound, DX_PLAYTYPE_BACK);
		if (g_NowCenter == -1)
		{
			if (!OptionSaveData::g_IsFullScreen)
				MessageBox(nullptr, _T("ごめんなさいエラーです。作者に文句を言ってください。 エラー項目C_M"), _T("エラー"), MB_OK);
		}
	}
	else if (key == 200)
	{
		//folderでesc title二戻る
		//0ならタイトルへ
		EscapeSelect(EscapeState::TITLE, g_NowCenter);
		isBack = true;
	}
	else if (key != 150)
	{
		if (input.keyEsc == 1)
		{
			SceneManager::SetNextScene(Scenes::TITLE);
			DxLib::PlaySoundMem(backSound, DX_PLAYTYPE_BACK);
			StopSoundMem(roop_music);
			DxLib::ClearDrawScreen();
			Screen::FadeOut();
			DxLib::ClearDrawScreen();
			DelSelectMusic_9(roop_music, backSound);
			isBack = true;
		}
	}
	else
	{
		//キー押してないとき
	}

	//Update
	if (selectmode == SelectMode::FOLDER)
	{
	}
	else
	{
		DrawGraph(500, 95, bpmFrameImage, TRUE);
	}

	//timerが0になったときの処理
	if (m_CurrentDisplayTime <= -1)
	{
		if (selectmode == SelectMode::FOLDER)
		{
			OnTimeUpFolder(musicP);
		}
		else
		{
			if (m_difNumberTemp[g_DifNowCenter][0] != 0 || m_difNumberTemp[g_DifNowCenter][1] != 0)
			{
				EscapeSelect(EscapeState::GAME, g_NowCenter);
				isBreak = true;
			}
			else
			{
				randomcenternum = GetRandom(g_CurrentAlbumArt[0] + 1, g_CurrentAlbumArt[1], musicP);
				g_NowCenter = randomcenternum;
				EscapeSelect(EscapeState::RANDOM_ALL, randomcenternum);
				isBreak = true;
			}
		}
	}

	//左押されたとき
	if (isLeft)
	{
		DrawGraph(WIN_WIDTH / 2 + 140, 326, right_click_pic, TRUE);
		MoveLeft(15, 30);
		MoveMirrorLeft(15, 30);
		m_InputFrameCount++;
	}

	//右押されたとき
	if (isRight)
	{
		DrawGraph(WIN_WIDTH / 2 - 240, 326, left_click_pic, TRUE);
		MoveRight(15, 30);
		MoveMirrorRight(15, 30);
		m_InputFrameCount++;
	}

	//左押し続け
	if (isLeftContinue)
	{
		countMoveAA++;
		//もし一定数以上押されたら
		if (countMoveAA >= 60)
		{
			//高速フラグを立てる
			isFastLeft = 1;
			//int over flowをなくす
			if (countMoveAA >= 10000)
				countMoveAA = 60;
		}
	}

	//右押し続け
	if (isRightContinue)
	{
		countMoveAA++;

		//もし一定数以上押されたら
		if (countMoveAA >= 60)
		{
			//高速フラグを立てる
			isFastRight = 1;
			//int over flowをなくす
			if (countMoveAA >= 10000)
				countMoveAA = 60;
		}
	}

	//どっちも押してすらないとき全部初期化
	if (!isLeftContinue && !isRightContinue)
	{
		countMoveAA = 0;
		isFastLeft = 0;
		isFastRight = 0;
	}

	//i,goによる高速化移動
	if (m_InputFrameCount == 6)
	{
		if (isFastLeft || isFastRight)
		{
			if (isLeft)
			{
				g_NowCenter++;
				isLeft = false;
				isLoadScore = true;
				DxLib::PlaySoundMem(wheelSound, DX_PLAYTYPE_BACK);
			}

			if (isRight)
			{
				g_NowCenter--;
				isRight = false;
				isLoadScore = true;
				DxLib::PlaySoundMem(wheelSound, DX_PLAYTYPE_BACK);
			}

			//座標設定
			GraphCoordinatesSet();

			if (selectmode == SelectMode::FOLDER)
			{
				if (g_NowCenter < 0)
					g_NowCenter = CENTER_DEFAULT + m_MusicKind - 1;
				else if (g_NowCenter > CENTER_DEFAULT + m_MusicKind - 1)
					g_NowCenter = CENTER_DEFAULT;
			}
			else {                //AA時
				if (g_NowCenter < 0)
					g_NowCenter = CENTER_DEFAULT + m_FolderMusicNum[m_SelectFolderNum] - 1;
				else if (g_NowCenter > CENTER_DEFAULT + m_FolderMusicNum[m_SelectFolderNum] - 1)
					g_NowCenter = CENTER_DEFAULT;

				DifNumberCalculate(m_difNumberTemp, g_NowCenter, musicP);
			}
			m_InputFrameCount = 0;
		}
	}

	//i,go~~の初期化 動くの終了
	if (m_InputFrameCount == 12)
	{
		m_InputFrameCount = 0;
		if (isLeft)
		{
			PlaySoundMem(wheelSound, DX_PLAYTYPE_BACK);
			g_NowCenter++;
			isLeft = 0;
			isLoadScore = true;
		}
		if (isRight)
		{
			PlaySoundMem(wheelSound, DX_PLAYTYPE_BACK);
			g_NowCenter--;
			isRight = 0;
			isLoadScore = true;
		}

		//座標設定
		GraphCoordinatesSet();

		if (selectmode == SelectMode::FOLDER)
		{
			if (g_NowCenter < 0)
				g_NowCenter = CENTER_DEFAULT + m_MusicKind - 1;
			else if (g_NowCenter > CENTER_DEFAULT + m_MusicKind - 1)
				g_NowCenter = CENTER_DEFAULT;
		}
		else //AA時
		{
			//最小最大bpmを得る
			CutBPM(GetNowAANumber(g_NowCenter), g_NowCenter, musicP);

			if (g_NowCenter < 0)
				g_NowCenter = CENTER_DEFAULT + m_FolderMusicNum[m_SelectFolderNum] - 1;
			else if (g_NowCenter > CENTER_DEFAULT + m_FolderMusicNum[m_SelectFolderNum] - 1)
				g_NowCenter = CENTER_DEFAULT;

			DifNumberCalculate(m_difNumberTemp, g_NowCenter, musicP);
		}
	}

	//音楽名描画
	DrawMusicString(g_NowCenter, m_fontHandle.data(), count, musicP);

	if (selectmode == SelectMode::AA)
	{
		DrawModiGraph(191, 550, 323, 550, 323, 684, 191, 684, g_AA[g_NowCenter], FALSE);
		DrawHiScore(g_NowCenter, m_SelecterCounter, fullcombo, hyouka, m_fontHandle.data(), &isLoadScore, musicP);
	}

	//breakフラグ回収
	if (isBreak)
	{
		//画面キャプチャ
		GetDrawScreenGraph(0, 0, WIN_WIDTH, WIN_HEIGHT, m_GraphCache);
		blackImage = m_GraphCache;
		//メモリ開放
		// 作成したフォントデータを削除する
		DeleteFontToHandle(m_fontHandle[0]);
		DeleteFontToHandle(m_fontHandle[1]);
		DeleteFontToHandle(m_fontHandle[2]);
		return;
	}

	/*****************************マーカー描画****************************/
	if (markerflag && m_MarkerOptionPositionY > 420)
		m_MarkerOptionPositionY -= 42;

	if (!markerflag && m_MarkerOptionPositionY <= 420 + 42 * 6)
	{
		m_MarkerOptionPositionY += 42;
		DrawGraph(42, m_MarkerOptionPositionY, marker_waku, TRUE);
	}

	//shift押し
	if (markerflag || input.keyShift > 0)
	{
		//マーカー移動
		if (isLeftMarker)
			g_MarkerCount++;

		if (isRightMarker)
			g_MarkerCount++;

		//マーカー停止
		if (g_MarkerCount == 12)
		{
			if (isRightMarker)
			{
				isRightMarker = 0;
				DxLib::PlaySoundMem(mrkMoveSound, DX_PLAYTYPE_BACK);
				g_SelectMarker++;
			}
			if (isLeftMarker)
			{
				isLeftMarker = 0;
				DxLib::PlaySoundMem(mrkMoveSound, DX_PLAYTYPE_BACK);
				g_SelectMarker--;
			}
			//マーカー座標
			MarkerCoordinatesSet();
			//直し
			if (g_SelectMarker >= Marker::GetInstance().marker_num)
				g_SelectMarker = 0;
			if (g_SelectMarker < 0)
				g_SelectMarker = Marker::GetInstance().marker_num - 1;
			g_MarkerCount = 0;
		}
		//マーカー描画
		if (input.keyShift == 1)
			PlaySoundMem(mrkSound, DX_PLAYTYPE_BACK);
		DrawGraph(42, m_MarkerOptionPositionY, marker_waku, TRUE);

		ShowMarker(m_time, g_SelectMarker, m_MarkerOptionPositionY);
	}

	//フレームの画像表示
	if (count < 20)
	{
		count++;
		DrawGraph(0, -200 + (10 * count), backoverImage, TRUE);
		DrawGraph(0, WIN_HEIGHT - 80 + 200 - (10 * count), g_underBarImage, TRUE);

		//時間の描画
		m_NumberDraw->DrawMenuTimer(m_CurrentDisplayTime, -200 + (10 * count));
	}
	else
	{
		DrawGraph(0, 0, backoverImage, TRUE);
		DrawGraph(0, WIN_HEIGHT - 80, g_underBarImage, TRUE);

		//時間の描画
		m_NumberDraw->DrawMenuTimer(m_CurrentDisplayTime);
	}

	//マウスカーソルの表示
	DxLib::DrawRotaGraph2(input.mouseX, input.mouseY, 50, 50, 1.0f, m_Counter / 42.0, input.casolPic, TRUE);

	m_mouseClickObjectList->Draw();

	//反転
	ScreenFlip();

	if (input.keyPrtsc == 1)
		m_ssc->CaptureScreenshot();

	if (isBack)
	{
		SceneManager::SetNextScene(Scenes::TITLE);
		return;
	}
}

void SelectMusic9keys::Finalize(const std::shared_ptr<Music> &musicP)
{
	// 作成したフォントデータを削除する
	DeleteFontToHandle(m_fontHandle[0]);
	DeleteFontToHandle(m_fontHandle[1]);
	DeleteFontToHandle(m_fontHandle[2]);

	if (isBack)
	{
		Screen::FadeOut();
		return;
	}
	return;
}

//選曲画面でマーカープレビュー
void SelectMusic9keys::ShowMarker(int time, int selectmarker, int marker_y)
{
	int sectionTime = time % 1000;
	markernum = selectmarker;
	//マーカーの種類
	for (int i = 0; i < Marker::GetInstance().marker_num; i++)
	{
		DrawExtendGraph(168 + i * 150 - 64, marker_y + 180 - 64, 168 + i * 150 + 64, marker_y + 180 + 64, noteMarkerImage[i][marker_just[i]], TRUE);
		if (i + 1 == selectmarker)i++;
	}
	for (int j = 0; j < marker_numb[selectmarker]; j++)
	{
		if (marker_time[selectmarker] * j <= sectionTime && sectionTime < marker_time[selectmarker] * (j + 1))
		{
			DrawExtendGraph(168 + selectmarker * 150 - 64, marker_y + 180 - 64, 168 + selectmarker * 150 + 64, marker_y + 180 + 64, noteMarkerImage[selectmarker][j], TRUE);
			break;
		}
	}
}

void SelectMusic9keys::OnTimeUpFolder(const std::shared_ptr<Music> &musicP)
{
	//完全ランダム選択
	randomcenternum = GetRandom(1, musicP->elebeatSong->m_TotalMusicNum - 1, musicP);
	//ランダムな数を渡す
	EscapeSelect(EscapeState::RANDOM_FOLDER, randomcenternum);
}

void SelectMusic9keys::OnTimeUpAA()
{
}

void SelectMusic9keys::ClearDifNum()
{
	for (std::array<int,3> &d : m_difNumberTemp)
	{
		d.fill(0);
	}
}