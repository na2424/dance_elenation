#pragma once

class IParticle 
{
public:

	virtual void Update() = 0;
	virtual void Draw() = 0 ;

	virtual ~IParticle() {}    
};