#ifndef EXTERN_H
#define EXTERN_H
#include <vector>
#include "define.h"
#include "SettingIni.h"

//画像のglobal
extern int g_BackImage;                    //背景を入れるための変数

extern int musicNumber;                                    //一般化の音楽変数受け渡し　すべて入ってる
extern int musicDif;                                    //選択音楽の難易度
extern int selectMerker;                                //選択したマーカー番号を渡す
extern int beforeSelectMusicNum;                        //ゲーム終了後その音楽を選択するため使用 初回は-1
extern int beforeSelectMusicNum_16;                        //ゲーム終了後その音楽を選択するため使用 初回は-1
extern int musicNumber4;                                    //一般化の音楽変数受け渡し　すべて入ってる
extern int beforeSelectMusicNum4;

//マーカー変数
extern std::vector<std::vector<tstring> > marker_handle_normal;
extern std::vector<std::vector<tstring> > marker_handle_perfect;
extern std::vector<std::vector<tstring> > marker_handle_grate;
extern std::vector<std::vector<tstring> > marker_handle_good;

//RandAAReplace
extern std::vector<int> RandAAReplace;
extern std::vector<int> RandAA4Replace;

/***1P用***/
extern int sc;
extern int judgeDiv;                //各判定結果を入れるための変数

extern std::vector <std::vector <int> > noteMarkerImage;    //マーカー変数　前の[]が種類の数
extern std::vector <int> row;
extern std::vector <std::vector < std::vector <int > > > noteJudgeImage;

extern int blackImage;
extern int g_underBarImage;

extern int timerImage[10];            //右上のタイマー画像を入れるための配列、各画面共通で使う。
extern int bpmImage[10];            //BPMの数字画像を入れるための配列
extern int musicAAImage;                //選曲したAAのパスを入れるための変数。選曲した時にそれだけ入れればその後のゲーム画面やリザルト画面で使える。

extern int difficult;                //今の難易度を入れる変数(0→BASIC、1→NORMAL、2→ADVANCED)

extern int AAnum;

extern SettingIni Setting;

extern std::vector<int>marker_numb;//m1
extern std::vector<int>marker_just;//m3
extern std::vector<int>marker_time;//m4
extern std::vector<int>marker_numj;//m2

//MMD関連
extern int ModelHandle;

extern int markernum;

#endif