#ifndef MARKERINI_H
#define MARKERINI_H

#include <string>
#include <tchar.h>



#ifdef _UNICODE
typedef std::wstring string_t;
#else
typedef std::string string_t;
#endif

//---------------------------------------------------------------------------
//Setting.ini input output class.
//---------------------------------------------------------------------------
class MarkerIni
{
public:

	// Marker
	struct _Marker
	{
		int      m1;
		int      m2;
		int      m3;
		int      m4;
	} Marker;

	MarkerIni(void);
	MarkerIni(string_t fname);
	bool Load(string_t fname);
	bool Save(string_t fname);

protected:

	string_t initFileName;
	string_t loadFileName;

	bool IniRw(string_t f, int r);
	void Init();
};
typedef MarkerIni SettingFile; //�V���݊�

#endif