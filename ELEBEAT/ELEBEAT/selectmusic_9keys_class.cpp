/*--------------------------AAの座標について書く--------------------------
CENTERLEFTX:352: 正面のAAの左側の座標
CENTERLEFTY:142: 左上y 今142
AAsize      :256: AAの大きさ
解像度変更などの際はCENTERLEFTXを変更
AAsizeも同じように対応できるはず
----------------------------------------------------------------------*/
#include "SelectMusic9keys.h"
#include "define.h"
#include "DxHelper.h"
#include "stdlib.h"
#include "header.h"
#include "Input.h"
#include "extern.h"
#include "GameBase.h"
#include "Music.h"
#include <random>

//プロトタイプ関数
//void CreateSelectMusic(void);                                //コンストラクタ
//void GraphCoordinatesSet(void);                                //座標設定関数
//void MarkerCoordinatesSet(void);                            //マーカー座標指定関数
//int DrawMarker(int selectmarker);                            //マーカー描画
//void DrawGraphAll (int nowcenter);                            //画像を描画
//int CatchKey(int nowcenter,int& selectdifsound);            //難易度変更の上下キー受け取り
//int ChangeMode(int nowcenter, music *musicP);                //folder<-->AAの相互表示変換関数
//void LoadGraphAll(music *musicP);                            //苦渋の決断
//int NowFolderNumber(int nowcenter, music *musicP);            //一般化の番号取得
//int NowAANumber(int nowcenter);
//int SetAAGraph(int,int,int);
//void Delselectmusic (void);                                    //デストラクタ
//void CutBPM(int num, int nowcenter, music *musicP);                                        //bpm最大最小を得る
//void EscapeSelect(int escapeflag,int nowcenter);
//int GetRandom(int min, int max, music *musicP);
//void ShowBPM(void);
//void DifNumberCalculate(int number[][3], int nowcenter, music *musicP);        //難易度のアレ計算用
//void DrawHiScore(int nowcenter, int count, int fullcombo_gazou[], int hyoka_gazou[], int FontHandle[], bool *s_flag, music *musicP);
//int LoadHiScore(int nowcenter, music *musicP);                            //ハイスコア読み込み処理
//void LayerStringDraw(int stringX, int stirngY, const TCHAR drawstring[], int FontHandle);// 文字をああやって書く
//void DelSelectMusic_9(int& loop_sound,int& back_sound);        ///デストラクタ
//void DrawLR(int LRfrag,int *right_pic,int *left_pic);
//void DrawLR_click(int LRfrag,int *right_click_pic,int *left_click_pic);
//void debug_drawmusic(int musicNumber, int FontHandle[], int counter, music *musicP);        //曲名とか出す

/*---------------------------------------サブ関数一覧---------------------------------------*/
//コンストラクタ
void SelectMusic9keys::CreateSelectMusic(void) const
{
}
//x,yを設定 Coordinates = 座標
void SelectMusic9keys::GraphCoordinatesSet(void)
{
	int i = 0;
	int j = 0;

	/*************************x座標*************************/

	//正面画像のx
	m_x[15][0] = x[15][0] = CENTER_LEFT_X;
	m_x[15][1] = x[15][1] = CENTER_LEFT_X + AA_SIZE;
	m_x[15][2] = x[15][2] = CENTER_LEFT_X + AA_SIZE;
	m_x[15][3] = x[15][3] = CENTER_LEFT_X;

	//一個右x
	m_x[16][0] = x[16][0] = CENTER_LEFT_X + AA_SIZE;
	m_x[16][1] = x[16][1] = CENTER_LEFT_X + AA_SIZE + hukasi + AA_AA_BETWEEN_PX;
	m_x[16][2] = x[16][2] = CENTER_LEFT_X + AA_SIZE + hukasi + AA_AA_BETWEEN_PX;
	m_x[16][3] = x[16][3] = CENTER_LEFT_X + AA_SIZE;

	//右二個以降
	for (i = 17; i <= 21; i++)
	{
		m_x[i][0] = x[i][0] = CENTER_LEFT_X + AA_SIZE + ((j + 1)*AA_AA_BETWEEN_PX);
		m_x[i][1] = x[i][1] = CENTER_LEFT_X + AA_SIZE + Modi + ((j + 1)*AA_AA_BETWEEN_PX);
		m_x[i][2] = x[i][2] = CENTER_LEFT_X + AA_SIZE + Modi + ((j + 1)*AA_AA_BETWEEN_PX);
		m_x[i][3] = x[i][3] = CENTER_LEFT_X + AA_SIZE + ((j + 1)*AA_AA_BETWEEN_PX);
		j++;
	}
	j = 0;

	//一個左x 125差左と右
	m_x[14][0] = x[14][0] = CENTER_LEFT_X - 125;
	m_x[14][1] = x[14][1] = CENTER_LEFT_X;
	m_x[14][2] = x[14][2] = CENTER_LEFT_X;
	m_x[14][3] = x[14][3] = CENTER_LEFT_X - 125;

	//左二個以降
	for (i = 13; i >= 9; i--)
	{
		m_x[i][0] = x[i][0] = CENTER_LEFT_X - Modi - (j + 1)*AA_AA_BETWEEN_PX;
		m_x[i][1] = x[i][1] = CENTER_LEFT_X - Modi - (j*AA_AA_BETWEEN_PX) + hukasi;
		m_x[i][2] = x[i][2] = CENTER_LEFT_X - Modi - (j*AA_AA_BETWEEN_PX) + hukasi;
		m_x[i][3] = x[i][3] = CENTER_LEFT_X - Modi - (j + 1)*AA_AA_BETWEEN_PX;
		j++;
	}
	j = 0;

	/*************************y座標*************************/
	//正面画像のy さっきのfor文の中を再設定
	y[15][0] = CENTER_LEFT_Y;
	y[15][1] = CENTER_LEFT_Y;
	y[15][2] = CENTER_LEFT_Y + AA_SIZE;
	y[15][3] = CENTER_LEFT_Y + AA_SIZE;
	m_y[15][0] = y[15][3] + AA_SIZE + AA_MIRROR_BETWEEN_PX;
	m_y[15][1] = y[15][2] + AA_SIZE + AA_MIRROR_BETWEEN_PX;
	m_y[15][2] = y[15][2] + AA_MIRROR_BETWEEN_PX;
	m_y[15][3] = y[15][3] + AA_MIRROR_BETWEEN_PX;

	//一個右y
	y[16][0] = CENTER_LEFT_Y + 50;
	y[16][1] = CENTER_LEFT_Y;
	y[16][2] = CENTER_LEFT_Y + AA_SIZE;
	y[16][3] = CENTER_LEFT_Y + AA_SIZE - 50;
	m_y[16][0] = y[16][0] + (y[16][2] - y[16][1]) + (y[16][3] - y[16][0]) + AA_MIRROR_BETWEEN_PX;
	m_y[16][1] = y[16][1] + (y[16][2] - y[16][1]) + (y[16][3] - y[16][0]) + AA_MIRROR_BETWEEN_PX;
	m_y[16][2] = y[16][2] + AA_MIRROR_BETWEEN_PX;
	m_y[16][3] = y[16][3] + AA_MIRROR_BETWEEN_PX;

	//右二個以降
	for (i = 17; i <= 21; i++)
	{
		y[i][0] = CENTER_LEFT_Y + 50;
		y[i][1] = CENTER_LEFT_Y;
		y[i][2] = CENTER_LEFT_Y + AA_SIZE;
		y[i][3] = CENTER_LEFT_Y + AA_SIZE - 50;
		m_y[i][0] = y[i][0] + (y[i][2] - y[i][1]) + (y[i][3] - y[i][0]) + AA_MIRROR_BETWEEN_PX;
		m_y[i][1] = y[i][1] + (y[i][2] - y[i][1]) + (y[i][3] - y[i][0]) + AA_MIRROR_BETWEEN_PX;
		m_y[i][2] = y[i][2] + AA_MIRROR_BETWEEN_PX;
		m_y[i][3] = y[i][3] + AA_MIRROR_BETWEEN_PX;
	}

	//一個左y
	y[14][0] = CENTER_LEFT_Y;
	y[14][1] = CENTER_LEFT_Y + 50;
	y[14][2] = CENTER_LEFT_Y + AA_SIZE - 50;
	y[14][3] = CENTER_LEFT_Y + AA_SIZE;
	m_y[14][0] = y[14][0] + (y[14][2] - y[14][1]) + (y[14][3] - y[14][0]) + AA_MIRROR_BETWEEN_PX;
	m_y[14][1] = y[14][1] + (y[14][2] - y[14][1]) + (y[14][3] - y[14][0]) + AA_MIRROR_BETWEEN_PX;
	m_y[14][2] = y[14][2] + AA_MIRROR_BETWEEN_PX;
	m_y[14][3] = y[14][3] + AA_MIRROR_BETWEEN_PX;

	//左二個以降
	for (i = 13; i >= 9; i--)
	{
		y[i][0] = CENTER_LEFT_Y;
		y[i][1] = CENTER_LEFT_Y + 50;
		y[i][2] = CENTER_LEFT_Y + AA_SIZE - 50;
		y[i][3] = CENTER_LEFT_Y + AA_SIZE;
		m_y[i][0] = y[i][0] + (y[i][2] - y[i][1]) + (y[i][3] - y[i][0]) + AA_MIRROR_BETWEEN_PX;
		m_y[i][1] = y[i][1] + (y[i][2] - y[i][1]) + (y[i][3] - y[i][0]) + AA_MIRROR_BETWEEN_PX;
		m_y[i][2] = y[i][2] + AA_MIRROR_BETWEEN_PX;
		m_y[i][3] = y[i][3] + AA_MIRROR_BETWEEN_PX;
	}
}
void SelectMusic9keys::MarkerCoordinatesSet(void)
{
	int j = 1;

	//真ん中
	g_MarkerX[8][0] = g_MarkerX[8][3] = MARKER_LEFT_X;
	g_MarkerX[8][1] = g_MarkerX[8][2] = MARKER_LEFT_X + MARKER_SIZE;

	j = 1;

	//x右
	for (int i = 9; i <= 13; i++)
	{
		g_MarkerX[i][0] = g_MarkerX[i][3] = g_MarkerX[8][0] + (MARKERBETWEENSIZE * j) + (MARKER_SIZE * j);    //左側
		g_MarkerX[i][1] = g_MarkerX[i][2] = g_MarkerX[8][1] + (MARKERBETWEENSIZE * j) + (MARKER_SIZE * j);    //右側
		j++;
	}

	j = 1;

	//x左
	for (int i = 7; i >= 3; i--)
	{
		g_MarkerX[i][0] = g_MarkerX[i][3] = g_MarkerX[8][0] - ((MARKERBETWEENSIZE * j) + (MARKER_SIZE * j));    //左側
		g_MarkerX[i][1] = g_MarkerX[i][2] = g_MarkerX[8][1] - ((MARKERBETWEENSIZE * j) + (MARKER_SIZE * j));    //右側
		j++;
	}

	//y
	for (int i = 3; i <= 13; i++)
	{
		g_MarkerY[i][0] = g_MarkerY[i][1] = MARKER_LEFT_Y;
		g_MarkerY[i][2] = g_MarkerY[i][3] = MARKER_LEFT_Y + MARKER_SIZE;
	}
}
//AA描画 引数はglobalAAを内部的に受け取る
//説明無髄 AA[1000] の1000の部分に関係
void SelectMusic9keys::DrawGraphAll(int nowcenter)
{
	//folderaa/AAの画像設定
	if (selectmode == SelectMode::FOLDER)
	{
		//フォルダなら
		/*******************************鏡面*******************************/
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 128);//以後半分の明るさでαブレンド
		int plus[6] = {};
		int minus[6] = {};
		for (int i = 0; i < 6; ++i)
		{
			plus[i] = (nowcenter + (i + 1)) % m_MusicKind;
			minus[i] = (nowcenter - (i + 1) + m_MusicKind * 6) % m_MusicKind;
		}
		//folder右側
		DrawModiGraph((int)m_x[21][0], (int)m_y[21][0], (int)m_x[21][1], (int)m_y[21][1], (int)m_x[21][2], (int)m_y[21][2], (int)m_x[21][3], (int)m_y[21][3], g_KindFolder[plus[5]], TRUE);
		DrawModiGraph((int)m_x[20][0], (int)m_y[20][0], (int)m_x[20][1], (int)m_y[20][1], (int)m_x[20][2], (int)m_y[20][2], (int)m_x[20][3], (int)m_y[20][3], g_KindFolder[plus[4]], TRUE);
		DrawModiGraph((int)m_x[19][0], (int)m_y[19][0], (int)m_x[19][1], (int)m_y[19][1], (int)m_x[19][2], (int)m_y[19][2], (int)m_x[19][3], (int)m_y[19][3], g_KindFolder[plus[3]], TRUE);
		DrawModiGraph((int)m_x[18][0], (int)m_y[18][0], (int)m_x[18][1], (int)m_y[18][1], (int)m_x[18][2], (int)m_y[18][2], (int)m_x[18][3], (int)m_y[18][3], g_KindFolder[plus[2]], TRUE);
		DrawModiGraph((int)m_x[17][0], (int)m_y[17][0], (int)m_x[17][1], (int)m_y[17][1], (int)m_x[17][2], (int)m_y[17][2], (int)m_x[17][3], (int)m_y[17][3], g_KindFolder[plus[1]], TRUE);
		DrawModiGraph((int)m_x[16][0], (int)m_y[16][0], (int)m_x[16][1], (int)m_y[16][1], (int)m_x[16][2], (int)m_y[16][2], (int)m_x[16][3], (int)m_y[16][3], g_KindFolder[plus[0]], TRUE);

		//folder左側
		DrawModiGraph((int)m_x[9][0], (int)m_y[9][0], (int)m_x[9][1], (int)m_y[9][1], (int)m_x[9][2], (int)m_y[9][2], (int)m_x[9][3], (int)m_y[9][3], g_KindFolder[minus[5]], TRUE);
		DrawModiGraph((int)m_x[10][0], (int)m_y[10][0], (int)m_x[10][1], (int)m_y[10][1], (int)m_x[10][2], (int)m_y[10][2], (int)m_x[10][3], (int)m_y[10][3], g_KindFolder[minus[4]], TRUE);
		DrawModiGraph((int)m_x[11][0], (int)m_y[11][0], (int)m_x[11][1], (int)m_y[11][1], (int)m_x[11][2], (int)m_y[11][2], (int)m_x[11][3], (int)m_y[11][3], g_KindFolder[minus[3]], TRUE);
		DrawModiGraph((int)m_x[12][0], (int)m_y[12][0], (int)m_x[12][1], (int)m_y[12][1], (int)m_x[12][2], (int)m_y[12][2], (int)m_x[12][3], (int)m_y[12][3], g_KindFolder[minus[2]], TRUE);
		DrawModiGraph((int)m_x[13][0], (int)m_y[13][0], (int)m_x[13][1], (int)m_y[13][1], (int)m_x[13][2], (int)m_y[13][2], (int)m_x[13][3], (int)m_y[13][3], g_KindFolder[minus[1]], TRUE);
		DrawModiGraph((int)m_x[14][0], (int)m_y[14][0], (int)m_x[14][1], (int)m_y[14][1], (int)m_x[14][2], (int)m_y[14][2], (int)m_x[14][3], (int)m_y[14][3], g_KindFolder[minus[0]], TRUE);

		//folder正面
		DrawModiGraph((int)m_x[15][0], (int)m_y[15][0], (int)m_x[15][1], (int)m_y[15][1], (int)m_x[15][2], (int)m_y[15][2], (int)m_x[15][3], (int)m_y[15][3], g_KindFolder[nowcenter], TRUE);

		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);//ブレンドモードをリセットする。

		/********************************非鏡面********************************/
		//folder右側
		DrawModiGraph((int)x[21][0], (int)y[21][0], (int)x[21][1], (int)y[21][1], (int)x[21][2], (int)y[21][2], (int)x[21][3], (int)y[21][3], g_KindFolder[plus[5]], TRUE);
		DrawModiGraph((int)x[20][0], (int)y[20][0], (int)x[20][1], (int)y[20][1], (int)x[20][2], (int)y[20][2], (int)x[20][3], (int)y[20][3], g_KindFolder[plus[4]], TRUE);
		DrawModiGraph((int)x[19][0], (int)y[19][0], (int)x[19][1], (int)y[19][1], (int)x[19][2], (int)y[19][2], (int)x[19][3], (int)y[19][3], g_KindFolder[plus[3]], TRUE);
		DrawModiGraph((int)x[18][0], (int)y[18][0], (int)x[18][1], (int)y[18][1], (int)x[18][2], (int)y[18][2], (int)x[18][3], (int)y[18][3], g_KindFolder[plus[2]], TRUE);
		DrawModiGraph((int)x[17][0], (int)y[17][0], (int)x[17][1], (int)y[17][1], (int)x[17][2], (int)y[17][2], (int)x[17][3], (int)y[17][3], g_KindFolder[plus[1]], TRUE);
		DrawModiGraph((int)x[16][0], (int)y[16][0], (int)x[16][1], (int)y[16][1], (int)x[16][2], (int)y[16][2], (int)x[16][3], (int)y[16][3], g_KindFolder[plus[0]], TRUE);

		//folder左側
		DrawModiGraph((int)x[9][0], (int)y[9][0], (int)x[9][1], (int)y[9][1], (int)x[9][2], (int)y[9][2], (int)x[9][3], (int)y[9][3], g_KindFolder[minus[5]], TRUE);
		DrawModiGraph((int)x[10][0], (int)y[10][0], (int)x[10][1], (int)y[10][1], (int)x[10][2], (int)y[10][2], (int)x[10][3], (int)y[10][3], g_KindFolder[minus[4]], TRUE);
		DrawModiGraph((int)x[11][0], (int)y[11][0], (int)x[11][1], (int)y[11][1], (int)x[11][2], (int)y[11][2], (int)x[11][3], (int)y[11][3], g_KindFolder[minus[3]], TRUE);
		DrawModiGraph((int)x[12][0], (int)y[12][0], (int)x[12][1], (int)y[12][1], (int)x[12][2], (int)y[12][2], (int)x[12][3], (int)y[12][3], g_KindFolder[minus[2]], TRUE);
		DrawModiGraph((int)x[13][0], (int)y[13][0], (int)x[13][1], (int)y[13][1], (int)x[13][2], (int)y[13][2], (int)x[13][3], (int)y[13][3], g_KindFolder[minus[1]], TRUE);
		DrawModiGraph((int)x[14][0], (int)y[14][0], (int)x[14][1], (int)y[14][1], (int)x[14][2], (int)y[14][2], (int)x[14][3], (int)y[14][3], g_KindFolder[minus[0]], TRUE);

		//folder正面
		DrawModiGraph((int)x[15][0], (int)y[15][0], (int)x[15][1], (int)y[15][1], (int)x[15][2], (int)y[15][2], (int)x[15][3], (int)y[15][3], g_KindFolder[nowcenter], TRUE);
	}
	else
	{
		//AAなら
		/*******************************鏡面*******************************/
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 128);//以後半分の明るさでαブレンド
		int plus[6] = {};
		int minus[6] = {};
		for (int i = 0; i < 6; ++i)
		{
			plus[i] = (nowcenter + (i + 1)) % m_FolderMusicNum[m_SelectFolderNum];
			minus[i] = (nowcenter - (i + 1) + m_FolderMusicNum[m_SelectFolderNum] * 6) % m_FolderMusicNum[m_SelectFolderNum];
		}
		//AA右側
		DrawModiGraph((int)m_x[21][0], (int)m_y[21][0], (int)m_x[21][1], (int)m_y[21][1], (int)m_x[21][2], (int)m_y[21][2], (int)m_x[21][3], (int)m_y[21][3], g_AA[plus[5]], FALSE);
		DrawModiGraph((int)m_x[20][0], (int)m_y[20][0], (int)m_x[20][1], (int)m_y[20][1], (int)m_x[20][2], (int)m_y[20][2], (int)m_x[20][3], (int)m_y[20][3], g_AA[plus[4]], FALSE);
		DrawModiGraph((int)m_x[19][0], (int)m_y[19][0], (int)m_x[19][1], (int)m_y[19][1], (int)m_x[19][2], (int)m_y[19][2], (int)m_x[19][3], (int)m_y[19][3], g_AA[plus[3]], FALSE);
		DrawModiGraph((int)m_x[18][0], (int)m_y[18][0], (int)m_x[18][1], (int)m_y[18][1], (int)m_x[18][2], (int)m_y[18][2], (int)m_x[18][3], (int)m_y[18][3], g_AA[plus[2]], FALSE);
		DrawModiGraph((int)m_x[17][0], (int)m_y[17][0], (int)m_x[17][1], (int)m_y[17][1], (int)m_x[17][2], (int)m_y[17][2], (int)m_x[17][3], (int)m_y[17][3], g_AA[plus[1]], FALSE);
		DrawModiGraph((int)m_x[16][0], (int)m_y[16][0], (int)m_x[16][1], (int)m_y[16][1], (int)m_x[16][2], (int)m_y[16][2], (int)m_x[16][3], (int)m_y[16][3], g_AA[plus[0]], FALSE);

		//AA左側
		DrawModiGraph((int)m_x[9][0], (int)m_y[9][0], (int)m_x[9][1], (int)m_y[9][1], (int)m_x[9][2], (int)m_y[9][2], (int)m_x[9][3], (int)m_y[9][3], g_AA[minus[5]], FALSE);
		DrawModiGraph((int)m_x[10][0], (int)m_y[10][0], (int)m_x[10][1], (int)m_y[10][1], (int)m_x[10][2], (int)m_y[10][2], (int)m_x[10][3], (int)m_y[10][3], g_AA[minus[4]], FALSE);
		DrawModiGraph((int)m_x[11][0], (int)m_y[11][0], (int)m_x[11][1], (int)m_y[11][1], (int)m_x[11][2], (int)m_y[11][2], (int)m_x[11][3], (int)m_y[11][3], g_AA[minus[3]], FALSE);
		DrawModiGraph((int)m_x[12][0], (int)m_y[12][0], (int)m_x[12][1], (int)m_y[12][1], (int)m_x[12][2], (int)m_y[12][2], (int)m_x[12][3], (int)m_y[12][3], g_AA[minus[2]], FALSE);
		DrawModiGraph((int)m_x[13][0], (int)m_y[13][0], (int)m_x[13][1], (int)m_y[13][1], (int)m_x[13][2], (int)m_y[13][2], (int)m_x[13][3], (int)m_y[13][3], g_AA[minus[1]], FALSE);
		DrawModiGraph((int)m_x[14][0], (int)m_y[14][0], (int)m_x[14][1], (int)m_y[14][1], (int)m_x[14][2], (int)m_y[14][2], (int)m_x[14][3], (int)m_y[14][3], g_AA[minus[0]], FALSE);

		//AA正面
		DrawModiGraph((int)m_x[15][0], (int)m_y[15][0], (int)m_x[15][1], (int)m_y[15][1], (int)m_x[15][2], (int)m_y[15][2], (int)m_x[15][3], (int)m_y[15][3], g_AA[nowcenter], FALSE);

		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);//ブレンドモードをリセットする。

		/********************************非鏡面********************************/
		//AA右側
		DrawModiGraph((int)x[21][0], (int)y[21][0], (int)x[21][1], (int)y[21][1], (int)x[21][2], (int)y[21][2], (int)x[21][3], (int)y[21][3], g_AA[plus[5]], FALSE);
		DrawModiGraph((int)x[20][0], (int)y[20][0], (int)x[20][1], (int)y[20][1], (int)x[20][2], (int)y[20][2], (int)x[20][3], (int)y[20][3], g_AA[plus[4]], FALSE);
		DrawModiGraph((int)x[19][0], (int)y[19][0], (int)x[19][1], (int)y[19][1], (int)x[19][2], (int)y[19][2], (int)x[19][3], (int)y[19][3], g_AA[plus[3]], FALSE);
		DrawModiGraph((int)x[18][0], (int)y[18][0], (int)x[18][1], (int)y[18][1], (int)x[18][2], (int)y[18][2], (int)x[18][3], (int)y[18][3], g_AA[plus[2]], FALSE);
		DrawModiGraph((int)x[17][0], (int)y[17][0], (int)x[17][1], (int)y[17][1], (int)x[17][2], (int)y[17][2], (int)x[17][3], (int)y[17][3], g_AA[plus[1]], FALSE);
		DrawModiGraph((int)x[16][0], (int)y[16][0], (int)x[16][1], (int)y[16][1], (int)x[16][2], (int)y[16][2], (int)x[16][3], (int)y[16][3], g_AA[plus[0]], FALSE);

		//AA左側
		DrawModiGraph((int)x[9][0], (int)y[9][0], (int)x[9][1], (int)y[9][1], (int)x[9][2], (int)y[9][2], (int)x[9][3], (int)y[9][3], g_AA[minus[5]], FALSE);
		DrawModiGraph((int)x[10][0], (int)y[10][0], (int)x[10][1], (int)y[10][1], (int)x[10][2], (int)y[10][2], (int)x[10][3], (int)y[10][3], g_AA[minus[4]], FALSE);
		DrawModiGraph((int)x[11][0], (int)y[11][0], (int)x[11][1], (int)y[11][1], (int)x[11][2], (int)y[11][2], (int)x[11][3], (int)y[11][3], g_AA[minus[3]], FALSE);
		DrawModiGraph((int)x[12][0], (int)y[12][0], (int)x[12][1], (int)y[12][1], (int)x[12][2], (int)y[12][2], (int)x[12][3], (int)y[12][3], g_AA[minus[2]], FALSE);
		DrawModiGraph((int)x[13][0], (int)y[13][0], (int)x[13][1], (int)y[13][1], (int)x[13][2], (int)y[13][2], (int)x[13][3], (int)y[13][3], g_AA[minus[1]], FALSE);
		DrawModiGraph((int)x[14][0], (int)y[14][0], (int)x[14][1], (int)y[14][1], (int)x[14][2], (int)y[14][2], (int)x[14][3], (int)y[14][3], g_AA[minus[0]], FALSE);

		//AA正面
		DrawModiGraph((int)x[15][0], (int)y[15][0], (int)x[15][1], (int)y[15][1], (int)x[15][2], (int)y[15][2], (int)x[15][3], (int)y[15][3], g_AA[nowcenter], FALSE);
	}
}

//難易度選択キー関数
//画像移動
int SelectMusic9keys::CatchKey(int nowcenter, int& selectdifsound)
{
	Input& input = Input::GetInstance();
	if (input.keyShift == 0)
	{
		//Lshift押してないとき
		//画像を左に
		if ((input.keyRight > 0 && !isRight&& m_InputFrameCount == 0) || (0 < input.mouseWheel && !isLeft && m_InputFrameCount == 0))
		{
			isLeft = true;
			isLeftContinue = true;
			m_InputFrameCount = 0;
		}

		if (input.keyRight == 0 && (0 == input.mouseWheel))
			isLeftContinue = false;

		//画像を右に
		if ((input.keyLeft > 0 && !isLeft && m_InputFrameCount == 0) || (input.mouseWheel < 0 && !isLeft && m_InputFrameCount == 0))
		{
			isRight = true;
			isRightContinue = true;
			m_InputFrameCount = 0;
		}

		if (input.keyLeft == 0 && (input.mouseWheel == 0))
			isRightContinue = false;
	}
	if (((WIN_WIDTH / 2 - 240 < input.mouseX && input.mouseX < WIN_WIDTH / 2 - 140) && (326 < input.mouseY && input.mouseY < 326 + 100)))
	{
		if (input.mouseClick > 0 && !isRight && m_InputFrameCount == 0)
		{
			isRight = true;
			isRightContinue = true;
			m_InputFrameCount = 0;
		}
	}
	if (((WIN_WIDTH / 2 + 140 < input.mouseX && input.mouseX < WIN_WIDTH / 2 + 240) && (326 < input.mouseY && input.mouseY < 326 + 100)))
	{
		if (Input::GetInstance().mouseClick > 0 && !isRight && m_InputFrameCount == 0)
		{
			isLeft = 1;
			isLeftContinue = 1;
			m_InputFrameCount = 0;
		}
	}

	//難易度
	if (Input::GetInstance().keyDown == 1 && one[0] == 0)
	{
		one[0] = 1;

		if (g_DifNowCenter == 0 || g_DifNowCenter == 1)
			g_DifNowCenter++;
		else
			g_DifNowCenter = 0;

		PlaySoundMem(selectdifsound, DX_PLAYTYPE_BACK);
	}
	if (Input::GetInstance().keyDown != 1)
		one[0] = 0;

	//難易度
	if (Input::GetInstance().keyUp == 1 && one[1] == 0)
	{
		one[1] = 1;

		if (g_DifNowCenter == 1 || g_DifNowCenter == 2)
			g_DifNowCenter--;
		else
			g_DifNowCenter = 2;

		PlaySoundMem(selectdifsound, DX_PLAYTYPE_BACK);
	}

	if (Input::GetInstance().keyUp != 1)
		one[1] = 0;

	//マウス難易度変更
	if (((WIN_WIDTH / 2 - 160 < input.mouseX && input.mouseX < WIN_WIDTH / 2 + 240) && (554 < input.mouseY && input.mouseY < 595)))
	{
		if (Input::GetInstance().mouseClick == 1 && !isRight && m_InputFrameCount == 0)
		{
			g_DifNowCenter = 0;
			PlaySoundMem(selectdifsound, DX_PLAYTYPE_BACK);
		}
	}
	if (((WIN_WIDTH / 2 - 160 < input.mouseX && input.mouseX < WIN_WIDTH / 2 + 240) && (598 < input.mouseY && input.mouseY < 639)))
	{
		if (Input::GetInstance().mouseClick == 1 && !isRight && m_InputFrameCount == 0)
		{
			g_DifNowCenter = 1;
			PlaySoundMem(selectdifsound, DX_PLAYTYPE_BACK);
		}
	}
	if (((WIN_WIDTH / 2 - 160 < input.mouseX && input.mouseX < WIN_WIDTH / 2 + 240) && (644 < input.mouseY && input.mouseY < 686)))
	{
		if (Input::GetInstance().mouseClick == 1 && !isRight && m_InputFrameCount == 0)
		{
			g_DifNowCenter = 2;
			PlaySoundMem(selectdifsound, DX_PLAYTYPE_BACK);
		}
	}

	//決定操作
	if (Input::GetInstance().keyEnter == 1 && !isRight && !isLeft && one[2] == 0)
	{
		if (selectmode == SelectMode::FOLDER)
		{
			if (nowcenter == CENTER_DEFAULT)
			{
				one[2] = 1;
				return (int)KeySelectMusicType::FOLDER_RANDOM;    //folderでrand選択
			}
			else
			{
				one[2] = 1;
				return (int)KeySelectMusicType::FOLDER_TO_AA;    //75のときfolder->AA　移動
			}
		}
		else
		{
			//AAのとき関数を抜け出す
			if (nowcenter == CENTER_DEFAULT) {//randの時
				one[2] = 1;
				return (int)KeySelectMusicType::AA_RANDOM;    //AAでrand選択
			}
			else {//通常選択の時
				one[2] = 1;
				return (int)KeySelectMusicType::DEFAULT_SELECT;    //100の時nowcenterを返す
			}
		}
	}
	//マウス決定操作
	if ((CENTER_LEFT_X < input.mouseX && input.mouseX < CENTER_LEFT_X + AA_SIZE) && (CENTER_LEFT_Y < input.mouseY && input.mouseY < CENTER_LEFT_Y + AA_SIZE))
	{
		if (Input::GetInstance().mouseClick == 1 && !isRight && !isLeft && one[2] == 0)
		{
			//フォルダの際
			if (selectmode == SelectMode::FOLDER)
			{
				//randの時
				if (nowcenter == CENTER_DEFAULT)
				{
					one[2] = 1;
					return (int)KeySelectMusicType::FOLDER_RANDOM;    //folderでrand選択
				}
				else
				{
					one[2] = 1;
					return (int)KeySelectMusicType::FOLDER_TO_AA;    //75のときfolder->AA　移動
				}
			}
			else //AAのとき関数を抜け出す
			{
				//randの時
				if (nowcenter == CENTER_DEFAULT)
				{
					one[2] = 1;
					return (int)KeySelectMusicType::AA_RANDOM;    //AAでrand選択
				}
				else //通常選択の時
				{
					one[2] = 1;
					return (int)KeySelectMusicType::DEFAULT_SELECT;    //100の時nowcenterを返す
				}
			}
		}
	}
	if (Input::GetInstance().keyEnter != 1 && Input::GetInstance().mouseClick == 0)
		one[2] = 0;
	//ESCキーが押されたら
	if (Input::GetInstance().keyEsc == 1 && !isRight && !isLeft && one[3] == 0)
	{
		one[3] = 1;
		if (selectmode == SelectMode::AA)
		{
			//folderに戻る
			return (int)KeySelectMusicType::AA_TO_FOLDER;
		}
		else
		{
			//全体的にbreak
			return (int)KeySelectMusicType::BREAK;
		}
	}

	if (Input::GetInstance().keyEsc == 0)
		one[3] = 0;

	//shift操作
	if (Input::GetInstance().keyShift > 0 && m_InputFrameCount == 0)
	{
		//移動させない
		isLeft = false;
		isRight = false;
		isLeftContinue = false;
		isRightContinue = false;
		isFastLeft = false;
		isFastRight = false;
		countMoveAA = 0;
		//マーカーまわり初期化処理
		markerflag = true;
		//DrawGraph(0,0,markerback,TRUE);
		//LSHIFTを押したとき　の間
		//右押し
		if (Input::GetInstance().keyRight == 1 && one[4] == 0) {
			isRightMarker = 1;
			one[4] = 1;
		}
		if (Input::GetInstance().keyRight == 1)
			one[4] = 0;
		//左押し
		if (Input::GetInstance().keyLeft == 1 && one[5] == 0)
		{
			isLeftMarker = 1;
			one[5] = 1;
		}
		if (Input::GetInstance().keyLeft == 0)
			one[5] = 0;
		//上押し
		if (Input::GetInstance().keyUp == 1 && one[6] == 0)
		{
			marker_goup = 1;
			one[6] = 1;
		}
		if (Input::GetInstance().keyUp == 1)
			one[6] = 0;
		//下押し
		if (Input::GetInstance().keyDown == 1 && one[7] == 0)
		{
			isDownMarker = 1;
			one[7] = 1;
		}
		if (Input::GetInstance().keyDown == 0)
			one[7] = 0;
	}
	if (Input::GetInstance().keyShift == 0)
	{
		one[4] = 0;
		one[5] = 0;
		one[6] = 0;
		one[7] = 0;
		markerflag = 0;
	}

	return (int)KeySelectMusicType::NONE;        //関係ないreturnは50で統一
}
//folder<-->AA表示の変更
int SelectMusic9keys::ChangeMode(int nowcenter, const std::shared_ptr<Music> &musicP)
{
	//folderとAAの相互変換
	if (selectmode == SelectMode::FOLDER)
	{
		//フォルダからAAなら
		//変数
		int i = 0;
		int foldernumber = 0;                        //一般化したフォルダ番号
		int musicsum = 0;                            //総計からフォルダを決定する

		//処理開始
		foldernumber = GetCurrentFolderNumber(nowcenter, musicP);    //開いたフォルダ番号取得
		this->m_SelectFolderNum = foldernumber;

		if (musicP->elebeatSong->m_MusicNumAt[foldernumber] == 0)        //フォルダ内部の曲が0の時
			return nowcenter;                        //何もしないで帰る
		selectmode = SelectMode::AA;                                //AA表示に変更
		this->m_SelectFolderNum = foldernumber;
		g_BeforeCenter = nowcenter;                    //前のfolder番号を保持

		TCHAR foldername[260];
		_tcscpy_s(foldername, 260, musicP->elebeatSong->m_FolderName[foldernumber].c_str());//フォルダ名を保持

		//0からそのフォルダまでの音楽総計
		while (i != foldernumber + 1)
		{
			musicsum += musicP->elebeatSong->m_MusicNumAt[i];            //曲数
			i++;
		}
		//グローバルに渡す
		g_CurrentAlbumArt[0] = musicsum - musicP->elebeatSong->m_MusicNumAt[foldernumber];        //最初の音楽番号(一桁)
		g_CurrentAlbumArt[1] = musicsum - 1;                                    //最後の音楽番号
		g_CurrentAlbumArt[2] = g_CurrentAlbumArt[1] - g_CurrentAlbumArt[0] + 1;                        //音楽総計

		SetAAGraph(CENTER_DEFAULT + musicsum - musicP->elebeatSong->m_MusicNumAt[foldernumber],
			CENTER_DEFAULT + musicsum - 1, nowcenter);                                    //番号よりAA設定

		return CENTER_DEFAULT;
	}
	else
	{
		//AAより戻るとき
		selectmode = SelectMode::FOLDER;
		return this->m_SelectFolderNum;
	}
	return -1;
}

void SelectMusic9keys::SetMode(int nowcenter, const std::shared_ptr<Music> &musicP)
{
	//変数
	int i = 0;
	int musicsum = 0;                            //総計からフォルダを決定する

													//処理開始
	int foldernumber = nowcenter;    //開いたフォルダ番号取得
	this->m_SelectFolderNum = foldernumber;

	g_BeforeCenter = beforeSelectMusicNum;
	TCHAR foldername[260];

	//フォルダ名を保持
	_tcscpy_s(foldername, 260, musicP->elebeatSong->m_FolderName[foldernumber].c_str());

	//0からそのフォルダまでの音楽総計
	while (i != foldernumber + 1)
	{
		musicsum += musicP->elebeatSong->m_MusicNumAt[i];            //曲数
		i++;
	}
	//グローバルに渡す
	g_CurrentAlbumArt[0] = musicsum - musicP->elebeatSong->m_MusicNumAt[foldernumber];        //最初の音楽番号(一桁)
	g_CurrentAlbumArt[1] = musicsum - 1;                                    //最後の音楽番号
	g_CurrentAlbumArt[2] = g_CurrentAlbumArt[1] - g_CurrentAlbumArt[0] + 1;                        //音楽総計

	SetAAGraph(CENTER_DEFAULT + musicsum - musicP->elebeatSong->m_MusicNumAt[foldernumber], CENTER_DEFAULT + musicsum - 1, g_BeforeCenter - g_CurrentAlbumArt[0]);//番号よりAA設定
}

// 最初にロードする関数
void SelectMusic9keys::LoadGraphAll(const std::shared_ptr<Music> &musicP)
{
	SelectMusic9keys::g_AA.clear();
	int centernum = 0;                                        //配列の内部構造を配慮するための変数
	this->m_MusicKind = musicP->elebeatSong->m_FolderNum;
	this->m_MusicNum = musicP->elebeatSong->m_TotalMusicNum;
	std::copy(musicP->elebeatSong->m_MusicNumAt.begin(), musicP->elebeatSong->m_MusicNumAt.end(), back_inserter(this->m_FolderMusicNum));
	///////////////////////////////////AA
	int i = 0;
	int j = CENTER_DEFAULT;

	while (i != musicP->elebeatSong->m_TotalMusicNum)
	{
		//エラ−のとき
		if (LoadGraph(musicP->elebeatSong->m_AlbumArtPath[i].c_str()) == -1)
		{
			if (i == musicP->elebeatSong->m_TotalMusicNum - 1)
			{
				SelectMusic9keys::g_AA.emplace_back(LoadGraph(TEXT("img\\nofolder.png")));
			}
			else
			{
				SelectMusic9keys::g_AA.emplace_back(LoadGraph(TEXT("img\\noalbumart.png")));
			}
		}
		else {//エラーじゃないとき
			SelectMusic9keys::g_AA.emplace_back(LoadGraph(musicP->elebeatSong->m_AlbumArtPath[i].c_str()));
		}
		i++;
		j++;
	}

	std::copy(SelectMusic9keys::g_AA.begin(), SelectMusic9keys::g_AA.end(), back_inserter(SelectMusic9keys::g_TempAA));

	/////////////////////folder

	i = 0;
	j = CENTER_DEFAULT;

	while (i != musicP->elebeatSong->m_FolderNum)
	{
		if (LoadGraph(musicP->elebeatSong->m_FolderAlbumArtPath[i].c_str()) == -1)
		{
			//エラ−のとき
			g_KindFolder.emplace_back(LoadGraph(TEXT("img\\nofolder.png")));
		}
		else
		{
			//エラーじゃないとき
			g_KindFolder.emplace_back(LoadGraph(musicP->elebeatSong->m_FolderAlbumArtPath[i].c_str()));
		}
		i++;
		j++;
	}

	i = CENTER_DEFAULT;
	centernum = j;

	g_IsLoaded = true;
}

//　デバック
void SelectMusic9keys::DrawMusicString(int musicNumber, int FontHandle[], int counter, const std::shared_ptr<Music> &musicP) const
{
	int flag = 0;
	int difference = g_MaxMinbpm[0] - g_MaxMinbpm[1];

	if (selectmode == SelectMode::AA)
	{
		int AAnum = GetNowAANumber(musicNumber);
		int stringx = (WIN_WIDTH / 2) - (GetDrawStringWidthToHandle(musicP->elebeatSong->m_Name[AAnum].c_str(), static_cast<int>(_tcslen(musicP->elebeatSong->m_Name[AAnum].c_str())) / 2, FontHandle[1]));
		LayerStringDraw(stringx, 430, musicP->elebeatSong->m_Name[AAnum].c_str(), FontHandle[1]);
		stringx = (WIN_WIDTH / 2) - (GetDrawStringWidthToHandle(musicP->elebeatSong->m_Artist[AAnum].c_str(), static_cast<int>(_tcslen(musicP->elebeatSong->m_Artist[AAnum].c_str())) / 2, FontHandle[1]));
		LayerStringDraw(stringx, 480, musicP->elebeatSong->m_Artist[AAnum].c_str(), FontHandle[1]);

		stringx = (WIN_WIDTH / 2) - (GetDrawStringWidthToHandle(musicP->elebeatSong->m_SubName[AAnum].c_str(), static_cast<int>(_tcslen(musicP->elebeatSong->m_SubName[AAnum].c_str())) / 2, FontHandle[2]));
		LayerStringDraw(stringx, 460, musicP->elebeatSong->m_SubName[AAnum].c_str(), FontHandle[2]);

		difference = g_MaxMinbpm[1] - g_MaxMinbpm[0];

		if (0 <= counter % 280 && counter % 280 < 140)
			flag = 1;
		else if (140 <= counter % 280 && counter % 280 < 280)
			flag = 0;

		if (flag == 1 && g_MaxMinbpm[0] + (difference / 60.0*(counter % 280)) < g_MaxMinbpm[1])
		{
			int bpm = static_cast<int>(g_MaxMinbpm[0] + (difference / 60.0*(counter % 280)));
			DrawSelectMusicBPM(bpm);
		}
		else if (flag == 0 && g_MaxMinbpm[1] - (difference / 60.0*((counter % 280) - 140)) > g_MaxMinbpm[0])
		{
			int bpm = static_cast<int>(g_MaxMinbpm[1] - (difference / 60.0*((counter % 280) - 140)));
			DrawSelectMusicBPM(bpm);
		}
		else
		{
			DrawSelectMusicBPM(g_MaxMinbpm[flag]);
		}
	}
	else
	{
		//folder
		int num = GetCurrentFolderNumber(musicNumber, musicP);
		//自分を好きになれた頃の番号に戻す作業
		int stringx = (WIN_WIDTH / 2) - (GetDrawStringWidthToHandle(musicP->elebeatSong->m_FolderName[num].c_str(), static_cast<int>(_tcslen(musicP->elebeatSong->m_FolderName[num].c_str())) / 2, FontHandle[0]));

		LayerStringDraw(stringx, 450, musicP->elebeatSong->m_FolderName[num].c_str(), FontHandle[0]);
	}
}

// folder番号酒盗
int SelectMusic9keys::GetCurrentFolderNumber(int nowcenter, const std::shared_ptr<Music> &musicP) const
{
	int num = nowcenter - CENTER_DEFAULT;    //自分を好きになれた頃の番号に戻す作業

	while (num < 0)
	{
		num += musicP->elebeatSong->m_FolderNum;
	}

	while (num >= musicP->elebeatSong->m_FolderNum)
	{
		num -= musicP->elebeatSong->m_FolderNum;
	}

	return num;
}

//AAの詳細番号を出す
int SelectMusic9keys::GetNowAANumber(int nowcenter) const
{
	int num = nowcenter - CENTER_DEFAULT;

	while (num < 0)
	{
		num += g_CurrentAlbumArt[2];
	}
	while (num >= g_CurrentAlbumArt[2])
	{
		num -= g_CurrentAlbumArt[2];
	}

	num += g_CurrentAlbumArt[0];

	return num;
}

//folder開いたときAA設定
void SelectMusic9keys::SetAAGraph(int firstAA, int endAA, int nowcenter)
{
	int num_song = endAA - firstAA;
	SelectMusic9keys::g_AA.clear();
	g_MaxMinbpm.fill(0);

	for (int i = 0; i < m_FolderMusicNum[m_SelectFolderNum]; ++i)
	{
		SelectMusic9keys::g_AA.emplace_back(SelectMusic9keys::g_TempAA[firstAA + i]);
	}

	SelectMusic9keys::g_AA[CENTER_DEFAULT] = g_KindFolder[m_SelectFolderNum];
}

//デストラクタ
void SelectMusic9keys::Delselectmusic(void)
{
	//AAとfolder
	int i = CENTER_DEFAULT;

	//画像
	DeleteGraph(g_BackImage);            //メイン背景
	DeleteGraph(difficultyImage);        //難易度文字
	DeleteGraph(backFrontImage);    //難易度文字の下
	DeleteGraph(bpmFrameImage);        //bpmの枠
	DeleteGraph(markerBackImage);    //マーカーの土台背景　いらん

	//音声
	DeleteSoundMem(roop_music);
}

// BPM切り取り
void SelectMusic9keys::CutBPM(int num, int nowcenter, const std::shared_ptr<Music> &musicP)
{
	if (nowcenter != CENTER_DEFAULT)
	{
		std::vector<int> getbpm;

		for (auto bpmText : musicP->elebeatSong->m_Bpm[num])
		{
			getbpm.emplace_back(_ttoi(bpmText.c_str()));
		}

		if (getbpm.size() == 0) return;

		//最大BPMと最少BPMを得る
		g_MaxMinbpm.fill(getbpm.at(0));

		for (auto bpm : getbpm)
		{
			if (g_MaxMinbpm[0] > bpm)
				g_MaxMinbpm[0] = bpm;
			if (g_MaxMinbpm[1] < bpm)
				g_MaxMinbpm[1] = bpm;
		}
	}
	else
	{
		g_MaxMinbpm.fill(0);
	}
}

//selectmusic関数から出るときのフラグを受け取りその分岐で抜け方を決定
void SelectMusic9keys::EscapeSelect(EscapeState escapeState, int nowcenter)
{
	m_EscapeState = escapeState;
	StopSoundMem(roop_music);
	Delselectmusic();

	auto GoToGameScene = []()
	{
		SceneManager::SetNextScene(Scenes::GAME);
		GameBase::g_MusicDif = g_DifNowCenter;
	};

	switch (escapeState)
	{
	case EscapeState::TITLE:
		//タイトルへ
		SceneManager::SetNextScene(Scenes::TITLE);
		PlaySoundMem(backSound, DX_PLAYTYPE_BACK);
		break;

	case EscapeState::GAME:
		//ゲームへ
		musicNumber = this->GetNowAANumber(nowcenter);
		GoToGameScene();
		break;

	case EscapeState::RANDOM_ALL:
		//完全ランダムをセレクトするとき musicNumberの受け渡しに注意
		musicNumber = nowcenter;
		GoToGameScene();
		break;

	case EscapeState::RANDOM_FOLDER:
		//folder内部でランダムAAの時　範囲制限
		musicNumber = nowcenter;
		GoToGameScene();
		break;
	}
}

//範囲内のランダムな値を取得
int SelectMusic9keys::GetRandom(int min, int max, const std::shared_ptr<Music> &musicP)
{
	int num = 0;
	std::random_device rnd;
	std::mt19937 mt(rnd());
	std::uniform_int_distribution<int> dist(min, max);
	bool canPlay = false;

	int count = 1;

	while (!canPlay)
	{
		if (count % 100 == 0)
			g_DifNowCenter = ((g_DifNowCenter + 1) + 3) % 3;

		++count;
		canPlay = true;
		num = dist(mt);

		if (musicP->elebeatSong->m_ElebeatDifficulty[0][g_DifNowCenter][num] == 0)
			canPlay = false;
	}
	return num;
}

void SelectMusic9keys::DifNumberCalculate(std::array<std::array<int, 3>,3> number, int nowcenter, const std::shared_ptr<Music> &musicP)
{
	int num = this->GetNowAANumber(nowcenter);

	for (int i = 0; i < 3; i++)
	{
		if (musicP->elebeatSong->m_ElebeatDifficulty[0][i][num] >= 10)
		{
			//二桁
			number[i][0] = musicP->elebeatSong->m_ElebeatDifficulty[0][i][num] / 10;
			number[i][1] = musicP->elebeatSong->m_ElebeatDifficulty[0][i][num] - (number[i][0] * 10);
		}
		else
		{
			//一桁
			number[i][0] = 0;
			number[i][1] = musicP->elebeatSong->m_ElebeatDifficulty[0][i][num];
		}
	}
}

void SelectMusic9keys::DrawLR(int *right_pic, int *left_pic)
{
	DrawGraph(WIN_WIDTH / 2 - 240, 326, *left_pic, TRUE);
	DrawGraph(WIN_WIDTH / 2 + 140, 326, *right_pic, TRUE);
}

void SelectMusic9keys::DrawLR_click(int *right_click_pic, int *left_click_pic)
{
	DrawGraph(WIN_WIDTH / 2 - 240, 326, *left_click_pic, TRUE);
	DrawGraph(WIN_WIDTH / 2 + 140, 326, *right_click_pic, TRUE);
}

void SelectMusic9keys::DrawHiScore(int nowcenter, int count, int fullComboImages[], int rankImages[], int FontHandle[], bool *isLoadScore, const std::shared_ptr<Music> &musicP)
{
	int black = GetColor(0, 0, 0);
	int white = GetColor(255, 255, 255);

	if (*isLoadScore)
	{
		LoadHiScore(nowcenter, musicP);
		*isLoadScore = false;
	}

	//*******描画********//

	auto playModeNumber = static_cast<int>(SceneManager::g_PlayMode);

	for (auto i = 0; i < 3; ++i)
	{
		if (m_SaveData.m_IsFullCombo[playModeNumber][i] == 1)
			DrawGraph(730, 550 + (45 * i), fullComboImages[count / 3 % 5], TRUE);
	}

	for (auto i = 0; i < 3; ++i)
	{
		if (m_SaveData.m_Rank[playModeNumber][i] != 10)
			DrawGraph(730, 550 + (45 * i), rankImages[m_SaveData.m_Rank[playModeNumber][i]], TRUE);
	}

	for (auto i = 0; i < 3; ++i)
	{
		if (m_SaveData.m_HighScore[playModeNumber][i] != 0)
			DxHelper::DrawOutlineStringToHandle(642, 565 + (45 * i), white, black, FontHandle[3], _T("%d"), m_SaveData.m_HighScore[playModeNumber][i]);
	}
}

//ハイスコア読み込み処理
void SelectMusic9keys::LoadHiScore(int nowcenter, const std::shared_ptr<Music> &musicP)
{
	FILE *fp1;
	errno_t error;

	std::fill(m_SaveData.m_IsFullCombo[0], m_SaveData.m_IsFullCombo[3], false);
	std::fill(m_SaveData.m_Rank[0], m_SaveData.m_Rank[3], 10);
	std::fill(m_SaveData.m_HighScore[0], m_SaveData.m_HighScore[3], static_cast<long>(0));

	//パス作成
	tstring str = musicP->elebeatSong->m_Name[GetNowAANumber(nowcenter)];
	ScoreData::GetInstance().TranslateToSafeFileName(str);
	tstring pass = _T("Data\\") + str + _T(".dat");

	/*ファイルの内容からデータをロード*/
	if ((error = _tfopen_s(&fp1, pass.c_str(), _T("rb"))) != 0)
	{
		;
	}
	//ファイルがあればデータを読み込む
	else
	{
		fread(&m_SaveData, sizeof(m_SaveData), 1, fp1);
		fclose(fp1);//解放
	}
}

//アウトライン文字の描画
void SelectMusic9keys::LayerStringDraw(int stringX, int stirngY, const TCHAR drawstring[], int FontHandle) const
{
	int black = Color::GetInstance().Black();
	int white = Color::GetInstance().White();
	//ななめ
	DrawStringToHandle(stringX + 1, stirngY + 1, drawstring, black, FontHandle);
	DrawStringToHandle(stringX + 1, stirngY - 1, drawstring, black, FontHandle);
	DrawStringToHandle(stringX - 1, stirngY + 1, drawstring, black, FontHandle);
	DrawStringToHandle(stringX - 1, stirngY - 1, drawstring, black, FontHandle);
	//たてよこ
	DrawStringToHandle(stringX + 1, stirngY, drawstring, black, FontHandle);
	DrawStringToHandle(stringX - 1, stirngY, drawstring, black, FontHandle);
	DrawStringToHandle(stringX, stirngY + 1, drawstring, black, FontHandle);
	DrawStringToHandle(stringX, stirngY - 1, drawstring, black, FontHandle);
	//真ん中
	DrawStringToHandle(stringX, stirngY, drawstring, white, FontHandle);
}

//デストラクタ
void SelectMusic9keys::DelSelectMusic_9(int& loopsound, int& back_sound)
{
	DeleteSoundMem(loopsound);
	DeleteSoundMem(back_sound);
}