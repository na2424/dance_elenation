#pragma once
#include "SceneManager.h"



class ExtraStage : public Scene
{
private:
public:
	void Initialize(const std::shared_ptr<Music>&) override;
	void Update(const std::shared_ptr<Music>& musicP) override;
	void Finalize(const std::shared_ptr<Music>&) override;
};