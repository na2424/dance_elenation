#include <unordered_map>
#include <boost/lexical_cast.hpp>
#include <boost/signals2.hpp>

#include "Game_DANCEELENATION.h"
#include "Color.h"
#include "Combo.h"
#include "DxHelper.h"
#include "extern.h"
#include "KeyInput.h"
#include "GamePlayOption.h"
#include "header.h"
#include "LuaHelper.h"
#include "Screen.h"
#include "Timing.h"
#include "Input.h"
#include "LongNoteImages.h"
#include "savedata_class.h"
#include "NoteDanceElenation.h"
#include "JudgeCount.h"
#include "SpecialFiles.h"

using namespace std;
using namespace DxLib;

namespace Elebeat
{

}

//-----------------
// 定数.
//-----------------

constexpr int ROW_NUM = 4;
constexpr int FIRST_DRAW_NOTE = 55;
constexpr int LAST_DRAW_NOTE = 16;
constexpr int CRAP_OFFSET = 55;
constexpr int COLOR_DIFF[5] = { 0x00ffff, 0xffff00, 0xff0000, 0x00ff00, 0xff00ff };
constexpr int COMBO_DRAWING_START_COUNT = 4;

GameDanceElenation::GameDanceElenation() :
	m_BpmIndex(0),
	m_StopIndex(0),
	m_RatioImageSize(0),
	m_SoftSoundHandle(0),
	m_StartTime(0),
	m_CurrentTime(0),
	isDead(false),
	isShowJudge(false),
	readyState(-1),
	m_EndTime(0),
	m_EndNow(0),
	m_ScoreAnimationFrame(0),
	m_ShutterCount(0),
	m_judgeDrawFlag(0),
	m_judgeDrawTime(0),
	m_comboImage(0),
	m_startImage(0),
	m_readyImage(0),
	m_goImage(0),
	m_lifeMaxImage(0),
	m_lifeRedImage(0),
	m_lifeGreenImage(0),
	m_failedImage(0),
	m_backgroundImage(0),
	m_cursorImage(0),
	m_bpmImage(0),
	m_elebeatImage(0),
	m_explosionImage(0),
	m_readyGoSound(0),
	m_failedSound(0),
	m_handClapSound(0),
	m_backSound(0), m_MusicSound(0)
{
	m_lifeGageImage.fill(0);
	m_shaterImage.fill(0);
	m_filterImage.fill(0);
	m_longCount.fill(0);
	m_note4Key = make_shared<NoteDanceElenation>();
	m_keyinput4Key = make_shared<KeyInput4Key>();
	m_longnoteImage = make_shared<LongNotePic>();
	m_score = make_shared<ScoreSt>();
	m_combo = make_shared<Combo>();
	m_life = make_shared<LifeSt>();
	m_timing = make_shared<Timing>();
}

void GameDanceElenation::ReadLua()
{
	LuaFuncParam params;
	LuaFuncParam results;

	//エラー時に読み直すか確認
	while (true)
	{
		if (!m_luaHelper.DoFile("Themes/DANCE ELENATION/lua/_file_game4keys.lua", &results, 1, &params))
		{
			//エラー時
			if (OptionSaveData::g_IsFullScreen) exit(0);
			tstring mes = m_luaHelper.GetErr();
			if (MessageBoxA(nullptr, (mes + _T("\n_file_game.lua が読み込めませんでした。\nLuaファイルを読み直しますか？")).c_str(), _T("読み込みエラー"), MB_RETRYCANCEL) != IDRETRY) exit(0);
		}
		else
		{
			break;
		}
	}

	vector<tstring>luaStackArrayImageStr = {
		_T("judgeImage"),
		_T("difImage")
	};
	vector<tstring>luaStackImageStr = {
		_T("lifeGageImage1"),
		_T("lifeGageImage2"),
		_T("shaterImage1"),
		_T("shaterImage2"),
		_T("filterImage1"),
		_T("filterImage2"),
		_T("cursorImage"),
		_T("startImage"),
		_T("readyImage"),
		_T("goImage"),
		_T("comboImage"),
		_T("failedImage"),
		_T("back"),
		_T("bpmImage"),
		_T("elebeatImage"),
		_T("bpmImage")
	};
	vector<tstring>luaStackSoundStr = {
		_T("readyGoSound"),
		_T("handClapSound"),
		_T("failedSound"),
		_T("backSound")
	};
	unordered_map<tstring, tstring> arrayImageName;
	unordered_map<tstring, tstring> imageName;
	unordered_map<tstring, tstring> soundName;
	//ファイル名をスタックに積む
	for each(tstring str in luaStackArrayImageStr)
	{
		lua_getglobal(L, str.c_str());
		if (lua_type(L, -1) == LUA_TSTRING)
			arrayImageName[str] = lua_tostring(L, -1);
	}
	for each(tstring str in luaStackImageStr)
	{
		lua_getglobal(L, str.c_str());
		if (lua_type(L, -1) == LUA_TSTRING)
			imageName[str] = lua_tostring(L, -1);
	}
	for each(tstring str in luaStackSoundStr)
	{
		lua_getglobal(L, str.c_str());
		if (lua_type(L, -1) == LUA_TSTRING)
			soundName[str] = lua_tostring(L, -1);
	}

	const int num = lua_gettop(L);
	if (num == 0)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("エラー"), _T("No stack"), MB_OK);
	}

	const tstring grGameStr = SpecialFiles::DEFAULT_THEMES_DIR + tstring("Graphics/Game/%s");
	const tstring seGameStr = SpecialFiles::DEFAULT_THEMES_DIR + tstring("Sounds/Game/%s");

	DxHelper::SetObject(g_JudgeImages.data(), DIV_IMAGE_TYPE, grGameStr, arrayImageName["judgeImage"].c_str(), 6, 1, 6, 360, 90);
	DxHelper::SetObject(m_DifImage.data(), DIV_IMAGE_TYPE, grGameStr, arrayImageName["difImage"].c_str(), 5, 1, 5, 100, 20);

	DxHelper::SetObject(m_lifeGageImage[0], IMAGE_TYPE, grGameStr, imageName["lifeGageImage1"].c_str());
	DxHelper::SetObject(m_lifeGageImage[1], IMAGE_TYPE, grGameStr, imageName["lifeGageImage2"].c_str());
	DxHelper::SetObject(m_shaterImage[0], IMAGE_TYPE, grGameStr, imageName["shaterImage1"].c_str());
	DxHelper::SetObject(m_shaterImage[1], IMAGE_TYPE, grGameStr, imageName["shaterImage2"].c_str());
	DxHelper::SetObject(m_filterImage[0], IMAGE_TYPE, grGameStr, imageName["filterImage1"].c_str());
	DxHelper::SetObject(m_filterImage[1], IMAGE_TYPE, grGameStr, imageName["filterImage2"].c_str());
	DxHelper::SetObject(m_cursorImage, IMAGE_TYPE, grGameStr, imageName["cursorImage"].c_str());
	DxHelper::SetObject(m_startImage, IMAGE_TYPE, grGameStr, imageName["startImage"].c_str());
	DxHelper::SetObject(m_readyImage, IMAGE_TYPE, grGameStr, imageName["readyImage"].c_str());
	DxHelper::SetObject(m_goImage, IMAGE_TYPE, grGameStr, imageName["goImage"].c_str());
	DxHelper::SetObject(m_comboImage, IMAGE_TYPE, grGameStr, imageName["comboImage"].c_str());
	DxHelper::SetObject(m_failedImage, IMAGE_TYPE, grGameStr, imageName["failedImage"].c_str());
	DxHelper::SetObject(g_BackImage, IMAGE_TYPE, grGameStr, imageName["back"].c_str());
	DxHelper::SetObject(m_backUnderImage, IMAGE_TYPE, grGameStr, imageName["backUnderImage"].c_str());
	DxHelper::SetObject(m_bpmImage, IMAGE_TYPE, grGameStr, imageName["bpmImage"].c_str());
	DxHelper::SetObject(m_elebeatImage, IMAGE_TYPE, grGameStr, imageName["elebeatImage"].c_str());

	DxHelper::SetObject(m_readyGoSound, SOUND_TYPE, seGameStr, soundName["readyGoSound"].c_str());
	DxHelper::SetObject(m_handClapSound, SOUND_TYPE, seGameStr, soundName["handClapSound"].c_str());
	DxHelper::SetObject(m_failedSound, SOUND_TYPE, seGameStr, soundName["failedSound"].c_str());
	DxHelper::SetObject(m_backSound, SOUND_TYPE, SpecialFiles::DEFAULT_THEMES_DIR + tstring("Sounds/%s"), soundName["backSound"].c_str());


	DxHelper::SetObject(m_lifeMaxImage, IMAGE_TYPE, grGameStr, "rbrb.png");
	DxHelper::SetObject(m_lifeRedImage, IMAGE_TYPE, grGameStr, "red.png");
	DxHelper::SetObject(m_lifeGreenImage, IMAGE_TYPE, grGameStr, "green.png");
}

void GameDanceElenation::DrawCombo(const std::shared_ptr<Combo>& f_combo)
{
	constexpr int POSITION_X = 240;
	int x = 0;
	auto combo = f_combo->m_ComboNum;

	// numが十進数で何桁になるか調べる
	auto beamWidth = 0;

	for (auto i = 1; combo >= i; i *= 10)
	{
		beamWidth++;
	}

	// 画面右上に右詰で表示
	// xは描く数字(一桁一桁各々)の左端のX座標
	if (combo < 100)
		x = POSITION_X;
	else if (combo < 1000)
		x = POSITION_X;
	else
		x = POSITION_X;

	for (auto i = 0; i < beamWidth; i++)
	{
		if (f_combo->m_ComboColorState == 1)
			DrawGraph(x, static_cast<int>(200 + GamePlayOption::GetInstance().isReverse * 55 + 16 * sin(static_cast<double>(f_combo->m_AnimationFrame / 50.0*PI))), GameBase::g_ComboParfect4KeysImages[combo % 10], TRUE);
		if (f_combo->m_ComboColorState == 2)
			DrawGraph(x, static_cast<int>(200 + GamePlayOption::GetInstance().isReverse * 55 + 16 * sin(static_cast<double>(f_combo->m_AnimationFrame / 50.0*PI))), GameBase::g_ComboGrate4KeysImages[combo % 10], TRUE);
		if (f_combo->m_ComboColorState == 3)
			DrawGraph(x, static_cast<int>(200 + GamePlayOption::GetInstance().isReverse * 55 + 16 * sin(static_cast<double>(f_combo->m_AnimationFrame / 50.0*PI))), GameBase::g_ComboGood4KeysImages[combo % 10], TRUE);
		combo /= 10;
		x -= 55;
	}
}

void GameDanceElenation::LoadNote(const std::shared_ptr<Music> &musicP) const
{
	tstring difficultName;

	if (g_MusicDif == 0)
		difficultName = _T("Beginner:");
	if (g_MusicDif == 1)
		difficultName = _T("Easy:");
	if (g_MusicDif == 2)
		difficultName = _T("Medium:");
	if (g_MusicDif == 3)
		difficultName = _T("Hard:");
	if (g_MusicDif == 4)
		difficultName = _T("Challenge:");

	//データの読み込み(16keys) #NOTEのあと選択した難易度の譜面を読み込む

	TCHAR stringText[300] = {};
	FILE    *fp;
	errno_t error;
	auto measure = 0;

	//ファイル開くハンドル
	if ((error = _tfopen_s(&fp, musicP->danceSong->m_SequencePath[musicNumber4].c_str(), _T("r"))) != 0)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("エラー"), _T("譜面ファイルを開けませんでした。"), MB_OK);
		return;
	}

	try
	{
		std::vector< std::vector <int> > tempNote(ROW_NUM);
		int note_j[4] = {};
		bool isPass[3] = {};
		auto stopIndex = 0;
		auto line = 0;
		auto stopTimes = 0.0;

		while (_fgetts(stringText, sizeof(stringText), fp) != nullptr)
		{
			auto lineStr = tstring(stringText);
			boost::algorithm::trim(lineStr);
			if (_tcsstr(lineStr.c_str(), _T("#NOTES:")) != nullptr)
			{
				isPass[0] = true;
				isPass[1] = false;
			}

			if (_tcsstr(lineStr.c_str(), _T("dance-single:")) != nullptr)
			{
				isPass[1] = true;
			}

			if (isPass[0] && isPass[1])
			{
				if (lineStr == difficultName || isPass[2])
				{
					isPass[2] = true;
					if (lineStr.size() == ROW_NUM)
					{
						for (auto in = 0; in < ROW_NUM; in++)
						{
							if (lineStr.at(in) == _T('0'))
								tempNote[in].emplace_back(0);
							//通常ノート
							else if (lineStr.at(in) == _T('1'))
								tempNote[in].emplace_back(1);
							//ロングノート始点
							else if (lineStr.at(in) == _T('2'))
								tempNote[in].emplace_back(2);
							//ロングノート終点
							else if (lineStr.at(in) == _T('3'))
								tempNote[in].emplace_back(3);
							//
							else if (lineStr.at(in) == _T('4'))
								tempNote[in].emplace_back(2);
							else if (lineStr.at(in) == _T('M'))
								tempNote[in].emplace_back(0);
						}
						++line;
					}
					else if (0 == _tcsncmp(lineStr.c_str(), _T(","), 1) || 0 == _tcsncmp(lineStr.c_str(), _T(";"), 1))
					{
						for (auto jn = 0; jn < line; ++jn)
						{
							for (auto line_i = 0; line_i < ROW_NUM; ++line_i)
							{
								if (1 <= tempNote[line_i][jn] && tempNote[line_i][jn] <= 3)
								{
									m_note4Key->m_Note[line_i].emplace_back((4.0*measure) + (4.0 * jn / static_cast<double>(line)));
									double addTime = 0;

									for (unsigned u = 0; u < m_timing->m_BpmPosition.size() - 1; ++u)
									{
										if (m_timing->m_BpmPosition[u + 1] > m_note4Key->m_Note[line_i].at(note_j[line_i]) || m_timing->m_BpmPosition[u + 1] == -1)
										{
											//(<を<=にすると停止場所とノートの場所が同じ時、停止分押すべきタイミングが遅れてしまう)
											if (m_timing->m_StopPosition[stopIndex] + EP < m_note4Key->m_Note[line_i].at(note_j[line_i]) && m_timing->m_StopPosition[stopIndex] != -1)
											{
												stopTimes += m_timing->m_Stop[stopIndex] * 1000.0f;
												stopIndex++;
											}
											//各ノートの押されるべき時間はここで決まる。
											m_note4Key->m_NoteTime[line_i].emplace_back(static_cast<int>(addTime + 60000.0 / m_timing->m_Bpm[u] * (m_note4Key->m_Note[line_i].at(note_j[line_i]) - m_timing->m_BpmPosition[u]) + stopTimes + m_timing->m_Offset));

											addTime = 0;
											m_note4Key->m_Maxnote++;
											break;
										}
										addTime += m_timing->m_bn[u];
									}
									m_note4Key->m_NoteFlag[line_i].emplace_back(-tempNote[line_i][jn]);
									m_note4Key->m_NoteFlagTime[line_i].emplace_back(-1);
									note_j[line_i]++;
								}
							}
						}

						for (auto in = 0; in < ROW_NUM; ++in)
						{
							tempNote[in].clear();
						}
						line = 0;
						measure++;

						if (0 == _tcsncmp(lineStr.c_str(), _T(";"), 1)) break;
					}
				}
			}
		}

		constexpr auto DELAY_TIME_MS = 2000;

		//最後のノート+2000ミリ秒の時間を求める
		for (auto i = 0; i < ROW_NUM; ++i)
		{
			if (m_timing->m_MusicEndTime < m_note4Key->m_NoteTime[i].back() + DELAY_TIME_MS)
				m_timing->m_MusicEndTime = m_note4Key->m_NoteTime[i].back() + DELAY_TIME_MS;
			m_note4Key->m_NoteFlag[i].emplace_back(-10);
			m_note4Key->m_NoteFlagTime[i].emplace_back(-10);
		}
		/////////////////////////////////////////////////////
	}
	catch (...)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("エラー"), _T("譜面ファイルを正しく読み込めませんでした。"), MB_OK);
		exit(0);
	}
}

//4keys選択時のゲーム関数
void GameDanceElenation::Initialize(const std::shared_ptr<Music> &musicP)
{
	//判定は初期値に戻す
	JudgeCount::GetInstance().ResetCount();

	//Lua初期化
	InitLua();

	//Luaファイル読み込み
	ReadLua();

	// 文字の大きさ
	SetFontSize(16);

	//ステージ表示
	Screen::TransitionToGame();
	Screen::FadeOut();

	Color &color = Color::GetInstance();

	musicAAImage = LoadGraph(musicP->danceSong->m_BannerPath[musicNumber4].c_str());
	m_RatioImageSize = DxHelper::GetRatioImage(musicAAImage);

	//ゲーム画面フェードイン
	FadeInGame(m_RatioImageSize);

	readyState = -1;
	int st = 0;                        //最初の譜面の時間を入れる変数
	int sc = 0;

	//Reverseオプションの時にスピード反転
	if (GamePlayOption::GetInstance().isReverse && GamePlayOption::GetInstance().speed > 0)
		GamePlayOption::GetInstance().speed = -GamePlayOption::GetInstance().speed;

	///////////////初期化が必要な変数に値を代入する///////////////

	//ロングノートの関数をシグナルに纏める
	/*sig.connect(boost::bind(&NoteClass4keys::LongNoteBottomDraw, note4key, _1,_2,_3));
	sig.connect(boost::bind(&NoteClass4keys::LongNoteBodyDraw, note4key, _1, _2, _3));
	sig.connect(boost::bind(&NoteClass4keys::LongNoteTopDraw, note4key, _1, _2, _3));
	sig.connect(boost::bind(&NoteClass4keys::LongNoteBottomActiveDraw, note4key, _1, _2, _3));
	sig.connect(boost::bind(&NoteClass4keys::LongNoteTopActiveDraw, note4key, _1, _2, _3));
	sig.connect(boost::bind(&NoteClass4keys::NoteBehindDraw, note4key, _1, _2, _3));*/

	m_score->m_ScoreNum = 0;
	m_score->m_ScoreSa = 0;

	m_life->m_Life = 250;
	m_life->m_Lifeflag = 250;

	m_note4Key->SetSpeed(boost::lexical_cast<double>(musicP->danceSong->m_Bpm[musicNumber4][0].c_str()) / 60.0);
	m_note4Key->SetPixelPerBeat();
	m_note4Key->SetMovedPixel(-1666);

	//フォント
	m_FontHandle[0] = CreateFontToHandle(nullptr, 40, 3, DX_FONTTYPE_EDGE);
	m_FontHandle[1] = CreateFontToHandle(nullptr, 30, 4, DX_FONTTYPE_EDGE);

	m_scrollSpeed = GamePlayOption::GetInstance().speed;
	if (GamePlayOption::GetInstance().isReverse)
		m_note4Key->m_JudgeLinePositionY = 555;

	DxLib::DrawString(250, 240, _T("初期化完了"), color.White());
	DxLib::ScreenFlip();

	////////////////////////////////////////////////////////////
	DxLib::DrawString(250, 240 + 32, _T("画像と音源を読み込み中...."), color.White());
	DxLib::ScreenFlip();

	//Note関連画像のロード
	LoadDivGraph(_T("NoteSkins/dance/_Fallback Receptor go 2x1.png"), 2, 2, 1, 64, 64, g_JudgeAreaImages.data());
	LoadDivGraph(_T("Themes/DANCE ELENATION/Graphics/Game/long_judge.png"), 2, 1, 2, 100, 100, m_longnoteImage->longJudge->data());
	m_explosionImage = LoadGraph(_T("NoteSkins/dance/Down Tap Explosion Bright.png"));

	m_note4Key->LoadNoteSkin(GamePlayOption::GetInstance().noteSkin);

	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/selectmusic_level01_num.png"), 10, 10, 1, 25, 50, m_difnumberColorImage[3].data());        //番号画像 緑
	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/selectmusic_level02_num.png"), 10, 10, 1, 25, 50, m_difnumberColorImage[1].data());        //番号画像 黄色
	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/selectmusic_level03_num.png"), 10, 10, 1, 25, 50, m_difnumberColorImage[2].data());        //番号画像 赤
	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/selectmusic_level04_num.png"), 10, 10, 1, 25, 50, m_difnumberColorImage[0].data());        //番号画像 黄色
	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/selectmusic_level05_num.png"), 10, 10, 1, 25, 50, m_difnumberColorImage[4].data());        //番号画像 赤

	DxLib::DrawString(250, 240 + 64, _T("画像と音源の読み込み完了"), color.White());
	DxLib::DrawString(250, 240 + 96, _T("選曲した曲情報を読み込み中....."), color.White());

	DxLib::ScreenFlip();

	//前の音楽番号を保持しておく
	beforeSelectMusicNum4 = musicNumber4;

	//選曲した曲の音源を読み込む
	m_SoftSoundHandle = LoadSoftSound(musicP->danceSong->m_MusicPath[musicNumber4].c_str());

	// ソフトサウンドハンドルからサウンドハンドルを作成
	this->m_MusicSound = LoadSoundMemFromSoftSound(m_SoftSoundHandle);

	if (m_MusicSound == -1)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("音楽ファイルを読み込めませんでした。\n音楽ファイルと.smファイル内の#MUSICを確認してください。"), _T("loading error"), MB_OK);
		SceneManager::SetNextScene(Scenes::MUSIC_SELECT);
		return;
	}

	//選曲した曲の背景を読み込む
	m_backgroundImage = LoadGraph(musicP->danceSong->m_BackGroundPath[musicNumber4].c_str());

	//曲名等を読み込む
	m_musicName = musicP->danceSong->m_Name[musicNumber4].c_str();
	m_musicSubName = musicP->danceSong->m_SubName[musicNumber4].c_str();
	m_musicArtist = musicP->danceSong->m_Artist[musicNumber4].c_str();

	m_stringWidth[0] = GetDrawStringWidthToHandle(m_musicName.c_str(), static_cast<int>(_tcslen(m_musicName.c_str())) / 2, m_FontHandle[1]);
	m_stringWidth[1] = GetDrawStringWidthToHandle(m_musicSubName.c_str(), static_cast<int>(_tcslen(m_musicSubName.c_str())) / 2, m_FontHandle[1]);
	m_stringWidth[2] = GetDrawStringWidthToHandle(m_musicArtist.c_str(), static_cast<int>(_tcslen(m_musicArtist.c_str())) / 2, m_FontHandle[1]);

	for (int i = 0; i < 3; i++)
	{
		m_musicStringX[i] = 600 - m_stringWidth[i];
	}

	m_timing->SetTiming(musicP, musicNumber4);

	/**************************/
	LoadNote(musicP);

	//ハンドクラップフラグを作成
	for (int i = 0; i < ROW_NUM; i++)
	{
		std::vector<bool> temp;
		m_hasCrap.emplace_back(temp);

		for (size_t j = 0; j < m_note4Key->m_NoteTime[i].size(); ++j)
		{
			m_hasCrap[i].push_back(false);
		}
	}

	DxLib::DrawString(250, 240 + 128, _T("選曲した曲情報の読み込み完了"), color.White());
	//反映
	DxLib::ScreenFlip();
	//裏画面に設定
	DxLib::SetDrawScreen(DX_SCREEN_BACK);
	//スタート時間を取得
	m_StartTime = GetNowCount();
}

void GameDanceElenation::Update(const std::shared_ptr<Music>& musicP)
{
	//シングルトン参照
	auto &gameOption = GamePlayOption::GetInstance();
	auto &input = Input::GetInstance();
	auto &color = Color::GetInstance();
	auto &judge = JudgeCount::GetInstance();

	clsDx();
	m_fps->Update();

	// キー入力の取得
	input.GetKey();
	m_CurrentTime = GetNowCount() - m_StartTime;

	//ESCキーが押されたら曲選択画面に戻る
	if (input.keyEsc > 1)
	{
		SceneManager::SetNextScene(Scenes::MUSIC_SELECT);
		DxLib::PlaySoundMem(m_backSound, DX_PLAYTYPE_BACK);
		return;
	}
	//ライフが0になったらフラグを-1にする
	if (m_life->m_Life <= 0)
	{
		judge.percent = 0;
		isDead = true;
	}
	//開始用
	PlayMusic(g_Now, m_CurrentTime, readyState, m_isDrawReadyGo.data());

	//曲の再生時間を取得.
	if (CheckSoundMem(m_MusicSound) == 1 && readyState == 1)
		g_Now = GetSoundCurrentTime(m_MusicSound);

	if (CheckSoundMem(m_MusicSound) == 0 && readyState == 1 && m_EndTime == 0)
	{
		m_EndTime = GetNowCount();
		m_EndNow = g_Now;
	}
	if (m_EndTime != 0)
		g_Now = m_EndNow + GetNowCount() - m_EndTime;

	//終了時間になったらor曲再生が終了したらゲームループから抜ける
	if (g_Now >= m_timing->m_MusicEndTime && readyState == 1)
	{
		SceneManager::SetNextScene(Scenes::RESULT);
		return;
	}

	//アニメーションフレームのカウント
	m_count++;

	//ハイスピ変更
	if (!gameOption.isReverse)
	{
		if (input.keyShift > 0 && input.keyUp == 1 && m_scrollSpeed >= 0)
			m_scrollSpeed += 0.5;
		if (Input::GetInstance().keyShift > 0 && input.keyDown == 1 && m_scrollSpeed > 0)
			m_scrollSpeed -= 0.5;
	}
	else
	{
		if (input.keyShift > 0 && input.keyUp == 1 && m_scrollSpeed <= 0)
			m_scrollSpeed -= 0.5;
		if (input.keyShift > 0 && input.keyDown == 1 && m_scrollSpeed < 0)
			m_scrollSpeed += 0.5;
	}

	if (m_scrollSpeed + EP < gameOption.speed)
		gameOption.speed -= 0.02;

	if (m_scrollSpeed - EP > gameOption.speed)
		gameOption.speed += 0.02;

	//STOPさせる時間になったらstops++とBPM変化によるズレを計算
	if (m_timing->IsStopTiming(g_Now, m_StopIndex))
	{
		if (m_timing->m_IsStop == false)
		{
			//停止フラグ
			m_timing->m_IsStop = true;
			m_note4Key->SetMovedPixel(m_timing->m_StopPositionTime[m_StopIndex]);
		}
		if (m_timing->m_StopPositionTime[m_StopIndex] + m_timing->m_Stop[m_StopIndex] * 1000 < g_Now && m_timing->m_TimeS == 1)
		{
			//ずれる座標を加算
			m_note4Key->GAPs += ONE_BEAT_PX * (m_timing->m_Stop[m_StopIndex] * 1000 * m_timing->m_Bpm[m_BpmIndex]) / ONE_MINUTE;
			//次の停止を準備
			++m_StopIndex;
			//スクロールフラグ
			m_timing->m_IsStop = false;
		}
	}
	//BPM変化させる時間になったらk++とBPM変化によるズレを計算
	else if (m_timing->IsBpmChangeTiming(g_Now, m_BpmIndex))
	{
		++m_BpmIndex;
		m_note4Key->GAPn = 0;
		for (int n = 0; n < m_BpmIndex; n++)
		{
			m_note4Key->GAPn += ONE_BEAT_PX * (m_timing->m_Cn[n + 1] - m_timing->m_Cn[n]) * (m_timing->m_Bpm[m_BpmIndex] - m_timing->m_Bpm[n]) / ONE_MINUTE;
		}
	}

	m_note4Key->SetSpeed(m_timing->m_Bpm[m_BpmIndex]);
	m_note4Key->SetPixelPerBeat();

	//停止でないとき.
	if (m_timing->m_TimeS == 0)
	{
		m_note4Key->m_BeatNowTime = GetNowCount() - m_note4Key->m_BeatTime;
		m_note4Key->NoteBeatCycle(m_CurrentTime);

		m_note4Key->SetMovedPixel(g_Now);
		m_note4Key->SetOffsetPix(m_timing->m_Offset, m_timing->m_Bpm[m_BpmIndex]);
	}

	//Auto反転
	if (input.F8 == 1)
		gameOption.isAuto = !gameOption.isAuto;

	//HandCrap反転
	if (input.F7 == 1)
		gameOption.isCrap = !gameOption.isCrap;

	/*Autoの場合*/
	if (gameOption.isAuto == true)
	{
		m_note4Key->GetKeyTimeAuto4Key(m_keyinput4Key, g_Now, ROW_NUM);
		m_note4Key->GetKeyNoteAuto4Key(m_keyinput4Key, ROW_NUM);
	}
	/*通常プレイ*/
	else
	{
		//キーを押した時の時間を取得
		m_note4Key->GetKeyTime(m_keyinput4Key, g_Now, ROW_NUM);

		//キーを押したとき最も近いノートが各方向の上から何番目かを求める.
		m_note4Key->GetKeyNote(m_keyinput4Key, ROW_NUM);
	}

	//Luaからスコアを計算
	if (m_ScoreAnimationFrame == 0)
	{
		LuaFuncParam params;
		LuaFuncParam results;

		params.Number(m_note4Key->m_Maxnote).Number(judge.parfect).Number(judge.grate).Number(judge.good).Number(judge.OK);
		if (!m_luaHelper.CallFunc(_T("UpdateScore"), &results, 1, &params))
		{
			if (!OptionSaveData::g_IsFullScreen)
				MessageBox(nullptr, _T("Lua関数の呼び出しに失敗"), _T("error"), MB_OK);
			exit(1);
		}
		else
		{
			//成功→返り値の取得
			judge.score = static_cast<int>(results.GetNumber(0));
			//本命のスコアと目に見えるスコアの差を求める
			m_score->m_ScoreSa = judge.score - m_score->m_ScoreNum;
		}
	}

	//スコア加算時の20フレーム処理
	if (m_ScoreAnimationFrame < 20)
	{
		m_score->m_ScoreNum += static_cast<int>(m_score->m_ScoreSa / 20.0);
		if (m_ScoreAnimationFrame == 19)
			m_score->m_ScoreNum = static_cast<int>(judge.score);
		m_ScoreAnimationFrame++;
	}

	//エクセなら満点にする(ぇ
	if (m_note4Key->m_Maxnote == judge.parfect + judge.OK && !gameOption.isAuto)
	{
		judge.score = 1000000;
	}

	//maxCombo処理
	judge.UpdateMaxComboOrNone(m_combo->m_ComboNum);

	//OK、NG描画の際のアニメーションフラグ
	for (auto m = 0; m < 4; m++)
	{
		if (m_note4Key->m_U[m] < 20)
			m_note4Key->m_U[m]++;
	}
	//コンボが増える際のアニメーションフラグ
	if (m_combo->m_AnimationFrame < 20)
		m_combo->m_AnimationFrame++;

	// 画面の初期化
	ClearDrawScreen();
	DxLib::SetDrawArea(0, 0, 800, 600);
	//背景画像描画
	DxLib::DrawExtendGraph(0, 0, 800, 600, m_backgroundImage, TRUE);

	//スペクトルを描画
	DrawSpectrum(m_SoftSoundHandle, m_MusicSound);

	//背景画像描画
	DxLib::DrawGraph(-2, 0, m_filterImage[0], TRUE);

	//コンボエフェクト.
	m_combo->ShowEffect100Combo(std::move(m_filterImage[1]));

	DxLib::SetDrawArea(-2, 0, WIN_WIDTH, WIN_HEIGHT);

	//枠の描画
	DxLib::DrawRotaGraph(MASU_X4 + 64 * (0), m_note4Key->m_JudgeLinePositionY, 1.0, PI / 2, g_JudgeAreaImages[1], TRUE);
	DxLib::DrawRotaGraph(MASU_X4 + 64 * (1), m_note4Key->m_JudgeLinePositionY, 1.0, 0, g_JudgeAreaImages[1], TRUE);
	DxLib::DrawRotaGraph(MASU_X4 + 64 * (2), m_note4Key->m_JudgeLinePositionY, 1.0, PI, g_JudgeAreaImages[1], TRUE);
	DxLib::DrawRotaGraph(MASU_X4 + 64 * (3), m_note4Key->m_JudgeLinePositionY, 1.0, PI * 3 / 2, g_JudgeAreaImages[1], TRUE);
	/*if (<now)*/
	if (gameOption.isAuto == false)
	{
		// キーを押した時の描画
		if (input.keyLeft >= 1)
			DxLib::DrawRotaGraph(MASU_X4 + 64 * (0), m_note4Key->m_JudgeLinePositionY, 1.0, PI / 2, g_JudgeAreaImages[0], TRUE);
		if (input.keyDown >= 1)
			DxLib::DrawRotaGraph(MASU_X4 + 64 * (1), m_note4Key->m_JudgeLinePositionY, 1.0, 0, g_JudgeAreaImages[0], TRUE);
		if (input.keyUp >= 1)
			DxLib::DrawRotaGraph(MASU_X4 + 64 * (2), m_note4Key->m_JudgeLinePositionY, 1.0, PI, g_JudgeAreaImages[0], TRUE);
		if (input.keyRight >= 1)
			DxLib::DrawRotaGraph(MASU_X4 + 64 * (3), m_note4Key->m_JudgeLinePositionY, 1.0, PI * 3 / 2, g_JudgeAreaImages[0], TRUE);
	}

	DxLib::SetDrawArea(0, 0, 800, 600);

	//ここから判定処理
	for (auto m = 0; m < ROW_NUM; ++m)
	{
		auto first = 0;
		auto last = 0;
		//ノートのカウント処理(現在何個目か)
		m_note4Key->m_NoteCount[m] = 0;
		for (unsigned x = 0; x < m_note4Key->m_Note[m].size(); ++x)
		{
			if (static_cast<int>(g_Now) > m_note4Key->m_NoteTime[m][x])
				++m_note4Key->m_NoteCount[m];
			else
				break;
		}
		//初期値(後方)
		if (m_note4Key->m_NoteCount[m] + FIRST_DRAW_NOTE >= static_cast<int>(m_note4Key->m_Note[m].size() - 1))
			first = static_cast<int>(m_note4Key->m_Note[m].size()) - 1;
		else
			first = m_note4Key->m_NoteCount[m] + FIRST_DRAW_NOTE;

		//終了条件(前方)
		if (m_note4Key->m_NoteCount[m] - LAST_DRAW_NOTE >= 0)
			last = m_note4Key->m_NoteCount[m] - LAST_DRAW_NOTE;
		else
			last = 0;

		//列ごとに処理
		for (auto i = first; i - last >= 0; --i)
		{
			//ノートの判定処理
			m_note4Key->NoteJudgement(m_score, m_combo, m_keyinput4Key, m_life, m, &m_ScoreAnimationFrame, g_Now, i, gameOption.isAuto);
			//ロングノートのホールド時の処理
			m_note4Key->LongNoteHold4Key(m_score, m_keyinput4Key, m, &m_ScoreAnimationFrame, g_Now, i, gameOption.isAuto);

			//ホールド状態から離した時間を取得
			if (m_keyinput4Key->m_ReleaseTime[m].elapsed() < 0.25)
				m_keyinput4Key->m_ReleaseTimes[m] = m_keyinput4Key->m_ReleaseTime[m].elapsed();
			else
				m_keyinput4Key->m_ReleaseTimes[m] = 0;

			////////////ハンクラ////////////
			if (GamePlayOption::GetInstance().isCrap && !m_hasCrap[m][i] && static_cast<int>(m_note4Key->m_NoteTime[m][i]) <= g_Now + CRAP_OFFSET && (0 < m_note4Key->m_NoteTime[m][i]))
			{
				//多重ハンクラ再生を避ける
				m_hasCrap[m][i] = true;
				if (tempClapedTime != static_cast<int>(m_note4Key->m_NoteTime[m][i]) && m_note4Key->m_NoteFlag[m][i] != -3)
				{
					tempClapedTime = static_cast<int>(m_note4Key->m_NoteTime[m][i]);
					PlaySoundMem(m_handClapSound, DX_PLAYTYPE_BACK);
				}
			}
			//////////////描画//////////////

			//ロングノート終点
			m_note4Key->DrawLongNoteBottom(m, i);

			//BODY
			m_note4Key->DrawLongNoteBody(m, i, m_keyinput4Key);

			//ロングノート始点
			m_note4Key->DrawLongNoteTop(m, i);

			//ロングノート終点(Active)
			m_note4Key->DrawLongNoteBottomActive(m, i, m_keyinput4Key);

			//ロングノート始点(Active)
			m_note4Key->DrawLongNoteTopActive(m, i, m_keyinput4Key);

			// 通常ノート描画
			if (gameOption.noteSkin != NoteSkinType::FLAT)
				m_note4Key->NoteDraw(m, i);
			else if (gameOption.noteSkin == NoteSkinType::FLAT)
				m_note4Key->NoteFlatDraw(m, i);

			//フラグが0の時
			m_note4Key->DrawNoteBehind(m, i);

			//シグナル版は重いのでコメントアウト
			//sig(m, i, s_keyinput4key);

			//判定の描画フラグ
			for (auto f = 1; f <= 3; f++)
			{
				if (!isShowJudge)
				{
					if (m_note4Key->m_JudgmentResult == f && m_note4Key->m_NoteFlag[m][i] == 2 && g_Now - m_note4Key->m_NoteFlagTime[m][i] < JUDGE_SHOW_TIME)
					{
						m_judgeDrawFlag = f;
						isShowJudge = true;
						m_judgeDrawTime = m_note4Key->m_NoteFlagTime[m][i];
						break;
					}
					//非描画フラグ
					else
					{
						m_judgeDrawFlag = -1;
						m_judgeDrawTime = 0;
					}
				}
			}

			//OKとNGはここで描画//
			DrawLongNoteJudge(m, i);

			//各判定の描画フラグ
			for (int j = 1; j <= 3; j++)
			{
				if (isShowJudge) continue;

				if (m_note4Key->m_JudgmentResult == j && m_note4Key->m_NoteFlag[m][i] == 1 && g_Now - m_note4Key->m_NoteFlagTime[m][i] < JUDGE_SHOW_TIME)
				{
					m_judgeDrawFlag = j;
					isShowJudge = true;
					m_judgeDrawTime = m_note4Key->m_NoteFlagTime[m][i];
					break;
				}
				//非描画フラグ
				else
				{
					m_judgeDrawFlag = -1;
					m_judgeDrawTime = 0;
				}
			}
			//ミスの描画フラグ
			if (m_note4Key->m_JudgmentResult == 4 && m_note4Key->m_NoteFlag[m][i] == 0 && (g_Now - m_note4Key->m_NoteFlagTime[m][i]) < JUDGE_SHOW_TIME)
			{
				m_judgeDrawFlag = 5;
				isShowJudge = true;
				m_judgeDrawTime = m_note4Key->m_NoteFlagTime[m][i];
			}

			//爆発描画
			if (0 < m_note4Key->m_JudgmentResult && m_note4Key->m_JudgmentResult < 3 && m_note4Key->m_NoteFlag[m][i] == 1 && g_Now - m_note4Key->m_NoteFlagTime[m][i] < EXPLOSION_TIME - 99)
				DxLib::DrawRotaGraph2(MASU_X4 + 64 * m, m_note4Key->m_JudgeLinePositionY, 32, 32, 1.0, 0, m_explosionImage, TRUE);
			//爆発描画
			if (0 < m_note4Key->m_JudgmentResult && m_note4Key->m_JudgmentResult < 3 && m_note4Key->m_NoteFlag[m][i] == 3 && g_Now - m_note4Key->m_NoteFlagTime[m][i] < EXPLOSION_TIME - 99)
				DxLib::DrawRotaGraph2(MASU_X4 + 64 * m, m_note4Key->m_JudgeLinePositionY, 32, 32, 1.0, 0, m_explosionImage, TRUE);
			//MISSED時のNGの描画(LONGNOTE)
			if (m_note4Key->m_JudgmentResult == 4 && m_note4Key->m_NoteFlag[m][i] == 0 && m_note4Key->m_NoteFlag[m].at(i + 1) == -3 && m_note4Key->m_NoteTime[m][i] + 250 < g_Now &&  g_Now < m_note4Key->m_NoteTime[m][i] + 450)
				DxLib::DrawRotaGraph2(MASU_X4 + 64 * (m), static_cast<int>(m_note4Key->m_JudgeLinePositionY + 66 + 8 * sin(static_cast<double>(m_note4Key->m_U[m] / 50.0*PI))), 50, 50, IMAGE_RATIO, 0, m_longnoteImage->longJudge->at(1), TRUE);
		}
	}//判定のループはここまで
	DxLib::SetDrawArea(0, 0, WIN_WIDTH, WIN_HEIGHT);

	//コンボ数の描画
	if (m_combo->m_ComboNum >= COMBO_DRAWING_START_COUNT)
	{
		DrawCombo(m_combo);
		DxLib::DrawRotaGraph2(248, static_cast<int>(290 + static_cast<int>(gameOption.isReverse) * 55 + 10 * sin(static_cast<double>(m_combo->m_AnimationFrame / 50.0*PI))), 71, 55 / 2, 1.0, 0, m_comboImage, TRUE);
	}

	//1Pライフを描画
	DxLib::DrawGraph(55, -39, m_lifeGageImage[0], TRUE);
	if (m_life->m_Life >= 0 && m_life->m_Life < 166)
	{
		DxLib::SetDrawArea(86 - 11, 18, static_cast<int>(84 - 11 + 5.36 / 10.0 * m_life->m_Life), 40);
		DxLib::DrawGraph(86 - 11 - static_cast<int>((g_Now + 2400) / 6.6) % 168, 18, m_lifeRedImage, TRUE);
		DxLib::DrawGraph(86 - 11 - static_cast<int>((g_Now + 2400) / 6.6) % 168 + 168, 18, m_lifeRedImage, TRUE);
		DxLib::DrawGraph(86 - 11 - static_cast<int>((g_Now + 2400) / 6.6) % 168 + 168 * 2, 18, m_lifeRedImage, TRUE);
		DxLib::SetDrawArea(0, 0, WIN_WIDTH, WIN_HEIGHT);
	}
	else if (m_life->m_Life >= 0 && m_life->m_Life < 500)
	{
		DxLib::SetDrawArea(86 - 11, 18, static_cast<int>(84 - 11 + 5.36 / 10.0 * m_life->m_Life), 40);
		DxLib::DrawGraph(86 - 11 - static_cast<int>((g_Now + 2400) / 6.6) % 168, 18, m_lifeGreenImage, TRUE);
		DxLib::DrawGraph(86 - 11 - static_cast<int>((g_Now + 2400) / 6.6) % 168 + 168, 18, m_lifeGreenImage, TRUE);
		DxLib::DrawGraph(86 - 11 - static_cast<int>((g_Now + 2400) / 6.6) % 168 + 168 * 2, 18, m_lifeGreenImage, TRUE);
		DxLib::SetDrawArea(0, 0, WIN_WIDTH, WIN_HEIGHT);
	}
	else if (m_life->m_Life == 500)
	{
		DxLib::SetDrawArea(86 - 11, 18, static_cast<int>(84 - 11 + 5.36 / 10.0 * m_life->m_Life), 40);
		DxLib::DrawGraph(86 - 11 - static_cast<int>((g_Now + 2400) / 6.6) % 168, 18, m_lifeMaxImage, TRUE);
		DxLib::DrawGraph(86 - 11 - static_cast<int>((g_Now + 2400) / 6.6) % 168 + 168, 18, m_lifeMaxImage, TRUE);
		DxLib::DrawGraph(86 - 11 - static_cast<int>((g_Now + 2400) / 6.6) % 168 + 168 * 2, 18, m_lifeMaxImage, TRUE);
		DxLib::SetDrawArea(0, 0, WIN_WIDTH, WIN_HEIGHT);
	}
	DxLib::DrawGraph(17 - 11 + 100 - 32, 16, m_lifeGageImage[1], TRUE);

	//判定の描画
	if (m_judgeDrawFlag != -1)
	{
		const auto nT = (g_Now - m_judgeDrawTime) / JUDGE_SHOW_TIME;
		DxLib::DrawRotaGraph(180, 45 + 150 + 204 * static_cast<int>(gameOption.isReverse), 1.0 + (1.0 - (nT * (2.0 - nT)))*0.138, 0, g_JudgeImages[m_judgeDrawFlag], TRUE);
	}

	isShowJudge = false;

	//曲名のスクロール座標を計算
	CalculateScrollTextPosition(m_moveX.data(), m_stopTime.data(), m_stringWidth.data());

	//曲名等を表示
	if (m_musicStringX[0] - 19 > 400)
		DxLib::DrawStringToHandle(m_musicStringX[0] - 19, 630 + 42 * 0, m_musicName.c_str(), color.White(), m_FontHandle[1]);
	if (m_musicStringX[1] - 19 > 400)
		DxLib::DrawStringToHandle(m_musicStringX[1] - 19, 630 + 42 * 1, m_musicSubName.c_str(), color.White(), m_FontHandle[1]);
	if (m_musicStringX[2] - 19 > 400)
		DxLib::DrawStringToHandle(m_musicStringX[2] - 19, 630 + 42 * 2, m_musicArtist.c_str(), color.White(), m_FontHandle[1]);

	DxLib::SetDrawArea(400, 600, 800, 768);

	if (m_musicStringX[0] - 19 <= 400)
	{
		DxLib::DrawStringToHandle(402 + 10 + m_moveX[0], 630 + 42 * 0, m_musicName.c_str(), color.White(), m_FontHandle[1]);
		DxLib::DrawStringToHandle(402 + 32 + m_stringWidth[0] * 2 + m_moveX[0], 630 + 42 * 0, m_musicName.c_str(), color.White(), m_FontHandle[1]);
	}
	if (m_musicStringX[1] - 19 <= 400)
	{
		DxLib::DrawStringToHandle(402 + 10 + m_moveX[1], 630 + 42 * 1, m_musicSubName.c_str(), color.White(), m_FontHandle[1]);
		DxLib::DrawStringToHandle(402 + 32 + m_stringWidth[1] * 2 + m_moveX[1], 630 + 42 * 1, m_musicSubName.c_str(), color.White(), m_FontHandle[1]);
	}
	if (m_musicStringX[2] - 19 <= 400)
	{
		DxLib::DrawStringToHandle(402 + 10 + m_moveX[2], 630 + 42 * 2, m_musicArtist.c_str(), color.White(), m_FontHandle[1]);
		DxLib::DrawStringToHandle(402 + 32 + m_stringWidth[2] * 2 + m_moveX[2], 630 + 42 * 2, m_musicArtist.c_str(), color.White(), m_FontHandle[1]);
	}

	DxLib::SetDrawArea(0, 0, WIN_WIDTH, WIN_HEIGHT);

	//背景画像の描画
	DxLib::DrawGraph(0, 0, m_backUnderImage, TRUE);

	//枠の描画
	DxLib::DrawGraph(6, 0, g_BackImage, TRUE);
	//スコアの描画
	DrawScore(m_score->m_ScoreNum);

	//バナー描画用
	if (m_RatioImageSize != 1.0)
		DxLib::DrawBox(880 - 64, 522 - 64, 880 + 64, 522 + 64, color.Black(), TRUE);

	//アルバムアート画像の描画
	DxLib::DrawModiGraph(880 - 64, static_cast<int>(522 - 64 * m_RatioImageSize), 880 + 64, static_cast<int>(522 - 64 * m_RatioImageSize), 880 + 64, static_cast<int>(522 + 64 * m_RatioImageSize), 880 - 64, static_cast<int>(522 + 64 * m_RatioImageSize), musicAAImage, FALSE);
	//アルバムアート画像の描画
	DxLib::DrawModiGraph(static_cast<int>(200 - 40.0 / m_RatioImageSize), 688 - 40, static_cast<int>(200 + 40.0 / m_RatioImageSize), 688 - 40, static_cast<int>(200 + 40.0 / m_RatioImageSize), 688 + 40, static_cast<int>(200 - 40.0 / m_RatioImageSize), 688 + 40, musicAAImage, FALSE);

	//ELEBEAT画像
	DxLib::DrawRotaGraph2(JUDGE_X_PX + 55, JUDGE_Y_PX - 55, 235 / 2, 53 / 2, 0.55, 0, m_elebeatImage, TRUE);
	//経過時間と判定カウントを表示させる関数の呼び出し
	DrawFormatStrings(g_Now);
	//シークバーを表示
	if (g_Now >= 0)
		DxLib::DrawBox(0, 598, static_cast<int>(800.0 / m_timing->m_MusicEndTime * g_Now), 601, 0x22ecea, TRUE);

	if (g_Now < 0)
		DxLib::DrawRotaGraph2(0, 600, 20, 20, 1.0f, 0, m_cursorImage, TRUE);
	else if (g_Now >= 0)
		DxLib::DrawRotaGraph2(static_cast<int>(800.0 / m_timing->m_MusicEndTime * g_Now), 600, 20, 20, 1.0f, 0, m_cursorImage, TRUE);

	//BPMを表示
	DxLib::DrawGraph(410, 6, m_bpmImage, TRUE);
	DxHelper::DrawOutlineStringToHandle(400, 16, color.White(), color.Black(), m_FontHandle[1], _T("%3d"), static_cast<int>(m_timing->m_Bpm[m_BpmIndex]));

	//難易度を表示

	DxLib::DrawBox(880 - 30, 66 - 30, 880 + 30, 66 + 30, COLOR_DIFF[GameBase::g_MusicDif], TRUE);
	DrawDifficultyNum(musicP->danceSong->m_Difficulty[GameBase::g_MusicDif][musicNumber4], m_difnumberColorImage[GameBase::g_MusicDif]);
	DxLib::DrawRotaGraph2(WIN_WIDTH - 80, 20, 50, 10, 1.0, 0, m_DifImage[GameBase::g_MusicDif], TRUE);

	//オプションを表示
	DxHelper::DrawOutlineString(24, 600 - 20, color.White(), color.Black(), _T("SPEED %4.2lf"), fabs(gameOption.speed));
	if (GamePlayOption::GetInstance().isReverse)
		DxHelper::DrawOutlineString(24 + 124 * 1, 600 - 20, color.White(), color.Black(), _T("REVERSE   %s"), noteSkinNames.at(gameOption.noteSkin));
	else
		DxHelper::DrawOutlineString(24 + 124 * 1, 600 - 20, color.White(), color.Black(), _T("%s"), noteSkinNames.at(gameOption.noteSkin));

	//RedyGoを描画
	if (m_isDrawReadyGo[0])
		DxLib::DrawRotaGraph2(400, 400, 350, 150, 1, 0, m_readyImage, TRUE);
	if (m_isDrawReadyGo[1])
		DxLib::DrawRotaGraph2(400, 400, 350, 150, 1, 0, m_goImage, TRUE);

	//ライフゼロ時のシャッター
	if (isDead)
	{
		if (OnDead(m_ShutterCount))
		{
			SceneManager::SetNextScene(Scenes::RESULT);
			return;
		}
	}

	//Auto Play
	if (GamePlayOption::GetInstance().isAuto)
		DxLib::DrawStringToHandle(WIN_WIDTH / 2 - 100, WIN_HEIGHT / 2, _T("Auto Play"), color.White(), m_FontHandle[0]);
	//Crap On
	if (GamePlayOption::GetInstance().isCrap)
		DxLib::DrawStringToHandle(WIN_WIDTH / 2 - 150, WIN_HEIGHT / 2 + 41, _T("(Assist Tick is On)"), color.White(), m_FontHandle[1]);

	m_fps->Draw();        //描画
	DxLib::ScreenFlip();
	m_fps->Wait();        //待機

	if (input.keyPrtsc == 1)
		m_ssc->CaptureScreenshot();
}

void GameDanceElenation::Finalize(const shared_ptr<Music> &musicP)
{
	GamePlayOption& gameOption = GamePlayOption::GetInstance();
	JudgeCount& judge = JudgeCount::GetInstance();

	//うまく押せた率を計算
	if (m_note4Key->m_NoteTotle != 0)
		judge.percent = 100 * (judge.parfect + (judge.grate*0.5) + (judge.good*0.2)) / m_note4Key->m_NoteTotle;
	else
		judge.percent = 0;

	//エクセなら満点にする
	if ((m_note4Key->m_Maxnote == judge.parfect + judge.OK &&  gameOption.isAuto == false) || judge.score > 1000000)
		judge.score = 1000000;

	//フルコンしたかどうか判定
	if (m_note4Key->m_NoteTotle == m_note4Key->m_NoteCount1 && gameOption.isAuto == false)
		judge.isDoneFullcombo = 1;//フルコン
	else
		judge.isDoneFullcombo = 0;

	DxLib::StopSoundMem(m_MusicSound);
	DxLib::InitSoundMem();
	// 作成したフォントデータを削除する
	DxLib::DeleteFontToHandle(m_FontHandle[0]);
	DxLib::DeleteFontToHandle(m_FontHandle[1]);
	if (SceneManager::g_CurrentScene != Scenes::FINISH)
		Screen::FadeOut();
}

void GameDanceElenation::DrawSpectrum(int softSoundHandle, int musicSound)
{
	// 現在の再生位置を取得
	const auto samplePos = GetCurrentPositionSoundMem(musicSound);
	float paramList[BUFFER_LENGTH];
	// 現在の再生位置から 4096 サンプルを使用して周波数分布を得る
	GetFFTVibrationSoftSound(softSoundHandle, -1, samplePos, 4096, paramList, BUFFER_LENGTH);
	//移動平均フィルタを掛ける
	GetMovingAverage(paramList, 7U);
	int x = -1, j = 0;
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);
	// 周波数分布を画面に描画する
	for (auto i = 0; i < BUFFER_LENGTH; i++)
	{
		if (static_cast<int>((log10(static_cast<double>(i))) * 10) != x)
		{
			++j;
			x = static_cast<int>((log10(static_cast<double>(i))) * 10);

			// 関数から取得できる値を描画に適した値に調整
			float param = pow(paramList[i], 0.5f);

			// 縦線を描画
			DxLib::DrawBox(j * 20, 600 - (int)(param * 222), j * 20 + 7, 600, Color::GetInstance().White(), TRUE);
		}
	}
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
}

void GameDanceElenation::DrawLongNoteJudge(int m, int i)
{
	auto judgeDrawY = 0;
	if (GamePlayOption::GetInstance().isReverse)
		judgeDrawY = static_cast<int>(555 - 99 + 8 * sin(static_cast<double>(m_note4Key->m_U[m] / 50.0*PI)));
	else
		judgeDrawY = static_cast<int>(m_note4Key->m_JudgeLinePositionY + 66 + 8 * sin(static_cast<double>(m_note4Key->m_U[m] / 50.0*PI)));

	//OKを描画
	if (m_note4Key->m_NoteFlag[m][i] == 2 && m_note4Key->m_NoteFlag[m][i + 1] == 3 && g_Now - m_note4Key->m_NoteTime[m][i + 1] < 200)
	{
		DxLib::DrawRotaGraph2(MASU_X4 + 64 * (m), judgeDrawY, 50, 50, IMAGE_RATIO, 0, m_longnoteImage->longJudge->at(0), TRUE);
	}
	//NGを描画
	else if (m_note4Key->m_NoteFlag[m][i] == 2 && m_note4Key->m_NoteFlag[m][i + 1] == 0 && g_Now - m_note4Key->m_NoteFlagTime[m][i + 1] < 200)
	{
		DxLib::DrawRotaGraph2(MASU_X4 + 64 * (m), judgeDrawY, 50, 50, IMAGE_RATIO, 0, m_longnoteImage->longJudge->at(1), TRUE);
	}
	//NGを描画
	if (m_note4Key->m_JudgmentResult == 6 && m_note4Key->m_NoteFlag[m][i] == 0 && m_note4Key->m_NoteFlag[m][i + 1] == -3 && m_note4Key->m_NoteTime[m][i] + 200 < g_Now &&  g_Now < m_note4Key->m_NoteTime[m][i] + 600)
	{
		DxLib::DrawRotaGraph2(MASU_X4 + 64 * (m), judgeDrawY, 50, 50, IMAGE_RATIO, 0, m_longnoteImage->longJudge->at(1), TRUE);
	}
}

void GameDanceElenation::GetMovingAverage(float paramList[], unsigned int cacheSize)
{
	for (int i = 0; i < BUFFER_LENGTH; ++i)
	{
		this->m_cacheFftVibration[i].emplace_back(paramList[i]);

		float sum = 0;
		for each(float v in this->m_cacheFftVibration[i])
		{
			sum += v;
		}
		paramList[i] = sum / cacheSize;
		if (this->m_cacheFftVibration[i].size() > cacheSize)
		{
			this->m_cacheFftVibration[i].pop_front();
		}
	}
}

void GameDanceElenation::CalculateScrollTextPosition(int moveX[], int stoptime[], const int stringWidth[])
{
	//曲名のスクロール処理
	for (auto sc = 0; sc < 3; ++sc)
	{
		//一周後は座標を元に戻す
		if (moveX[sc] == -stringWidth[sc] * 2 - 22)
			moveX[sc] = 0;

		if (moveX[sc] == 0)
		{
			//一周して停止する
			stoptime[sc]++;

			if (stoptime[sc] >= 300)
			{
				moveX[sc]--;
				stoptime[sc] = 0;
			}
		}
		else
		{
			moveX[sc]--;
		}
	}
}

bool GameDanceElenation::OnDead(int &count)
{
	//ライフゼロ時のシャッター
	if (count == 1)
		DxLib::PlaySoundMem(m_failedSound, DX_PLAYTYPE_BACK);

	if (count < ((WIN_HEIGHT / 2) / 24))
	{
		count++;
		DxLib::DrawGraph(0, 0 - WIN_HEIGHT / 2 + count * 24, m_shaterImage[0], TRUE);
		DxLib::DrawGraph(0, WIN_HEIGHT / 2 + WIN_HEIGHT / 2 - count * 24, m_shaterImage[1], TRUE);
		DxLib::DrawRotaGraph(WIN_WIDTH / 2, WIN_HEIGHT / 2, 1.0, 0, m_failedImage, TRUE);
	}
	else if (count >= ((WIN_HEIGHT / 2) / 24))
	{
		DxLib::DrawGraph(0, 0, m_shaterImage[0], TRUE);
		DxLib::DrawGraph(0, WIN_HEIGHT / 2, m_shaterImage[1], TRUE);
		DxLib::DrawRotaGraph(WIN_WIDTH / 2, WIN_HEIGHT / 2, 1.0, 0, m_failedImage, TRUE);
		DxLib::ScreenFlip();
		JudgeCount::GetInstance().percent = 0;
		return true;
	}

	return false;
}

void GameDanceElenation::PlayMusic(long& now, int nowTime, int &readyState, bool isShowReadyGo[]) const
{
	if (readyState == -1)
	{
		now = nowTime - STGO_TIME;
		//READY・GOを鳴らす
		DxLib::PlaySoundMem(m_readyGoSound, DX_PLAYTYPE_BACK);
		readyState = 0;
		isShowReadyGo[0] = false;
		isShowReadyGo[1] = false;
	}
	else if (nowTime - STGO_TIME < 0 && readyState == 0)
	{
		now = nowTime - STGO_TIME;
		isShowReadyGo[0] = false;
		isShowReadyGo[1] = false;
		//RedyGoを描画
		if ((0 - STGO_TIME) < now && now < (900 - STGO_TIME))
		{
			isShowReadyGo[0] = true;
			isShowReadyGo[1] = false;
		}
		if ((900 - STGO_TIME) < now && now < (1500 - STGO_TIME))
		{
			isShowReadyGo[0] = false;
			isShowReadyGo[1] = true;
		}
	}
	//読み込んだ曲を鳴らす
	else if (nowTime - STGO_TIME >= 0 && readyState == 0)
	{
		now = nowTime - STGO_TIME;
		DxLib::PlaySoundMem(m_MusicSound, DX_PLAYTYPE_BACK);
		readyState = 1;
	}
}

void GameDanceElenation::FadeInGame(double ratioYX) const
{
	const auto &color = Color::GetInstance();
	const auto from = 0;
	const auto to = 255;

	//黒から画像表示へ
	for (auto i = from; i <= to; i += 4)
	{
		ClearDrawScreen();
		//輝度をだんだんあげていく
		SetDrawBright(i, i, i);
		//背景描画
		DxLib::DrawGraph(0, 0, m_backUnderImage, TRUE);
		DxLib::DrawGraph(6, 0, g_BackImage, TRUE);

		if (ratioYX != 1.0)
			DxLib::DrawBox(880 - 64, 522 - 64, 880 + 64, 522 + 64, color.Black(), TRUE);

		//アルバムアート画像の描画
		DxLib::DrawModiGraph(880 - 64, static_cast<int>(522 - 64 * ratioYX), 880 + 64, static_cast<int>(522 - 64 * ratioYX), 880 + 64, static_cast<int>(522 + 64 * ratioYX), 880 - 64, static_cast<int>(522 + 64 * ratioYX), musicAAImage, FALSE);
		//アルバムアート画像の描画
		DxLib::DrawRotaGraph(JUDGE_X_PX + 55, JUDGE_Y_PX - 55, 0.55, 0, m_elebeatImage, TRUE);
		//経過時間と判定カウントを表示させる関数の呼び出し
		DrawFormatStrings(0);
		DxLib::ScreenFlip();
	}
	SetDrawBright(to, to, to);
}