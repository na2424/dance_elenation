#include "SelectMusicBase.h"
#include "SelectMusic4keys.h"
#include "Enums.h"



void SelectMusicBase::MoveLeft(int nowcenter, int nowmaxpicture, int folderMusicNum)
{
	double ratio[2] = { 1.0, 1.0 };
	if (selectmode == SelectMode::FOLDER || SceneManager::g_PlayMode == SelectKeys::KEYS_9)
	{
		ratio[0] = 1.0;
		ratio[1] = 1.0;
	}
	else
	{
		ratio[0] = SelectMusic4Keys::g_BannerSizeRatio[this->g_NowCenter + 1];
		ratio[1] = SelectMusic4Keys::g_BannerSizeRatio[this->g_NowCenter];
	}
	if (m_InputFrameCount >= 0 && m_InputFrameCount < 3)
	{
		//中央座標設定
		if (m_InputFrameCount == 0 || m_InputFrameCount == 1)
		{
			if (m_InputFrameCount == 0)
			{
				x[nowcenter][1] -= 41;//右上
				y[nowcenter][1] += 9 * ratio[1];
				x[nowcenter][2] -= 41;//右下
				y[nowcenter][2] -= 9 * ratio[1];
			}
			if (m_InputFrameCount == 1)
			{
				x[nowcenter][1] -= 40;//右上
				y[nowcenter][1] += 9 * ratio[1];
				x[nowcenter][2] -= 40;//右下
				y[nowcenter][2] -= 9 * ratio[1];
			}
			x[nowcenter][0] -= 21;//左上
			x[nowcenter][3] -= 21;//左下
		}
		if (m_InputFrameCount == 2)
		{
			x[nowcenter][0] -= 20;//左上
			x[nowcenter][1] -= 40;//右上
			y[nowcenter][1] += 8 * ratio[1];
			x[nowcenter][2] -= 40;//右下
			y[nowcenter][2] -= 8 * ratio[1];
			x[nowcenter][3] -= 20;//左下
		}

		//横の座標設定
		if (m_InputFrameCount == 0 || m_InputFrameCount == 1)
		{
			if (m_InputFrameCount == 0)
			{
				x[nowcenter + 1][0] -= 41;//左上
				y[nowcenter + 1][0] -= 9 * ratio[0];
				x[nowcenter + 1][3] -= 41;//左下
				y[nowcenter + 1][3] += 9 * ratio[0];
			}
			if (m_InputFrameCount == 1)
			{
				x[nowcenter + 1][0] -= 40;//左上
				y[nowcenter + 1][0] -= 9 * ratio[0];
				x[nowcenter + 1][3] -= 40;//左下
				y[nowcenter + 1][3] += 9 * ratio[0];
			}
			x[nowcenter + 1][1] -= 21;//右上
			x[nowcenter + 1][2] -= 21;//右下
		}
		if (m_InputFrameCount == 2)
		{
			x[nowcenter + 1][0] -= 40;//左上
			y[nowcenter + 1][0] -= 8 * ratio[0];//左上
			x[nowcenter + 1][1] -= 20;//右上
			x[nowcenter + 1][2] -= 20;//右下
			x[nowcenter + 1][3] -= 40;//左下
			y[nowcenter + 1][3] += 8 * ratio[0];//左下
		}

		//その他の画像
		for (auto j = 0; j < nowmaxpicture; j++)
		{
			//正面とか右じゃないとき
			if (j != nowcenter && j != nowcenter + 1)
			{
				if (m_InputFrameCount == 0 || m_InputFrameCount == 1)
				{
					x[j][0] -= 18;//左上
					x[j][1] -= 18;//右上
					x[j][2] -= 18;//右下
					x[j][3] -= 18;//左下
				}
				if (m_InputFrameCount == 2)
				{
					x[j][0] -= 16;//左上
					x[j][1] -= 16;//右上
					x[j][2] -= 16;//右下
					x[j][3] -= 16;//左下
				}
			}
		}
	}
	//次時間
	if (m_InputFrameCount >= 3 && m_InputFrameCount < 6)
	{
		//正面画像設定
		x[nowcenter][0] -= 10;//左上
		x[nowcenter][1] -= 20;//右上
		y[nowcenter][1] += 6 * ratio[1];
		x[nowcenter][2] -= 20;//右下
		y[nowcenter][2] -= 6 * ratio[1];
		x[nowcenter][3] -= 10;//左下

		//正面画像右の画像
		x[nowcenter + 1][0] -= 20;//左上
		y[nowcenter + 1][0] -= 6 * ratio[0];
		x[nowcenter + 1][1] -= 10;//右上
		x[nowcenter + 1][2] -= 10;//右下
		x[nowcenter + 1][3] -= 20;//左下
		y[nowcenter + 1][3] += 6 * ratio[0];

		//その他の画像
		for (auto j = 0; j < nowmaxpicture; j++)
		{
			//正面とか右じゃないとき
			if (j != nowcenter && j != nowcenter + 1)
			{
				x[j][0] -= 8;//左上
				x[j][1] -= 8;//右上
				x[j][2] -= 8;//右下
				x[j][3] -= 8;//左下
			}
		}
	}
	//次時間
	if (m_InputFrameCount >= 6 && m_InputFrameCount < 9)
	{
		//正面画像設定
		x[nowcenter][0] -= 7;//左上
		x[nowcenter][1] -= 15;//右上
		y[nowcenter][1] += 1 * ratio[1];
		x[nowcenter][2] -= 15;//右下
		y[nowcenter][2] -= 1 * ratio[1];
		x[nowcenter][3] -= 7;//左下

		//正面画像右の画像
		x[nowcenter + 1][0] -= 15;//左上
		y[nowcenter + 1][0] -= 1 * ratio[0];
		x[nowcenter + 1][1] -= 7;//右上
		x[nowcenter + 1][2] -= 7;//右下
		x[nowcenter + 1][3] -= 15;//左下
		y[nowcenter + 1][3] += 1 * ratio[0];

		//その他の画像
		for (auto j = 0; j < nowmaxpicture; j++)
		{
			//正面とか右じゃないとき
			if (j != nowcenter && j != nowcenter + 1)
			{
				x[j][0] -= 4;//左上
				x[j][1] -= 4;//右上
				x[j][2] -= 4;//右下
				x[j][3] -= 4;//左下
			}
		}
	}

	//次時間
	if (m_InputFrameCount >= 9 && m_InputFrameCount < 12)
	{
		//正面画像設定
		x[nowcenter][0] -= 4;//左上
		x[nowcenter][1] -= 10;//右上
		y[nowcenter][1] += 1 * ratio[1];
		x[nowcenter][2] -= 10;//右下
		y[nowcenter][2] -= 1 * ratio[1];
		x[nowcenter][3] -= 4;//左下

		//正面画像右の画像
		x[nowcenter + 1][0] -= 10;//左上
		y[nowcenter + 1][0] -= 1 * ratio[0];//asdfghjkl
		x[nowcenter + 1][1] -= 4;//右上
		x[nowcenter + 1][2] -= 4;//右下
		x[nowcenter + 1][3] -= 10;//左下
		y[nowcenter + 1][3] += 1 * ratio[0];

		//その他の画像
		for (auto j = 0; j < nowmaxpicture; j++)
		{
			//正面とか右じゃないとき
			if (j != nowcenter && j != nowcenter + 1)
			{
				x[j][0] -= 2;//左上
				x[j][1] -= 2;//右上
				x[j][2] -= 2;//右下
				x[j][3] -= 2;//左下
			}
		}
	}
}

void SelectMusicBase::MoveMirrorLeft(int nowcenter, int nowmaxpicture, int folderMusicNum)
{
	double ratio[2] = { 1.0, 1.0 };
	if (selectmode == SelectMode::FOLDER || SceneManager::g_PlayMode == SelectKeys::KEYS_9)
	{
		ratio[0] = 1.0;
		ratio[1] = 1.0;
	}
	else
	{
		ratio[0] = SelectMusic4Keys::g_BannerSizeRatio[this->g_NowCenter + 1];
		ratio[1] = SelectMusic4Keys::g_BannerSizeRatio[this->g_NowCenter];
	}
	if (m_InputFrameCount >= 0 && m_InputFrameCount < 3)
	{
		//正面画像設定
		if (m_InputFrameCount == 0 || m_InputFrameCount == 1)
		{
			if (m_InputFrameCount == 0)
			{
				m_x[nowcenter][1] -= 41;//右上
				m_y[nowcenter][1] -= 9 * ratio[1];
				m_x[nowcenter][2] -= 41;//右下
				m_y[nowcenter][2] -= 9 * ratio[1];
			}
			if (m_InputFrameCount == 1)
			{
				m_x[nowcenter][1] -= 40;//右上
				m_y[nowcenter][1] -= 9 * ratio[1];
				m_x[nowcenter][2] -= 40;//右下
				m_y[nowcenter][2] -= 9 * ratio[1];
			}
			m_x[nowcenter][0] -= 21;//左上
			m_y[nowcenter][0] -= 17 * ratio[1];
			m_x[nowcenter][3] -= 21;//左下
		}
		if (m_InputFrameCount == 2)
		{
			m_x[nowcenter][0] -= 20;//左上
			m_y[nowcenter][0] -= 15 * ratio[1];
			m_x[nowcenter][1] -= 40;//右上
			m_y[nowcenter][1] -= 8 * ratio[1];
			m_x[nowcenter][2] -= 40;//右下
			m_y[nowcenter][2] -= 8 * ratio[1];
			m_x[nowcenter][3] -= 20;//左下
		}

		//正面画像右の画像
		if (m_InputFrameCount == 0 || m_InputFrameCount == 1)
		{
			if (m_InputFrameCount == 0)
			{
				m_x[nowcenter + 1][0] -= 41;//左上
				m_y[nowcenter + 1][0] += 9 * ratio[0];
				m_x[nowcenter + 1][3] -= 41;//左下
				m_y[nowcenter + 1][3] += 9 * ratio[0];
			}
			if (m_InputFrameCount == 1)
			{
				m_x[nowcenter + 1][0] -= 40;//左上
				m_y[nowcenter + 1][0] += 9 * ratio[0];
				m_x[nowcenter + 1][3] -= 40;//左下
				m_y[nowcenter + 1][3] += 9 * ratio[0];
			}
			m_x[nowcenter + 1][1] -= 21;//右上
			m_y[nowcenter + 1][1] += 17 * ratio[0];
			m_x[nowcenter + 1][2] -= 21;//右下
		}
		if (m_InputFrameCount == 2)
		{
			m_x[nowcenter + 1][0] -= 40;//左上
			m_y[nowcenter + 1][0] += 8 * ratio[0];
			m_x[nowcenter + 1][1] -= 20;//右上
			m_y[nowcenter + 1][1] += 15 * ratio[0];
			m_x[nowcenter + 1][2] -= 20;//右下
			m_x[nowcenter + 1][3] -= 40;//左下
			m_y[nowcenter + 1][3] += 8 * ratio[0];
		}

		//右二個以降の画像
		for (int j = 0; j < nowmaxpicture; j++)
		{
			//正面とか右じゃないとき
			if (j != nowcenter && j != nowcenter + 1)
			{
				if (m_InputFrameCount == 0 || m_InputFrameCount == 1)
				{
					m_x[j][0] -= 18;//左上
					m_x[j][1] -= 18;//右上
					m_x[j][2] -= 18;//右下
					m_x[j][3] -= 18;//左下
				}
				if (m_InputFrameCount == 2)
				{
					m_x[j][0] -= 16;//左上
					m_x[j][1] -= 16;//右上
					m_x[j][2] -= 16;//右下
					m_x[j][3] -= 16;//左下
				}
			}
		}
	}

	//次時間
	if (m_InputFrameCount >= 3 && m_InputFrameCount < 6)
	{
		//正面画像設定
		m_x[nowcenter][0] -= 10;//左上
		m_y[nowcenter][0] -= 10 * ratio[1];
		m_x[nowcenter][1] -= 20;//右上
		m_y[nowcenter][1] -= 6 * ratio[1];
		m_x[nowcenter][2] -= 20;//右下
		m_y[nowcenter][2] -= 6 * ratio[1];
		m_x[nowcenter][3] -= 10;//左下

		//正面画像右の画像
		m_x[nowcenter + 1][0] -= 20;//左上
		m_y[nowcenter + 1][0] += 6 * ratio[0];
		m_x[nowcenter + 1][1] -= 10;//右上
		m_y[nowcenter + 1][1] += 10 * ratio[0];
		m_x[nowcenter + 1][2] -= 10;//右下
		m_x[nowcenter + 1][3] -= 20;//左下
		m_y[nowcenter + 1][3] += 6 * ratio[0];

		//右二個以降の画像
		for (int j = 0; j < nowmaxpicture; j++)
		{
			//正面とか右じゃないとき
			if (j != nowcenter && j != nowcenter + 1)
			{
				m_x[j][0] -= 8;//左上
				m_x[j][1] -= 8;//右上
				m_x[j][2] -= 8;//右下
				m_x[j][3] -= 8;//左下
			}
		}
	}

	//次時間
	if (m_InputFrameCount >= 6 && m_InputFrameCount < 9)
	{
		//正面画像設定
		m_x[nowcenter][0] -= 7;//左上
		m_y[nowcenter][0] -= 4 * ratio[1];
		m_x[nowcenter][1] -= 15;//右上
		m_y[nowcenter][1] -= 1 * ratio[1];
		m_x[nowcenter][2] -= 15;//右下
		m_y[nowcenter][2] -= 1 * ratio[1];
		m_x[nowcenter][3] -= 7;//左下

		//正面画像右の画像
		m_x[nowcenter + 1][0] -= 15;//左上
		m_y[nowcenter + 1][0] += 1 * ratio[0];
		m_x[nowcenter + 1][1] -= 7;//右上
		m_y[nowcenter + 1][1] += 4 * ratio[0];
		m_x[nowcenter + 1][2] -= 7;//右下
		m_x[nowcenter + 1][3] -= 15;//左下
		m_y[nowcenter + 1][3] += 1 * ratio[0];

		//右二個以降の画像
		for (int j = 0; j < nowmaxpicture; j++)
		{
			//正面とか右じゃないとき
			if (j != nowcenter && j != nowcenter + 1)
			{
				m_x[j][0] -= 4;//左上
				m_x[j][1] -= 4;//右上
				m_x[j][2] -= 4;//右下
				m_x[j][3] -= 4;//左下
			}
		}
	}

	//次時間
	if (m_InputFrameCount >= 9 && m_InputFrameCount < 12)
	{
		//正面画像設定
		m_x[nowcenter][0] -= 4;//左上
		m_y[nowcenter][0] -= 3 * ratio[1];
		m_x[nowcenter][1] -= 10;//右上
		m_y[nowcenter][1] -= 1 * ratio[1];
		m_x[nowcenter][2] -= 10;//右下
		m_y[nowcenter][2] -= 1 * ratio[1];
		m_x[nowcenter][3] -= 4;//左下

		//正面画像右の画像
		m_x[nowcenter + 1][0] -= 10;//左上
		m_y[nowcenter + 1][0] += 1 * ratio[0];
		m_x[nowcenter + 1][1] -= 4;//右上
		m_y[nowcenter + 1][1] += 3 * ratio[0];
		m_x[nowcenter + 1][2] -= 4;//右下
		m_x[nowcenter + 1][3] -= 10;//左下
		m_y[nowcenter + 1][3] += 1 * ratio[0];

		//右二個以降の画像
		for (int j = 0; j < nowmaxpicture; j++)
		{
			//正面とか右じゃないとき
			if (j != nowcenter && j != nowcenter + 1)
			{
				m_x[j][0] -= 2;//左上
				m_x[j][1] -= 2;//右上
				m_x[j][2] -= 2;//右下
				m_x[j][3] -= 2;//左下
			}
		}
	}
}

//MoveLeftFast
void SelectMusicBase::MoveLeftFast(int nowcenter, int nowmaxpicture, int folderMusicNum)
{
	double ratio[2] = { 1.0, 1.0 };
	if (selectmode == SelectMode::FOLDER || SceneManager::g_PlayMode == SelectKeys::KEYS_9)
	{
		ratio[0] = 1.0;
		ratio[1] = 1.0;
	}
	else
	{
		ratio[0] = SelectMusic4Keys::g_BannerSizeRatio[this->g_NowCenter + 1];
		ratio[1] = SelectMusic4Keys::g_BannerSizeRatio[this->g_NowCenter];
	}
	//中央座標設定
	if (m_InputFrameCount == 0)
	{
		x[nowcenter][1] -= 81;//右上
		y[nowcenter][1] += 18 * ratio[1];
		x[nowcenter][2] -= 81;//右下
		y[nowcenter][2] -= 18 * ratio[1];
		x[nowcenter][0] -= 42;//左上
		x[nowcenter][3] -= 42;//左下
	}

	//横の座標設定
	if (m_InputFrameCount == 0)
	{
		x[nowcenter + 1][0] -= 81;//左上
		y[nowcenter + 1][0] -= 18 * ratio[0];
		x[nowcenter + 1][3] -= 81;//左下
		y[nowcenter + 1][3] += 18 * ratio[0];
		x[nowcenter + 1][1] -= 42;//右上
		x[nowcenter + 1][2] -= 42;//右下
	}

	//その他の画像
	if (m_InputFrameCount == 0)
	{
		for (auto j = 0; j < nowmaxpicture; j++)
		{
			//正面とか右じゃないとき
			if (j != nowcenter && j != nowcenter + 1)
			{
				if (m_InputFrameCount == 0)
				{
					x[j][0] -= 36;//左上
					x[j][1] -= 36;//右上
					x[j][2] -= 36;//右下
					x[j][3] -= 36;//左下
				}
			}
		}
	}

	//正面画像設定
	if (m_InputFrameCount == 1)
	{
		x[nowcenter][0] -= 30;//左上
		x[nowcenter][1] -= 60;//右上
		y[nowcenter][1] += 14 * ratio[1];
		x[nowcenter][2] -= 60;//右下
		y[nowcenter][2] -= 14 * ratio[1];
		x[nowcenter][3] -= 30;//左下
	}

	//正面画像右の画像
	if (m_InputFrameCount == 1)
	{
		x[nowcenter + 1][0] -= 60;//左上
		y[nowcenter + 1][0] -= 14 * ratio[0];
		x[nowcenter + 1][1] -= 30;//右上
		x[nowcenter + 1][2] -= 30;//右下
		x[nowcenter + 1][3] -= 60;//左下
		y[nowcenter + 1][3] += 14 * ratio[0];
	}

	//その他の画像
	if (m_InputFrameCount == 1)
	{
		for (auto j = 0; j < nowmaxpicture; j++)
		{
			//正面とか右じゃないとき
			if (j != nowcenter && j != nowcenter + 1)
			{
				x[j][0] -= 24;//左上
				x[j][1] -= 24;//右上
				x[j][2] -= 24;//右下
				x[j][3] -= 24;//左下
			}
		}
	}

	if (m_InputFrameCount == 2)
	{
		//正面画像設定
		x[nowcenter][0] -= 20;//左上
		x[nowcenter][1] -= 40;//右上
		y[nowcenter][1] += 12 * ratio[1];
		x[nowcenter][2] -= 40;//右下
		y[nowcenter][2] -= 12 * ratio[1];
		x[nowcenter][3] -= 20;//左下
	}

	if (m_InputFrameCount == 2)
	{
		//正面画像右の画像
		x[nowcenter + 1][0] -= 40;//左上
		y[nowcenter + 1][0] -= 12 * ratio[0];
		x[nowcenter + 1][1] -= 20;//右上
		x[nowcenter + 1][2] -= 20;//右下
		x[nowcenter + 1][3] -= 40;//左下
		y[nowcenter + 1][3] += 12 * ratio[0];
	}

	if (m_InputFrameCount == 2)
	{
		//その他の画像
		for (auto j = 0; j < nowmaxpicture; j++)
		{
			//正面とか右じゃないとき
			if (j != nowcenter && j != nowcenter + 1)
			{
				x[j][0] -= 16;//左上
				x[j][1] -= 16;//右上
				x[j][2] -= 16;//右下
				x[j][3] -= 16;//左下
			}
		}
	}

	if (m_InputFrameCount == 3)
	{
		//正面画像設定
		x[nowcenter][0] -= 14;//左上
		x[nowcenter][1] -= 30;//右上
		y[nowcenter][1] += 2 * ratio[1];
		x[nowcenter][2] -= 30;//右下
		y[nowcenter][2] -= 2 * ratio[1];
		x[nowcenter][3] -= 14;//左下
	}

	if (m_InputFrameCount == 3)
	{
		//正面画像右の画像
		x[nowcenter + 1][0] -= 30;//左上
		y[nowcenter + 1][0] -= 2 * ratio[0];
		x[nowcenter + 1][1] -= 14;//右上
		x[nowcenter + 1][2] -= 14;//右下
		x[nowcenter + 1][3] -= 30;//左下
		y[nowcenter + 1][3] += 2 * ratio[0];
	}

	if (m_InputFrameCount == 3)
	{
		for (int j = 0; j < nowmaxpicture; j++)
		{
			//正面とか右じゃないとき
			if (j != nowcenter && j != nowcenter + 1)
			{
				x[j][0] -= 8;//左上
				x[j][1] -= 8;//右上
				x[j][2] -= 8;//右下
				x[j][3] -= 8;//左下
			}
		}
	}

	if (m_InputFrameCount == 4)
	{
		//正面画像設定
		x[nowcenter][0] -= 11;//左上
		x[nowcenter][1] -= 25;//右上
		y[nowcenter][1] += 2 * ratio[1];
		x[nowcenter][2] -= 25;//右下
		y[nowcenter][2] -= 2 * ratio[1];
		x[nowcenter][3] -= 11;//左下
	}

	if (m_InputFrameCount == 4)
	{
		//正面画像右の画像
		x[nowcenter + 1][0] -= 25;//左上
		y[nowcenter + 1][0] -= 2 * ratio[0];
		x[nowcenter + 1][1] -= 11;//右上
		x[nowcenter + 1][2] -= 11;//右下
		x[nowcenter + 1][3] -= 25;//左下
		y[nowcenter + 1][3] += 2 * ratio[0];
	}

	if (m_InputFrameCount == 4)
	{
		for (auto j = 0; j < nowmaxpicture; j++)
		{
			//正面とか右じゃないとき
			if (j != nowcenter && j != nowcenter + 1)
			{
				x[j][0] -= 6;//左上
				x[j][1] -= 6;//右上
				x[j][2] -= 6;//右下
				x[j][3] -= 6;//左下
			}
		}
	}

	if (m_InputFrameCount == 5)
	{
		//正面画像設定
		x[nowcenter][0] -= 8;//左上
		x[nowcenter][1] -= 20;//右上
		y[nowcenter][1] += 2 * ratio[1];
		x[nowcenter][2] -= 20;//右下
		y[nowcenter][2] -= 2 * ratio[1];
		x[nowcenter][3] -= 8;//左下
	}
	if (m_InputFrameCount == 5)
	{
		//正面画像右の画像
		x[nowcenter + 1][0] -= 20;//左上
		y[nowcenter + 1][0] -= 2 * ratio[0];
		x[nowcenter + 1][1] -= 8;//右上
		x[nowcenter + 1][2] -= 8;//右下
		x[nowcenter + 1][3] -= 20;//左下
		y[nowcenter + 1][3] += 2 * ratio[0];
	}
	if (m_InputFrameCount == 5)
	{
		for (auto j = 0; j < nowmaxpicture; j++)
		{
			//正面とか右じゃないとき
			if (j != nowcenter && j != nowcenter + 1)
			{
				x[j][0] -= 4;//左上
				x[j][1] -= 4;//右上
				x[j][2] -= 4;//右下
				x[j][3] -= 4;//左下
			}
		}
	}
}