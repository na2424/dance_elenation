#pragma once
#include <math.h>
#include <limits>

class DoubleUtils
{
private:
public:
	inline static bool AreSame(double a, double b)
	{
		return fabs(a - b) < std::numeric_limits<double>::epsilon();
	}
};