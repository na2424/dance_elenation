#include "NoteBase.h"
#include "define.h"
#include "DxLib.h"
#include "extern.h"
#include "header.h"
#include "struct.h"
#include <boost/timer.hpp>
#include <string>
#include <vector>


using namespace DxLib;

//実数の剰余を求める
double NoteBase::MyMod(double x, double y)
{
	return (x - y * static_cast<double>(static_cast<int>(x / y)));
}