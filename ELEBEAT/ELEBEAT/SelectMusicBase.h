#ifndef SELECTMUSIC_CLASS_H
#define SELECTMUSIC_CLASS_H
#include <vector>

#include "define.h"
#include "LuaHelper.h"
#include "MouseClickObject.h"
#include "savedata_class.h"
#include "SceneManager.h"

using namespace std;

class TimerDrawer;

class SelectMusicBase : public Scene
{
private:
	int _y;
	array<int, 10> m_bpmNumberImages;
	static const int IMAGE_NUM = 40;
	static const int VERTEX_NUM = 4;

protected:
	const int START_SELECT_TIME = 60;

	int startTime;
	int loadingImage;
	lua_State* L;
	LuaHelper luaHelper;
	unique_ptr<MouseClickObjectList> m_mouseClickObjectList;

public:

	SelectMusicBase();

	void InitLua(void);
	int ReadLua(void);
	int m_InputFrameCount;

	int randomcenternum;
	int countMoveAA;

	//画像位置
	double x[IMAGE_NUM][VERTEX_NUM];    //[50]は画像分の個数　[4]はアルバムアート変形のため
	double y[IMAGE_NUM][VERTEX_NUM];    //[50]は画像分の個数　[4]はアルバムアート変形のため
	double m_x[IMAGE_NUM][VERTEX_NUM];  //(回転画像)の座標保持
	double m_y[IMAGE_NUM][VERTEX_NUM];  //(回転画像)の座標保持

	//MoveLeft.cpp
	void MoveLeft(int nowcenter, int nowmaxpicture, int folderMusicNum = 0);
	void MoveMirrorLeft(int nowcenter, int nowmaxpicture, int folderMusicNum = 0);
	void MoveLeftFast(int nowcenter, int nowmaxpicture, int folderMusicNum = 0);

	//MoveRight.cpp
	void MoveRight(int nowcenter, int nowmaxpicture, int folderMusicNum = 0);
	void MoveMirrorRight(int nowcenter, int nowmaxpicture, int folderMusicNum = 0);

	void DrawSelectMusicBPM(int num) const;

	void Replace(tstring& String1, tstring String2, tstring String3) const;

	//グローバル(9keys,4keys)
	//int
	static int g_DifNowCenter;
	static int g_BeforeCenter;
	static int g_MarkerX[16][4];
	static int g_MarkerY[16][4];
	static int g_SelectMarker;
	static int g_MarkerCount;
	static array<int, 3> g_CurrentAlbumArt;
	static array<int, 2> g_MaxMinbpm;

	SelectMode selectmode;

	//bool
	static bool one[10];
	bool isLeft;
	bool isRight;
	bool isLeftContinue;
	bool isRightContinue;
	bool isFastLeft;
	bool isFastRight;
	bool markerflag;
	bool isLeftMarker;
	bool isRightMarker;
	bool isDownMarker;
	bool marker_goup;
	static bool g_IsLoaded;
	static bool g_IsLoaded4Keys;

	ScoreSaveData m_SaveData;

	//-----------------------
	// 画像変数
	//-----------------------
	Image bpmFrameImage;                                        //bpmの枠
	Image backFrontImage;                                        //更に背景
	Image markerBackImage;                                        //マーカーの背景　土台
	Image difficultyImage;                                //難易度文字
	Image stageNumImage;

	Image rightImage;                        //右にパネル
	Image leftImage;                        //左にパネル
	Image markerImage;                        //マーカー選択
	Image enterImage;                        //ok
	Image backImage;                        //戻る
	Image square[3];                        //難易度数の後ろの四角
	Image difstring[3];                    //難易度文字
	Image detailstring;                    //アーティストとかのあれ
	Image backblack;
	Image numstring;

	//-----------------------
	// 音楽変数(initへ)
	//-----------------------
	Sound backSound;
	Sound roop_music;
	Sound selectdifsound;                    //dif変えたときの効果音

	//glbal変数(16keys)
	//int
	static int g_NowCenter;        //いつもの
	static int x_16[10];                    //座標
	static int g_MarkerGraph16[1000];        //マーカー画像(サムネイル)
	int count;                        //移動のカウンタ
	int selectkeynumber;            //押したキーを返す
	int maxline_folder;                //最大ライン数
	int maxline_AA;                    //最大ライン数　AA
	int maxline_marker;                //最大ライン数　マーカー
	static int nowAA_16[3];                //開いてるfolder->AAの[0]最初の番号[1]最後の番号[2]総
	static int beforecenter_16;            //前のフォルダ番号
	int selectfoldernum;        //選択folder番号 一桁
	static int nowselectglobalAAnum;
	static int difselect;                    //難易度 0 と 1 と 2
	static int beforeselectAA;            //前選択したAAnum
	static int beforeselectmarker;        //前セレクトしたマーカー

	static int strlength[3];                //文字列の長さ

	//bool
	static bool one_16[15];                //一回押し
	static bool stop[3];                    //12枚以下の時folder,AA それぞれ1 で動けない AAはそれぞれ変動
	bool moveleft;                    //左へ
	bool moveright;                    //右へ
	bool leftcountflag_16;            //押し始め
	bool rightcountflag_16;            //押し始め
	bool fastleftflag_16;            //高速移動許可
	bool fastrightflag_16;            //高速移動許可
	static bool selectmusicflag;
	bool movedrawflag[3];            //文字をスクロールするかどうか
	static bool loadendflag_16;        //ロード終わってるなら立てる

	//4keys
	Image backoverImage;
	Image setumei1;
	Image setumei2;
	Image blend_gh;
	Image black_gh;
	Image right_pic;
	Image left_pic;
	Image right_click_pic;
	Image left_click_pic;
	Image marker_waku;
	Image player_option;
	Image selecter;
	Image raderImage;
	Sound wheelSound;
	Sound decideSound;
	Sound expandSound;
	Sound mrkSound;
	Sound closeSound;
	Sound mrkMoveSound;
};

#endif