#include "ScreenShot.h"
#include "extern.h"
#include "SpecialFiles.h"

using namespace DxLib;

ScreenShot::ScreenShot()
{
	m_fileNameCache.clear();
	m_fileName.clear();
	this->m_sound = LoadSoundMem((SpecialFiles::DEFAULT_THEMES_DIR + tstring("Sounds\\ScreenShot.mp3")).c_str());
}

void ScreenShot::CaptureScreenshot()
{
	const auto filePath = SpecialFiles::DATA_DIR + tstring("Setting.ini");
	Setting.Load(filePath);
	this->m_fileNameCache = std::to_string(Setting.ScreenShot.PRTSC_ELE);
	this->m_fileName = _T("ScreenShot\\PRTSC_ELE_") + this->m_fileNameCache + _T(".png");
	SaveDrawScreen(0, 0, WIN_WIDTH, WIN_HEIGHT, m_fileName.c_str(), DX_IMAGESAVETYPE_PNG, 6, TRUE, 6);
	PlaySoundMem(this->m_sound, DX_PLAYTYPE_BACK, TRUE);
	Setting.ScreenShot.PRTSC_ELE++;
	Setting.Save(filePath);
}