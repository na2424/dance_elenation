#ifndef NOTE_BASE_H
#define NOTE_BASE_H

//スコアに関する構造体
class ScoreSt
{
public:
	int m_ScoreNum;            //目に見えるスコアを入れる変数。本体はグローバル変数のscore。
	double m_ScoreSa;            //本体のスコアと目に見えるスコアとの差を入れるための変数
};

//ライフに関する構造体
class LifeSt
{
public:
	int m_Lifeflag;            //コンボが増える際のアニメーションで使う変数
	int m_Life;                //コンボ数を入れるための変数
};

//NOTEに関する基底クラス
class NoteBase
{
public:
	int m_NoteCount0;
	int m_NoteCount1;
	int m_NoteTotle;
	int m_Maxnote;                //総ノート数を入れる変数
	int m_JudgmentResult;        //各判定結果を入れるための変数

	//int static maxc(int x, int y);
	static double MyMod(double x, double y);
	//コンストラクタ
	NoteBase(void) :
		m_NoteCount0(0),
		m_NoteCount1(0),
		m_NoteTotle(0),
		m_Maxnote(0),
		m_JudgmentResult(-1) {}
};

#endif