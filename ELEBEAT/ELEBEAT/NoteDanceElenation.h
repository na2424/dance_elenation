#ifndef NOTE_DANCE_ELENATION_H
#define NOTE_DANCE_ELENATION_H

#include <vector>
#include <memory>

#include "NoteBase.h"
#include "define.h"
#include "NumberDraw.h"
#include "Enums.h"

class Combo;
class JudgeCount;
class KeyInput4Key;

//ノートに関する構造体(クラス)4keys
class NoteDanceElenation : public NoteBase
{
private:

	int m_cycle;
	int m_cycle2;
	//１拍分のピクセル.
	double pixelPerBeat;
	//動かしたピクセル
	double m_movedPixel;
	double m_offsetPix;
	double m_rote[4];

	//-------------------------
	// 画像.
	//-------------------------
	Image m_downHoldHeadInactiveImage;
	Image m_downHoldHeadActiveImage;
	Image m_holdBottomCapInactiveImages[4];
	Image m_holdBottomCapActiveImages[4];
	Image m_holdBodyInactiveImages[4];
	Image m_holdBodyActiveImages[4];
	Image m_note8Image;

	Image m_Note4Images[16];
	Image m_Note8[16];
	Image m_Note12[16];
	Image m_Note16[16];

public:

	std::vector<double> m_Note[4];        //ノートを入れるための３次元配列変数。                        [横][縦][何番目か]
	std::vector<int> m_NoteFlag[4];    //ノートのフラグを入れるための３次元配列変数。                [横][縦][何番目か]
	std::vector<int> m_NoteFlagTime[4];    //ノートのフラグが１か０になったときの時間を入れるための３次元配列変数。[横][縦][時間]
	std::vector<int> m_NoteTime[4];    //ノートの押されるべき時間を入れるための３次元配列変数。[横][縦][時間]
	int m_KeyoutTime[4];    //LONGNOTEでキーを離した時の時間を入れる変数
	int m_LongFlag[4];
	int m_LongEndFlag[4];
	int m_NoteCount[4];
	int m_U[4];                //OK!やNGを微妙に動かすための変数

	int m_JudgeLinePositionY;
	int m_KeyState[4];
	int m_BeatTime;
	int m_BeatNowTime;
	double m_Fps;                //fpsは60
	double m_Speed;            //1フレームあたりに動かすピクセル→speedに代入
	double m_InvBps16;

	double GAPn;
	double GAPs;

	//-------------------
	// 関数.
	//-------------------

	static void GetKeyTime(const std::shared_ptr<KeyInput4Key>& f_keyinput, const long int now, const int matrix);
	void LoadNoteSkin(NoteSkinType noteSkinType);
	void GetKeyNote(const std::shared_ptr<KeyInput4Key>& f_keyinput, int matrix);
	void GetKeyTimeAuto4Key(const std::shared_ptr<KeyInput4Key>& f_keyinput, long int now, int matrix);
	void GetKeyNoteAuto4Key(const std::shared_ptr<KeyInput4Key>& f_keyinput, int matrix);
	void NoteJudgement(const std::shared_ptr<ScoreSt>& f_score, const std::shared_ptr<Combo>& f_combo, const std::shared_ptr<KeyInput4Key>& f_keyinput, const std::shared_ptr<LifeSt>& f_life, int m, int *k, const long int now, int i, int Auto_flag);
	void LongNoteHold4Key(const std::shared_ptr<ScoreSt>& fScore, const std::shared_ptr<KeyInput4Key>& fKeyinput, int m, int *k, long int now, int i, bool isAuto);
	void DrawHoldBody(const std::shared_ptr<KeyInput4Key>& f_keyinput, int m, int i, double calculater, double calculater2, double offsetPix, long int now, int picHandles[], int picHandles2[]);
	void NoteBeatCycle(int startTime);
	void SetSpeed(double bpm);
	void SetPixelPerBeat(void);
	void SetMovedPixel(double now);
	void SetOffsetPix(double offset, double bpm);
	//描画系関数
	void NoteDraw(int m, int i);
	void NoteFlatDraw(int m, int i);
	void DrawLongNoteBottom(int m, int i/*, const std::shared_ptr<KeyInputClass4key>& f_keyinput*/);
	void DrawLongNoteBody(int m, int i, const std::shared_ptr<KeyInput4Key>& f_keyinput);
	void DrawLongNoteTop(int m, int i/*, const std::shared_ptr<KeyInputClass4key>& f_keyinput*/);
	void DrawLongNoteBottomActive(int m, int i, const std::shared_ptr<KeyInput4Key>& f_keyinput);
	void DrawLongNoteTopActive(int m, int i, const std::shared_ptr<KeyInput4Key>& f_keyinput);
	void DrawNoteBehind(int m, int i/*, const std::shared_ptr<KeyInputClass4key>& f_keyinput*/);
	//上下反転して描画.
	static void DrawRectYMirrorGraph(int x, int y, int srcX, int srcY, int srcWidth, int srcHeight, int graphHandle);

	NoteDanceElenation();
};

#endif