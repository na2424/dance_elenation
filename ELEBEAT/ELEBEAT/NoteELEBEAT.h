#ifndef NOTE_ELEBEAT_H
#define NOTE_ELEBEAT_H

#include "NoteBase.h"
#include "define.h"    // for MAXX
#include "memory"      // for shared_ptr

class KeyInput;
class JudgeCount;
class Combo;

//9Keysと16Keysのノートに関する構造体(クラス)
class NoteElebeat : public NoteBase
{
public:
	int note[4][4][MAXX];        //ノートを入れるための３次元配列変数。                        [横][縦][何番目か]
	int m_NoteKind[4][4][MAXX];    //ノートのフラグを入れるための３次元配列変数。                [横][縦][何番目か]
	int m_NoteHitTime[4][4][MAXX];    //ノートのフラグが１か０になったときの時間を入れるための３次元配列変数。[横][縦][時間]
	int note_time[4][4][MAXX];    //ノートの押されるべき時間を入れるための３次元配列変数。[横][縦][時間]
	int m_KeyoutTime[4][4];    //LONGNOTEでキーを離した時の時間を入れる変数
	int m_LongFlag[4][4];
	int m_LongEndFlag[4][4];
	int m_U[4][4];                //OK!やNGを微妙に動かすための変数
	int m_NoteCount[4][4];
	void GetKeyTime(const std::shared_ptr<KeyInput>& f_keyinput, long int *now, int matrix) const;
	void GetKeyNote(const std::shared_ptr<KeyInput>& f_keyinput, int matrix);
	void NoteJudgement(const std::shared_ptr<ScoreSt>& f_score, const std::shared_ptr<Combo>& f_combo, const std::shared_ptr<KeyInput>& f_keyinput, int m, int n, int *k, long int now, int i, bool autoFlag);

	void LongNoteHold(NoteElebeat *note_mn, ScoreSt *f_score, KeyInput *f_keyinput, int m, int n, int *k, long int now, bool autoFlag);
	void GetKeyTimeAuto(const std::shared_ptr<KeyInput>& f_keyinput, long int *now, int matrix, const std::shared_ptr<NoteElebeat>& f_NoteClass);
	void GetKeyNoteAuto(const std::shared_ptr<KeyInput>& f_keyinput, const std::shared_ptr<NoteElebeat>& note_mn, int matrix);
	//コンストラクタ
	NoteElebeat(void);
};

#endif