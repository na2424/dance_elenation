#ifndef SCREEN_SHOT_H
#define SCREEN_SHOT_H

#include "DxLib.h"
#include "define.h"

class ScreenShot
{
private:
	tstring m_fileNameCache;
	tstring m_fileName;
	int m_sound;

public:
	ScreenShot();
	void CaptureScreenshot();
};
#endif