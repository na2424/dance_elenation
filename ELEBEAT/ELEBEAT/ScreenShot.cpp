#include "DxLib.h"
#include "ScreenShot.h"
#include "extern.h"
#include "SpecialFiles.h"

using namespace std;
using namespace DxLib;

ScreenShot::ScreenShot()
{
	m_fileNameCache.clear();
	m_fileName.clear();
	this->m_sound = LoadSoundMem((SpecialFiles::DEFAULT_THEMES_DIR + tstring("Sounds\\ScreenShot.mp3")).c_str());
}

void ScreenShot::CaptureScreenshot()
{
	tstring filePath = SpecialFiles::DATA_DIR + tstring("Setting.ini");

	Setting.Load(filePath);
	this->m_fileNameCache = to_string(Setting.ScreenShot.PRTSC_ELE);
	this->m_fileName = "ScreenShot\\PRTSC_ELE_" + this->m_fileNameCache + ".png";

	SaveDrawScreen(0, 0, WIN_WIDTH, WIN_HEIGHT, this->m_fileName.c_str(), DX_IMAGESAVETYPE_PNG, 6, TRUE, 6);
	PlaySoundMem(this->m_sound, DX_PLAYTYPE_BACK, TRUE);
	Setting.ScreenShot.PRTSC_ELE++;
	Setting.Save(filePath);
}