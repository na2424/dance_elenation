﻿#include "KeysSelect.h"
#include "DxLib.h"
#include "extern.h"
#include "BlueLightBall.h"
#include "header.h"
#include "Screen.h"
#include "NumberDraw.h"
#include "Input.h"
#include "savedata_class.h"
#include "MouseClickObject.h"
#include "LuaHelper.h"
#include "Color.h"
#include "SpecialFiles.h"


using namespace DxLib;

KeysSelect::KeysSelect() :
	animationFrame(20),
	m_time(30),
	startTime(0),
	flag_a(1),
	flag_b(0), selectCursorImage(0), keysselectOverImage(0), informationImage(0),
	keysSelectMusicSound(0), moveSound(0), BackSound(0)
{
	offsetModelPosition = std::make_unique<VECTOR>();
	offsetModelPosition->x = 0;
	offsetModelPosition->y = 0;
	offsetModelPosition->z = 0;
	numberDraw = std::make_unique<TimerDrawer>();
	m_lightballList = std::make_unique<BlueLightBallList>();
	m_mouseClickObjectList = std::make_unique<MouseClickObjectList>();
};

//画像をロードする
void KeysSelect::LoadSceneImage()
{
	g_BackImage = LoadGraph((SpecialFiles::DEFAULT_THEMES_DIR + tstring("Graphics/back.jpg")).c_str());
	selectCursorImage = LoadGraph((SpecialFiles::DEFAULT_THEMES_DIR + tstring("Graphics/KeysSelect/KeySelectCursor.png")).c_str());
}

//音声をロードする
void KeysSelect::LoadSceneSound()
{
	moveSound = LoadSoundMem((SpecialFiles::DEFAULT_THEMES_DIR + tstring("Sounds/KeysSelect/move.ogg")).c_str());
	BackSound = LoadSoundMem((SpecialFiles::DEFAULT_THEMES_DIR + tstring("Sounds/Common back.ogg")).c_str());
}

//3Dモデル読み込み
void KeysSelect::LoadSceneModel()
{
	ringModel[0] = MV1LoadModel(_T("MMD/RING.x"));
	ringModel[1] = MV1LoadModel(_T("MMD/RING.x"));
}

void KeysSelect::Initialize(const std::shared_ptr<Music>& musicP)
{
	LoadSceneImage();
	LoadSceneSound();
	LoadSceneModel();

	//Lua読み込み
	ReadLua();

	auto save_data_option = std::make_unique<OptionSaveData>();
	//セーブデータの読み込み(無ければ書き出し)
	save_data_option->LoadDat();

	// ３Ｄモデルのスケールを変更
	DxLib::MV1SetScale(ModelHandle, VGet(12.0f, 12.0f, 12.0f));

	MV1SetScale(ringModel[0], VGet(0.01f, 0.01f, 0.01f));
	MV1SetScale(ringModel[1], VGet(0.01f, 0.01f, 0.01f));
	MV1SetMaterialDrawBlendMode(ModelHandle, 1, DX_BLENDMODE_ADD);

	if (OptionSaveData::g_IsFullScreen)
		offsetModelPosition->x = 22;

	//パーティクル初期化
	m_lightballList->Init(nullptr, "Graphics/Common/particle.png");

	SetDrawArea(0, 0, WIN_WIDTH, WIN_HEIGHT);

	//スタート時間
	startTime = GetNowCount();

	// 音声をループ再生する
	DxLib::PlaySoundMem(keysSelectMusicSound, DX_PLAYTYPE_LOOP);
	//フェードイン
	Screen::FadeIn();
	// ムービーを再生状態にします
	//PlayMovieToGraph(MovieGraphHandle, DX_PLAYTYPE_LOOP);
}

void KeysSelect::Update(const std::shared_ptr<Music>& musicP)
{
	auto& input = Input::GetInstance();

	m_count++;
	m_fps->Update();
	//タイマー処理
	if (!OptionSaveData::g_IsEventMode)
		m_time = static_cast<int>(21 - (GetNowCount() - startTime) / 1000);
	else
		m_time = 88;

	// キー入力の取得
	clsDx();
	input.GetKey();
	//マウスの座標を取得
	input.GetMouse();

	m_mouseClickObjectList->Update();

	//選択が変わる際のアニメーションフラグ
	if (animationFrame < 30) animationFrame++;

	if (input.m_Key[1][1] > 1)
		offsetModelPosition->x++;

	if (input.m_Key[1][2] > 1)
		offsetModelPosition->x--;

	if (input.m_Key[2][1] > 1)
		offsetModelPosition->y++;

	if (input.m_Key[2][2] > 1)
		offsetModelPosition->y--;

	if (input.m_Key[3][1] > 1)
		offsetModelPosition->z += 3;

	if (input.m_Key[3][2] > 1)
		offsetModelPosition->z -= 3;

	constexpr float OFFSET_X = 480.0f;
	constexpr float OFFSET_Y = 363.0f;
	constexpr float OFFSET_Z = -576.0f;

	// 画面に映る位置に３Ｄモデルを移動
	MV1SetPosition(ModelHandle, VGet(OFFSET_X + offsetModelPosition->x, OFFSET_Y - 40 + offsetModelPosition->y, OFFSET_Z + 186 + offsetModelPosition->z));
	MV1SetPosition(ringModel[0], VGet(OFFSET_X + offsetModelPosition->x, OFFSET_Y - 20 + offsetModelPosition->y, OFFSET_Z + 108));
	MV1SetPosition(ringModel[1], VGet(OFFSET_X + offsetModelPosition->x, OFFSET_Y - 20 + offsetModelPosition->y, OFFSET_Z + 108));

	if (input.keyLeft == 1 && casol_high > 0)
	{
		casol_high--;
		animationFrame = 0;
		if (flag_a == 2 && casol_high == 0)
		{
			flag_a = 1;
			flag_b = 1;
		}
		//カーソルの移動の音を鳴らす
		PlaySoundMem(moveSound, DX_PLAYTYPE_BACK);
	}
	else if (input.keyRight == 1 && casol_high < 2)
	{
		casol_high++;
		animationFrame = 0;
		if (flag_a == 1 && casol_high == 2)
		{
			flag_a = 2;
			flag_b = 1;
		}
		//カーソルの移動の音を鳴らす
		PlaySoundMem(moveSound, DX_PLAYTYPE_BACK);
	}

	//keys設定
	if (casol_high == 0)
		SceneManager::g_PlayMode = SelectKeys::KEYS_4;
	if (casol_high == 1)
		SceneManager::g_PlayMode = SelectKeys::KEYS_9;
	if (casol_high == 2)
		SceneManager::g_PlayMode = SelectKeys::KEYS_16;

	//timeが無くなるか、決定キーが押されたら
	if (m_time <= -1 || input.keyEnter == 1)
	{
		//次に曲選択画面に移るようする
		SceneManager::SetNextScene(Scenes::MUSIC_SELECT);
		return;
	}

	//ESCキーが押されたら
	if (input.keyEsc == 1)
	{
		SceneManager::SetNextScene(Scenes::TITLE);
		PlaySoundMem(BackSound, DX_PLAYTYPE_BACK);
		return;
	}

	//マウス操作関係
	for (auto u = 0; u < 2; u++)
	{
		if (((place_y + 77 < input.mouseY) && (input.mouseY < place_y + 327)) && ((place_x + spacing * u + 74 < input.mouseX) && (input.mouseX < place_x + spacing * u + 324)))
		{
			if (input.mouseClick == 1)
			{
				which = u;
				SceneManager::SetNextScene(Scenes::MUSIC_SELECT);

				if (flag_a == 1)
				{
					if (which == 0)
					{
						SceneManager::g_PlayMode = SelectKeys::KEYS_4;
						//ループ脱出フラグ
						which = 6;
						break;
					}
					else if (which == 1)
					{
						SceneManager::g_PlayMode = SelectKeys::KEYS_9;
						which = 6;
						break;
					}
				}
				//flag_a->2の時
				if (flag_a == 2)
				{
					if (which == 0)
					{
						SceneManager::g_PlayMode = SelectKeys::KEYS_9;
						which = 6;
						break;
					}
					else if (which == 1)
					{
						SceneManager::g_PlayMode = SelectKeys::KEYS_16;
						which = 6;
						break;
					}
				}
			}
		}
	}

	if (which == 6)
		return;

	if (abs(place_x_num - (place_x + spacing * casol_high)) < 1)
		flag_b = 0;

	//３段階の速さでそれらしく
	if (0 > place_x_num - (place_x + spacing * casol_high) && place_x_num - (place_x + spacing * casol_high) > -10)
		place_x_num += 2;
	else if (0 < place_x_num - (place_x + spacing * casol_high) && place_x_num - (place_x + spacing * casol_high) < 10)
		place_x_num -= 2;
	else if (0 > place_x_num - (place_x + spacing * casol_high) && place_x_num - (place_x + spacing * casol_high) > -60)
		place_x_num += 10;
	else if (0 < place_x_num - (place_x + spacing * casol_high) && place_x_num - (place_x + spacing * casol_high) < 60)
		place_x_num -= 10;
	else if (place_x_num < place_x + spacing * casol_high)
		place_x_num += 60;
	else if (place_x_num > place_x + spacing * casol_high)
		place_x_num -= 60;

	//パーティクル位置を更新
	m_lightballList->Update(nullptr);

	//--------------------
	// 描画
	//--------------------

	// 画面の初期化
	DxLib::ClearDrawScreen();

	//背景の描画
	DrawGraph(0, 0, g_BackImage, FALSE);

	//パーティクルの描画
	SetDrawBlendMode(DX_BLENDMODE_ADD, 240);
	m_lightballList->Draw();
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	//3Dモデルを回転
	MV1SetRotationXYZ(ModelHandle, VGet(0.0f, m_count * 2.0f * PI_F / 180.0f, 0.0f));
	MV1SetRotationXYZ(ringModel[0], VGet(m_count * 0.5f * -PI_F / 180.0f, m_count * 0.5f * -PI_F / 180.0f, 0.0f));
	MV1SetRotationXYZ(ringModel[1], VGet(0.0f, m_count * 0.5f * -PI_F / 120.0f, m_count * 0.5f * -PI_F / 120.0f));

	MV1DrawModel(ModelHandle);
	MV1DrawModel(ringModel[0]);
	MV1DrawModel(ringModel[1]);

	//黒半透明の四角をアニメーション描画
	DrawBackBox(m_count);

	//２０フレームまでブレンドON(フェードイン)
	if (m_count < 20)
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, static_cast<int>((255 / 20.0)*m_count));

	//カーソルの画像表示
	if (flag_b == 0 && flag_a == 1)
		DrawGraph(place_x_num, place_y, selectCursorImage, true);
	if (flag_b == 0 && flag_a == 2)
		DrawGraph(place_x_num - spacing, place_y, selectCursorImage, true);
	if (flag_b == 1 && flag_a == 1)
		DrawGraph(place_x + spacing * 0, place_y, selectCursorImage, true);
	if (flag_b == 1 && flag_a == 2)
		DrawGraph(place_x + spacing * 1, place_y, selectCursorImage, true);

	//4keys,9keys,16keys画像の描画
	const auto modeNum = static_cast<int>(keysImage.size());
	if (flag_b == 0 && flag_a == 1)
	{
		for (auto i = 0; i < modeNum; ++i)
			DrawGraph(place_x + spacing * i + 74, place_y + 77, keysImage.at(i), FALSE);
	}
	if (flag_b == 0 && flag_a == 2)
	{
		for (auto i = 0; i < modeNum; ++i)
			DrawGraph(place_x + spacing * (static_cast<int>(i) - 1) + 74, place_y + 77, keysImage.at(i), FALSE);
	}
	if (flag_b == 1 && flag_a == 1)
	{
		for (auto i = 0; i < modeNum; ++i)
			DrawGraph(place_x - (place_x_num - 100) + spacing * i + 74, place_y + 77, keysImage.at(i), FALSE);
	}
	if (flag_b == 1 && flag_a == 2)
	{
		for (auto i = 0; i < modeNum; ++i)
			DrawGraph(place_x - (place_x_num - 100) + spacing * (i + 1) + 74, place_y + 77, keysImage.at(i), FALSE);
	}

	//9keysmode,16keysmode画像の描画
	if (animationFrame < 30)
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, static_cast<int>((255 / 30.0)*animationFrame));

	if (SceneManager::g_PlayMode == SelectKeys::KEYS_4)
		DrawRotaGraph(WIN_WIDTH / 2 + 50, 170, 1, 0, keysModeImage[0], TRUE);
	if (SceneManager::g_PlayMode == SelectKeys::KEYS_9)
		DrawRotaGraph(WIN_WIDTH / 2 + 50, 170, 1, 0, keysModeImage[1], TRUE);
	if (SceneManager::g_PlayMode == SelectKeys::KEYS_16)
		DrawRotaGraph(WIN_WIDTH / 2 + 50, 170, 1, 0, keysModeImage[2], TRUE);

	//ブレンドOFF
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	/*----------------------------------------------*/

	//INFORMAITOM画像の描画
	DrawGraph(-10, 100, informationImage, true);

	//フレームの画像表示
	DrawFrame(m_count);

	//マウスカーソルの表示
	DxLib::DrawRotaGraph(input.mouseX, input.mouseY, 1.0f, m_count / 42.0, input.casolPic, TRUE);

	m_mouseClickObjectList->Draw();

	//描画した画面の反映
	m_fps->Draw();        //描画
	DxLib::ScreenFlip();
	m_fps->Wait();        //待機

	if (input.keyPrtsc == 1)
		m_ssc->CaptureScreenshot();
}

//黒半透明の四角をアニメーション描画
void KeysSelect::DrawBackBox(const int count) const
{
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 128);

	(count < 20) ?
		DrawBox(0, 170 - count * 4, WIN_WIDTH, 170 + count * 4, Color::GetInstance().Black(), TRUE) :
		DrawBox(0, 170 - 20 * 4, WIN_WIDTH, 170 + 20 * 4, Color::GetInstance().Black(), TRUE);

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
}

void KeysSelect::Finalize(const std::shared_ptr<Music>& musicP)
{
	//サウンドの停止
	StopSoundMem(keysSelectMusicSound);
	StopSoundMem(moveSound);

	DeleteGraph(g_BackImage);
	for (int i = 0; i < 3; ++i)
	{
		DeleteGraph(keysImage[i]);
		DeleteGraph(keysModeImage[i]);
	}
	DeleteGraph(keysselectOverImage);
	DeleteSoundMem(keysSelectMusicSound);
	DeleteSoundMem(moveSound);

	//立方体のブレンドを元に戻す
	MV1SetMaterialDrawBlendMode(ModelHandle, 1, DX_BLENDMODE_NOBLEND);

	if (SceneManager::g_CurrentScene == Scenes::MUSIC_SELECT)
		Screen::TransitionKeysSelectToMusicSelect();

	//フェードアウト
	DxLib::clsDx();
	Screen::FadeOut();
}

void KeysSelect::DrawFrame(const int &count) const
{
	if (count < 20)
	{
		DrawGraph(0, -200 + (10 * count), keysselectOverImage, TRUE);
		DrawGraph(0, WIN_HEIGHT - 80 + 200 - (10 * count), g_underBarImage, TRUE);
	}
	else
	{
		DrawGraph(0, 0, keysselectOverImage, TRUE);
		DrawGraph(0, WIN_HEIGHT - 80, g_underBarImage, TRUE);
		//時間の描画
		numberDraw->DrawMenuTimer(m_time);
	}
}

void KeysSelect::ReadLua()
{
	//lua読み込み
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);
	if (luaL_loadfile(L, "Themes/DANCE ELENATION/lua/_file_key_select.lua") || lua_pcall(L, 0, 0, 0))
	{
		printfDx(_T("key_select.luaを開けませんでした\n"));
		printfDx(_T("error : %s\n"), lua_tostring(L, -1));
		lua_close(L);
		return;
	}

	//画像ファイル名をスタックに積む
	lua_getglobal(L, "keys4_pic");
	lua_getglobal(L, "keys9_pic");
	lua_getglobal(L, "keys16_pic");
	lua_getglobal(L, "keysmode4_pic");
	lua_getglobal(L, "keysmode9_pic");
	lua_getglobal(L, "keysmode16_pic");
	lua_getglobal(L, "keysselect_over");
	lua_getglobal(L, "information");
	lua_getglobal(L, "keys_select_music");
	lua_getglobal(L, "move_sound");

	const int num = lua_gettop(L);
	std::vector<tstring> cg_name;

	if (num == 0)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("エラー"), _T("No stack"), MB_OK);
	}
	for (auto i = num; i > 0; i--)
	{
		cg_name.emplace_back(lua_tostring(L, i));
	}
	TCHAR assetName[100] = {};

	auto count = 1;

	const auto setImageAsset = [&](int &asset)
	{
		_stprintf_s(assetName, sizeof(assetName), _T("Themes/DANCE ELENATION/Graphics/KeysSelect/%s"), cg_name[num - count].c_str());
		asset = LoadGraph(assetName);
		memset(assetName, _T('\0'), sizeof(assetName));
		count++;
	};

	const auto setSoundAsset = [&](int &asset)
	{
		_stprintf_s(assetName, sizeof(assetName), _T("Themes/DANCE ELENATION/Sounds/KeysSelect/%s"), cg_name[num - count].c_str());
		asset = LoadSoundMem(assetName);
		memset(assetName, _T('\0'), sizeof(assetName));
		count++;
	};

	setImageAsset(keysImage[0]);
	setImageAsset(keysImage[1]);
	setImageAsset(keysImage[2]);
	setImageAsset(keysModeImage[0]);
	setImageAsset(keysModeImage[1]);
	setImageAsset(keysModeImage[2]);
	setImageAsset(keysselectOverImage);
	setImageAsset(informationImage);
	setSoundAsset(keysSelectMusicSound);
	setSoundAsset(moveSound);

	//lua閉じる
	lua_close(L);
}