#include "DxLib.h"
//// MSR_NuiApi.hの前にWindows.hをインクルードする
//Windows7のみ対応、他だと落ちるので調査中
//#include "NuiApi.h"
//
//#include "KinectSDKforDxLib_v1.0.h"
//KinectDX kinectdx;
////lookups for color tinting based on player index
//static const int g_IntensityShiftByPlayerR[] = { 0, 2, 0, 2, 0, 0, 2, 0 };
//static const int g_IntensityShiftByPlayerG[] = { 0, 2, 2, 0, 2, 0, 0, 1 };
//static const int g_IntensityShiftByPlayerB[] = { 0, 0, 2, 2, 0, 2, 0, 2 };
//RGBQUAD m_rgbWk[640 * 480];
//        //-------------------------------------------------------------------
//        // Zero out member variables
//        //-------------------------------------------------------------------
//        void KinectDX::Nui_Zero()
//        {
//            if (m_pNuiSensor)
//            {
//                m_pNuiSensor->Release();
//                m_pNuiSensor = NULL;
//            }
//
//            this->m_DepthEvent = NULL;
//            this->m_ImageEvent = NULL;
//            this->m_SkeletonEvent = NULL;
//            this->m_DepthStreamHandle = NULL;
//            this->m_VideoStreamHandle = NULL;
//        }
//
//        //-------------------------------------------------------------------
//        // Kinectの初期化
//        //-------------------------------------------------------------------
//        HRESULT KinectDX::Nui_Init(){
//            HRESULT  hr;
//
//            if (!m_pNuiSensor){
//                // インデックス番号で初期化（0番）
//                hr = NuiCreateSensorByIndex(0, &m_pNuiSensor);
//
//                // hrが0未満であれば
//                if (FAILED(hr)){
//                    return hr;
//                }
//
//                SysFreeString(m_instanceId);
//
//                m_instanceId = m_pNuiSensor->NuiDeviceConnectionId();
//            }
//
//            // イベントハンドラの生成
//            this->m_ImageEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
//            this->m_SkeletonEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
//            this->m_DepthEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
//
//            this->m_VideoStreamHandle = NULL;
//            this->m_DepthStreamHandle = NULL;
//
//            DWORD nuiFlags = NUI_INITIALIZE_FLAG_USES_DEPTH_AND_PLAYER_INDEX | NUI_INITIALIZE_FLAG_USES_SKELETON | NUI_INITIALIZE_FLAG_USES_COLOR;
//            hr = m_pNuiSensor->NuiInitialize(nuiFlags);
//            if (hr == E_NUI_SKELETAL_ENGINE_BUSY)
//            {
//                nuiFlags = NUI_INITIALIZE_FLAG_USES_DEPTH | NUI_INITIALIZE_FLAG_USES_COLOR;
//                hr = m_pNuiSensor->NuiInitialize(nuiFlags);
//            }
//
//            // 初期化失敗
//            if (FAILED(hr)){
//                if (E_NUI_DEVICE_IN_USE == hr){
//                    DxLib::printfDx("[Kinect] 初期化失敗：E_NUI_DEVICE_IN_USE \n");
//                }
//                else{
//                    DxLib::printfDx("[Kinect] 初期化失敗：よく分からないエラー \n");
//                }
//                return hr;
//            }
//
//            // 骨格トラッキングの初期化
//            if (HasSkeletalEngine(m_pNuiSensor)){
//                hr = m_pNuiSensor->NuiSkeletonTrackingEnable(m_SkeletonEvent, 0);
//                if (FAILED(hr)){
//                    DxLib::printfDx("[Kinect] トラッキング初期化失敗：IDS_ERROR_SKELETONTRACKING \n");
//                    return hr;
//                }
//            }
//
//            // RGBカメラ初期化
//            hr = m_pNuiSensor->NuiImageStreamOpen(
//                NUI_IMAGE_TYPE_COLOR,
//                NUI_IMAGE_RESOLUTION_640x480,
//                0,
//                2,
//                this->m_ImageEvent,
//                &m_VideoStreamHandle);
//
//            if (FAILED(hr)){
//                DxLib::printfDx("[Kinect] RGBカメラ初期化失敗：IDS_ERROR_VIDEOSTREAM \n");
//                return hr;
//            }
//
//            // 深度カメラ初期化
//            hr = m_pNuiSensor->NuiImageStreamOpen(
//                HasSkeletalEngine(m_pNuiSensor) ? NUI_IMAGE_TYPE_DEPTH_AND_PLAYER_INDEX : NUI_IMAGE_TYPE_DEPTH,
//                NUI_IMAGE_RESOLUTION_640x480,
//                0,
//                2,
//                this->m_DepthEvent,
//                &m_DepthStreamHandle);
//
//            if (FAILED(hr)){
//                DxLib::printfDx("[Kinect] 深度カメラ初期化失敗：IDS_ERROR_DEPTHSTREAM \n");
//                return hr;
//            }
//
//            DxLib::printfDx("[Kinect] 初期化完了？ \n");
//            return hr;
//        }
//
//        //-------------------------------------------------------------------
//        // Uninitialize Kinect
//        //-------------------------------------------------------------------
//        void KinectDX::Nui_UnInit()
//        {
//            if (m_pNuiSensor)
//            {
//                m_pNuiSensor->NuiShutdown();
//            }
//            if (m_SkeletonEvent && (m_SkeletonEvent != INVALID_HANDLE_VALUE))
//            {
//                CloseHandle(m_SkeletonEvent);
//                m_SkeletonEvent = NULL;
//            }
//            if (m_DepthEvent && (m_DepthEvent != INVALID_HANDLE_VALUE))
//            {
//                CloseHandle(m_DepthEvent);
//                m_DepthEvent = NULL;
//            }
//            if (m_ImageEvent && (m_ImageEvent != INVALID_HANDLE_VALUE))
//            {
//                CloseHandle(m_ImageEvent);
//                m_ImageEvent = NULL;
//            }
//
//            if (m_pNuiSensor)
//            {
//                m_pNuiSensor->Release();
//                m_pNuiSensor = NULL;
//            }
//        }
//
//        //-------------------------------------------------------------------
//        // 初回イメージハンドル作成
//        //-------------------------------------------------------------------
//        HRESULT KinectDX::InitDrawImage(int *GrHandle, HANDLE StreamHandle, BASEIMAGE *BaseImage){
//            ::WaitForSingleObject(m_ImageEvent, INFINITE);
//            ::WaitForSingleObject(m_DepthEvent, INFINITE);
//            // カメラデータの取得
//            NUI_IMAGE_FRAME imageFrame;
//            HRESULT hr = m_pNuiSensor->NuiImageStreamGetNextFrame(StreamHandle, 0, &imageFrame);
//            if (FAILED(hr))    return hr;
//
//            // 画像データの取得
//            INuiFrameTexture * pTexture = imageFrame.pFrameTexture;
//            NUI_LOCKED_RECT LockedRect;
//            pTexture->LockRect(0, &LockedRect, NULL, 0);
//
//            DWORD frameWidth, frameHeight;
//            if (LockedRect.Pitch != 0){
//                // 画面サイズを取得
//                NuiImageResolutionToSize(imageFrame.eResolution, frameWidth, frameHeight);
//                BaseImage->Width = frameWidth;
//                BaseImage->Height = frameHeight;
//
//                // Dxlibで使用する画像データの構造体設定
//                if (StreamHandle == m_VideoStreamHandle){
//                    BaseImage->GraphData = static_cast<BYTE *>(LockedRect.pBits);
//                    BaseImage->Pitch = LockedRect.Pitch;                    // ピッチは横１ラインあたりのByte数
//                }
//                else{
//                    // draw the bits to the bitmap
//                    RGBQUAD * rgbrun = m_rgbWk;
//                    USHORT * pBufferRun = (USHORT *)LockedRect.pBits;        // 深度データの取り込み？
//
//                    // end pixel is start + width*height - 1
//                    USHORT * pBufferEnd = pBufferRun + (frameWidth * frameHeight);
//
//                    USHORT RealDepth;    // 深度情報
//                    USHORT Player;        // プレイヤーID情報
//
//                    // 背景描画，プレイーヤーに色を付けない
//                    while (pBufferRun < pBufferEnd){
//                        RealDepth = NuiDepthPixelToDepth(*pBufferRun);
//                        Player = NuiDepthPixelToPlayerIndex(*pBufferRun);
//                        BYTE intensity = (BYTE)~(RealDepth >> 5);    // 13bit → 8bit
//                        rgbrun->rgbRed = intensity;
//                        rgbrun->rgbGreen = intensity;
//                        rgbrun->rgbBlue = intensity;
//                        ++pBufferRun;
//                        ++rgbrun;
//                    }
//
//                    BaseImage->GraphData = m_rgbWk;                    // RGBデータの直接書き込み
//                    BaseImage->Pitch = frameWidth * 4;                // ピッチは横１ラインあたりのByte数
//                }
//            }
//            else{
//                OutputDebugString("Buffer length of received texture is bogus\r\n");
//                return -1;
//            }
//
//            // カメラデータの解放
//            pTexture->UnlockRect(0);
//            m_pNuiSensor->NuiImageStreamReleaseFrame(StreamHandle, &imageFrame);
//
//            // 最初の場合はグラフィックハンドルの作成と映像の転送を一度に行う
//            *GrHandle = CreateGraphFromBaseImage(BaseImage);
//
//            return hr;
//        }
//
//        // デストラクタ
//        KinectDX::~KinectDX(){
//
//            // ソフトイメージ解放
//            DeleteSoftImage(softhandle);
//
//            Nui_UnInit();
//            Nui_Zero();
//            SysFreeString(m_instanceId);
//            NuiShutdown();
//        }
//
//
//        //-------------------------------------------------------------------
//        // モーターの操作    （-27〜+27　：それ以上も以下も頭打ちで一応動作する）
//        //-------------------------------------------------------------------
//        HRESULT KinectDX::SetTiltMotor(int angle){
//            // 前回更新から1350ms以上経過していないと動作させない
//            if ((DxLib::GetNowCount() - this->lastSetSensorAngleTime) < 1350){
//                return E_FAIL;
//            }
//            ::NuiCameraElevationSetAngle(angle);
//            this->lastSetSensorAngleTime = DxLib::GetNowCount();    // Windowsを起動してからのms時間を取得
//            return S_OK;
//        }
//
//
//        //-------------------------------------------------------------------
//        // RGBカメラからデータを取得してグラフィックハンドルを返す
//        //-------------------------------------------------------------------
//        int KinectDX::GetVideoGrHandle(void){
//            ::WaitForSingleObject(m_ImageEvent, INFINITE);
//            // カメラデータの取得
//            NUI_IMAGE_FRAME imageFrame;
//            HRESULT hr = m_pNuiSensor->NuiImageStreamGetNextFrame(m_VideoStreamHandle, 0, &imageFrame);
//            if (FAILED(hr))    return -1;
//
//            // 画像データの取得
//            INuiFrameTexture * pTexture = imageFrame.pFrameTexture;
//            NUI_LOCKED_RECT LockedRect;
//            pTexture->LockRect(0, &LockedRect, NULL, 0);
//            if (LockedRect.Pitch != 0){
//                // Dxlibで使用する画像データの構造体設定
//                BaseVideoImage.GraphData = static_cast<BYTE *>(LockedRect.pBits);        // イメージデータをそのままコピー
//            }
//            else{
//                OutputDebugString("Buffer length of received texture is bogus\r\n");
//            }
//
//            // カメラデータの解放
//            pTexture->UnlockRect(0);
//            m_pNuiSensor->NuiImageStreamReleaseFrame(m_VideoStreamHandle, &imageFrame);
//
//            // ２回目以降はグラフィックハンドルへ映像を転送
//            ReCreateGraphFromBaseImage(&BaseVideoImage, VideoGrHandle);
//            return VideoGrHandle;
//        }
//
//        //-------------------------------------------------------------------
//        // 深度カメラからデータを取得してグラフィックハンドルを返す
//        //-------------------------------------------------------------------
//        int KinectDX::_GetDepthGrHandle(bool ColorPlayer, bool DrawBG){
//            ::WaitForSingleObject(m_DepthEvent, INFINITE);
//            // カメラデータの取得
//            NUI_IMAGE_FRAME depthFrame;
//            HRESULT hr = m_pNuiSensor->NuiImageStreamGetNextFrame(m_DepthStreamHandle, 0, &depthFrame);
//            if (FAILED(hr))    return -1;
//
//            // 画像データの取得
//            INuiFrameTexture * pTextureDepth = depthFrame.pFrameTexture;
//            NUI_LOCKED_RECT LockedRectDepth;
//            pTextureDepth->LockRect(0, &LockedRectDepth, NULL, 0);
//
//            if (LockedRectDepth.Pitch != 0){
//                // draw the bits to the bitmap
//                RGBQUAD * rgbrun = m_rgbWk;
//                USHORT * pBufferRun = (USHORT *)LockedRectDepth.pBits;        // 深度データの取り込み？
//
//                // end pixel is start + width*height - 1
//                USHORT * pBufferEnd = pBufferRun + (BaseDepthImage.Width * BaseDepthImage.Height);
//
//                USHORT RealDepth;    // 深度情報
//                USHORT Player;        // プレイヤーID情報
//
//                if (DrawBG == true){
//                    if (ColorPlayer == true){
//                        // 背景描画，プレイーヤーに色を付ける
//                        while (pBufferRun < pBufferEnd){
//                            RealDepth = NuiDepthPixelToDepth(*pBufferRun);
//                            Player = NuiDepthPixelToPlayerIndex(*pBufferRun);
//                            BYTE intensity = (BYTE)~(RealDepth >> 5);    // 13bit → 8bit
//                            rgbrun->rgbRed = intensity >> g_IntensityShiftByPlayerR[Player];
//                            rgbrun->rgbGreen = intensity >> g_IntensityShiftByPlayerG[Player];
//                            rgbrun->rgbBlue = intensity >> g_IntensityShiftByPlayerB[Player];
//                            ++pBufferRun;
//                            ++rgbrun;
//                        }
//                    }
//                    else{
//                        // 背景描画，プレイーヤーに色を付けない
//                        while (pBufferRun < pBufferEnd){
//                            RealDepth = NuiDepthPixelToDepth(*pBufferRun);
//                            Player = NuiDepthPixelToPlayerIndex(*pBufferRun);
//                            BYTE intensity = (BYTE)~(RealDepth >> 5);    // 13bit → 8bit
//                            rgbrun->rgbRed = intensity;
//                            rgbrun->rgbGreen = intensity;
//                            rgbrun->rgbBlue = intensity;
//                            ++pBufferRun;
//                            ++rgbrun;
//                        }
//                    }
//                }
//                else{
//                    if (ColorPlayer == true){
//                        // 背景なし，プレイーヤーに色を付ける
//                        while (pBufferRun < pBufferEnd){
//                            RealDepth = NuiDepthPixelToDepth(*pBufferRun);
//                            Player = NuiDepthPixelToPlayerIndex(*pBufferRun);
//                            BYTE intensity = (BYTE)~(RealDepth >> 5);    // 13bit → 8bit
//                            rgbrun->rgbRed = (intensity >> g_IntensityShiftByPlayerR[Player]) * (Player != 0);
//                            rgbrun->rgbGreen = (intensity >> g_IntensityShiftByPlayerG[Player]) * (Player != 0);
//                            rgbrun->rgbBlue = (intensity >> g_IntensityShiftByPlayerB[Player]) * (Player != 0);
//                            ++pBufferRun;
//                            ++rgbrun;
//                        }
//                    }
//                    else{
//                        // 背景なし，プレイーヤーに色を付けない
//                        while (pBufferRun < pBufferEnd){
//                            RealDepth = NuiDepthPixelToDepth(*pBufferRun);
//                            Player = NuiDepthPixelToPlayerIndex(*pBufferRun);
//                            BYTE intensity = (BYTE)~(RealDepth >> 5);    // 13bit → 8bit
//                            rgbrun->rgbRed = intensity * (Player != 0);
//                            rgbrun->rgbGreen = intensity * (Player != 0);
//                            rgbrun->rgbBlue = intensity * (Player != 0);
//                            ++pBufferRun;
//                            ++rgbrun;
//                        }
//                    }
//                }
//                //RGBデータの直接書き込み
//                BaseDepthImage.GraphData = m_rgbWk;
//            }
//            else{
//                OutputDebugString("Buffer length of received texture is bogus\r\n");
//            }
//
//            // カメラデータの解放
//            pTextureDepth->UnlockRect(0);
//            m_pNuiSensor->NuiImageStreamReleaseFrame(m_DepthStreamHandle, &depthFrame);
//
//            // ２回目以降はグラフィックハンドルへ映像を転送
//            ReCreateGraphFromBaseImage(&BaseDepthImage, DepthGrHandle);
//            return DepthGrHandle;
//        }
//
//        //-------------------------------------------------------------------
//        // カラー画像との座標変換した深度カメラからデータを取得してグラフィックハンドルを返す
//        //-------------------------------------------------------------------
//        int KinectDX::_GetDepthGrHandleFixed(bool ColorPlayer, bool DrawBG){
//            ::WaitForSingleObject(m_DepthEvent, INFINITE);
//            // カメラデータの取得
//            NUI_IMAGE_FRAME depthFrame;
//            HRESULT hr = m_pNuiSensor->NuiImageStreamGetNextFrame(m_DepthStreamHandle, 0, &depthFrame);
//            if (FAILED(hr))    return -1;
//
//            // 画像データの取得
//            INuiFrameTexture * pTextureDepth = depthFrame.pFrameTexture;
//            NUI_LOCKED_RECT LockedRectDepth;
//            pTextureDepth->LockRect(0, &LockedRectDepth, NULL, 0);
//
//            // 空のフルカラー画像を作成する
//            int handle = MakeXRGB8ColorSoftImage(640, 480);
//            int r, g, b;
//            GetTransColor(&r, &g, &b);
//            FillSoftImage(softhandle, r, g, b, 0);
//
//            if (LockedRectDepth.Pitch != 0){
//                // draw the bits to the bitmap
//                USHORT * pBufferRun = (USHORT *)LockedRectDepth.pBits;        // 深度データの取り込み？
//                USHORT RealDepth;    // 深度情報
//                USHORT Player;        // プレイヤーID情報
//
//                LONG nx, ny;
//                if (DrawBG == true){
//                    if (ColorPlayer == true){
//                        // 背景描画，プレイーヤーに色を付ける
//                        for (int y = 0; y<BaseDepthImage.Height; y++){
//                            for (int x = 0; x<BaseDepthImage.Width; x++){
//                                if (*pBufferRun == 0){
//                                    //++pBufferRun;
//                                    //continue;
//                                }
//                                RealDepth = NuiDepthPixelToDepth(*pBufferRun);
//                                Player = NuiDepthPixelToPlayerIndex(*pBufferRun);
//                                BYTE intensity = (BYTE)~(RealDepth >> 5);    // 13bit → 8bit
//                                // ズレ補正
//                                hr = ::NuiImageGetColorPixelCoordinatesFromDepthPixelAtResolution(NUI_IMAGE_RESOLUTION_640x480,
//                                    NUI_IMAGE_RESOLUTION_640x480,
//                                    NULL,
//                                    x, y,                                // coordinate in depth image space
//                                    0,                                // The depth value in depth image space.
//                                    &nx, &ny);
//                                if (hr != S_OK){
//                                    ++pBufferRun;
//                                    continue;
//                                }
//                                DrawPixelSoftImage(handle, nx, ny,
//                                    intensity >> g_IntensityShiftByPlayerR[Player],
//                                    intensity >> g_IntensityShiftByPlayerG[Player],
//                                    intensity >> g_IntensityShiftByPlayerB[Player],
//                                    0);
//                                ++pBufferRun;
//                            }
//                        }
//                    }
//                    else{
//                        // 背景描画，プレイーヤーに色を付けない
//                        for (int y = 0; y<BaseDepthImage.Height; y++){
//                            for (int x = 0; x<BaseDepthImage.Width; x++){
//                                if (*pBufferRun == 0){
//                                    ++pBufferRun;
//                                    continue;
//                                }
//                                RealDepth = NuiDepthPixelToDepth(*pBufferRun);
//                                Player = NuiDepthPixelToPlayerIndex(*pBufferRun);
//                                BYTE intensity = (BYTE)~(RealDepth >> 5);    // 13bit → 8bit
//                                // ズレ補正
//                                hr = ::NuiImageGetColorPixelCoordinatesFromDepthPixelAtResolution(NUI_IMAGE_RESOLUTION_640x480,
//                                    NUI_IMAGE_RESOLUTION_640x480,
//                                    NULL,
//                                    x, y,                                // coordinate in depth image space
//                                    0,                                // The depth value in depth image space.
//                                    &nx, &ny);
//                                if (hr != S_OK){
//                                    ++pBufferRun;
//                                    continue;
//                                }
//                                DrawPixelSoftImage(handle, nx, ny,
//                                    intensity,
//                                    intensity,
//                                    intensity,
//                                    0);
//                                ++pBufferRun;
//                            }
//                        }
//                    }
//                }
//                else{
//                    if (ColorPlayer == true){
//                        // 背景なし，プレイーヤーに色を付ける
//                        for (int y = 0; y<BaseDepthImage.Height; y++){
//                            for (int x = 0; x<BaseDepthImage.Width; x++){
//                                if (*pBufferRun == 0){
//                                    ++pBufferRun;
//                                    continue;
//                                }
//                                RealDepth = NuiDepthPixelToDepth(*pBufferRun);
//                                Player = NuiDepthPixelToPlayerIndex(*pBufferRun);
//                                if (Player == 0){
//                                    ++pBufferRun;
//                                    continue;
//                                }
//                                BYTE intensity = (BYTE)~(RealDepth >> 5);    // 13bit → 8bit
//                                // ズレ補正
//                                hr = ::NuiImageGetColorPixelCoordinatesFromDepthPixelAtResolution(NUI_IMAGE_RESOLUTION_640x480,
//                                    NUI_IMAGE_RESOLUTION_640x480,
//                                    NULL,
//                                    x, y,                                // coordinate in depth image space
//                                    0,                                // The depth value in depth image space.
//                                    &nx, &ny);
//                                if (hr != S_OK){
//                                    ++pBufferRun;
//                                    continue;
//                                }
//                                DrawPixelSoftImage(handle, nx, ny,
//                                    (intensity >> g_IntensityShiftByPlayerR[Player]) * (Player != 0),
//                                    (intensity >> g_IntensityShiftByPlayerG[Player]) * (Player != 0),
//                                    (intensity >> g_IntensityShiftByPlayerB[Player]) * (Player != 0),
//                                    0);
//                                ++pBufferRun;
//                            }
//                        }
//                    }
//                    else{
//                        // 背景なし，プレイーヤーに色を付けない
//                        for (int y = 0; y<BaseDepthImage.Height; y++){
//                            for (int x = 0; x<BaseDepthImage.Width; x++){
//                                if (*pBufferRun == 0){
//                                    ++pBufferRun;
//                                    continue;
//                                }
//                                RealDepth = NuiDepthPixelToDepth(*pBufferRun);
//                                Player = NuiDepthPixelToPlayerIndex(*pBufferRun);
//                                if (Player == 0){
//                                    ++pBufferRun;
//                                    continue;
//                                }
//                                BYTE intensity = (BYTE)~(RealDepth >> 5);    // 13bit → 8bit
//                                // ズレ補正
//                                hr = ::NuiImageGetColorPixelCoordinatesFromDepthPixelAtResolution(NUI_IMAGE_RESOLUTION_640x480,
//                                    NUI_IMAGE_RESOLUTION_640x480,
//                                    NULL,
//                                    x, y,                                // coordinate in depth image space
//                                    0,                                // The depth value in depth image space.
//                                    &nx, &ny);
//                                if (hr != S_OK){
//                                    ++pBufferRun;
//                                    continue;
//                                }
//                                DrawPixelSoftImage(handle, nx, ny,
//                                    intensity * (Player != 0),
//                                    intensity * (Player != 0),
//                                    intensity * (Player != 0),
//                                    0);
//                                ++pBufferRun;
//                            }
//                        }
//                    }
//
//                }
//            }
//            else{
//                OutputDebugString("Buffer length of received texture is bogus\r\n");
//            }
//
//            // ＣＰＵで扱うイメージからグラフィックハンドルを作成する
//            int DepthGrHandle = CreateGraphFromSoftImage(handle);
//
//            // カメラデータの解放
//            pTextureDepth->UnlockRect(0);
//            m_pNuiSensor->NuiImageStreamReleaseFrame(m_DepthStreamHandle, &depthFrame);
//            // ソフトイメージ解放
//            DeleteSoftImage(handle);
//            //InitSoftImage();
//
//            return DepthGrHandle;
//        }
//
//        //-------------------------------------------------------------------
//        // 深度カメラからデータを取得してグラフィックハンドルを返す（多用途版：重い？）
//        //-------------------------------------------------------------------
//        int KinectDX::GetDepthGrHandle(void){
//            ::WaitForSingleObject(m_DepthEvent, INFINITE);
//            // カメラデータの取得
//            NUI_IMAGE_FRAME depthFrame;
//            HRESULT hr = m_pNuiSensor->NuiImageStreamGetNextFrame(m_DepthStreamHandle, 0, &depthFrame);
//            if (FAILED(hr))    return -1;
//
//            // 画像データの取得
//            INuiFrameTexture * pTextureDepth = depthFrame.pFrameTexture;
//            NUI_LOCKED_RECT LockedRectDepth;
//            pTextureDepth->LockRect(0, &LockedRectDepth, NULL, 0);
//
//            // 初期色
//            if (GetDepthGrHandleOption.FillTransColor == true){
//                int r, g, b;
//                GetTransColor(&r, &g, &b);
//                FillSoftImage(softhandle, r, g, b, 0);
//            }
//            else{
//                FillSoftImage(softhandle, 0, 0, 0, 0);
//            }
//
//            if (LockedRectDepth.Pitch != 0){
//                // draw the bits to the bitmap
//                USHORT * pBufferRun = (USHORT *)LockedRectDepth.pBits;        // 深度データの取り込み？
//                USHORT RealDepth;    // 深度情報
//                USHORT Player;        // プレイヤーID情報
//
//                LONG nx, ny;
//                BYTE R, G, B;
//                for (int y = 0; y<BaseDepthImage.Height; y++){
//                    for (int x = 0; x<BaseDepthImage.Width; x++){
//
//                        // 深度情報0の領域は描画しない
//                        if (GetDepthGrHandleOption.EnableZeroDepth == false){
//                            if (*pBufferRun == 0){
//                                ++pBufferRun;
//                                continue;
//                            }
//                        }
//
//                        RealDepth = NuiDepthPixelToDepth(*pBufferRun);
//                        Player = NuiDepthPixelToPlayerIndex(*pBufferRun);
//                        BYTE intensity = (BYTE)~(RealDepth >> 5);    // 13bit → 8bit
//
//                        // プレイヤー以外の深度情報は無視
//                        if (GetDepthGrHandleOption.DrawBackGround == false){
//                            if (Player == 0){
//                                ++pBufferRun;
//                                continue;
//                            }
//                        }
//
//                        // ズレ補正
//                        if (GetDepthGrHandleOption.EnableCorrection == true){
//                            hr = ::NuiImageGetColorPixelCoordinatesFromDepthPixelAtResolution(NUI_IMAGE_RESOLUTION_640x480,
//                                NUI_IMAGE_RESOLUTION_640x480,
//                                NULL,
//                                x, y,                                // coordinate in depth image space
//                                0,                                // The depth value in depth image space.
//                                &nx, &ny);
//                            if (hr != S_OK){
//                                ++pBufferRun;
//                                continue;
//                            }
//                        }
//                        else{
//                            nx = x;
//                            ny = y;
//                        }
//
//                        // プレイヤーに色を付ける
//                        if (GetDepthGrHandleOption.ColorPlayer == true){
//                            R = intensity >> g_IntensityShiftByPlayerR[Player];
//                            G = intensity >> g_IntensityShiftByPlayerG[Player];
//                            B = intensity >> g_IntensityShiftByPlayerB[Player];
//                        }
//                        else{
//                            R = intensity;
//                            G = intensity;
//                            B = intensity;
//                        }
//
//                        DrawPixelSoftImage(softhandle, nx, ny, R, G, B, 0);
//                        ++pBufferRun;
//                    }
//                }
//            }
//            else{
//                OutputDebugString("Buffer length of received texture is bogus\r\n");
//            }
//
//            // 前のグラフィックハンドルを削除する
//            DeleteGraph(DepthGrHandle);
//
//            // ＣＰＵで扱うイメージからグラフィックハンドルを作成する
//            DepthGrHandle = CreateGraphFromSoftImage(softhandle);
//
//            // カメラデータの解放
//            pTextureDepth->UnlockRect(0);
//            m_pNuiSensor->NuiImageStreamReleaseFrame(m_DepthStreamHandle, &depthFrame);
//
//            return DepthGrHandle;
//        }
//
//        //-------------------------------------------------------------------
//        // トラッキングデータを取得する
//        //-------------------------------------------------------------------
//        int KinectDX::GetSkeletonData(Vector4 *pSkel1, Vector4 *pSkel2){
//            ::WaitForSingleObject(m_SkeletonEvent, INFINITE);
//            NUI_SKELETON_FRAME SkeletonFrame = { 0 };
//            bool bFoundSkeleton = false;
//            int user[2] = { -1, -1 };
//            int count = 0;
//
//            if (SUCCEEDED(m_pNuiSensor->NuiSkeletonGetNextFrame(0, &SkeletonFrame))){
//                // トラッキングされているかを調べる
//                for (int i = 0; i < NUI_SKELETON_COUNT; i++){
//                    if (SkeletonFrame.SkeletonData[i].eTrackingState == NUI_SKELETON_TRACKED){
//                        bFoundSkeleton = true;    // どれかがトラッキングされていればtrue
//                        user[count] = i;
//                        count++;
//                    }
//                }
//            }
//            // no skeletons!
//            if (!bFoundSkeleton){
//                return -1;
//            }
//
//            // トラッキングデータのスムージング
//            HRESULT hr = m_pNuiSensor->NuiTransformSmooth(&SkeletonFrame, NULL);
//            if (FAILED(hr)){
//                return -2;
//            }
//
//            if (user[0] != -1){
//                for (int i = 0; i<NUI_SKELETON_POSITION_COUNT; i++){
//                    pSkel1[i] = SkeletonFrame.SkeletonData[user[0]].SkeletonPositions[i];
//                }
//            }
//            if (user[1] != -1){
//                for (int i = 0; i<NUI_SKELETON_POSITION_COUNT; i++){
//                    pSkel2[i] = SkeletonFrame.SkeletonData[user[1]].SkeletonPositions[i];
//                }
//            }
//            return count;
//        }
//
//        //-------------------------------------------------------------------
//        // 骨格の座標をカラー座標に変換する
//        //-------------------------------------------------------------------
//        HRESULT KinectDX::MapSkeletonPointToColor(Vector4 point, long *x, long *y){
//            float nx, ny;
//            NuiTransformSkeletonToDepthImage(point, &nx, &ny, NUI_IMAGE_RESOLUTION_640x480);
//            hr = ::NuiImageGetColorPixelCoordinatesFromDepthPixelAtResolution(NUI_IMAGE_RESOLUTION_640x480,
//                NUI_IMAGE_RESOLUTION_640x480,
//                NULL,
//                (LONG)nx, (LONG)ny,            // coordinate in depth image space
//                0,                            // The depth value in depth image space.
//                x, y);
//            return 0;
//        }