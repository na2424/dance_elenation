#include "MarkerIni.h"
#include "extern.h"


MarkerIni::MarkerIni(void)
{
	initFileName = _T("Markers/default/Marker.ini"); //Default file.
	this->Init();
}

MarkerIni::MarkerIni(string_t fname = _T(""))
{
	initFileName = _T("Markers/default/Marker.ini"); //Default file.
	this->Init();
	this->Load(fname);
}

/**
	INI file is read.
	@param fname Filename
	When there is not a file, It becomes initFileName.
	When there is not a pass, It becomes Windows folder.
	*/
bool MarkerIni::Load(string_t fname = _T(""))
{
	if (fname.empty()) {
		fname = initFileName;
	}
	loadFileName = fname;
	WIN32_FIND_DATA fd;
	HANDLE h = ::FindFirstFile(fname.c_str(), &fd);
	if (h != INVALID_HANDLE_VALUE) {
		this->IniRw(fname, 1);
	}
	return (h != INVALID_HANDLE_VALUE);
}

/**
	It writes it in the INI file.
	@param fname Filename
	When there is not a file, It becomes open file.
	When there is not a pass, It becomes Windows folder.
	*/
bool MarkerIni::Save(string_t fname = _T(""))
{
	if (fname.empty()) {
		fname = loadFileName;
	}
	IniRw(fname, 0);
	return true;
}

bool MarkerIni::IniRw(string_t f, int r)
{
	string_t s = _T("Marker");
	Inirw(r, f, s, _T("m1             "), Marker.m1);
	Inirw(r, f, s, _T("m2             "), Marker.m2);
	Inirw(r, f, s, _T("m3             "), Marker.m3);
	Inirw(r, f, s, _T("m4             "), Marker.m4);

	return true;
}

void MarkerIni::Init()
{
	Marker.m1 = 37;
	Marker.m2 = 7;
	Marker.m3 = 24;
	Marker.m4 = 24;
}