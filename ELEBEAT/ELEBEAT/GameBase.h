#ifndef GAMEBASE_H
#define GAMEBASE_H
#include <array>
#include "SceneManager.h"
#include "LuaHelper.h"
#include "define.h"



class Combo;

class GameBase : public Scene
{
protected:
	void DrawDifficultyNum(int num, std::array<int, 10> pic) const;
	void DrawFormatStrings(long now) const;
	void (GameBase::*GameKey[3])(Music *musicP);    // 関数ポインタの配列
	std::array<int, 10> m_scoreNumImage;    //(ゲーム画面での)スコア画像を入れるための配列
	int m_funcCount;
	//typedef boost::signals2::signal<void(int m, int i, const std::shared_ptr<KeyInputClass4key>& f_keyinput)> signalLongNote;
	virtual void DrawCombo(const std::shared_ptr<Combo>& fCombo) = 0;

	//Lua
	lua_State *L;
	LuaHelper m_luaHelper;

	static const int CRAP_OFFSET = 55;

	//Auto&Crap
	bool m_isAuto;
	bool m_isCrap;

public:
	static int g_MusicDif;
	static long int g_Now;
	static std::array<Image,2> g_JudgeAreaImages;           //判定ゾーン画像を入れるための変数 判定の枠画像
	static std::array<Image,6> g_JudgeImages;
	static std::array<Image,8> g_GradeImages;               //評価画像を入れるための変数 SSからEまで
	static std::array<Image,10> g_ComboElebeatImages;       ////コンボ数字の画像
	static std::array<Image,10> g_ComboParfect4KeysImages;           ////コンボ数字の画像
	static std::array<Image,10> g_ComboGrate4KeysImages;           ////コンボ数字の画像
	static std::array<Image,10> g_ComboGood4KeysImages;           ////コンボ数字の画像

	std::array<Image, 5> m_DifImage;

	void DrawScore(int num);

	void InitLua(void);

	GameBase();
};
#endif