#include <boost/lexical_cast.hpp>
#include <thread>

#include "SelectMusic4keys.h"
#include "BlueLightBall.h"
#include "extern.h"
#include "Foreach.h"
#include "Input.h"
#include "MouseClickObject.h"
#include "NumberDraw.h"
#include "Screen.h"
#include "GamePlayOption.h"
#include "Color.h"
#include "Enums.h"
#include "cpplinq.hpp"


using namespace DxLib;

std::vector<Image> SelectMusic4Keys::g_Banner;
std::vector<Image> SelectMusic4Keys::g_TempBanner;
std::vector<Image> SelectMusic4Keys::g_KindFolder4;

std::vector<double> SelectMusic4Keys::g_BannerSizeRatio;
std::vector<double> SelectMusic4Keys::g_TempBannerSizeRatio;
std::vector<double> SelectMusic4Keys::g_KindFolder4SizeRatio;

int SelectMusic4Keys::g_SelectFolderIndex;
int SelectMusic4Keys::g_Beforecenter4 = 0;

SelectMusic4Keys::SelectMusic4Keys() :
	m_Points(),
	m_displayTime(99),
	m_internalTime(0),
	m_breakState(EscapeState::NONE),
	m_difRect(0),
	m_ViewPoint(),
	m_isBack(false)
{
	std::array<int, 3> temp;
	temp.fill(0);
	m_fontHandle.fill(0);
	m_DifNumber.fill(temp);
	m_numberDraw = std::make_shared<TimerDrawer>();
	m_mouseClickObjectList = std::make_unique<MouseClickObjectList>();
	lightball.resize(BALL_MAX);
}

void SelectMusic4Keys::LoadSceneImage()
{
	g_BackImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\back.jpg"));

	m_aaFrameImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\aa_flame.png"));
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\selectmusic04.png"), 5, 1, 5, 200, 50, m_difImage.data());    //足画像
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\selectmusic-foot.png"), 6, 6, 1, 100, 100, m_footImage.data());    //足画像
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Numbers\\selectmusic_level00_num.png"), 10, 10, 1, 25, 50, difnumberColorImage[0].data());        //番号画像 灰色
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Numbers\\selectmusic_level01_num.png"), 10, 10, 1, 25, 50, difnumberColorImage[4].data());        //番号画像 緑
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Numbers\\selectmusic_level02_num.png"), 10, 10, 1, 25, 50, difnumberColorImage[2].data());        //番号画像 黄色
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Numbers\\selectmusic_level03_num.png"), 10, 10, 1, 25, 50, difnumberColorImage[3].data());        //番号画像 赤
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Numbers\\selectmusic_level04_num.png"), 10, 10, 1, 25, 50, difnumberColorImage[1].data());        //番号画像 黄色
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Numbers\\selectmusic_level05_num.png"), 10, 10, 1, 25, 50, difnumberColorImage[5].data());        //番号画像 赤
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\selectmusic_hyouka60x50.png"), 8, 1, 8, 60, 50, m_rankImage.data());
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\rotation48x48.png"), 5, 5, 1, 48, 48, m_fullcombo.data());

	if (OptionSaveData::g_IsEventMode)
	{
		stageNumImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\eventmode.png"));
	}
	else
	{
		if (OptionSaveData::g_SongCount == OptionSaveData::g_SongCountSetting + 1)
			stageNumImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\finalstage.png"));
		else
			stageNumImage = LoadGraph((_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\stage") + boost::lexical_cast<tstring>(OptionSaveData::g_SongCount) + _T(".png")).c_str());
	}
}

void SelectMusic4Keys::LoadSceneSound()
{
	this->backSound = LoadSoundMem(_T("Themes\\DANCE ELENATION\\Sounds\\Common back.ogg"));
	int randNum = DxLib::GetRand(2);
	if (randNum == 0)
		this->roop_music = LoadSoundMem(_T("Themes\\DANCE ELENATION\\Sounds\\MusicSelect\\roop.ogg"));
	else if (randNum == 1)
		this->roop_music = LoadSoundMem(_T("Themes\\DANCE ELENATION\\Sounds\\MusicSelect\\roop1.ogg"));
	else
		this->roop_music = LoadSoundMem(_T("Themes\\DANCE ELENATION\\Sounds\\TRANCE Loop 001.mp3"));
}

void SelectMusic4Keys::Initialize(const std::shared_ptr<Music>& musicP)
{
	//Lua初期化
	InitLua();
	// 描画可能領域セット
	SetDrawArea(0, 0, WIN_WIDTH, WIN_HEIGHT);
	setlocale(LC_ALL, "ja_JP.UTF-8");

	m_reverseFlag = GamePlayOption::GetInstance().isReverse;
	m_noteskinFlag = static_cast<int>(GamePlayOption::GetInstance().noteSkin);

	m_markerY = 420 + 42 * 6;

	//宣言 char
	//TCHAR string[256] = {};
	//TCHAR name[256];
	//std::memset(name, _T('\0'), sizeof(name));

	const double hiSpeed[] = { 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 8.0 };

	for (int i = 0; i <= 11; i++)
	{
		if (GamePlayOption::GetInstance().speed == hiSpeed[i])
			m_speedIndex = i;
	}

	LoadSceneImage();
	LoadSceneSound();

	//Lua読み込み
	ReadLua();

	// TODO: BlueLightBall
	//FOREACH( BlueLightBall, lightball, lb )
	//{
	//	lb->Init();
	//}

	if (g_IsLoaded4Keys == false)
		this->selectmode = SelectMode::FOLDER;                        //再度入るとき用

	//画像ロード
	if (!g_IsLoaded4Keys)
	{
		try
		{
			std::thread t1(&SelectMusic4Keys::LoadGraphAll, this, std::ref(musicP));
			//std::thread t2([&]()
			//{
			//    int count = 0;

			//    while(ProcessMessage() != -1 && !isLoadedGraphAll.load())
			//    {
			//        //ClearDrawScreen();
			//        DrawRotaGraph(WIN_WIDTH / 2, WIN_HEIGH / 2, 1.0, count / 20.0, loadingImage, TRUE);
			//        ScreenFlip();
			//    }

			//});
			t1.join();
			//t2.join();
		}
		catch (std::exception& ex)
		{
			MessageBox(nullptr, _T("エラー"), ex.what(), MB_OK);
		}
		//LoadGraphAll4(musicP);
		g_NowCenter = CENTER_DEFAULT;
		g_BeforeCenter = CENTER_DEFAULT;
	}
	//一回でもゲームしたならばそのAAを選択するようにする
	else if (beforeSelectMusicNum4 != -1)
	{
		this->m_musicKind = musicP->danceSong->m_FolderNum;
		this->m_musicNum = musicP->danceSong->m_TotalMusicNum;
		std::copy(musicP->danceSong->m_MusicNumAt.begin(), musicP->danceSong->m_MusicNumAt.end(), back_inserter(this->m_musicKind4Max));

		TCHAR foldername[260] = {};

		auto count = 0;
		auto beforefoldernumber = g_SelectFolderIndex;

		SetMode(beforefoldernumber, musicP);
		CalculateDifNumber(m_DifNumber, g_NowCenter, musicP);
		CutBpm(NowAANumber(g_NowCenter), g_NowCenter, musicP);
		this->selectmode = SelectMode::AA;
	}
	else //その他
	{
		this->m_musicKind = musicP->danceSong->m_FolderNum;
		this->m_musicNum = musicP->danceSong->m_TotalMusicNum;
		copy(musicP->danceSong->m_MusicNumAt.begin(), musicP->danceSong->m_MusicNumAt.end(), back_inserter(this->m_musicKind4Max));
		this->m_selectFolderNum = 0;
		g_NowCenter = CENTER_DEFAULT;
		g_BeforeCenter = CENTER_DEFAULT;
		this->selectmode = SelectMode::FOLDER;
	}

	m_InputFrameCount = 0;

	//フォント
	m_fontHandle[0] = CreateFontToHandle(nullptr, 42, 6, DX_FONTTYPE_ANTIALIASING_EDGE);
	m_fontHandle[1] = CreateFontToHandle(nullptr, 29, 6, DX_FONTTYPE_ANTIALIASING_EDGE);
	m_fontHandle[2] = CreateFontToHandle(nullptr, 18, 6, DX_FONTTYPE_ANTIALIASING_EDGE);
	m_fontHandle[3] = CreateFontToHandle(nullptr, 20, 4, DX_FONTTYPE_ANTIALIASING_EDGE);
	// 音声をループ再生する
	PlaySoundMem(roop_music, DX_PLAYTYPE_LOOP);
	//座標設定
	GraphCoordinatesSet();
	//マーカー座標
	MarkerCoordinatesSet();
	clsDx();
	Screen::FadeIn();
	//時間代入
	startTime = GetNowCount();
	m_speedFlag = true;
	SetDrawScreen(DX_SCREEN_BACK);
}//関数

void SelectMusic4Keys::Update(const std::shared_ptr<Music>& musicP)
{
	//シングルトン参照
	auto& input = Input::GetInstance();
	auto& color = Color::GetInstance();

	//更新
	m_fps->Update();

	//カウンター				  
	++count;

	//キー操作
	input.GetKey();

	//マウスの座標を取得
	input.GetMouse();

	m_mouseClickObjectList->Update();

	//画面消去
	ClearDrawScreen();

	//背景画像
	DrawGraph(0, 0, g_BackImage, TRUE);

	//パーティクル位置を更新と描画
	FOREACH(BlueLightBall, this->lightball, lb)
	{
		lb->Update(nullptr);
		lb->Draw();
	}

	//AA描画
	DrawGraphAll(g_NowCenter);

	//AAフレーム
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, static_cast<int>(127 * sin(count * PI / 30.0) + 128));
	DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT / 2 - 114, 150, 150, 1.0, 0, m_aaFrameImage, true);
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	//その一個上におく画像
	DrawGraph(0, 0, this->backFrontImage, TRUE);

	//タイマー処理
	m_internalTime = GetNowCount() - startTime;

	if (OptionSaveData::g_IsEventMode)
		m_displayTime = 99;
	else
		m_displayTime = START_SELECT_TIME - m_internalTime / 1000;

	if (2 < g_DifNowCenter - m_difRect)
		++m_difRect;
	else if (g_DifNowCenter - m_difRect < 0)
		--m_difRect;

	if (m_difRect < 0)
	{
		m_difRect = 2;
		g_DifNowCenter = 4;
	}
	else if (m_difRect > 2)
	{
		m_difRect = 0;
		g_DifNowCenter = 0;
	}

	DrawBox((WIN_WIDTH / 2) - 150, 554 + 45 * (g_DifNowCenter - m_difRect), (WIN_WIDTH / 2) + 290, 595 + 45 * (g_DifNowCenter - m_difRect), m_difficultBoxColor[g_DifNowCenter], TRUE);
	DrawBox((WIN_WIDTH / 2) - 150, 554 + 45 * (g_DifNowCenter - m_difRect), (WIN_WIDTH / 2) + 290, 595 + 45 * (g_DifNowCenter - m_difRect), Color::GetInstance().White(), FALSE);
	//グルーヴレーダー画像
	DrawGraph(256 - 149, 616 - 155, raderImage, TRUE);

	//足
	//灰色足描画
	for (auto i = 0; i < 3; ++i)
	{
		DrawGraph(515, 526 + 45 * i, m_footImage[0], TRUE);
	}
	//撰択足
	DrawGraph(515, 526 + 45 * (g_DifNowCenter - m_difRect), m_footImage[g_DifNowCenter + 1], TRUE);

	//選択難易度の数字を色付け、それ以外は灰色
	for (auto i = 0; i < 3; ++i)
	{
		DrawGraph(580, 550 + 45 * i, ((i + m_difRect) == g_DifNowCenter) ? difnumberColorImage[(i + m_difRect) + 1][m_DifNumber[(i + m_difRect)][0]] : difnumberColorImage[0][m_DifNumber[(i + m_difRect)][0]], TRUE);
		DrawGraph(599, 550 + 45 * i, ((i + m_difRect) == g_DifNowCenter) ? difnumberColorImage[(i + m_difRect) + 1][m_DifNumber[(i + m_difRect)][1]] : difnumberColorImage[0][m_DifNumber[(i + m_difRect)][1]], TRUE);
	}

	//難易度文字
	for (auto j = 0; j < 3; ++j)
	{
		DrawGraph(366, 548 + 45 * j, m_difImage[j + m_difRect], TRUE);
	}

	//くるくる回るアレ
	DrawRotaGraph2(360, 575 + 45 * (g_DifNowCenter - m_difRect), 25, 25, 1.0, (PI / 20) * count, selecter, TRUE, FALSE);
	//回転処理
	m_selectercounter++;

	if (m_selectercounter == 15)
		m_selectercounter = 1;

	DrawGraph(-42, 631, setumei1, TRUE);
	DrawGraph(742, 631, setumei2, TRUE);

	//左右画像
	DrawLR(right_pic, left_pic);

	//ステージ数画像
	if (count < 20)
		DrawGraph((count - 20) * 13 + 20, 80, stageNumImage, TRUE);
	else
		DrawGraph(20, 80, stageNumImage, TRUE);

	/************************************こっから処理系統/************************************/

	//キー操作受け取り
	KeySelectMusicType keyType = CatchKey(this->g_NowCenter, this->selectdifsound, m_difRect, &m_speedFlag);

	//folderからAAへ
	if (keyType == KeySelectMusicType::FOLDER_TO_AA)
	{
		PlaySoundMem(expandSound, DX_PLAYTYPE_BACK);
		g_NowCenter = ChangeMode(g_NowCenter, musicP);
		if (g_NowCenter == -1)
		{
			if (!OptionSaveData::g_IsFullScreen)
				MessageBox(nullptr, _T("ごめんなさいエラーです。作者に文句を言ってください エラー項目C_M"), _T("エラー"), MB_OK);
		}
	}
	//AAでenter
	else if (keyType == KeySelectMusicType::DEFAULT_SELECT)
	{
		//譜面が存在するか否か
		if (m_DifNumber[g_DifNowCenter][0] != 0 || m_DifNumber[g_DifNowCenter][1] != 0)
		{
			//1ならげーむへ
			EscapeSelect(EscapeState::GAME, g_NowCenter);
			m_breakState = EscapeState::GAME;
		}
	}
	//folderでrandを選択
	else if (keyType == KeySelectMusicType::FOLDER_RANDOM)
	{
		//完全ランダム選択
		randomcenternum = GetRandom(1, musicP->danceSong->m_TotalMusicNum - 1, musicP);
		//ランダムな数を渡す
		m_breakState = EscapeState::RANDOM_ALL;
	}
	//AAからfolderに
	else if (keyType == KeySelectMusicType::AA_TO_FOLDER)
	{
		g_NowCenter = ChangeMode(g_NowCenter, musicP);
		//難易度数字をゼロに
		ZeroMemory(m_DifNumber.data(), sizeof(m_DifNumber.data()));
		DxLib::PlaySoundMem(closeSound, DX_PLAYTYPE_BACK);
		if (g_NowCenter == -1)
		{
			if (!OptionSaveData::g_IsFullScreen)
				MessageBox(nullptr, _T("ごめんなさいエラーです。作者に文句を言ってください。 エラー項目C_M"), _T("エラー"), MB_OK);
		}
	}
	//AAでフォルダを選択した
	else if (keyType == KeySelectMusicType::FOLDER_TO_AA)
	{
		g_NowCenter = ChangeMode(g_NowCenter, musicP);
		//難易度数字をゼロに
		ZeroMemory(m_DifNumber.data(), sizeof(m_DifNumber.data()));
		DxLib::PlaySoundMem(closeSound, DX_PLAYTYPE_BACK);
		if (g_NowCenter == -1)
		{
			if (!OptionSaveData::g_IsFullScreen)
				MessageBox(nullptr, _T("ごめんなさいエラーです。作者に文句を言ってください。 エラー項目C_M"), _T("エラー"), MB_OK);
		}
	}
	//folderでesc titleに戻る
	else if (keyType == KeySelectMusicType::BREAK)
	{
		m_isBack = true;

		//タイトルへ
		EscapeSelect(EscapeState::TITLE, g_NowCenter);
	}
	else if (keyType != (KeySelectMusicType)150)
	{
		if (input.keyEsc == 1)
		{
			SceneManager::g_CurrentScene = Scenes::TITLE;
			PlaySoundMem(backSound, DX_PLAYTYPE_BACK);
			StopSoundMem(roop_music);
			const auto graph = MakeGraph(WIN_WIDTH, WIN_HEIGHT);
			GetDrawScreenGraph(0, 0, WIN_WIDTH, WIN_HEIGHT, graph);
			Screen::FadeOut();
			ClearDrawScreen();
			DelSelectMusic(roop_music, backSound);            //デストラクタ
			m_isBack = true;
		}
	}

	//Update
	if (selectmode == SelectMode::FOLDER)
	{
		//folder
	}
	else
	{
		//song(AA)
		//選択中の難易度
		m_selectMusicDif = m_DifNumber[g_DifNowCenter][0] * 10 + m_DifNumber[g_DifNowCenter][1];

		DrawGraph(500, 95, bpmFrameImage, TRUE);
		if (m_selectMusicDif > 0)
			DrawChart();
	}

	//timerが0以下になったとき
	if (m_displayTime <= -1)
	{
		if (selectmode == SelectMode::FOLDER)
		{
			OnTimeUpFolder(musicP);
		}
		else
		{
			if (m_selectMusicDif > 0)
			{
				//1ならげーむへ
				EscapeSelect(EscapeState::GAME, g_NowCenter);
				m_breakState = EscapeState::GAME;
			}
			else
			{
				randomcenternum = GetRandom(g_CurrentAlbumArt[0] + 1, g_CurrentAlbumArt[1], musicP);
				g_NowCenter = randomcenternum;
				//画面キャプチャ
				EscapeSelect(EscapeState::RANDOM_ALL, randomcenternum);                        //ランダムな数を渡す
				m_breakState = EscapeState::RANDOM_ALL;
			}
		}
	}

	//左押されたとき
	if (isLeft == true)
	{
		DrawGraph(WIN_WIDTH / 2 + 140, 326, this->right_click_pic, TRUE);
		if (selectmode == SelectMode::FOLDER)
		{
			MoveLeft(15, 30);
			MoveMirrorLeft(15, 30);
		}
		else
		{
			MoveLeft(15, 30, m_musicKind4Max[m_selectFolderNum]);
			MoveMirrorLeft(15, 30, m_musicKind4Max[m_selectFolderNum]);
		}
		m_InputFrameCount++;
	}

	//右押されたとき
	if (isRight == true)
	{
		DrawGraph(WIN_WIDTH / 2 - 240, 326, this->left_click_pic, TRUE);
		if (selectmode == SelectMode::FOLDER)
		{
			MoveRight(15, 30);
			MoveMirrorRight(15, 30);
		}
		else
		{
			MoveRight(15, 30, m_musicKind4Max[m_selectFolderNum]);
			MoveMirrorRight(15, 30, m_musicKind4Max[m_selectFolderNum]);
		}
		m_InputFrameCount++;
	}

	//左押し続け
	if (isLeftContinue == true)
	{
		++countMoveAA;
		//もし一定数以上押されたら
		if (countMoveAA >= 60)
		{
			//高速フラグを立てる
			isFastLeft = true;
			//int over flowをなくす
			if (countMoveAA >= 10000)
				countMoveAA = 60;
		}
	}

	//右押し続け
	if (isRightContinue == true)
	{
		++countMoveAA;
		//もし一定数以上押されたら
		if (countMoveAA >= 60)
		{
			//高速フラグを立てる
			isFastRight = true;
			//int over flowをなくす
			if (countMoveAA >= 10000)
				countMoveAA = 60;
		}
	}

	//どっちも押してすらないとき全部初期化
	if (isLeftContinue == false && isRightContinue == false)
	{
		countMoveAA = 0;
		isFastLeft = false;
		isFastRight = false;
	}

	//index,goによる高速化移動
	if (m_InputFrameCount == 6)
	{
		if (isFastLeft || isFastRight)
		{
			if (isLeft)
			{
				++g_NowCenter;
				isLeft = false;
				m_speedFlag = true;
				PlaySoundMem(wheelSound, DX_PLAYTYPE_BACK);
			}
			if (isRight)
			{
				--g_NowCenter;
				isRight = false;
				m_speedFlag = true;
				PlaySoundMem(wheelSound, DX_PLAYTYPE_BACK);
			}
			//座標設定
			GraphCoordinatesSet();
			//フォルダ時
			if (selectmode == SelectMode::FOLDER)
			{
				if (g_NowCenter < 0)
					g_NowCenter = CENTER_DEFAULT + m_musicKind - 1;
				else if (g_NowCenter > CENTER_DEFAULT + m_musicKind - 1)
					g_NowCenter = CENTER_DEFAULT;
			}
			//AA時
			else
			{
				if (g_NowCenter < 0)
					g_NowCenter = CENTER_DEFAULT + m_musicKind4Max[m_selectFolderNum] - 1;
				else if (g_NowCenter > CENTER_DEFAULT + m_musicKind4Max[m_selectFolderNum] - 1)
					g_NowCenter = CENTER_DEFAULT;
			}
			//AAのとき番号計算一回
			if (selectmode == SelectMode::AA)
				CalculateDifNumber(m_DifNumber, g_NowCenter, musicP);

			m_InputFrameCount = 0;
		}
	}

	//iindex,go~~の初期化 動くの終了
	if (m_InputFrameCount == 12)
	{
		m_InputFrameCount = 0;
		if (isLeft)
		{
			PlaySoundMem(wheelSound, DX_PLAYTYPE_BACK);
			++g_NowCenter;
			isLeft = false;
			m_speedFlag = true;
		}
		if (isRight)
		{
			PlaySoundMem(wheelSound, DX_PLAYTYPE_BACK);
			--g_NowCenter;
			isRight = false;
			m_speedFlag = true;
		}
		//座標設定
		GraphCoordinatesSet();
		//フォルダ時
		if (selectmode == SelectMode::FOLDER)
		{
			if (g_NowCenter < 0)
			{
				g_NowCenter = CENTER_DEFAULT + m_musicKind - 1;
			}
			else if (g_NowCenter > CENTER_DEFAULT + m_musicKind - 1)
			{
				g_NowCenter = CENTER_DEFAULT;
			}
		}
		//AA時
		else
		{
			//最小最大bpmを得る
			CutBpm(NowAANumber(g_NowCenter), g_NowCenter, musicP);

			if (g_NowCenter < 0)
			{
				g_NowCenter = CENTER_DEFAULT + m_musicKind4Max[m_selectFolderNum] - 1;
			}
			else if (g_NowCenter > CENTER_DEFAULT + m_musicKind4Max[m_selectFolderNum] - 1)
			{
				g_NowCenter = CENTER_DEFAULT;
			}
		}

		//AAのとき番号計算一回
		if (selectmode == SelectMode::AA)
			CalculateDifNumber(m_DifNumber, g_NowCenter, musicP);
	}
	//音楽名描画
	DrawMusicInfo(g_NowCenter, m_fontHandle.data(), count, musicP);

	//ハイスコア表示はAA時のみ
	if (selectmode == SelectMode::AA)
		DrawHiScore(g_NowCenter, m_selectercounter, m_fullcombo.data(), m_rankImage.data(), m_fontHandle.data(), m_speedFlag, m_difRect, musicP, m_Points);

	//breakフラグ回収
	if (m_breakState == EscapeState::GAME)
	{
		//変数キャッシュ.
		g_SelectFolderIndex = m_selectFolderNum;

		//画面キャプチャ
		const auto graph = MakeGraph(WIN_WIDTH, WIN_HEIGHT);
		GetDrawScreenGraph(0, 0, WIN_WIDTH, WIN_HEIGHT, graph);
		blackImage = graph;

		// 作成したフォントデータを削除する
		for (auto font : m_fontHandle)
		{
			DeleteFontToHandle(font);
		}

		return;
	}
	if (m_breakState == EscapeState::RANDOM_ALL)
	{
		//画面キャプチャ
		const auto graph = MakeGraph(WIN_WIDTH, WIN_HEIGHT);
		GetDrawScreenGraph(0, 0, WIN_WIDTH, WIN_HEIGHT, graph);
		blackImage = graph;

		// 作成したフォントデータを削除する
		for (auto font : m_fontHandle)
		{
			DeleteFontToHandle(font);
		}

		return;
	}
	/*****************************オプション描画****************************/
	if (markerflag && m_markerY > 420)
		m_markerY -= 42;
	if (!markerflag && m_markerY < 420 + 42 * 6)
	{
		m_markerY += 42;
		DrawGraph(42, m_markerY, player_option, TRUE);
	}
	//shift押し
	if (markerflag || input.keyShift > 0)
	{
		//マーカー移動
		if (isLeftMarker && m_optionFlag == 0)
		{
			isLeftMarker = false;
			DxLib::PlaySoundMem(mrkMoveSound, DX_PLAYTYPE_BACK);
			m_speedIndex--;
		}

		if (isRightMarker && m_optionFlag == 0)
		{
			isRightMarker = false;
			DxLib::PlaySoundMem(mrkMoveSound, DX_PLAYTYPE_BACK);
			m_speedIndex++;
		}
		//マーカー移動
		if (isLeftMarker && m_optionFlag == 1)
		{
			isLeftMarker = false;
			DxLib::PlaySoundMem(mrkMoveSound, DX_PLAYTYPE_BACK);
			m_reverseFlag--;
		}

		if (isRightMarker && m_optionFlag == 1)
		{
			isRightMarker = false;
			DxLib::PlaySoundMem(mrkMoveSound, DX_PLAYTYPE_BACK);
			m_reverseFlag++;
		}
		//マーカー移動
		if (isLeftMarker && m_optionFlag == 2)
		{
			isLeftMarker = false;
			DxLib::PlaySoundMem(mrkMoveSound, DX_PLAYTYPE_BACK);
			m_noteskinFlag--;
		}

		if (isRightMarker && m_optionFlag == 2)
		{
			isRightMarker = false;
			DxLib::PlaySoundMem(mrkMoveSound, DX_PLAYTYPE_BACK);
			m_noteskinFlag++;
		}

		//マーカー移動(上下)
		if (isDownMarker)
		{
			isDownMarker = false;
			DxLib::PlaySoundMem(mrkMoveSound, DX_PLAYTYPE_BACK);
			m_optionFlag++;
		}

		if (marker_goup)
		{
			marker_goup = false;
			DxLib::PlaySoundMem(mrkMoveSound, DX_PLAYTYPE_BACK);
			m_optionFlag--;
		}

		//speed
		if (m_speedIndex >= 12)
			m_speedIndex = 0;

		if (m_speedIndex < 0)
			m_speedIndex = 11;

		//reverse
		if (m_reverseFlag >= 2)
			m_reverseFlag = 0;

		if (m_reverseFlag < 0)
			m_reverseFlag = 1;

		//note
		if (m_noteskinFlag >= 3)
			m_noteskinFlag = 0;

		if (m_noteskinFlag < 0)
			m_noteskinFlag = 2;

		//option
		if (m_optionFlag >= 3)
			m_optionFlag = 0;

		if (m_optionFlag < 0)
			m_optionFlag = 2;

		//マーカー描画
		if (input.keyShift == 1)
			DxLib::PlaySoundMem(mrkSound, DX_PLAYTYPE_BACK);

		DrawGraph(42, m_markerY, player_option, TRUE);

		ShowPlayerOption(m_fontHandle.data(), m_optionFlag, m_speedIndex, m_reverseFlag, m_noteskinFlag, m_markerY);
	}

	//フレームの画像表示
	if (count < 20)
	{
		count++;
		DrawGraph(0, -200 + (10 * count), this->backoverImage, TRUE);
		DrawGraph(0, WIN_HEIGHT - 80 + 200 - (10 * count), g_underBarImage, TRUE);
		m_numberDraw->DrawMenuTimer(m_displayTime, -200 + (10 * count));
	}
	else
	{
		DrawGraph(0, 0, this->backoverImage, TRUE);
		DrawGraph(0, WIN_HEIGHT - 80, g_underBarImage, TRUE);
		m_numberDraw->DrawMenuTimer(m_displayTime);
	}

	//マウスカーソルの表示
	DxLib::DrawRotaGraph2(input.mouseX, input.mouseY, 50, 50, 1.0f, count / 30.0, input.casolPic, TRUE);

	m_mouseClickObjectList->Draw();

	m_fps->Draw();        //描画
	DxLib::ScreenFlip();       //反転
	if (input.keyPrtsc == 1)
		m_ssc->CaptureScreenshot();
	m_fps->Wait();        //待機
}


void SelectMusic4Keys::Finalize(const std::shared_ptr<Music>& musicP)
{
	// 作成したフォントデータを削除する
	DeleteFontToHandle(m_fontHandle[0]);
	DeleteFontToHandle(m_fontHandle[1]);
	DeleteFontToHandle(m_fontHandle[2]);

	if (m_isBack)
	{
		Screen::FadeOut();
	}

	return;
}

//選曲画面でプレイヤーオプション
void SelectMusic4Keys::ShowPlayerOption(int FontHandle[], int option_flag, int speed_flag, int rv_flag, int noteSkinType, int marker_y)
{
	GamePlayOption& gameFlag = GamePlayOption::GetInstance();

	auto length = static_cast<int>(gameFlag.playerOptionNameList.size());

	for (int n = 0; n < length; n++)
	{
		LayerStringDraw(PLAYER_OPTION_WIDTH, marker_y + HEIGHT_OFFSET + PLAYER_OPTION_HEIGHT * (n * 2), gameFlag.playerOptionNameList[n].c_str(), FontHandle[2], 0);

		switch (n)
		{
		case 0:

			//Speed
			for (int i = 0; i < static_cast<int>(gameFlag.speedList.size()); i++)
			{
				if (i == speed_flag)
					gameFlag.speed = gameFlag.speedList[i];
			}
			DrawPlayerOptionList(gameFlag.speedNameList, FontHandle[2], option_flag, speed_flag, marker_y, gameFlag.speedNameList.size(), n, 66);
			break;

		case 1:

			//Scroll
			for (int i = 0; i < static_cast<int>(gameFlag.scrollList.size()); i++)
			{
				if (i == rv_flag)
					gameFlag.isReverse = gameFlag.scrollList[i];
			}
			DrawPlayerOptionList(gameFlag.scrollNameList, FontHandle[2], option_flag, rv_flag, marker_y, gameFlag.scrollNameList.size(), n, 155);
			break;

		case 2:

			//NoteSkin
			for (int i = 0; i < static_cast<int>(gameFlag.noteSkinList.size()); i++)
			{
				if (i == noteSkinType)
					gameFlag.noteSkin = gameFlag.noteSkinList[i];
			}
			DrawPlayerOptionList(gameFlag.noteSkinNameList, FontHandle[2], option_flag, noteSkinType, marker_y, gameFlag.noteSkinNameList.size(), n, 132);
			break;

		default:
			break;
		}
	}
}

void SelectMusic4Keys::DrawPlayerOptionList(const std::vector<tstring>& nameList, int FontHandle, int option_flag, int flag, int marker_y, int listLength, int n, int width) const
{
	int height = (2 * n + 1);
	int positionY = marker_y + HEIGHT_OFFSET + PLAYER_OPTION_HEIGHT * height;

	for (int i = 0; i < listLength; i++)
	{
		LayerStringDraw(PLAYER_OPTION_WIDTH + i * width, positionY, nameList[i].c_str(), FontHandle, 0);
		if (flag == i)
		{
			LayerStringDraw(PLAYER_OPTION_WIDTH + flag * width, positionY, nameList[i].c_str(), FontHandle, 2);
			if (option_flag == n)
				LayerStringDraw(PLAYER_OPTION_WIDTH + flag * width, positionY, nameList[i].c_str(), FontHandle, 1);
		}
	}
}

void SelectMusic4Keys::LoadingAnimation(void) const
{
	int count = 0;

	while (DxLib::ProcessMessage() != -1 && !m_isLoadedGraphAll)
	{
		ClearDrawScreen();
		DrawRotaGraph(WIN_WIDTH / 2, WIN_HEIGHT / 2, 1.0, count / 20.0, loadingImage, TRUE);
		ScreenFlip();
	}
}

void SelectMusic4Keys::OnTimeUpFolder(const std::shared_ptr<Music>& musicP)
{
	randomcenternum = GetRandom(1, musicP->danceSong->m_TotalMusicNum - 1, musicP);                //完全ランダム選択
	EscapeSelect(EscapeState::RANDOM_ALL, randomcenternum);                        //ランダムな数を渡す
	m_breakState = EscapeState::RANDOM_ALL;
}

void SelectMusic4Keys::OnTimeUpAA() const
{
}