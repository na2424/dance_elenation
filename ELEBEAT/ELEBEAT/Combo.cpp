#include "Combo.h"
#include "DxLib.h"
#include <memory>



void Combo::ShowEffect100Combo(int&& filter02)
{
	//100コンボ毎のアニメーションフラグ
	if (m_effect100ComboFrame < 25)
	{
		m_effect100ComboFrame++;
		if (m_effect100ComboFrame == 25 && m_ComboNum % 100 == 0)
			m_effect100ComboFrame--;
	}

	if (m_ComboNum % 100 == 0 && m_ComboNum != 0 && m_effect100ComboFrame >= 25)
		m_effect100ComboFrame = 0;

	if (0 <= m_effect100ComboFrame && m_effect100ComboFrame < 25)
	{
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, static_cast<int>(255.0 - (255.0 / 24.0*(m_effect100ComboFrame))));
		DrawGraph(0, 0, std::forward<int>(filter02), TRUE);
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}
}