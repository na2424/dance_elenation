#pragma once
#include "define.h"
class DxHelper
{
public:
	//オブジェクトのセットを補助
	static void SetObject(int& setObject, const int setType, const tstring& path, const char* cgName);
	static void SetObject(int setObject[], const int setType, const tstring& path, const char* cgName, int allNum, int xNum, int yNum, int xSize, int ySize);

	static double DxHelper::GetRatioImage(const int image);

	static void DrawOutlineString(const int x, const int y, const int textColor, const int outlineColor, const tstring& text, const tstring& t);

	static void DrawOutlineString(const int x, const int y, const int textColor, const int outlineColor, const tstring& text, int num);
	static void DrawOutlineString(const int x, const int y, const int textColor, const int outlineColor, const tstring& text, double num);

	static void DrawOutlineStringToHandle(const int x, const int y, const int textColor, const int outlineColor, const int fontHandle, const tstring& text, const int value);
	static void DrawOutlineStringToHandle(const int x, const int y, const int textColor, const int outlineColor, const int fontHandle, const tstring& text, const double value);
	//template<class T> static void DrawOutlineStringToHandle(int x, int y, int textColor, int outlineColor, int fontHandle, const tstring& text, const T& value, ...);
};