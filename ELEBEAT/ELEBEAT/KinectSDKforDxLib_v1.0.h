//動作確認済み.現在未使用

////------------------------------------------------------------------------------
//// KinectSDK for DxLib
//// SDK ver 1.0
////
//// 2012/02/09
//// Tea Sound Project
//// 茶音 麦粉　@ lscyane
//// http://www.cyane.info/
////------------------------------------------------------------------------------
//
//
//#include "DxLib.h"
//// MSR_NuiApi.hの前にWindows.hをインクルードする
//#include "NuiApi.h"
//
//#ifndef _KINECT_DXLIB
//#define _KINECT_DXLIB
//#ifdef __cplusplus
//extern "C" {
//#endif
///*
////#pragma comment(lib,"Kinect10.lib")
//#pragma comment(lib,"C:\\Program Files\\Microsoft SDKs\\Kinect\\v1.0\\lib\\x86\\Kinect10.lib")        //フルパス
//*/
//
//
////lookups for color tinting based on player index
////static const int g_IntensityShiftByPlayerR[] = { 0, 2, 0, 2, 0, 0, 2, 0 };
////static const int g_IntensityShiftByPlayerG[] = { 0, 2, 2, 0, 2, 0, 0, 1 };
////static const int g_IntensityShiftByPlayerB[] = { 0, 0, 2, 2, 0, 2, 0, 2 };
//
////RGBQUAD m_rgbWk[640 * 480];
//
//    class KinectDX{
//    private:
//        HRESULT            hr;                            // エラーチェック変数
//        HWND            m_hWnd;                        // メインウインドウハンドル
//        HINSTANCE       m_hInstance;
//
//        HANDLE            m_ImageEvent;
//        HANDLE            m_SkeletonEvent;
//        HANDLE            m_DepthEvent;
//        HANDLE            m_VideoStreamHandle;
//        HANDLE            m_DepthStreamHandle;
//
//        int                VideoGrHandle;                // ＤＸライブラリのグラフィックハンドル
//        int                DepthGrHandle;                // ＤＸライブラリのグラフィックハンドル
//        BASEIMAGE        BaseVideoImage;                // ＤＸライブラリ内部で画像を扱うために使用している構造体（カラーカメラ用）
//        BASEIMAGE        BaseDepthImage;                // （深さカメラ用）
//        int                softhandle;                    // ＤＸライブラリのソフトウェアイメージ
//
//        NUI_SKELETON_FRAME SkeletonFrame;            // スケルトン用のフレーム
//
//        // Current kinect
//        INuiSensor *            m_pNuiSensor;
//        BSTR                    m_instanceId;
//
//        int                lastSetSensorAngleTime;        // モーターの角度を変更した時間（we wait at least 1350ms afterwards）
//
//        //-------------------------------------------------------------------
//        // Zero out member variables
//        //-------------------------------------------------------------------
//        void Nui_Zero();
//
//        //-------------------------------------------------------------------
//        // Kinectの初期化
//        //-------------------------------------------------------------------
//        HRESULT Nui_Init();
//
//        //-------------------------------------------------------------------
//        // Uninitialize Kinect
//        //-------------------------------------------------------------------
//        void Nui_UnInit();
//
//        //-------------------------------------------------------------------
//        // 初回イメージハンドル作成
//        //-------------------------------------------------------------------
//        HRESULT InitDrawImage(int *GrHandle, HANDLE StreamHandle, BASEIMAGE *BaseImage);
//
//
//
//    public:
//        int k;
//        struct DepthStatus{
//            bool ColorPlayer;
//            bool DrawBackGround;
//            bool EnableCorrection;
//            bool EnableZeroDepth;
//            bool FillTransColor;
//        }GetDepthGrHandleOption;
//
//
//
//        // コンストラクタ
//        KinectDX() : m_hInstance(NULL){        // コンストラクタ
//
//            // 変数初期化
//            this->GetDepthGrHandleOption.ColorPlayer = true;
//            this->GetDepthGrHandleOption.DrawBackGround = true;
//            this->GetDepthGrHandleOption.EnableCorrection = true;
//            this->GetDepthGrHandleOption.EnableZeroDepth = true;
//            this->GetDepthGrHandleOption.FillTransColor = true;
//            this->k = 0;
//            softhandle = MakeXRGB8ColorSoftImage(640, 480);
//
//            // カラーフォーマットは変化しないので最初に設定
//            DxLib::CreateXRGB8ColorData(&BaseVideoImage.ColorData);        // キネクトからのデータは8bit*4なんでCreateFullColorDataじゃ駄目っぽい
//            DxLib::CreateXRGB8ColorData(&BaseDepthImage.ColorData);        // 深度データはようわからん（←
//
//            // ミップマップではないので０
//            BaseVideoImage.MipMapCount = 0;
//            BaseDepthImage.MipMapCount = 0;
//
//            // このクラスがグローバル領域での生成じゃないときは手動初期化しておくと吉
//            m_pNuiSensor = NULL;
//            m_instanceId = NULL;
//
//            Nui_Zero();                                // クラスのzero初期化
//            m_hWnd = DxLib::GetMainWindowHandle();    // メインウィンドウハンドルの保存
//            if (Nui_Init() != S_OK){                    // Kinectの初期化
//                this->k = -1;
//                return;
//            }
//
//            // 最初にグラフィックハンドルの作成を行う
//            WaitForSingleObject(m_ImageEvent, INFINITE);    // データの更新を待つ
//            WaitForSingleObject(m_DepthEvent, INFINITE);
//            InitDrawImage(&VideoGrHandle, m_VideoStreamHandle, &BaseVideoImage);
//            InitDrawImage(&DepthGrHandle, m_DepthStreamHandle, &BaseDepthImage);
//
//        }
//
//
//        // デストラクタ
//        ~KinectDX();
//
//
//        //-------------------------------------------------------------------
//        // モーターの操作    （-27〜+27　：それ以上も以下も頭打ちで一応動作する）
//        //-------------------------------------------------------------------
//        HRESULT SetTiltMotor(int angle);
//
//
//        //-------------------------------------------------------------------
//        // RGBカメラからデータを取得してグラフィックハンドルを返す
//        //-------------------------------------------------------------------
//        int GetVideoGrHandle(void);
//
//        //-------------------------------------------------------------------
//        // 深度カメラからデータを取得してグラフィックハンドルを返す
//        //-------------------------------------------------------------------
//        int _GetDepthGrHandle(bool ColorPlayer = true, bool DrawBG = true);
//
//        //-------------------------------------------------------------------
//        // カラー画像との座標変換した深度カメラからデータを取得してグラフィックハンドルを返す
//        //-------------------------------------------------------------------
//        int _GetDepthGrHandleFixed(bool ColorPlayer = true, bool DrawBG = true);
//
//        //-------------------------------------------------------------------
//        // 深度カメラからデータを取得してグラフィックハンドルを返す（多用途版：重い？）
//        //-------------------------------------------------------------------
//        int GetDepthGrHandle(void);
//
//        //-------------------------------------------------------------------
//        // トラッキングデータを取得する
//        //-------------------------------------------------------------------
//        int GetSkeletonData(Vector4 *pSkel1, Vector4 *pSkel2);
//
//        //-------------------------------------------------------------------
//        // 骨格の座標をカラー座標に変換する
//        //-------------------------------------------------------------------
//        HRESULT MapSkeletonPointToColor(Vector4 point, long *x, long *y);
//
//    };
//
//
//#ifdef __cplusplus
//}
//#endif
//
//#endif /* _KINECT */