#include"header.h"
//#include<omp.h>
#include"extern.h"
//トリプルDES暗号
unsigned char pc1[56] = {
	57,49,41,33,25,17, 9, 1,58,50,42,34,26,18,
	10, 2,59,51,43,35,27,19,11, 3,60,52,44,36,
	63,55,47,39,31,23,15, 7,62,54,46,38,30,22,
	14, 6,61,53,45,37,29,21,13, 5,28,20,12, 4
};

unsigned char pc2[48] = {
	14,17,11,24, 1, 5, 3,28,15, 6,21,10,
	23,19,12, 4,26, 8,16, 7,27,20,13, 2,
	41,52,31,37,47,55,30,40,51,45,33,48,
	44,49,39,56,34,53,46,42,50,36,29,32
};

unsigned char nshift[16] = {
	1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1
};

unsigned char inv_ipt[64] = {
	40, 8,48,16,56,24,64,32,
	39, 7,47,15,55,23,63,31,
	38, 6,46,14,54,22,62,30,
	37, 5,45,13,53,21,61,29,
	36, 4,44,12,52,20,60,28,
	35, 3,43,11,51,19,59,27,
	34, 2,42,10,50,18,58,26,
	33, 1,41, 9,49,17,57,25
};

unsigned char et[48] = {
	32, 1, 2, 3, 4, 5,
	 4, 5, 6, 7, 8, 9,
	 8, 9,10,11,12,13,
	12,13,14,15,16,17,
	16,17,18,19,20,21,
	20,21,22,23,24,25,
	24,25,26,27,28,29,
	28,29,30,31,32, 1
};

unsigned char sbox[8][64] = {
   {14, 4,13, 1, 2,15,11, 8, 3,10, 6,12, 5, 9, 0, 7, //SBox1
	 0,15, 7, 4,14, 2,13, 1,10, 6,12,11, 9, 5, 3, 8,
	 4, 1,14, 8,13, 6, 2,11,15,12, 9, 7, 3,10, 5, 0,
	15,12, 8, 2, 4, 9, 1, 7, 5,11, 3,14,10, 0, 6,13},

   {15, 1, 8,14, 6,11, 3, 4, 9, 7, 2,13,12, 0, 5,10, //SBox2
	 3,13, 4, 7,15, 2, 8,14,12, 0, 1,10, 6, 9,11, 5,
	 0,14, 7,11,10, 4,13, 1, 5, 8,12, 6, 9, 3, 2,15,
	13, 8,10, 1, 3,15, 4, 2,11, 6, 7,12, 0, 5,14, 9},

   {10, 0, 9,14, 6, 3,15, 5, 1,13,12, 7,11, 4, 2, 8, //SBox3
	13, 7, 0, 9, 3, 4, 6,10, 2, 8, 5,14,12,11,15, 1,
	13, 6, 4, 9, 8,15, 3, 0,11, 1, 2,12, 5,10,14, 7,
	 1,10,13, 0, 6, 9, 8, 7, 4,15,14, 3,11, 5, 2,12},

   { 7,13,14, 3, 0, 6, 9,10, 1, 2, 8, 5,11,12, 4,15, //SBox4
	13, 8,11, 5, 6,15, 0, 3, 4, 7, 2,12, 1,10,14, 9,
	10, 6, 9, 0,12,11, 7,13,15, 1, 3,14, 5, 2, 8, 4,
	 3,15, 0, 6,10, 1,13, 8, 9, 4, 5,11,12, 7, 2,14},

   { 2,12, 4, 1, 7,10,11, 6, 8, 5, 3,15,13, 0,14, 9, //SBox5
	14,11, 2,12, 4, 7,13, 1, 5, 0,15,10, 3, 9, 8, 6,
	 4, 2, 1,11,10,13, 7, 8,15, 9,12, 5, 6, 3, 0,14,
	11, 8,12, 7, 1,14, 2,13, 6,15, 0, 9,10, 4, 5, 3},

   {12, 1,10,15, 9, 2, 6, 8, 0,13, 3, 4,14, 7, 5,11, //SBox6
	10,15, 4, 2, 7,12, 9, 5, 6, 1,13,14, 0,11, 3, 8,
	 9,14,15, 5, 2, 8,12, 3, 7, 0, 4,10, 1,13,11, 6,
	 4, 3, 2,12, 9, 5,15,10,11,14, 1, 7, 6, 0, 8,13},

   { 4,11, 2,14,15, 0, 8,13, 3,12, 9, 7, 5,10, 6, 1, //SBox7
	13, 0,11, 7, 4, 9, 1,10,14, 3, 5,12, 2,15, 8, 6,
	 1, 4,11,13,12, 3, 7,14,10,15, 6, 8, 0, 5, 9, 2,
	 6,11,13, 8, 1, 4,10, 7, 9, 5, 0,15,14, 2, 3,12},

   {13, 2, 8, 4, 6,15,11, 1,10, 9, 3,14, 5, 0,12, 7, //SBox8
	 1,15,13, 8,10, 3, 7, 4,12, 5, 6,11, 0,14, 9, 2,
	 7,11, 4, 1, 9,12,14, 2, 0, 6,10,13,15, 3, 5, 8,
	 2, 1,14, 7, 4,10, 8,13,15,12, 9, 0, 3, 5, 6,11}
};

unsigned char pt[32] = {
	16, 7,20,21,29,12,28,17,
	 1,15,23,26, 5,18,31,10,
	 2, 8,24,14,32,27, 3, 9,
	19,13,30, 6,22,11, 4,25
};

unsigned char charkeys[3][8] =
{
	{ 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF },
	{ 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0 },
	{ 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0x01 },
};

unsigned char init[8] =
{
	'P', 'U', 'N', 'I', 'S', 'H', 'E', 'R'
};

void setdeskeys(unsigned _int64 key, unsigned _int64 deskeys[])
{
	unsigned _int64 pck1, pck2;
	int i, j, n;
	pck1 = 0;
	for (i = 0; i < 56; i++) {
		pck1 <<= 1;
		pck1 |= (key >> (64 - static_cast<int>(pc1[i]))) & 1;
	}
	for (i = 0; i < 16; i++) {
		n = nshift[i];
		pck1 <<= n;
		if (n == 1) {
			pck2 = pck1 & 0x0100000010000000;
			pck1 &= 0x00ffffffeffffffe;
		}
		else {
			pck2 = pck1 & 0x0300000030000000;
			pck1 &= 0x00ffffffcffffffc;
		}
		pck1 |= (pck2 >> 28);
		pck2 = 0;
		for (j = 0; j < 48; j++) {
			pck2 <<= 1;
			pck2 |= (pck1 >> (56 - static_cast<int>(pc2[j]))) & 1;
		}
		deskeys[i] = pck2;
	}
}

unsigned _int64 ip(unsigned _int64 cod)
{
	unsigned _int64 ans = 0;

	for (int i = 63; i >= 0; i--) {
		ans |= (cod & 1) << (64 - inv_ipt[i]);
		cod >>= 1;
	}
	return ans;
}

unsigned _int64 inv_ip(unsigned _int64 cod)
{
	unsigned _int64 ans = 0;

	for (int i = 0; i < 64; i++) {
		ans <<= 1;
		ans |= (cod >> (64 - inv_ipt[i])) & 1;
	}
	return ans;
}

unsigned _int64 c32to48(unsigned int cod)
{
	unsigned _int64 ans = 0;
	for (int i = 0; i < 48; i++) {
		ans |= (cod >> (32 - et[i])) & 1;
		ans <<= 1;
	}
	return ans;
}

unsigned int c32to32(unsigned int fes)
{
	unsigned int ans = 0;
	for (int i = 0; i < 32; i++) {
		ans <<= 1;
		ans |= (fes >> (32 - pt[i])) & 1;
	}
	return ans;
}

unsigned int feistel(unsigned _int64 deskey, unsigned int rn)
{
	unsigned int ans = 0, ps, fes = 0;
	unsigned _int64 e;

	e = c32to48(rn);
	e ^= deskey;
	for (int i = 0; i < 8; i++) {
		fes <<= 4;
		ps = (unsigned int)((e >> ((7 - i) * 6)) & 0x003f);
		fes |= sbox[i][((ps & 0x001e) >> 1) | (ps & 0x0020) | ((ps & 0x0001) << 4)];
	}
	ans = c32to32(fes);
	return ans;
}

unsigned _int64 des_encode(unsigned _int64 inf, unsigned _int64 key)
{
	unsigned _int64 enc, cod, deskeys[16];
	unsigned int l0, r0, ls;

	//    des-keys generation for 16 feistel-processes
	setdeskeys(key, deskeys);

	cod = ip(inf);
	l0 = (unsigned int)((cod & 0xffffffff00000000) >> 32);
	r0 = (unsigned int)(cod & 0x00000000ffffffff);
	for (int i = 0; i < 16; i++) {
		ls = r0;
		r0 = l0 ^ feistel(deskeys[i], r0); l0 = ls;
	}
	cod = (((unsigned _int64)l0) << 32) | (unsigned _int64)r0;
	enc = inv_ip(cod);
	return enc;
}

unsigned _int64 des_decode(unsigned _int64 cip, unsigned _int64 key)
{
	unsigned _int64 dec, cod, deskeys[16];
	unsigned int l0, r0, rs;

	//    des-keys generation for 16 feistel-processes
	setdeskeys(key, deskeys);

	cod = ip(cip);
	l0 = (unsigned int)((cod & 0xffffffff00000000) >> 32);
	r0 = (unsigned int)(cod & 0x00000000ffffffff);
	for (int i = 15; i >= 0; i--) {
		rs = l0;
		l0 = r0 ^ feistel(deskeys[i], l0); r0 = rs;
	}
	cod = (((unsigned _int64)l0) << 32) | (unsigned _int64)r0;
	dec = inv_ip(cod);
	return dec;
}

unsigned _int64 tr_des_encode(unsigned _int64 inf, unsigned _int64 keys[])
{
	unsigned _int64 cod;
	cod = des_encode(inf, keys[0]);
	cod = des_decode(cod, keys[1]);
	cod = des_encode(cod, keys[2]);
	return cod;
}

unsigned _int64 tr_des_decode(unsigned _int64 cip, unsigned _int64 keys[])
{
	unsigned _int64 cod;
	cod = des_decode(cip, keys[2]);
	cod = des_encode(cod, keys[1]);
	cod = des_decode(cod, keys[0]);
	return cod;
}

_int64 setchartoint64(unsigned char ch[])
{
	_int64 ans = 0;
	for (int i = 0; i < 8; i++) {
		ans <<= 8; ans |= (unsigned _int64)ch[i];
	}
	return ans;
}