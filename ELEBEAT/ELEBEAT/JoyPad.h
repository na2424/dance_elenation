#ifndef JOYPAD_H
#define JOYPAD_H

class JoyPad
{
public:
	int m_Key[8][2];
	int m_K[4][4];
	JoyPad()
	{
		std::fill(m_Key[0], m_Key[8], -1);
		std::fill(m_K[0], m_K[4], -1);
	}
};

#endif