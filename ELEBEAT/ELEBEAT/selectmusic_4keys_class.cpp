/*--------------------------AAの座標について書く--------------------------
CENTERLEFTX:352: 正面のAAの左側の座標
CENTERLEFTY:142: 左上y 今142
AAsize      :256: AAの大きさ
解像度変更などの際はCENTERLEFTXを変更
AAsizeも同じように対応できるはず
----------------------------------------------------------------------*/
#include "SelectMusic4keys.h"
#include "DxLib.h"
#include "Color.h"
#include "define.h"
#include "DxHelper.h"
#include "extern.h"
#include "header.h"
#include "Input.h"
#include "stdlib.h"
#include "GameBase.h"
#include <algorithm>
#include <iterator>
#include <random>
#include <thread>
#include <vector>
#include <boost/math/special_functions/sign.hpp>

#include <fstream>
#include <iterator>
#include <vector>

using namespace DxLib;


//x,yを設定 Coordinates = 座標
void SelectMusic4Keys::GraphCoordinatesSet()
{
	/*************************x座標*************************/

	//正面画像のx
	m_x[15][0] = x[15][0] = CENTER_LEFT_X;
	m_x[15][1] = x[15][1] = CENTER_LEFT_X + AA_SIZE;
	m_x[15][2] = x[15][2] = CENTER_LEFT_X + AA_SIZE;
	m_x[15][3] = x[15][3] = CENTER_LEFT_X;

	//一個右x
	m_x[16][0] = x[16][0] = CENTER_LEFT_X + AA_SIZE;
	m_x[16][1] = x[16][1] = CENTER_LEFT_X + AA_SIZE + hukasi + AA_AA_BETWEEN_PX;
	m_x[16][2] = x[16][2] = CENTER_LEFT_X + AA_SIZE + hukasi + AA_AA_BETWEEN_PX;
	m_x[16][3] = x[16][3] = CENTER_LEFT_X + AA_SIZE;

	//右二個以降
	for (auto i = 17, j = 0; i <= 21; ++i, ++j)
	{
		m_x[i][0] = x[i][0] = CENTER_LEFT_X + AA_SIZE + ((j + 1)*AA_AA_BETWEEN_PX);
		m_x[i][1] = x[i][1] = CENTER_LEFT_X + AA_SIZE + Modi + ((j + 1)*AA_AA_BETWEEN_PX);
		m_x[i][2] = x[i][2] = CENTER_LEFT_X + AA_SIZE + Modi + ((j + 1)*AA_AA_BETWEEN_PX);
		m_x[i][3] = x[i][3] = CENTER_LEFT_X + AA_SIZE + ((j + 1)*AA_AA_BETWEEN_PX);
	}

	//一個左x 125差左と右
	m_x[14][0] = x[14][0] = CENTER_LEFT_X - 125;
	m_x[14][1] = x[14][1] = CENTER_LEFT_X;
	m_x[14][2] = x[14][2] = CENTER_LEFT_X;
	m_x[14][3] = x[14][3] = CENTER_LEFT_X - 125;

	//左二個以降
	for (auto i = 13, j = 0; i >= 9; --i, ++j)
	{
		m_x[i][0] = x[i][0] = CENTER_LEFT_X - Modi - (j + 1)*AA_AA_BETWEEN_PX;
		m_x[i][1] = x[i][1] = CENTER_LEFT_X - Modi - (j*AA_AA_BETWEEN_PX) + hukasi;
		m_x[i][2] = x[i][2] = CENTER_LEFT_X - Modi - (j*AA_AA_BETWEEN_PX) + hukasi;
		m_x[i][3] = x[i][3] = CENTER_LEFT_X - Modi - (j + 1)*AA_AA_BETWEEN_PX;
	}

	/*************************y座標*************************/
	//正面画像のy さっきのfor文の中を再設定
	y[15][0] = CENTER_LEFT_Y;
	y[15][1] = CENTER_LEFT_Y;
	y[15][2] = CENTER_LEFT_Y + AA_SIZE;
	y[15][3] = CENTER_LEFT_Y + AA_SIZE;
	m_y[15][0] = y[15][3] + AA_SIZE + AA_MIRROR_BETWEEN_PX;
	m_y[15][1] = y[15][2] + AA_SIZE + AA_MIRROR_BETWEEN_PX;
	m_y[15][2] = y[15][2] + AA_MIRROR_BETWEEN_PX;
	m_y[15][3] = y[15][3] + AA_MIRROR_BETWEEN_PX;

	//一個右y
	y[16][0] = CENTER_LEFT_Y + 50;
	y[16][1] = CENTER_LEFT_Y;
	y[16][2] = CENTER_LEFT_Y + AA_SIZE;
	y[16][3] = CENTER_LEFT_Y + AA_SIZE - 50;
	m_y[16][0] = y[16][0] + (y[16][2] - y[16][1]) + (y[16][3] - y[16][0]) + AA_MIRROR_BETWEEN_PX;
	m_y[16][1] = y[16][1] + (y[16][2] - y[16][1]) + (y[16][3] - y[16][0]) + AA_MIRROR_BETWEEN_PX;
	m_y[16][2] = y[16][2] + AA_MIRROR_BETWEEN_PX;
	m_y[16][3] = y[16][3] + AA_MIRROR_BETWEEN_PX;

	//右二個以降
	for (auto i = 17; i <= 21; ++i)
	{
		y[i][0] = CENTER_LEFT_Y + 50;
		y[i][1] = CENTER_LEFT_Y;
		y[i][2] = CENTER_LEFT_Y + AA_SIZE;
		y[i][3] = CENTER_LEFT_Y + AA_SIZE - 50;
		m_y[i][0] = y[i][0] + (y[i][2] - y[i][1]) + (y[i][3] - y[i][0]) + AA_MIRROR_BETWEEN_PX;
		m_y[i][1] = y[i][1] + (y[i][2] - y[i][1]) + (y[i][3] - y[i][0]) + AA_MIRROR_BETWEEN_PX;
		m_y[i][2] = y[i][2] + AA_MIRROR_BETWEEN_PX;
		m_y[i][3] = y[i][3] + AA_MIRROR_BETWEEN_PX;
	}

	//一個左y
	y[14][0] = CENTER_LEFT_Y;
	y[14][1] = CENTER_LEFT_Y + 50;
	y[14][2] = CENTER_LEFT_Y + AA_SIZE - 50;
	y[14][3] = CENTER_LEFT_Y + AA_SIZE;
	m_y[14][0] = y[14][0] + (y[14][2] - y[14][1]) + (y[14][3] - y[14][0]) + AA_MIRROR_BETWEEN_PX;
	m_y[14][1] = y[14][1] + (y[14][2] - y[14][1]) + (y[14][3] - y[14][0]) + AA_MIRROR_BETWEEN_PX;
	m_y[14][2] = y[14][2] + AA_MIRROR_BETWEEN_PX;
	m_y[14][3] = y[14][3] + AA_MIRROR_BETWEEN_PX;

	//左二個以降
	for (auto i = 13; i >= 9; --i)
	{
		y[i][0] = CENTER_LEFT_Y;
		y[i][1] = CENTER_LEFT_Y + 50;
		y[i][2] = CENTER_LEFT_Y + AA_SIZE - 50;
		y[i][3] = CENTER_LEFT_Y + AA_SIZE;
		m_y[i][0] = y[i][0] + (y[i][2] - y[i][1]) + (y[i][3] - y[i][0]) + AA_MIRROR_BETWEEN_PX;
		m_y[i][1] = y[i][1] + (y[i][2] - y[i][1]) + (y[i][3] - y[i][0]) + AA_MIRROR_BETWEEN_PX;
		m_y[i][2] = y[i][2] + AA_MIRROR_BETWEEN_PX;
		m_y[i][3] = y[i][3] + AA_MIRROR_BETWEEN_PX;
	}
}
void SelectMusic4Keys::MarkerCoordinatesSet()
{
	auto j = 1;

	//真ん中
	g_MarkerX[8][0] = g_MarkerX[8][3] = MARKER_LEFT_X;
	g_MarkerX[8][1] = g_MarkerX[8][2] = MARKER_LEFT_X + MARKER_SIZE;

	j = 1;

	//x右
	for (auto i = 9; i <= 13; ++i)
	{
		g_MarkerX[i][0] = g_MarkerX[i][3] = g_MarkerX[8][0] + (MARKERBETWEENSIZE * j) + (MARKER_SIZE * j);    //左側
		g_MarkerX[i][1] = g_MarkerX[i][2] = g_MarkerX[8][1] + (MARKERBETWEENSIZE * j) + (MARKER_SIZE * j);    //右側
		j++;
	}

	j = 1;

	//x左
	for (auto i = 7; i >= 3; --i)
	{
		g_MarkerX[i][0] = g_MarkerX[i][3] = g_MarkerX[8][0] - ((MARKERBETWEENSIZE * j) + (MARKER_SIZE * j));    //左側
		g_MarkerX[i][1] = g_MarkerX[i][2] = g_MarkerX[8][1] - ((MARKERBETWEENSIZE * j) + (MARKER_SIZE * j));    //右側
		j++;
	}

	//y
	for (auto i = 3; i <= 13; ++i)
	{
		g_MarkerY[i][0] = g_MarkerY[i][1] = MARKER_LEFT_Y;
		g_MarkerY[i][2] = g_MarkerY[i][3] = MARKER_LEFT_Y + MARKER_SIZE;
	}
}

//AA描画 引数はglobalAAを内部的に受け取る
//AA[1000] の1000の部分に関係
void SelectMusic4Keys::DrawGraphAll(int nowcenter)
{
	constexpr auto CENTER_INDEX = 15;
	constexpr auto LINE_NUM = 6;
	int plus[LINE_NUM] = {};
	int minus[LINE_NUM] = {};

	const auto drawImage = [](auto& x, auto& y, int image, int i)
	{
		DrawModiGraph(
			static_cast<int>(x[i][0]), static_cast<int>(y[i][0]),
			static_cast<int>(x[i][1]), static_cast<int>(y[i][1]),
			static_cast<int>(x[i][2]), static_cast<int>(y[i][2]),
			static_cast<int>(x[i][3]), static_cast<int>(y[i][3]),
			image, TRUE);
	};

	const auto drawImageList = [&drawImage](auto& x, auto& y, auto& images, auto& n, int start, int end)
	{
		auto isFoward = (start - end) < 0;
		auto isLoop = [&isFoward, &end](int num)
		{
			return isFoward ? num <= end : num >= end;
		};

		for (auto i = start, j = 5; isLoop(i); isFoward ? ++i : --i, --j)
		{
			drawImage(x, y, images[n[j]], i);
		}
	};

	//folderaa/AAの画像設定
	if (selectmode == SelectMode::FOLDER)
	{
		//フォルダなら
		/*******************************鏡面*******************************/
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 128);//以後半分の明るさでαブレンド
		for (auto i = 0; i < LINE_NUM; ++i)
		{
			plus[i] = (nowcenter + (i + 1)) % m_musicKind;
			minus[i] = (nowcenter - (i + 1) + m_musicKind * 6) % m_musicKind;///!!!
		}

		//folder右側
		drawImageList(m_x, m_y, g_KindFolder4, plus, CENTER_INDEX + LINE_NUM, CENTER_INDEX + 1);

		//folder左側
		drawImageList(m_x, m_y, g_KindFolder4, minus, CENTER_INDEX - LINE_NUM, CENTER_INDEX - 1);

		//folder正面
		drawImage(m_x, m_y, g_KindFolder4[nowcenter], CENTER_INDEX);

		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);//ブレンドモードをリセットする。

		/********************************非鏡面********************************/
		//folder右側
		drawImageList(x, y, g_KindFolder4, plus, CENTER_INDEX + LINE_NUM, CENTER_INDEX + 1);

		//folder左側
		drawImageList(x, y, g_KindFolder4, minus, CENTER_INDEX - LINE_NUM, CENTER_INDEX - 1);

		//folder正面
		drawImage(x, y, g_KindFolder4[nowcenter], CENTER_INDEX);
	}
	else
	{
		//AAなら
		/*******************************鏡面*******************************/
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 128);//以後半分の明るさでαブレンド

		for (auto i = 0; i < LINE_NUM; ++i)
		{
			plus[i] = (nowcenter + (i + 1)) % m_musicKind4Max[m_selectFolderNum];
			minus[i] = (nowcenter - (i + 1) + m_musicKind4Max[m_selectFolderNum] * 6) % m_musicKind4Max[m_selectFolderNum];
		}

		constexpr auto MODI = AA_SIZE - TRANCE;
		constexpr auto MODI_HARF = 0.5 * MODI;
		constexpr auto AA_SIZE_HARF = 0.5 * AA_SIZE;

		//AA右側
		for (auto i = 0; i < LINE_NUM; ++i)
		{
			auto globalIndex = CENTER_INDEX + LINE_NUM - i;
			auto localIndex = (LINE_NUM - 1) - i;

			DrawModiGraph(
				(int)m_x[globalIndex][0], (int)(m_y[globalIndex][0] - MODI_HARF*   (1.0 - g_BannerSizeRatio[plus[localIndex]]) - ((1.0 - g_BannerSizeRatio[plus[localIndex]])*AA_SIZE)),
				(int)m_x[globalIndex][1], (int)(m_y[globalIndex][1] - AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[plus[localIndex]]) - ((1.0 - g_BannerSizeRatio[plus[localIndex]])*MODI)),
				(int)m_x[globalIndex][2], (int)(m_y[globalIndex][2] - AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[plus[localIndex]])),
				(int)m_x[globalIndex][3], (int)(m_y[globalIndex][3] - MODI_HARF*   (1.0 - g_BannerSizeRatio[plus[localIndex]])),
				g_Banner[plus[localIndex]], FALSE);
		}

		//AA左側
		for (auto i = 0; i < LINE_NUM; ++i)
		{
			auto globalIndex = CENTER_INDEX - LINE_NUM + i;
			auto localIndex = (LINE_NUM - 1) - i;

			DrawModiGraph(
				(int)m_x[globalIndex][0], (int)(m_y[globalIndex][0] - AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[minus[localIndex]]) - ((1.0 - g_BannerSizeRatio[minus[localIndex]])*MODI)),
				(int)m_x[globalIndex][1], (int)(m_y[globalIndex][1] - MODI_HARF*   (1.0 - g_BannerSizeRatio[minus[localIndex]]) - ((1.0 - g_BannerSizeRatio[minus[localIndex]])*AA_SIZE)),
				(int)m_x[globalIndex][2], (int)(m_y[globalIndex][2] - MODI_HARF*   (1.0 - g_BannerSizeRatio[minus[localIndex]])),
				(int)m_x[globalIndex][3], (int)(m_y[globalIndex][3] - AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[minus[localIndex]])),
				g_Banner[minus[localIndex]], FALSE);
		}

		//AA正面
		DrawModiGraph(
			(int)m_x[15][0], (int)(m_y[15][0] - AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[nowcenter]) - ((1.0 - g_BannerSizeRatio[nowcenter])*AA_SIZE)),
			(int)m_x[15][1], (int)(m_y[15][1] - AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[nowcenter]) - ((1.0 - g_BannerSizeRatio[nowcenter])*AA_SIZE)),
			(int)m_x[15][2], (int)(m_y[15][2] - AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[nowcenter])),
			(int)m_x[15][3], (int)(m_y[15][3] - AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[nowcenter])),
			g_Banner[nowcenter], FALSE);

		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);//ブレンドモードをリセットする。

		/********************************非鏡面********************************/
		//AA右側
		for (auto i = 0; i < LINE_NUM; ++i)
		{
			auto globalIndex = CENTER_INDEX + LINE_NUM - i;
			auto localIndex = (LINE_NUM - 1) - i;

			DrawModiGraph(
				(int)x[globalIndex][0], (int)(y[globalIndex][0] + MODI_HARF*   (1.0 - g_BannerSizeRatio[plus[localIndex]])), //xy1
				(int)x[globalIndex][1], (int)(y[globalIndex][1] + AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[plus[localIndex]])), //xy2
				(int)x[globalIndex][2], (int)(y[globalIndex][2] - AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[plus[localIndex]])), //xy3
				(int)x[globalIndex][3], (int)(y[globalIndex][3] - MODI_HARF*   (1.0 - g_BannerSizeRatio[plus[localIndex]])), //xy4
				g_Banner[plus[localIndex]], FALSE);
		}

		//AA左側
		for (auto i = 0; i < LINE_NUM; ++i)
		{
			auto globalIndex = CENTER_INDEX - LINE_NUM + i;
			auto localIndex = (LINE_NUM - 1) - i;

			DrawModiGraph(
				(int)x[globalIndex][0], (int)(y[globalIndex][0] + AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[minus[localIndex]])), //xy1
				(int)x[globalIndex][1], (int)(y[globalIndex][1] + MODI_HARF*   (1.0 - g_BannerSizeRatio[minus[localIndex]])), //xy2
				(int)x[globalIndex][2], (int)(y[globalIndex][2] - MODI_HARF*   (1.0 - g_BannerSizeRatio[minus[localIndex]])), //xy3
				(int)x[globalIndex][3], (int)(y[globalIndex][3] - AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[minus[localIndex]])), //xy4
				g_Banner[minus[localIndex]], FALSE);
		}

		//AA正面
		DrawModiGraph(
			(int)x[CENTER_INDEX][0], (int)(y[CENTER_INDEX][0] + AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[nowcenter])),
			(int)x[CENTER_INDEX][1], (int)(y[CENTER_INDEX][1] + AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[nowcenter])),
			(int)x[CENTER_INDEX][2], (int)(y[CENTER_INDEX][2] - AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[nowcenter])),
			(int)x[CENTER_INDEX][3], (int)(y[CENTER_INDEX][3] - AA_SIZE_HARF*(1.0 - g_BannerSizeRatio[nowcenter])),
			g_Banner[nowcenter], FALSE);
	}
}

/// <summary>
/// キーとマウス操作のenumを返す
/// </summary>
/// <param name="nowcenter"></param>
/// <param name="selectdifsound"></param>
/// <param name="difRect"></param>
/// <param name="s_flag"></param>
/// <returns></returns>
KeySelectMusicType SelectMusic4Keys::CatchKey(int nowcenter, int& selectdifsound, int difRect, bool *s_flag)
{
	constexpr auto MOUSE_RECT_LEFT = WIN_WIDTH / 2 - 160;
	constexpr auto MOUSE_RECT_RIGHT = WIN_WIDTH / 2 + 240;
	constexpr auto MOUSE_RECT_TOP_START = 554;
	constexpr auto MOUSE_RECT_BOTTOM_START = 639;
	constexpr auto MOUSE_RECT_INTERVAL = 42;
	constexpr auto MAX_DIFFICULTY = 5;

	Input& input = Input::GetInstance();

	// キー左右移動
	if (Input::GetInstance().keyShift == 0)
	{
		//Lshift押してないとき
		//画像を左に
		if (Input::GetInstance().keyRight > 0 && isRight != 1 && m_InputFrameCount == 0)
		{
			isLeft = true;
			isLeftContinue = true;
		}
		if (Input::GetInstance().keyRight == 0)
			isLeftContinue = false;
		//画像を右に
		if (Input::GetInstance().keyLeft > 0 && isLeft != 1 && m_InputFrameCount == 0)
		{
			isRight = true;
			isRightContinue = true;
		}
		if (Input::GetInstance().keyLeft == 0)
			isRightContinue = false;
	}

	// マウス左右移動
	if (((WIN_WIDTH / 2 - 240 < input.mouseX && input.mouseX < WIN_WIDTH / 2 - 140) && (326 < input.mouseY && input.mouseY < 326 + 100)))
	{
		if (Input::GetInstance().mouseClick > 0 && isRight != 1 && m_InputFrameCount == 0)
		{
			isRight = true;
			isRightContinue = true;
		}
	}
	if (((WIN_WIDTH / 2 + 140 < input.mouseX && input.mouseX < WIN_WIDTH / 2 + 240) && (326 < input.mouseY && input.mouseY < 326 + 100)))
	{
		if (Input::GetInstance().mouseClick > 0 && isRight != 1 && m_InputFrameCount == 0)
		{
			isLeft = true;
			isLeftContinue = true;
		}
	}
	// Lshift押してないとき
	if (Input::GetInstance().keyShift == 0)
	{
		//難易度変更
		if (((Input::GetInstance().keyDown == 1 && one[0] == 0) || (Input::GetInstance().mouseWheel < 0 && one[0] == false)) && g_DifNowCenter<(MAX_DIFFICULTY-1))
		{
			one[0] = true;
			*s_flag = TRUE;
			g_DifNowCenter++;
			DxLib::PlaySoundMem(selectdifsound, DX_PLAYTYPE_BACK);
		}
		if (Input::GetInstance().keyDown != 1 && Input::GetInstance().mouseWheel == 0)
			one[0] = false;

		//難易度変更
		if (((Input::GetInstance().keyUp == 1 && one[1] == 0) || (0 < Input::GetInstance().mouseWheel && one[1] == false)) && g_DifNowCenter > 1)
		{
			one[1] = true;
			*s_flag = TRUE;
			g_DifNowCenter--;
			DxLib::PlaySoundMem(selectdifsound, DX_PLAYTYPE_BACK);
		}
		if (Input::GetInstance().keyUp != 1 && Input::GetInstance().mouseWheel == 0)
			one[1] = false;
	}
	//マウス難易度変更
	for (auto i = 0; i < 3; ++i)
	{
		if (((MOUSE_RECT_LEFT < input.mouseX && input.mouseX < MOUSE_RECT_RIGHT) && (MOUSE_RECT_TOP_START + MOUSE_RECT_INTERVAL * i < input.mouseY && input.mouseY < MOUSE_RECT_BOTTOM_START + MOUSE_RECT_INTERVAL * i)))
		{
			if (Input::GetInstance().mouseClick == 1 && !isRight && !isLeft && m_InputFrameCount == 0)
			{
				g_DifNowCenter = i + difRect;
				DxLib::PlaySoundMem(selectdifsound, DX_PLAYTYPE_BACK);
				*s_flag = TRUE;
			}
		}
	}

	//決定操作
	if (Input::GetInstance().keyEnter == 1 && !isRight && !isLeft && one[2] == false)
	{
		if (selectmode == SelectMode::FOLDER) 
		{
			//グループフォルダ中
			if (nowcenter == CENTER_DEFAULT) 
			{
				one[2] = true;
				//folderでrand選択
				return KeySelectMusicType::FOLDER_RANDOM;
			}
			else 
			{
				one[2] = true;
				//folder->AA
				return KeySelectMusicType::FOLDER_TO_AA;
			}
		}
		else 
		{   
			//AA中
			if (nowcenter == CENTER_DEFAULT) 
			{
				//自分のフォルダの時
				one[2] = true;
				//AA->folder
				return KeySelectMusicType::AA_TO_FOLDER;
			}
			else 
			{
				//通常選択の時
				one[2] = true;
				//100の時nowcenterを返す
				return KeySelectMusicType::DEFAULT_SELECT;
			}
		}
	}
	//マウス決定操作
	if ((CENTER_LEFT_X < input.mouseX && input.mouseX < CENTER_LEFT_X + AA_SIZE) && (CENTER_LEFT_Y < input.mouseY && input.mouseY < CENTER_LEFT_Y + AA_SIZE))
	{
		if (Input::GetInstance().mouseClick == 1 && !isRight && !isLeft && one[2] == false)
		{
			if (selectmode == SelectMode::FOLDER) 
			{
				//グループフォルダ中
				if (nowcenter == CENTER_DEFAULT) 
				{
					//randの時
					one[2] = true;
					//folderでrand選択
					return KeySelectMusicType::FOLDER_RANDOM;    
				}
				else 
				{
					one[2] = true;
					//folder->AA
					return KeySelectMusicType::FOLDER_TO_AA;
				}
			}
			else 
			{    
				//AA中
				if (nowcenter == CENTER_DEFAULT) 
				{
					//自分のフォルダの時
					one[2] = true;
					return KeySelectMusicType::AA_TO_FOLDER;
				}
				else 
				{
					//通常選択の時
					one[2] = true;
					//100の時nowcenterを返す
					return KeySelectMusicType::DEFAULT_SELECT;    
				}
			}
		}
	}
	if (Input::GetInstance().keyEnter != 1 && Input::GetInstance().mouseClick == 0)
		one[2] = false;

	//ESCキーが押されたら
	if (Input::GetInstance().keyEsc == 1 && !isRight && !isLeft && one[3] == false)
	{
		one[3] = true;
		if (selectmode == SelectMode::AA) {
			//folderに戻る
			return KeySelectMusicType::AA_TO_FOLDER;
		}
		else
		{
			//全体的にbreak
			return KeySelectMusicType::BREAK;
		}
	}
	if (Input::GetInstance().keyEsc == 0)
		one[3] = false;

	//shift操作
	if (Input::GetInstance().keyShift > 0 && m_InputFrameCount == 0)
	{
		//移動させない
		isLeft = false;
		isRight = false;
		isLeftContinue = false;
		isRightContinue = false;
		isFastLeft = false;
		isFastRight = false;
		countMoveAA = 0;
		//マーカーまわり初期化処理
		markerflag = true;
		//左右　LSHIFTを押したときの間
		if (Input::GetInstance().keyRight == 1 && one[4] == false)
		{
			isRightMarker = true;
			one[4] = true;
		}
		if (Input::GetInstance().keyRight == 0)
			one[4] = false;
		if (Input::GetInstance().keyLeft == 1 && one[5] == false)
		{
			isLeftMarker = true;
			one[5] = true;
		}
		if (Input::GetInstance().keyLeft == 0)
			one[5] = false;

		if (Input::GetInstance().keyDown == 1 && one[0] == false)
		{
			isDownMarker = true;
			one[0] = true;
		}
		if (Input::GetInstance().keyDown == 0)
			one[0] = false;
		if (Input::GetInstance().keyUp == 1 && one[1] == false)
		{
			marker_goup = true;
			one[1] = true;
		}
		if (Input::GetInstance().keyUp == 0)
			one[1] = false;
	}
	if (Input::GetInstance().keyShift == 0)
	{
		one[4] = false;
		one[5] = false;
		markerflag = false;
	}

	return KeySelectMusicType::NONE;        //関係ないreturnは50で統一
}
//folder<-->AA表示の変更 (folderとAAの相互変換)
int SelectMusic4Keys::ChangeMode(int nowcenter, const std::shared_ptr<Music> &musicP)
{
	if (selectmode == SelectMode::FOLDER) //フォルダからAAなら
	{
		//変数
		int foldernumber = 0;                        //一般化したフォルダ番号
		int musicsum = 0;                            //総計からフォルダを決定する

		//処理開始
		foldernumber = NowFolderNumber(nowcenter, musicP);    //開いたフォルダ番号取得
		this->m_selectFolderNum = foldernumber;

		if (musicP->danceSong->m_MusicNumAt[foldernumber] == 0)        //フォルダ内部の曲が0の時
			return nowcenter;                        //何もしないで帰る
		selectmode = SelectMode::AA;                                //AA表示に変更

		g_Beforecenter4 = nowcenter;                    //前のfolder番号を保持
		TCHAR foldername[260];
		_tcscpy_s(foldername, 260, musicP->danceSong->m_FolderName[foldernumber].c_str());//フォルダ名を保持

		for (int i = 0; i <= foldernumber; ++i)
		{
			//曲数
			musicsum += musicP->danceSong->m_MusicNumAt[i];
		}
		//グローバルに渡す
		g_CurrentAlbumArt[0] = musicsum - musicP->danceSong->m_MusicNumAt[foldernumber];        //最初の音楽番号(一桁)
		g_CurrentAlbumArt[1] = musicsum - 1;                                    //最後の音楽番号
		g_CurrentAlbumArt[2] = g_CurrentAlbumArt[1] - g_CurrentAlbumArt[0] + 1;                        //音楽総計

		SetAAGraph(CENTER_DEFAULT + musicsum - musicP->danceSong->m_MusicNumAt[foldernumber], CENTER_DEFAULT + musicsum - 1, nowcenter);//番号よりAA設定

		return CENTER_DEFAULT;
	}
	else //AAより戻るとき
	{
		selectmode = SelectMode::FOLDER;
		return this->m_selectFolderNum;
	}
	return -1;
}

void SelectMusic4Keys::SetMode(int nowcenter, const std::shared_ptr<Music> &musicP)
{
	//総計からフォルダを決定する
	auto musicsum = 0;

	//処理開始

	//開いたフォルダ番号取得
	const auto foldernumber = nowcenter;
	this->m_selectFolderNum = foldernumber;
	g_Beforecenter4 = beforeSelectMusicNum4;

	//0からそのフォルダまでの音楽総計
	for (auto i = 0; i <= foldernumber; ++i)
	{
		musicsum += musicP->danceSong->m_MusicNumAt[i];
	}
	//グローバルに渡す
	g_CurrentAlbumArt[0] = musicsum - musicP->danceSong->m_MusicNumAt[foldernumber];        //最初の音楽番号(一桁)
	g_CurrentAlbumArt[1] = musicsum - 1;                                    //最後の音楽番号
	g_CurrentAlbumArt[2] = g_CurrentAlbumArt[1] - g_CurrentAlbumArt[0] + 1;                        //音楽総計

	SetAAGraph(CENTER_DEFAULT + musicsum - musicP->danceSong->m_MusicNumAt[foldernumber], CENTER_DEFAULT + musicsum - 1, g_Beforecenter4 - g_CurrentAlbumArt[0]);//番号よりAA設定
}

void SelectMusic4Keys::UpdateLoadingImage(lua_State *L) const
{
	const auto angle = static_cast<double>(lua_tonumber(L, 1));
	ClearDrawScreen();
	DrawRotaGraph(200, 200, 1.0, angle, this->loadingImage, TRUE, TRUE);
	ScreenFlip();
}

void SelectMusic4Keys::RegisterLuaFunctions()
{
	//lua_register(L, "c_UpdateLoadingImage", UpdateLoadingImage);
}

//void SelectMusic4keys::UpdateLoadingImage(lua_State *L)
//{
//    ClearDrawScreen();
//    DrawRotaGraph(200,200, 1.0, Angle, this->loadingImage, TRUE,TRUE);
//    ScreenFlip();
//}

// 最初にロードする関数
void SelectMusic4Keys::LoadGraphAll(const std::shared_ptr<Music> &musicP)
{
	//luaopen_math(L);

	//// 関数の登録
	//RegisterLuaFunctions();

	SelectMusic4Keys::g_Banner.clear();
	SelectMusic4Keys::g_BannerSizeRatio.clear();
	SelectMusic4Keys::g_TempBannerSizeRatio.clear();

	this->m_musicKind = musicP->danceSong->m_FolderNum;
	this->m_musicNum = musicP->danceSong->m_TotalMusicNum;
	copy(musicP->danceSong->m_MusicNumAt.begin(), musicP->danceSong->m_MusicNumAt.end(), back_inserter(this->m_musicKind4Max));
	///////////////////////////////////AA

	int j = CENTER_DEFAULT;
	//縦横サイズBNとフォルダ用
	int BN_X = 0;
	int BN_Y = 0;
	int FL_X = 0;
	int FL_Y = 0;
	clsDx();
	printfDx("Now Loading...");
	ScreenFlip();

	for (int i = 0; i < musicP->danceSong->m_TotalMusicNum; ++i)
	{
		if (LoadGraph(musicP->danceSong->m_BannerPath[i].c_str()) == -1)
		{
			//画像が無い時
			if (i == musicP->danceSong->m_TotalMusicNum - 1)
			{
				SelectMusic4Keys::g_Banner.emplace_back(LoadGraph(_T("img\\nofolder.png")));
			}
			else
			{
				SelectMusic4Keys::g_Banner.emplace_back(LoadGraph(_T("img\\noalbumart.png")));
			}
		}
		else
		{
			//画像が有る時
			SelectMusic4Keys::g_Banner.emplace_back(LoadGraph(musicP->danceSong->m_BannerPath[i].c_str()));
		}
	}

	copy(SelectMusic4Keys::g_Banner.begin(), SelectMusic4Keys::g_Banner.end(), back_inserter(SelectMusic4Keys::g_TempBanner));

	/////////////////////folder

	int i = 0;
	j = CENTER_DEFAULT;

	while (i != musicP->danceSong->m_FolderNum)
	{
		if (LoadGraph(musicP->danceSong->m_FolderBannerPath[i].c_str()) == -1)
		{   //エラ−のとき
			g_KindFolder4.emplace_back(LoadGraph(TEXT("img\\nofolder.png")));
		}
		else
		{   //エラーじゃないとき
			g_KindFolder4.emplace_back(LoadGraph(musicP->danceSong->m_FolderBannerPath[i].c_str()));
		}
		i++;
		j++;
	}

	i = CENTER_DEFAULT;

	g_IsLoaded4Keys = true;
	i = CENTER_DEFAULT;
	j = 0;
	//画像の縦横比を保持
	while (i != musicP->danceSong->m_TotalMusicNum + CENTER_DEFAULT) {
		GetGraphSize(SelectMusic4Keys::g_TempBanner[i], &BN_X, &BN_Y);
		g_BannerSizeRatio.emplace_back(static_cast<double>(BN_Y) / static_cast<double>(BN_X));
		g_TempBannerSizeRatio.emplace_back(g_BannerSizeRatio[i]);
		i++;
	}
	while (j != musicP->danceSong->m_FolderNum) {
		GetGraphSize(this->g_KindFolder4[j], &FL_X, &FL_Y);
		g_KindFolder4SizeRatio.emplace_back(static_cast<double>(FL_Y) / static_cast<double>(FL_X));
		j++;
	}
	/*mux.lock();*/
	m_isLoadedGraphAll.store(true);
	/*mux.unlock();*/
}

void SelectMusic4Keys::DrawMusicInfo(int musicNumber4, int fontHandle[], int counter, const std::shared_ptr<Music> &musicP) const
{
	auto flag = 0;
	auto difference = g_MaxMinbpm[0] - g_MaxMinbpm[1];

	if (selectmode == SelectMode::AA)
	{
		//AA
		const int bNnum = NowAANumber(musicNumber4);
		int strSize = _mbclen((BYTE *)musicP->danceSong->m_Name[bNnum].c_str());
		auto stringX = (WIN_WIDTH / 2) - (GetDrawStringWidthToHandle(musicP->danceSong->m_Name[bNnum].c_str(), static_cast<int>(_tcslen(musicP->danceSong->m_Name[bNnum].c_str())) / (2 * strSize), fontHandle[1]));
		LayerStringDraw(stringX, 430, musicP->danceSong->m_Name[bNnum], fontHandle[1], 0);

		strSize = _mbclen((BYTE *)musicP->danceSong->m_Artist[bNnum].c_str());
		stringX = (WIN_WIDTH / 2) - (GetDrawStringWidthToHandle(musicP->danceSong->m_Artist[bNnum].c_str(), static_cast<int>(_tcslen(musicP->danceSong->m_Artist[bNnum].c_str())) / (2 * strSize), fontHandle[1]));
		LayerStringDraw(stringX, 480, musicP->danceSong->m_Artist[bNnum], fontHandle[1], 0);

		strSize = _mbclen((BYTE *)musicP->danceSong->m_SubName[bNnum].c_str());
		stringX = (WIN_WIDTH / 2) - (GetDrawStringWidthToHandle(musicP->danceSong->m_SubName[bNnum].c_str(), static_cast<int>(_tcslen(musicP->danceSong->m_SubName[bNnum].c_str())) / (2 * strSize), fontHandle[2]));
		LayerStringDraw(stringX, 460, musicP->danceSong->m_SubName[bNnum], fontHandle[2], 0);

		difference = g_MaxMinbpm[1] - g_MaxMinbpm[0];
		if (0 <= counter % 280 && counter % 280 < 140)
			flag = 1;
		else if (140 <= counter % 280 && counter % 280 < 280)
			flag = 0;

		if (flag == 1 && g_MaxMinbpm[0] + (difference / 60.0*(counter % 280)) < g_MaxMinbpm[1])
		{
			const auto bpm = static_cast<int>(g_MaxMinbpm[0] + (difference / 60.0*(counter % 280)));
			this->DrawSelectMusicBPM(bpm);
		}
		else if (flag == 0 && g_MaxMinbpm[1] - (difference / 60.0*((counter % 280) - 140)) > g_MaxMinbpm[0])
		{
			const auto bpm = static_cast<int>(g_MaxMinbpm[1] - (difference / 60.0*((counter % 280) - 140)));
			this->DrawSelectMusicBPM(bpm);
		}
		else
		{
			this->DrawSelectMusicBPM(g_MaxMinbpm[flag]);
		}
	}
	else
	{
		//folder
		const auto num = this->NowFolderNumber(musicNumber4, musicP);
		const int strSize = _mbclen((BYTE *)musicP->danceSong->m_FolderName[num].c_str());
		const auto stringx = (WIN_WIDTH / 2) - (GetDrawStringWidthToHandle(musicP->danceSong->m_FolderName[num].c_str(), static_cast<int>(_tcslen(musicP->danceSong->m_FolderName[num].c_str())) / (2 * strSize), fontHandle[0]));

		LayerStringDraw(stringx, 450, musicP->danceSong->m_FolderName[num].c_str(), fontHandle[0], 0);
	}
}
// folder番号酒盗
int SelectMusic4Keys::NowFolderNumber(int nowcenter, const std::shared_ptr<Music> &musicP) const
{
	auto num = nowcenter - CENTER_DEFAULT;    //自分を好きになれた頃の番号に戻す作業

	while (num < 0) {
		num += musicP->danceSong->m_FolderNum;
	}
	while (num >= musicP->danceSong->m_FolderNum) {
		num -= musicP->danceSong->m_FolderNum;
	}
	return num;
}
//AAの詳細番号を出す
int SelectMusic4Keys::NowAANumber(int nowcenter) const
{
	auto num = nowcenter - CENTER_DEFAULT;    //自分を好きになれた頃の番号に戻す作業

	while (num < 0) {
		num += g_CurrentAlbumArt[2];
	}
	while (num >= g_CurrentAlbumArt[2]) {
		num -= g_CurrentAlbumArt[2];
	}

	num += g_CurrentAlbumArt[0];

	return num;
}
//folder開いたときAA設定
int SelectMusic4Keys::SetAAGraph(int firstAA, int endAA, int nowcenter)
{
	auto num_song = endAA - firstAA;
	g_Banner.clear();
	g_BannerSizeRatio.clear();

	auto j = firstAA;
	g_MaxMinbpm.fill(0);

	for (auto i = CENTER_DEFAULT; i < CENTER_DEFAULT + m_musicKind4Max[m_selectFolderNum]; ++i, ++j)
	{
		g_Banner.emplace_back(g_TempBanner[j]);
		g_BannerSizeRatio.emplace_back(g_TempBannerSizeRatio[j]);
	}
	//選曲が循環するので最初の比を最後に加える
	g_BannerSizeRatio.emplace_back(g_TempBannerSizeRatio[firstAA]);

	g_Banner[CENTER_DEFAULT] = g_KindFolder4[m_selectFolderNum];
	return 0;
}
//デストラクタ
void SelectMusic4Keys::Delselectmusic( void )
{
	//画像
	DeleteGraph(g_BackImage);            //メイン背景
	DeleteGraph(difficultyImage);        //難易度文字
	DeleteGraph(backFrontImage);    //難易度文字の下
	DeleteGraph(bpmFrameImage);        //bpmの枠
	DeleteGraph(markerBackImage);    //マーカーの土台背景　いらん

	//音声
	//DeleteSoundMem(backSound);
	DeleteSoundMem(roop_music);
}
// BPM切り取り
void SelectMusic4Keys::CutBpm(int num, int nowcenter, const std::shared_ptr<Music> &musicP) const
{
	if (nowcenter != CENTER_DEFAULT)
	{
		std::vector<int> getbpm;

		for (unsigned i = 0; i < musicP->danceSong->m_Bpm[num].size(); ++i)
		{
			getbpm.emplace_back(_ttoi(musicP->danceSong->m_Bpm[num][i].c_str()));
		}

		if (getbpm.size() == 0) return;

		//最大BPMと最少BPMを得る
		g_MaxMinbpm[0] = getbpm.at(getbpm.size() - 1);
		g_MaxMinbpm[1] = getbpm.at(getbpm.size() - 1);
		for (auto i = static_cast<int>(getbpm.size()) - 1; i >= 0; --i)
		{
			if (getbpm.at(i) < 0) continue;

			if (g_MaxMinbpm[0] > getbpm.at(i))
				g_MaxMinbpm[0] = getbpm.at(i);

			if (g_MaxMinbpm[1] < getbpm.at(i))
				g_MaxMinbpm[1] = getbpm.at(i);
		}

		// 昇順（小さい順）で並び替え(バブルソート)
		/*for (i = 0; i <= k - 1; i++){
			for (j = k - 1; j>i; j--){
				if (getbpm[j] < getbpm[j - 1]){
					temp = getbpm[j];
					getbpm[j] = getbpm[j - 1];
					getbpm[j - 1] = temp;
				}
			}
		}*/
	}
	else
	{
		g_MaxMinbpm[0] = _T('\0');
		g_MaxMinbpm[1] = _T('\0');
	}
}

//selectmusic関数から出るときのフラグを受け取りその分岐で抜け方を決定
void SelectMusic4Keys::EscapeSelect(EscapeState escapeState, int nowcenter)
{
	switch (escapeState)
	{
	case EscapeState::TITLE: //タイトルへ

		SceneManager::g_CurrentScene = Scenes::TITLE;
		DxLib::PlaySoundMem(backSound, DX_PLAYTYPE_BACK);
		break;

	case EscapeState::GAME: //ゲームへ

		SceneManager::g_CurrentScene = Scenes::GAME;
		PlaySoundMem(decideSound, DX_PLAYTYPE_BACK);
		musicNumber4 = NowAANumber(nowcenter);
		GameBase::g_MusicDif = g_DifNowCenter;
		break;

	case EscapeState::RANDOM_ALL: //完全ランダムをセレクトするとき musicNumberの受け渡しに注意

		SceneManager::g_CurrentScene = Scenes::GAME;
		PlaySoundMem(decideSound, DX_PLAYTYPE_BACK);
		musicNumber4 = nowcenter;
		GameBase::g_MusicDif = g_DifNowCenter;
		break;

	case EscapeState::RANDOM_FOLDER: //folder内部でランダムAAの時　範囲制限

		SceneManager::g_CurrentScene = Scenes::GAME;
		musicNumber4 = nowcenter;
		GameBase::g_MusicDif = g_DifNowCenter;
		break;

	default:;
	}
	StopSoundMem(roop_music);
	Delselectmusic();
}
//範囲のランダム数を取得
int SelectMusic4Keys::GetRandom(int min, int max, const std::shared_ptr<Music> &musicP)
{
	std::random_device rnd;
	std::mt19937 mt(rnd());
	const std::uniform_int_distribution<int> dist(min, max);

	auto canPlay = false;
	auto num = 0;
	auto count = 1;

	while (!canPlay)
	{
		if (count % 100 == 0)
		{
			g_DifNowCenter = ((g_DifNowCenter + 1) + 5) % 5;
		}
		++count;

		canPlay = true;
		num = dist(mt);

		if (musicP->danceSong->m_Difficulty[g_DifNowCenter][num] == 0)
			canPlay = false;
	}
	return num;
}
//bpm表示
void SelectMusic4Keys::ShowBpm(void)
{
	//minmaxbpm
}
//難易度のアレ計算用
void SelectMusic4Keys::CalculateDifNumber(std::array<std::array<int, 3>, 5> &number, int nowcenter, const std::shared_ptr<Music> &musicP)
{
	const auto num = NowAANumber(nowcenter);

	for (auto i = 0; i < 5; i++)
	{
		//二桁
		if (musicP->danceSong->m_Difficulty[i][num] >= 10)
		{
			number[i][0] = musicP->danceSong->m_Difficulty[i][num] / 10;
			number[i][1] = musicP->danceSong->m_Difficulty[i][num] - (number[i][0] * 10);
		}
		//一桁
		else
		{
			number[i][0] = 0;
			number[i][1] = musicP->danceSong->m_Difficulty[i][num];
		}
	}
}
void SelectMusic4Keys::DrawLR(int right_pic, int left_pic) const
{
	DxLib::DrawGraph(WIN_WIDTH / 2 - 240, 326, left_pic, TRUE);
	DxLib::DrawGraph(WIN_WIDTH / 2 + 140, 326, right_pic, TRUE);
}
void SelectMusic4Keys::DrawLR_Clicked(int right_click_pic, int left_click_pic)
{
	DxLib::DrawGraph(WIN_WIDTH / 2 - 240, 326, left_click_pic, TRUE);
	DxLib::DrawGraph(WIN_WIDTH / 2 + 140, 326, right_click_pic, TRUE);
}

void SelectMusic4Keys::DrawHiScore(int nowcenter, int count, int fullcomboImage[], int hyoka_gazou[], int fontHandle[], bool& s_flag, int flag_a, const std::shared_ptr<Music> &musicP, VECTOR points[])
{
	if (s_flag)
	{
		LoadHiScore(nowcenter, musicP);
		// チャートを生成
		BuildChart(musicP->danceSong->m_RadarChart[NowAANumber(nowcenter)][g_DifNowCenter].data(), points);
		s_flag = false;
	}

	//*******描画********//
	auto& color = Color::GetInstance();
	auto playModeNumber = static_cast<int>(SceneManager::g_PlayMode);
	constexpr auto OFFSET_X = 730;
	constexpr auto OFFSET_Y = 550;

	for (auto i = 0; i < 3; i++)
	{
		if (m_SaveData.m_IsFullCombo[playModeNumber][i + flag_a])
			DxLib::DrawGraph(OFFSET_X, OFFSET_Y + i * 45, fullcomboImage[count / 3 % 5], TRUE);

		if (m_SaveData.m_Rank[playModeNumber][i + flag_a] != 10)
			DxLib::DrawGraph(OFFSET_X, OFFSET_Y + i * 45, hyoka_gazou[m_SaveData.m_Rank[playModeNumber][i + flag_a]], TRUE);

		if (m_SaveData.m_HighScore[playModeNumber][i + flag_a] != 0)
			DxHelper::DrawOutlineStringToHandle(642, 565 + i * 45, color.White(), color.Black(), fontHandle[3], _T("%d"), m_SaveData.m_HighScore[playModeNumber][i + flag_a]);
	}
}

//ハイスコア読み込み処理
void SelectMusic4Keys::LoadHiScore(int nowcenter, const std::shared_ptr<Music> &musicP)
{
	for (auto m = 0; m < 3; m++)
	{
		for (auto n = 0; n < 5; n++)
		{
			m_SaveData.m_IsFullCombo[m][n] = false;
			m_SaveData.m_Rank[m][n] = 10;
			m_SaveData.m_HighScore[m][n] = 0;
		}
	}

	//パス作成
	auto str = musicP->danceSong->m_Name[NowAANumber(nowcenter)];
	ScoreData::TranslateToSafeFileName(str);
	auto pass = _T("Data\\") + str + _T(".dat");

	std::fstream file(pass.c_str(), std::ios::binary | std::ios::out);
	file.read(reinterpret_cast<char*>(&m_SaveData), sizeof(ScoreSaveData));
	file.close();

	/*ファイルの内容からデータをロード*/
	//if ((error = _tfopen_s(&fp1, pass.c_str(), _T("rb"))) != 0)
	//{
	//	;
	//}
	//else
	//{
	//	fread(&m_SaveData, sizeof(ScoreSaveData), 1, fp1);
	//}
	//fclose(fp1);//解放

	//save_data = ScoreData::GetInstance().scoreDataList.m_scoreSaveDataList[musicP->m_Name4Keys[NowAANumber4(nowcenter)].c_str()];

}

void SelectMusic4Keys::LayerStringDraw(int stringX, int stirngY, const tstring& drawstring, int fontHandle, int colorFlag) const
{
	auto &color = Color::GetInstance();

	//フラグ0→白 1→赤
	//ななめ
	DrawStringToHandle(stringX + 1, stirngY + 1, drawstring.c_str(), color.Black(), fontHandle);
	DrawStringToHandle(stringX + 1, stirngY - 1, drawstring.c_str(), color.Black(), fontHandle);
	DrawStringToHandle(stringX - 1, stirngY + 1, drawstring.c_str(), color.Black(), fontHandle);
	DrawStringToHandle(stringX - 1, stirngY - 1, drawstring.c_str(), color.Black(), fontHandle);
	//たてよこ
	DrawStringToHandle(stringX + 1, stirngY, drawstring.c_str(), color.Black(), fontHandle);
	DrawStringToHandle(stringX - 1, stirngY, drawstring.c_str(), color.Black(), fontHandle);
	DrawStringToHandle(stringX, stirngY + 1, drawstring.c_str(), color.Black(), fontHandle);
	DrawStringToHandle(stringX, stirngY - 1, drawstring.c_str(), color.Black(), fontHandle);
	//真ん中
	if (colorFlag == 0)
		DrawStringToHandle(stringX, stirngY, drawstring.c_str(), color.White(), fontHandle);
	if (colorFlag == 1)
		DrawStringToHandle(stringX, stirngY, drawstring.c_str(), color.Red(), fontHandle);
	if (colorFlag == 2)
		DrawStringToHandle(stringX, stirngY, drawstring.c_str(), color.Orange(), fontHandle);
}

void SelectMusic4Keys::DelSelectMusic(int& loopSound, int& backSound)
{
	//音楽
	DeleteSoundMem(loopSound);
	//DeleteSoundMem(backSound);
}

//レーダーチャート生成(座標設定)
void SelectMusic4Keys::BuildChart(const double *status, VECTOR *points) const
{
	// 項目ごとの回転角度
	const auto radian = (360.0f / ITEM_NUM) * (PI / 180.0f);

	// 開始角度
	const auto startRadian = -90.0f * (PI / 180.0f);

	for (auto i = 0; i < ITEM_NUM; i++)
	{
		// 中心からの距離を求める
		const auto length = CHART_SIZE * (static_cast<double>(status[i] * 86.00 + 14.00) / MAX_STATUS);

		// 単位ベクトルに距離を乗算
		const auto x = cos(startRadian - radian * i) * length;
		const auto y = sin(startRadian - radian * i) * length;

		points[i].x = static_cast<float>(x);
		points[i].y = static_cast<float>(y);
	}
}

//レーダーチャートの描画
void SelectMusic4Keys::DrawChart() const
{
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, 225);
	for (auto i = 0; i < ITEM_NUM; i++)
	{
		const auto x1 = static_cast<int>(m_Points[i].x);
		const auto y1 = static_cast<int>(m_Points[i].y);

		const auto x2 = static_cast<int>(m_Points[(i + 1) % ITEM_NUM].x);
		const auto y2 = static_cast<int>(m_Points[(i + 1) % ITEM_NUM].y);

		DrawTriangle(static_cast<int>(x1 + CHART_CENTER_X),
			static_cast<int>(y1 + CHART_CENTER_Y),
			static_cast<int>(x2 + CHART_CENTER_X),
			static_cast<int>(y2 + CHART_CENTER_Y),
			static_cast<int>(CHART_CENTER_X),
			static_cast<int>(CHART_CENTER_Y),
			0x66CCFF, TRUE);
	}
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	for (auto i = 0; i < ITEM_NUM; i++)
	{
		const auto x1 = static_cast<int>(m_Points[i].x);
		const auto y1 = static_cast<int>(m_Points[i].y);

		const auto x2 = static_cast<int>(m_Points[(i + 1) % ITEM_NUM].x);
		const auto y2 = static_cast<int>(m_Points[(i + 1) % ITEM_NUM].y);

		DrawLine(x1 + CHART_CENTER_X, y1 + CHART_CENTER_Y, x2 + CHART_CENTER_X, y2 + CHART_CENTER_Y, 0x66CCFF, 3);
	}
}