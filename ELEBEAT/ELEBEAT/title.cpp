#include "Title.h"
#include "BlueLightBall.h"
#include "savedata_class.h"
#include "MouseClickObject.h"
#include "Input.h"
#include "extern.h"
#include "Foreach.h"
#include "Screen.h"
#include "Rect.h"

Title::Title() :
	textImagePosY(0),
	cursorHigh(0),
	mainObjImage(0),
	hexaImage(0),
	frameImage(0), lightImage(0),
	txtImage(0),
	lineImage(0),
	casolPicImage(0),
	lightballSprite(0),
	loopBgm(0),
	moveSound(0), m_ObjectNum(0)
{
	titleImage.fill(0);
	menuTextImage.fill(0);
	m_mouseColliderRect = std::make_shared<Rect>((WIN_WIDTH / 2) - 100, 500 - 1 - 24, (WIN_WIDTH / 2) + 100, 500 - 1 + 42);
	m_mouseClickObjectList = std::make_shared<MouseClickObjectList>();
	m_lightballList = std::make_shared<BlueLightBallList>();
}

//-------------------------
// タイトル画面
//-------------------------
void Title::Initialize(const std::shared_ptr<Music> &musicP)
{
	// TODO: BlueLightBall
	m_lightballList->Init([](auto b)
	{
		b->x = GetRand(960);
		b->y = GetRand(768);
		b->vx = 1 + static_cast<double>(GetRand(6) / 4.0);
		b->size = 0.20 + static_cast<double>(GetRand(8) / 10.0);
	},"Graphics/Title/lightball.png");

	//ロード
	LoadTitleImage();
	LoadTitleSound();

	//１曲目に初期化
	OptionSaveData::g_SongCount = 1;
	beforeSelectMusicNum = -1;    //音楽番号初期に変更
	beforeSelectMusicNum_16 = -1;    //音楽番号初期に変更
	beforeSelectMusicNum4 = -1;    //音楽番号初期に変更
	// 音声をループ再生する
	PlaySoundMem(loopBgm, DX_PLAYTYPE_LOOP);
	//光度をもとに戻す
	SetDrawBright(255, 255, 255);
	this->Ini();
	SetDrawScreen(DX_SCREEN_BACK);
}

void Title::Update(const std::shared_ptr<Music>& musicP)
{
	//フレームカウント関連
	if (m_count > INT_MAX)
		m_count = 0;
	else
		m_count++;

	m_fps->Update();

	//シングルトン参照
	Input& input = Input::GetInstance();

	// キー入力の取得
	input.GetKey();
	//マウスの座標を取得
	input.GetMouse();

	m_mouseClickObjectList->Update();

	m_lightballList->Update([](auto b)
	{
		b->y -= (*b).vx;
		if (b->y < -40)
		{
			b->x = GetRand(960);
			b->y = 768 + 40;
			b->vx = 1 + static_cast<double>(GetRand(6) / 4.0);
			b->size = 0.20 + static_cast<double>(GetRand(8) / 10.0);
		}
	});
	//TODO:BlueLightBall
	/*FOREACH(BlueLightBall, lightball, b)
	{

	}*/

	//DxLib::MV1SetRotationXYZ(ModelHandle2, VGet(0.0f, m_count * 0.5f * -PI_F / 120.0f, m_count * 0.5f * -PI_F / 120.0f));

	if (textImagePosY % (234 * 2) == 0)
		textImagePosY = 0;
	textImagePosY++;

	co[0] = static_cast<int>(60 - ((m_count + LOOP_COUNT) % LOOP_COUNT) / 60.0 * 60);
	co[3] = static_cast<int>(100 * (cos(m_count*PI / 100.0)) + 100);
	co[1] = static_cast<int>(127 * sin(m_count*PI / 30.0)) + 128;

	if (input.keyUp == 1 || 0 < input.mouseWheel)
	{
		// カーソルを上に移動
		cursorHigh--;
		DxLib::PlaySoundMem(moveSound, DX_PLAYTYPE_BACK);
	}
	if (10 < input.keyUp && input.keyUp % 16 == 0)
	{
		// 移動スピードを下げている
		cursorHigh--;
		DxLib::PlaySoundMem(moveSound, DX_PLAYTYPE_BACK);
	}
	if (input.keyDown == 1 || input.mouseWheel < 0)
	{
		// カーソルを下に移動
		cursorHigh++;
		DxLib::PlaySoundMem(moveSound, DX_PLAYTYPE_BACK);
	}
	if (10 < input.keyDown && input.keyDown % 16 == 0)
	{
		// 移動スピードを下げている
		cursorHigh++;
		DxLib::PlaySoundMem(moveSound, DX_PLAYTYPE_BACK);
	}
	// 下端に達したら上に移動
	if (cursorHigh >= 3)
		cursorHigh = 0;
	// 上端に達したら下に移動
	if (cursorHigh <= -1)
		cursorHigh = 2;

	// 決定キーが押されたら
	if (input.keyEnter == 1)
	{
		//GAMESTARTを選択
		if (cursorHigh == 0)
		{
			SceneManager::SetNextScene(Scenes::KEYS_SELECT);
			return;
		}
		//OPTIONSを選択
		else if (cursorHigh == 1)
		{
			SceneManager::SetNextScene(Scenes::OPTIONS);
			return;
		}
		//EXITを選択
		else if (cursorHigh == 2)
		{
			SceneManager::SetNextScene(Scenes::FINISH);
			return;
		}
	}

	//ESCキーが押されたら
	if (input.keyEsc == 1)
	{
		SceneManager::SetNextScene(Scenes::FINISH);
		return;
	}

	//マウス操作関係
	for (int u = 0; u < 3; u++)
	{
		if ((((m_mouseColliderRect->GetPoint1().y + LINE_SPACE_PIXEL * u) < input.mouseY) &&
			(input.mouseY < (m_mouseColliderRect->GetPoint2().y + LINE_SPACE_PIXEL * u))) &&
			((m_mouseColliderRect->GetPoint1().x < input.mouseX) &&
			(input.mouseX < m_mouseColliderRect->GetPoint2().x)))
		{
			if (input.mouseClick == 1)
			{
				cursorHigh = u;
				//GAMESTARTを選択
				if (cursorHigh == 0)
				{
					//次はkeys_selectに移るようにする
					SceneManager::SetNextScene(Scenes::KEYS_SELECT);
					return;
				}
				//OPTIONSを選択
				else if (cursorHigh == 1)
				{
					//次はoptionsに移るようにする
					SceneManager::SetNextScene(Scenes::OPTIONS);
					return;
				}
				//EXITを選択
				else if (cursorHigh == 2)
				{
					SceneManager::SetNextScene(Scenes::FINISH);
					return;
				}
			}
		}
	}

	if (SceneManager::g_CurrentScene != Scenes::TITLE) return;

	//-----------------------
	// ココから描画関係
	//-----------------------

	// 画面の初期化
	DxLib::ClearDrawScreen();

	//背景の描画
	DrawBack(textImagePosY);

	//パーティクル描画
	m_lightballList->Draw();

	//FOREACH(BlueLightBall, lightball, b)
	//{
	//	DxLib::DrawRotaGraph2(static_cast<int>(b->x), static_cast<int>(b->y), 75, 75, b->size, 0, lightballSprite, TRUE);
	//}

	//タイトルロゴの描画
	DrawTitle(m_count, co.data());

	//カーソルの描画
	DrawCursor(cursorHigh, co.data());

	//マウスカーソルの描画
	DxLib::DrawRotaGraph2(input.mouseX, input.mouseY, 50, 50, 1.0f, m_count / 42.0, input.casolPic, TRUE);
	//マウスクリックのリングを描画
	m_mouseClickObjectList->Draw();

	m_fps->Draw();        //描画
	DxLib::ScreenFlip();

	if (input.keyPrtsc == 1)
		m_ssc->CaptureScreenshot();

	m_fps->Wait();        //待機
}

void Title::Finalize(const std::shared_ptr<Music> &musicP)
{
	DxLib::StopSoundMem(loopBgm);

	if (SceneManager::g_CurrentScene == Scenes::KEYS_SELECT)
		Screen::TransitionTitleToKeysSelect();

	if (SceneManager::g_CurrentScene != Scenes::FINISH)
		Screen::FadeOut();
}

void Title::LoadTitleImage()
{
	//画像
	g_BackImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/back.png"));

	mainObjImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/MainObj.png"));
	titleImage[0] = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/dance_elenation-logo.png"));
	titleImage[1] = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/title_ele.png"));
	hexaImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/hexa.png"));
	frameImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/frame.png"));
	lineImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/line.png"));
	lightImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/light.png"));
	lightballSprite = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/lightball.png"));
	txtImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/txt.png"));
	menuTextImage[0] = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/gamestart.png"));
	menuTextImage[1] = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/options.png"));
	menuTextImage[2] = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/exit.png"));
}

void Title::LoadTitleSound()
{
	moveSound = LoadSoundMem(_T("Themes/DANCE ELENATION/Sounds/Title/kick.mp3"));
	loopBgm = LoadSoundMem(_T("Themes/DANCE ELENATION/Sounds/TRANCE Loop 001.mp3"));
}

//背景(アニメーション)の描画
void Title::DrawBack(int textImagePosY)
{
	DxLib::DrawGraph(0, 0, mainObjImage, true);

	DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 124);
	for (int i = 1; i <= 5; i++)
	{
		DxLib::DrawRotaGraph2(876, static_cast<int>(234 * i - textImagePosY), 106, 117, 1.0, 0, txtImage, TRUE);
	}

	DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 200);
	DxLib::DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT / 2, 480, 62, 1.0, 0, lineImage, TRUE);

	ClacObject();
	SortObject();
	DxLib::SetDrawMode(DX_DRAWMODE_BILINEAR);//ポリゴンが荒く見えないような描画の仕方「バイリニア法」
	for (int t = 0; t < m_ObjectNum; t++)
	{
		for (int s = 0; s < m_Object[t].ObchindMax; s++)
		{
			DrawPolygon3D(m_Object[t].ObChild[s].Vertex, 2, m_Object[t].Img, TRUE);
		}
	}
	DxLib::SetDrawMode(DX_DRAWMODE_NEAREST);//描画の仕方を元に戻す
}

//タイトルロゴの描画
void Title::DrawTitle(int count, int blendValue[])
{
	//ブレンドモードをオフ
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	DxLib::DrawGraph(383, 500, titleImage[1], true);
	DxLib::DrawRotaGraph2(480, 384, 400, 80, 1.0, 0, titleImage[0], TRUE);
	//ブレンドモードを加算
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, blendValue[3]);
	DxLib::DrawRotaGraph2(480, 384, 400, 80, 1.0, 0, titleImage[0], TRUE);
	//ブレンドモードをオフ
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	int countLoop = GetCountLoop(count);

	if (0 < countLoop && countLoop < 60)
	{
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, blendValue[0]);
		DxLib::DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT / 2, 400, 80, 1 + static_cast<double>(countLoop / 160.0*2.4), 0, titleImage[0], TRUE);
		DxLib::DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT / 2, 400, 80, 1 + static_cast<double>(countLoop / 110.0*2.4), 0, titleImage[0], TRUE);
		DxLib::DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT / 2, 400, 80, 1 + static_cast<double>(countLoop / 80.0*2.4), 0, titleImage[0], TRUE);
		//ブレンドモードをオフ
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}
}

int Title::GetCountLoop(int count)
{
	return (count + LOOP_COUNT) % LOOP_COUNT;
}

//カーソルの描画
void Title::DrawCursor(int casol_high, int blendValue[])
{
	const int offsetX[] = { -4, 1, 3 };

	DxLib::DrawRotaGraph2(WIN_WIDTH / 2 + 18, WIN_HEIGHT / 2 + 135 + LINE_SPACE_PIXEL * casol_high, 300, 50, 1.0, 0, casolPicImage, true);

	// 画像で色替え
	DxLib::DrawGraph(383 + offsetX[casol_high], 500 + LINE_SPACE_PIXEL * casol_high, menuTextImage[casol_high], true);
	DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, blendValue[1]);        //ブレンドモードを加算
	DxLib::DrawGraph(383 + offsetX[casol_high], 500 + LINE_SPACE_PIXEL * casol_high, menuTextImage[casol_high], true);
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);           //ブレンドモードをオフ

	DxLib::DrawGraph(0, 0, frameImage, true);

	DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, blendValue[1]);        //ブレンドモードを加算
	DxLib::DrawGraph(642, 113, hexaImage, true);
	DxLib::DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT / 2 - 4, 453, 759 / 2, 1.0, 0, lightImage, TRUE);
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);           //ブレンドモードをオフ
}

/*
int ImgHandle   : 画像ハンドル
int ImgSize     : 画像サイズ
int ImgX1       : 画像の使用する部分左上座標
int ImgY1       : 画像の使用する部分左上座標
int ImgX2       : 画像の使用する部分右下座標
int ImgY2       : 画像の使用する部分右下座標
float LargeX    : 描画する大きさ（横）
float LargeY    : 描画する大きさ（縦）
int Type        : 描画タイプ　0:画面と同じ向き　1:画面に垂直（壁）　2:画面に垂直（地面）
float FromZ     : 描画を始める奥行き
float FadeFromZ : フェードインを始める奥行き
float FadeToZ   : フェードアウトを始める奥行き
float ToZ       : 描画を終わる奥行き
float GraphX    : 描画する中心点
float GraphY    : 描画する中心点
int ObchildMax  : typeが0の場合のみ、同時にいくつ表示するか
*/
void Title::IniObj(Object_t *Ob, int ImgHandle, int ImgSize, int ImgX1, int ImgY1, int ImgX2, int ImgY2, float LargeX, float LargeY, int Type, float FromZ, float FadeFromZ, float FadeToZ, float ToZ, float GraphX, float GraphY, int ObchildMax)
{
	if (m_ObjectNum >= OBJECT_NUM_MAX - 1) {
		printfDx(_T("オブジェクト登録オーバー\n"));
		return;
	}
	m_ObjectNum++;//オブジェクトの登録数加算

	Ob->Img = ImgHandle;//画像ハンドル
	Ob->ImgSize = ImgSize;//画像サイズ
	Ob->ImgX1 = ImgX1;
	Ob->ImgY1 = ImgY1;
	Ob->ImgX2 = ImgX2;
	Ob->ImgY2 = ImgY2;
	Ob->LargeX = LargeX;//とりあえず描画する大きさを適当に設定。縦・横比は素材の通りにする
	Ob->LargeY = LargeY;
	Ob->m_Type = Type;//タイプを垂直に
	Ob->FromZ = FromZ;//描画開始地点
	Ob->FadeFromZ = FadeFromZ;//描画フェードイン開始地点
	Ob->FadeToZ = FadeToZ;//描画フェードアウト開始地点
	Ob->ToZ = ToZ;//描画終了地点
	Ob->ObchindMax = OBCHILD_MAX;
	if (Ob->m_Type == 0) {
		Ob->ObchindMax = ObchildMax;
	}
	if (Ob->ObchindMax - 1 <= 0) {
		printfDx(_T("表示数の設定が異常です\n"));
		return;
	}
	//Zの幅計算
	Ob->Zhaba = (Ob->FromZ - Ob->ToZ) / (Ob->ObchindMax - 1);

	const auto ou1 = static_cast<float>(Ob->ImgX1) / Ob->ImgSize;
	const auto ou2 = static_cast<float>(Ob->ImgX2 - Ob->ImgX1) / Ob->ImgSize;
	const auto ov1 = static_cast<float>(Ob->ImgY1) / Ob->ImgSize;
	const auto ov2 = static_cast<float>(Ob->ImgY2 - Ob->ImgY1) / Ob->ImgSize;
	for (auto s = 0; s < Ob->ObchindMax; s++)
	{
		Ob->ObChild[s].x = GraphX;
		Ob->ObChild[s].y = GraphY;
		Ob->ObChild[s].z = Ob->ToZ - Ob->Zhaba + Ob->Zhaba * s;;
		for (auto i = 0; i < 6; i++)
		{
			Ob->ObChild[s].Vertex[i].r = Ob->ObChild[s].Vertex[i].g = Ob->ObChild[s].Vertex[i].b = Ob->ObChild[s].Vertex[i].a = 255;
			Ob->ObChild[s].Vertex[i].u = ou1 + ou2 * m_vtPm[i].u;
			Ob->ObChild[s].Vertex[i].v = ov1 + ov2 * m_vtPm[i].v;
		}
	}
}

void Title::Ini()
{
	m_ObjectNum = 0;
	auto imgHandle = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/linewall.jpg"));
	this->IniObj(&m_Object[0], imgHandle, 512, 0, 0, 256, 128, 250, 50, 2, 1000, 400, -200, -400, 320, 240 - 90, OBCHILD_MAX);
	this->IniObj(&m_Object[1], imgHandle, 512, 60, 270, 405, 512, 180, 125, 0, 1000, 400, -200, -400, 470, 275, 6);
	imgHandle = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/linewall.jpg"));
	this->IniObj(&m_Object[2], imgHandle, 777, 0, 0, 777, 777, 777, 444, 1, 1000, 400, -200, -400, 170, 444, OBCHILD_MAX);
}

void Title::ClacObject()
{
	for (auto t = 0; t < m_ObjectNum; t++)
	{
		for (auto s = 0; s < m_Object[t].ObchindMax; s++)
		{
			m_Object[t].ObChild[s].z -= 3;
			for (auto i = 0; i < 6; i++)
			{
				switch (m_Object[t].m_Type)
				{
				case 0://画面に平行
					m_Object[t].ObChild[s].Vertex[i].pos.x = m_Object[t].ObChild[s].x + m_Object[t].LargeX * m_vtPm[i].x;
					m_Object[t].ObChild[s].Vertex[i].pos.y = m_Object[t].ObChild[s].y + m_Object[t].LargeY * m_vtPm[i].y;
					m_Object[t].ObChild[s].Vertex[i].pos.z = m_Object[t].ObChild[s].z;
					break;
				case 1://画面に垂直(壁)
					m_Object[t].ObChild[s].Vertex[i].pos.x = m_Object[t].ObChild[s].x;
					m_Object[t].ObChild[s].Vertex[i].pos.y = m_Object[t].ObChild[s].y + m_Object[t].LargeY * m_vtPm[i].y;
					m_Object[t].ObChild[s].Vertex[i].pos.z = m_Object[t].ObChild[s].z + m_Object[t].Zhaba / 2 * m_vtPm[i].x;
					break;
				case 2://画面に垂直(床)
					m_Object[t].ObChild[s].Vertex[i].pos.x = m_Object[t].ObChild[s].x + m_Object[t].LargeX * m_vtPm[i].x;
					m_Object[t].ObChild[s].Vertex[i].pos.y = m_Object[t].ObChild[s].y;
					m_Object[t].ObChild[s].Vertex[i].pos.z = m_Object[t].ObChild[s].z + m_Object[t].Zhaba / 2 * m_vtPm[i].y;
					break;
				default:;
				}
			}
		}

		if (m_Object[t].FromZ - m_Object[t].FadeFromZ <= 0)
		{
			printfDx(_T("Object[%d].Fromの設定がおかしい\n"), t);
		}
		else if (m_Object[t].FadeToZ - m_Object[t].ToZ <= 0)
		{
			printfDx(_T("Object[%d].Toの設定がおかしい\n"), t);
		}
		else
		{
			for (auto s = 0; s < m_Object[t].ObchindMax; s++)
			{
				for (auto i = 0; i < 6; i++)
				{
					const auto z = m_Object[t].ObChild[s].Vertex[i].pos.z;
					//位置が描画する範囲より遠かったら透過0
					if (z < m_Object[t].ToZ) {
						m_Object[t].ObChild[s].Vertex[i].a = 0;
					}
					//(近づいている場合)フェードインする位置だったら
					else if (m_Object[t].ToZ < z && z <= m_Object[t].FadeToZ) {
						m_Object[t].ObChild[s].Vertex[i].a = static_cast<unsigned char>(255.0f / (m_Object[t].FadeToZ - m_Object[t].ToZ) * (z - m_Object[t].ToZ));
					}
					//通常描画する位置なら
					else if (m_Object[t].FadeToZ <= z && z <= m_Object[t].FadeFromZ) {
						m_Object[t].ObChild[s].Vertex[i].a = 255;
					}
					//(近づいてる場合)フェードアウトする位置だったら
					else if (m_Object[t].FadeFromZ <= z && z < m_Object[t].FromZ) {
						m_Object[t].ObChild[s].Vertex[i].a = static_cast<unsigned char>(255.0f / (m_Object[t].FromZ - m_Object[t].FadeFromZ) * (m_Object[t].FromZ - z));
					}
					//描画する範囲より近かったら透過0
					else if (m_Object[t].FromZ < z) {
						m_Object[t].ObChild[s].Vertex[i].a = 0;
					}
				}
				//近づいて見えなくなったら
				if (m_Object[t].ObChild[s].z < m_Object[t].ToZ - m_Object[t].Zhaba*0.5f) {
					//一番向こう側へ
					const auto sub = (m_Object[t].ToZ - m_Object[t].Zhaba*0.5f) - m_Object[t].ObChild[s].z;
					m_Object[t].ObChild[s].z = m_Object[t].FromZ + m_Object[t].Zhaba*0.5f - sub;
				}
				//遠ざかって見えなくなったら
				else if (m_Object[t].ObChild[s].z > m_Object[t].FromZ + m_Object[t].Zhaba*0.5f) {
					//一番こちら側へ
					const auto sub = m_Object[t].ObChild[s].z - (m_Object[t].FromZ + m_Object[t].Zhaba*0.5f);
					m_Object[t].ObChild[s].z = m_Object[t].ToZ - m_Object[t].Zhaba*0.5f + sub;
				}
			}
		}
	}
}

void Title::SwapObChild(ObChild_t *ob1, ObChild_t *ob2)
{
	const auto t = *ob1;
	*ob1 = *ob2;
	*ob2 = t;
}

//Zでテクスチャをソート
void Title::SortObject()
{
	for (auto t = 0; t < m_ObjectNum; t++)
	{
		for (auto i = 0; i < m_Object[t].ObchindMax; i++)
		{
			for (auto j = i + 1; j < m_Object[t].ObchindMax; j++)
			{
				if (m_Object[t].ObChild[i].z < m_Object[t].ObChild[j].z)
					this->SwapObChild(&m_Object[t].ObChild[i], &m_Object[t].ObChild[j]);
			}
		}
	}
}

void Title::Efect05()
{
	const auto pic = MakeGraph(WIN_WIDTH, WIN_HEIGHT);
	GetDrawScreenGraph(0, 0, WIN_WIDTH, WIN_HEIGHT, pic);
	for (auto i = 0; i <= 255; i = i + 4)
	{
		DrawRotaGraph(WIN_WIDTH / 2, WIN_HEIGHT / 2, 1 + i / 100, 0, pic, FALSE);
		ScreenFlip();
	}
	DeleteGraph(pic);
}

Title::~Title()
{
	DxLib::DeleteGraph(mainObjImage);
	DxLib::DeleteGraph(titleImage[0]);
	DxLib::DeleteGraph(titleImage[1]);
	DxLib::DeleteGraph(hexaImage);
	DxLib::DeleteGraph(frameImage);
	DxLib::DeleteGraph(lineImage);
	DxLib::DeleteGraph(lightImage);
	DxLib::DeleteGraph(lightballSprite);
	DxLib::DeleteGraph(txtImage);
	DxLib::DeleteGraph(menuTextImage[0]);
	DxLib::DeleteGraph(menuTextImage[1]);
	DxLib::DeleteGraph(menuTextImage[2]);

	DxLib::DeleteGraph(g_BackImage);
	DxLib::DeleteSoundMem(loopBgm);
	DxLib::DeleteSoundMem(moveSound);
}