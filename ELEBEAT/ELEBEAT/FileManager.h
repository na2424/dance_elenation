#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include "Singleton.h"
#include "define.h"
#include <mutex>

class Music;

class FileManager : public Singleton<FileManager>
{
public:
	friend class Singleton < FileManager >;
	FileManager() : m_isLoaded(false), m_isLoadedSequence(false), m_isLoadedSmFile(false) {};
	enum BitFlagSong : int
	{
		TITLE = 1      << 0,
		SUBTITLE = 1   << 1,
		ARTIST = 1     << 2,
		BANNER = 1     << 3,
		BACKGROUND = 1 << 4,
		MUSIC = 1      << 5,
		OFFSET = 1     << 6,
		BPMS = 1       << 7,
		STOPS = 1      << 8
	};

	void LoadFile(std::shared_ptr<Music> &musicP);
	void Init();
	void IsLoaded(bool isLoaded, bool isLoadedSequence);
	[[nodiscard]] bool IsLoaded() const;

private:

	std::vector<tstring> kind;                        //音ゲーの題名パスを保持
	std::vector<tstring> musicText;                        //音ゲーのtxtを探す為のパス保存変数前が音ゲー種類真ん中音楽数
	std::vector<tstring> musicTextFront;                //\songs\jubeat\ + (曲名フォルダ)\ を保持する変数
	std::vector<tstring> musicHandle;                    //音ゲー種類までのパス保持変数
	std::vector<tstring> musicFolderHandle;

	static void CheckDirectory();
	void LoadMmdFile() const;
	void LoadGlobalImage() const;
	void LoadNumberImage() const;
	void LoadMarker() const;
	void LoadElebeatFile(std::shared_ptr<Music> &musicP) const;
	void LoadDanceElenationFile(std::shared_ptr<Music> &musicP);
	static void LoadScoreData();
	static void CheckBitFlagSong(const int bitFlagSong, const tstring& str, const std::shared_ptr<Music>& musicP);
	void GetDanceElenationSongPaths(std::shared_ptr<Music> &musicP);
	bool m_isLoaded;
	bool m_isLoadedSequence;
	bool m_isLoadedSmFile;
	std::mutex m_mux;
};

#endif