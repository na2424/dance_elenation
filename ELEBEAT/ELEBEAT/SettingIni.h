#ifndef SETTINGINI_H
#define SETTINGINI_H

//#include "define.h"
#include <string>
#include <windows.h>
#include <tchar.h>
#include <stdio.h>



#ifdef _UNICODE
typedef std::wstring string_t;
#else
typedef std::string string_t;
#endif

template<class T>
/*static*/ bool Inirw(int isRead, string_t& fname, string_t sec, string_t key, T& val_t);

//---------------------------------------------------------------------------
//Setting.ini input output class.
//---------------------------------------------------------------------------
class SettingIni
{
public:

	// Judge
	struct _Judge
	{
		int      JudgePerfect;
		int      JudgeGreat;
		int      JudgeGood;
	} Judge;

	// Life
	struct _Life
	{
		int      LifePerfect;
		int      LifeGreat;
		int      LifeGood;
		int      LifeMiss;
	} Life;

	// KeyConfig
	struct _KeyConfig
	{
		int      KeyDown;
		int      KeyLeft;
		int      KeyRight;
		int      KeyUp;
		int      KeyStart;
		int      KeyBack;
		int      KeyL;
		int      KeyR;
		int      Key1;
		int      Key2;
		int      Key3;
		int      Key4;
		int      Key5;
		int      Key6;
		int      Key7;
		int      Key8;
		int      Key9;
		int      Key10;
		int      Key11;
		int      Key12;
		int      Key13;
		int      Key14;
		int      Key15;
		int      Key16;
	} KeyConfig;

	//JoyPad
	struct _JoyPad
	{
		int      JoyPadKey[8][2];
		int      JoyPad[4][4];
	} JoyPad;

	struct _GamePlaySetting
	{
		int     GlobalOffset;
	}GamePlaySetting;

	// ScreenShot
	struct _ScreenShot
	{
		int      PRTSC_ELE;
	} ScreenShot;

	SettingIni(void);
	SettingIni(string_t fname);
	bool Load(string_t fname);
	bool Save(string_t fname);
	void Init();

protected:

	string_t initFileName;
	string_t loadFileName;

	bool IniRw(string_t f, int r);
};
//typedef SettingIni SettingFile; //�V���݊�

//---------------------------------------------------------------------------
// Common method
//---------------------------------------------------------------------------
#ifndef INIMONI_INIRW
#define INIMONI_INIRW

	/*
	Read and Write INI file
	int     is_read  1=Read mode, 0=Write mode
	string  fname    Filename (The Windows folder when there is not path)
	string  sec      Section name
	string  key      Key name
	T       val_t    [Read]Init+Output, [Write]Input
	*/
template<class T>
bool Inirw(int is_read, string_t& fname, string_t sec, string_t key, T& val_t);
bool read(string_t ifn, string_t sec, string_t key, int& dst);
bool read(string_t ifn, string_t sec, string_t key, std::basic_string<TCHAR>& dst);
bool read(string_t ifn, string_t sec, string_t key, double& dst);

template<class T>
bool Write(string_t ifn, string_t sec, string_t key, T val_t);

/*template<class T>
void to_string(tstring str, T val);*/

template<class T>
void ToString(string_t &str, T val);

#endif
#endif