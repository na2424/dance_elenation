#include "MouseClickObject.h"
#include "DxLib.h"
#include "Color.h"
#include "Input.h"



//マウスコンストラクタ
MouseClickObject::MouseClickObject(int Mouse_x, int Mouse_y)
{
	this->m_x = Mouse_x;
	this->m_y = Mouse_y;
	this->m_r = 0;
	this->m_time = 0;
	this->m_IsEnable = false;
}
//円用マウスクリック座標
void MouseClickObject::OnClick()
{
	const auto& input = Input::GetInstance();

	if (input.mouseClick == 1 && !this->m_IsEnable)
	{
		this->m_x = input.mouseX;
		this->m_y = input.mouseY;
		this->m_IsEnable = true;
	}
}
//マウスリング
void MouseClickObject::DrawCircle()
{
	if (this->m_IsEnable)
	{
		this->m_time++;
		this->m_r = 10 * m_time;
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, static_cast<int>(256 - m_time*8.0));
		DxLib::DrawCircle(this->m_x, this->m_y, this->m_r, Color::GetInstance().White(), FALSE);
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);       //ブレンドモードをオフ
	}
	if (this->m_time > SHOW_COUNT)
	{
		this->m_IsEnable = false;
		this->m_time = 0;
		this->m_r = 0;
	}
}

MouseClickObjectList::MouseClickObjectList()
{
	m_mouseClickObjects.resize(MAX_NUM);
}

void MouseClickObjectList::Update()
{
	for (auto& mc : m_mouseClickObjects)
	{
		if (!mc.m_IsEnable)
		{
			mc.OnClick();
			break;
		}
	}
}

void MouseClickObjectList::Draw()
{
	for (auto& mc : m_mouseClickObjects)
	{
		mc.DrawCircle();
	}
}