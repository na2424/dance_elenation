#ifndef MARKER_H
#define MARKER_H

#include "Singleton.h"
#include <mutex>
#include <tchar.h>



class Marker : public Singleton<Marker>
{
public:
	void Init();
	TCHAR marker_handle[40][50];                //マーカーの本命パス
	int marker_num;                                //マーカーの個数　0なら0配列にのみ入ってる 1なら0と1
};

#endif