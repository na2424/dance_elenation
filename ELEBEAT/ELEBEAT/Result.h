#ifndef RESULT_H
#define RESULT_H
#include <array>
#include <vector>
#include "SceneManager.h"
#include "Rect.h"
#include "LuaHelper.h"

//class Music;
//class JudgeCount;
class TimerDrawer;
class BlueLightBall;

/// <summary>
/// リザルトシーン.
/// (ゲームのスコアと評価を表示する.
/// EnterもしくはEscが押されるか、タイム切れで次のシーンに遷移する.)
/// </summary>
class Result : public Scene
{
private:
	lua_State *L;
	LuaHelper luaHelper;
	std::shared_ptr<TimerDrawer> numberDraw;
	std::shared_ptr<Rect> m_JacketRect;
	std::vector<BlueLightBall> lightball;

	int m_Time;
	int m_Rank;

	int m_Percent;
	int m_ScoreNum;        //目に見えるスコアを入れる、スコアがどんどん足されていく演出で使用

	int m_PlaceX;
	int m_PlaceY;
	int m_Spacing;

	int m_StartTime;
	double m_AspectRatio;

	//-----------------------
	// 定数.
	//-----------------------
	const int ANIMATE_SCORE_COUNT = 120;
	const int PLACE_Y = 360;
	const int BETWEEN = 40;
	const int BALL_MAX = 39;

	const int GRADE_1P_X = 90;             //評価画像(AAAとか)のx座標
	const int GRADE_1P_Y = 130;            //評価画像(鳥Aとか)のy座標(game_9keys.cppとgame_16keys.cpp)

	//-----------------------
	// 画像.
	//-----------------------
	Image clearImage;
	Image failedImage;
	Image resultOverImage;
	Image resultImage;
	Image dotImage;

	std::array<Image, 8> rankImages;         //評価画像
	std::array<Image, 5> fullcomboImages;    //フルコンマーク画像
	std::array<Image, 10> scoreNumImages;    //(リザルト画面での)スコア画像
	std::array<Image, 10> judgeNumImages;    //各判定カウントの数字画像
	std::array<Image, 10> perNumImages;      //うまく押せた率(?)の数字画像

	//-----------------------
	// 音声.
	//-----------------------
	Sound startSound;
	Sound gameoverSound;
	Sound loopSound;

	//-----------------------
	// 関数.
	//-----------------------
	void SavaScore(const std::shared_ptr<Music> &musicP) const;
	void ReadLua();
	void DrawScoreResult(int num);
	void DrawScorePercent(int num);
	void DrawJudgeCount(int num, int y);
	void InitLua();
	void LoadSceneImage();
	void SetNextScene();
public:
	Result();

	void Initialize(const std::shared_ptr<Music> &musicP) override;
	void Update(const std::shared_ptr<Music>& musicP) override;
	void Finalize(const std::shared_ptr<Music> &musicP) override;
};

#endif