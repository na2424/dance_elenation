#pragma once
#include <array>
#include "Music.h"
#include "SceneManager.h"

enum class ScreenTransitionType
{
	FadeIn,
	FadeOut,
	TitleToKeys,
	KeysToSelectMusic
};

class Screen : public SceneManager
{
public:
	static void TransitionTitleToKeysSelect(void);
	static void TransitionKeysSelectToMusicSelect(void);
	static void TransitionToGame(void);
	static void TransitionFromResult(void);
	static void FadeOut(void);
	static void FadeIn(void);
	static void GameFadeIn(const std::unique_ptr<Music> &musicP);
	Screen() {}
};