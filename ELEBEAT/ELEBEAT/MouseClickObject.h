#ifndef MOUSE_CLICK_OBJECT
#define MOUSE_CLICK_OBJECT
#include <vector>
#include "IParticle.h"



//マウスクリックオブジェクト(輪)
class MouseClickObject
{
private:
	int m_x;
	int m_y;
	int m_r;
	int m_time;
	static const int SHOW_COUNT = 66;
public:
	bool m_IsEnable;
	MouseClickObject() = default;
	MouseClickObject(int x, int y);
	void OnClick();
	void DrawCircle();
};

class MouseClickObjectList : IParticle
{
private:
	std::vector<MouseClickObject> m_mouseClickObjects;
	const static int MAX_NUM = 10;
public:
	MouseClickObjectList();
	void Update() override;
	void Draw() override;
};

#endif