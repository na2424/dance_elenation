#include <string>



#ifndef DEFINE_H
#define DEFINE_H
//#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1
#ifdef _DEBUG
#define _SCL_SECURE_NO_WARNINGS
#endif

#define PI 3.141592
#define PI_F 3.141592f

#define SY 4
#define EP 0.0001 //ε(小さい値の代わり)

static constexpr int WIN_WIDTH = 960;
static constexpr int WIN_HEIGHT = 768;

static constexpr int AA_SIZE = 256;
static constexpr int AA_AA_BETWEEN_PX = 94;                    //AAの間の距離
static constexpr int Modi = 125;                //画像変形したときのAAの横幅　伝えにくいなこれ
static constexpr int hukasi = 31;                //Modi-aida=hukasi(不可視)
static constexpr int AA_MIRROR_BETWEEN_PX = 2;                   //AAと逆AAの間
static constexpr int TRANCE = 100;

//game_9keys.cppとgame_16keys.cpp
static constexpr int MASU_X = 86;                // 左上マスのx座標(基準用)
static constexpr int MAXX = 300;                  // 読み込む譜面の同場所最大数
static constexpr int ONE_BEAT_PX = 64;            // 等速時の１拍分のノートのピクセル数
static constexpr float ONE_MINUTE = 60000.0f;     // １分をミリ秒で表したもの
static constexpr int EXPLOSION_TIME = 188;        // 爆発時間

static constexpr int SQUARE_SIZE = 200;                // 判定枠の大きさ
static constexpr double IMAGE_RATIO = 0.92 * 0.75;    // マーカー等の画像の大きさを調整する

static constexpr int CENTER_DEFAULT = 0;                                    //デフォルトのnowcenter
static constexpr int CENTER_DEFAULT_16 = 13;
static constexpr int MARKER_SIZE = 128;                                        //マーカーの大きさ
static constexpr int PLAYER_OPTION_HEIGHT = 32;  //プレイヤーオプションの文字y座標調整
static constexpr int PLAYER_OPTION_WIDTH = 80;   //プレイヤーオプションの文字y座標調整
static constexpr int HEIGHT_OFFSET = 100;
static constexpr int STGO_TIME = 1666;

static constexpr double SIZE_RATIO = 0.75;                //拡大・縮小率

//result.cpp
static constexpr int MASU_X4 = 100;
static constexpr int JUDGE_X_PX = 824;            // 判定カウントのx座標
static constexpr int JUDGE_Y_PX = 188;            // 判定カウントのy座標

//NumberDraw.cpp
static constexpr int COMBO_X = 500;               // コンボ画像の表示基準X座標
static constexpr int WIDTH = 400;                // 数字グラフィック横幅
static constexpr int HEIGHT = 300;                // 数字グラフィック高さ

static constexpr int INPUT_KEY_MAX = 24;            //キーの最大数
static constexpr int INPUT_JOYPAD_MAX = 32;        //ジョイパッドキーの最大数

//efect.cpp
static constexpr int OBCHILD_MAX = 11;
static constexpr int OBJECT_NUM_MAX = 10;

//4keys
static constexpr int CHART_SIZE = 68;            // チャート半径
static constexpr int CHART_CENTER_X = 256;      // チャート中心座標 X
static constexpr int CHART_CENTER_Y = 616;      // チャート中心座標 Y
static constexpr int MAX_STATUS = 100;          // ステータス最大値
static constexpr int ITEM_NUM = 5;              // ステータス個数
static constexpr double JUDGE_SHOW_TIME = 240.0;		// 判定表示時間
static constexpr double MEASURE = 4.0;            // 1小節の拍数

//9keys
static constexpr int CENTER_LEFT_X = (WIN_WIDTH / 2) - (AA_SIZE / 2);        //960/2 - AAsize/2 //前384->今352
static constexpr int CENTER_LEFT_Y = 142;                                        //左上y 今142
static constexpr int MARKER_CENTER = 250;                                    //マーカーの真ん中
static constexpr int MARKER_LEFT_X = (WIN_WIDTH / 2) - (MARKER_SIZE / 2);    //マーカーの真ん中x座標
static constexpr int MARKER_LEFT_Y = 666;                                        //マーカーの真ん中y座標
static constexpr int MARKERBETWEENSIZE = 50;                                //マーカーとマーカーの間の長さ

//16keys
static constexpr int LEFTX = 0;                    //左側のx
static constexpr int LEFTY = 0;                   //左側のy
static constexpr int AASIZE = 167;                //AA大きさ
static constexpr int AABETWEEN = 33;            //AAの間s
#define ARRAY_LENGTH(array) (sizeof(array) / sizeof(array[0]))

#endif

//文字コード
#ifdef _UNICODE
typedef std::wstring tstring;
#else
typedef std::string tstring;
#endif

//using byte = unsigned char;

using Image = int;
using Sound = int;
using Model = int;