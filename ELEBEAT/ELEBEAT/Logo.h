#ifndef LOGO_H
#define LOGO_H
#include "SceneManager.h"

class Music;
class JudgeCount;

class Logo : public Scene
{
private:
	const int WAIT_COUNT = 160;
	Image logoImage;
public:
	void Initialize(const std::shared_ptr<Music> &musicP) override;
	void Update(const std::shared_ptr<Music>& musicP) override;
	void Finalize(const std::shared_ptr<Music> &musicP) override;
	Logo() :logoImage(0)
	{};
};
#endif