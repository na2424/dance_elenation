#include <DxLib.h>
#include "Color.h"

void Color::Init()
{
	m_red = GetColor(255, 0, 0);
	m_white = GetColor(255, 255, 255);
	m_black = GetColor(0, 0, 0);
	m_gray = GetColor(128, 128, 128);
	m_blue = GetColor(0, 0, 255);
	m_green = GetColor(0, 255, 0);
	m_yellow = GetColor(255, 255, 0);
	m_orange = GetColor(255, 168, 0);
}