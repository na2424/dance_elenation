#ifndef SINGLETON_H
#define SINGLETON_H
#include <memory>

//シングルトン基底クラス(このクラスを継承するとシングルトンになる).
template<class T>
class Singleton
{
public:
	static T& GetInstance()
	{
		static typename T::SingletonPointerType S_SingleTon(T::CreateInstance());
		return GetReference(S_SingleTon);
	}

	//代入演算子禁止
	void operator=(const Singleton& obj) = delete;
	//コピーコンストラクタ禁止
	Singleton(const Singleton &obj) = delete;

protected:
	Singleton() {}
	virtual ~Singleton() {}

private:
	typedef std::unique_ptr<T> SingletonPointerType;
	inline static T *CreateInstance()
	{
		return new T();
	}

	inline static T &GetReference(const SingletonPointerType &ptr)
	{
		return *ptr;
	}
};
#endif