#ifndef _LUA_HELPER_H
#define _LUA_HELPER_H
#include <Lua.hpp>

class LuaFuncParamItem
{
public:
	//パラメータタイプ：LUA_TNIL/LUA_TNUMBER/LUA_TSTRING/LUA_TBOOLEAN
	unsigned int m_type;
	union
	{
		// 数値の場合の値
		double m_number;
		// 文字列の場合の値
		char *m_str;
		// ブールの場合の値
		bool m_bool;
	};
public:
	LuaFuncParamItem() : m_type(LUA_TNIL), m_number(0) {}
	~LuaFuncParamItem() { ReleaseValue(); }

	//型取得関数
	unsigned int GetType() const { return m_type; }
	//型チェック
	bool IsNil() const { return m_type == LUA_TNIL; }
	bool IsNumber() const { return m_type == LUA_TNUMBER; }
	bool IsString() const { return m_type == LUA_TSTRING; }
	bool IsBool() const { return m_type == LUA_TBOOLEAN; }

	//値取得関数
	double GetNumber()      const { return m_number; }
	const char *GetString() const { return m_str; }
	bool GetBool()          const { return m_bool; }

	//バッファ解放
	void ReleaseValue();

	//各種値をセット
	void SetNil();
	void SetNumber(double number);
	void SetString(const char * str);
	void SetBool(bool value);

	//格納している値をLuaスタックに積む
	void PushToStack(lua_State *L) const;
};

// ユーザーから直接使う関数パラメータクラス
// 複数のパラメータ値を格納できる
//  LuaFuncParam param;
//  param.Number(10).String("hello").Nil().Number(50);
//  のように使える.
class LuaFuncParam
{
private:
	enum
	{
		// 最大パラメータ数
		PARAMS_MAX = 20,
	};
	LuaFuncParamItem m_params[PARAMS_MAX];
	int m_params_count;

public:
	LuaFuncParam() : m_params_count(0) {}
	~LuaFuncParam() { Clear(); }
	// パラメータを全クリア
	void Clear();
	// 数値パラメータの追加
	LuaFuncParam & Number(double number);
	// 文字列パラメータの追加
	LuaFuncParam & String(const char *str);
	// nilパラメータの追加
	LuaFuncParam & Nil();
	// ブール値パラメータの追加
	LuaFuncParam & Bool(bool value);
	//パラメータ数を渡す
	int GetCount() const { return m_params_count; }

	//各種インデックスのパラメータ値取得(インデックスは0ベース)
	bool IsNil(int index) const;
	double GetNumber(int index) const;
	const char *GetString(int index) const;
	bool GetBool(int index) const;

	//Luaスタックに引数をプッシュして、プッシュした引数の数を返す
	int PushToStack(lua_State *L) const;
	//Luaスタックから値を取得
	//スタックトップからresult_count個の値を取得して格納
	void GetFromStack(lua_State *L, int result_count);
};

//Lua関数呼び出しを簡単にするヘルパークラス
class LuaHelper
{
private:
	// Luaステート
	lua_State *m_L;
	// エラー文字列
	char m_err[1000];
	// debug.tracebackの実装へのポインタ
	lua_CFunction m_pGetStackTraceFunc;

public:
	LuaHelper() : m_L(nullptr), m_pGetStackTraceFunc(nullptr) {}
	~LuaHelper() { Close(); }

	//Luaステートを閉じる
	void Close();

	//Luaステートをセットする
	//同時に、debug.tracebackの実装へのポインタを得る
	//(そのため、Luaライブラリのオープン後が望ましい)
	void SetLua(lua_State *L);
	//Luaステートの取得
	lua_State *GetLua() const { return m_L; }

	// print関数を設定
	void InitPrintFunc() const;

#ifdef _WIN32
	// print関数実装：VC++のメッセージとして出力
	static int LuaPrintWindows(lua_State *L);
#endif

	//直前のCallFancについてのエラーメッセージを取得する
	const char *GetErr() const { return m_err; }

	//関数呼び出し
	//result,paramsはNULLでも良い
	bool CallFunc(const char *funcname, LuaFuncParam* result = nullptr, int result_count = 0, LuaFuncParam* params = nullptr);

	//ファイル実行
	//result,paramsはNULLでも良い
	bool DoFile(const char *path, LuaFuncParam* result = nullptr, int result_count = 0, LuaFuncParam* params = nullptr);

protected:
	//エラーメッセージをセットする
	void SetErr(const char* location, const char *message);
	//返り値とスタックの値からエラーメッセージを設定
	void AnalyzeError(int res_call, const char* location);
	//pcallおよびスタック周りの処理を行うサブルーチン
	//int CallSub(LuaFuncParam* result, int result_count, LuaFuncParam* params, int errfunc_index);
};

#endif