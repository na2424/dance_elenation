#ifndef COLOR_H
#define COLOR_H
#include "Singleton.h"

class Color : public Singleton<Color>
{
private:
	int m_red;
	int m_white;
	int m_black;
	int m_gray;
	int m_blue;
	int m_green;
	int m_yellow;
	int m_orange;

public:
	friend class Singleton < Color >;
	void Init();
	int Red() const { return m_red; }
	int White() const { return m_white; }
	int Black() const { return m_black; }
	int Gray() const { return m_gray; }
	int Blue() const { return m_blue; }
	int Green() const { return m_green; }
	int Yellow() const { return m_yellow; }
	int Orange() const { return m_orange; }
};

#endif