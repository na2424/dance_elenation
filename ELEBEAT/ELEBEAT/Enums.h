#ifndef ENUMS_H
#define ENUMS_H

enum class Scenes : int
{
	FINISH = -1,
	LOGO = 0,
	TITLE,
	OPTIONS,
	KEYS_SELECT,
	MUSIC_SELECT,
	GAME,
	RESULT
};

enum class SelectKeys : int
{
	KEYS_9 = 0,
	KEYS_16,
	KEYS_4
};

enum
{
	IMAGE_TYPE = 0,
	SOUND_TYPE,
	DIV_IMAGE_TYPE
};

enum class SelectMode
{
	FOLDER,
	AA,
	MARKER
};

enum class EscapeState
{
	NONE,
	TITLE,
	GAME,
	RANDOM_ALL,
	RANDOM_FOLDER
};

enum class SelectMusicCatchKeyType : int
{
	NONE = 50,
	FOLDER_RANDOM = 125,
	FOLDER_AA = 75,
	AA_RANDOM = 175,
	NOWCENTER = 100,
	BACK_FOLDER = 150,
	BREAK = 200
};

enum class NoteSkinType : int
{
	RAINBOW = 0,
	NOTE,
	FLAT
};

enum class KeySelectMusicType : int
{
	NONE = 50,
	FOLDER_RANDOM = 125,
	FOLDER_TO_AA = 75,
	AA_RANDOM = 175,
	DEFAULT_SELECT = 100,
	AA_TO_FOLDER = 150,
	BREAK = 200,
	//UNKNOWN = 50
};


#endif