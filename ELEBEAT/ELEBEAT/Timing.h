#ifndef TIMING_H
#define TIMING_H
#include <vector>
#include <memory>



class Music;

class Timing
{
public:
	Timing() :
		m_TimeN(0),
		m_TimeS(0),
		m_IsStop(false),
		m_Offset(0),
		m_MusicEndTime(0)
	{ };

	std::vector<double> m_Bpm;
	std::vector<double> m_BpmPosition;
	std::vector<double> m_Stop;
	std::vector<double> m_StopPosition;

	std::vector<double> m_Cn;
	std::vector<double> m_bn;
	std::vector<double> m_dn;

	double m_TimeN;
	double m_TimeS;
	bool m_IsStop;
	double m_Offset;                //音源の始まりと譜面の始まりを合わせるための変数 (オフセット値) [単位ミリ秒]
	long int m_MusicEndTime;

	std::vector <double> m_StopPositionTime;

	void SetTiming(const std::shared_ptr<Music> &musicP, const int musicIndex);
	bool IsStopTiming(long time, int stopIndex);
	bool IsBpmChangeTiming(long time, int bpmIndex);

private:

	void SetBpm(const std::shared_ptr<Music> &musicP, const int musicIndex);
	void SetStop(const std::shared_ptr<Music> &musicP, const int musicIndex);
	void SetOffset(const std::shared_ptr<Music> &musicP, const int musicIndex);

	void SetDiffBpmPosition();
	void SetDiffTimeBpmPosition();
	void SetStopTime();
	void SetDiffTimeBpmPositionWithStop();
	void SetBpmTime();
};

#endif