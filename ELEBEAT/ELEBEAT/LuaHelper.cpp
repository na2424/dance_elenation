#ifdef _WIN32
#include <windows.h>
#endif

#include <string.h>

#include "struct.h"
#include "LuaHelper.h"

//------------------------------------------------------
// class LuaFuncParamItem  : パラメータ１つを表すクラス
//------------------------------------------------------

void LuaFuncParamItem::ReleaseValue()
{
	if (m_type == LUA_TSTRING)
	{
		if (m_str) delete[] m_str;
		m_str = nullptr;
	}
}

//数値をセット
void LuaFuncParamItem::SetNumber(double number)
{
	ReleaseValue();
	m_type = LUA_TNUMBER;
	m_number = number;
}

//文字列値をセット
void LuaFuncParamItem::SetString(const char* str)
{
	ReleaseValue();
	m_type = LUA_TSTRING;
	const auto len = strlen(str);
	m_str = new char[len + 1];
	if (m_str)
	{
		tstring string = str;
		string._Copy_s(m_str, sizeof(m_str), len);
		m_str[len] = '\0';
	}
}

//nil値をセット
void LuaFuncParamItem::SetNil()
{
	ReleaseValue();
	m_type = LUA_TNIL;
}

//bool値をセット
void LuaFuncParamItem::SetBool(bool value)
{
	ReleaseValue();
	m_type = LUA_TBOOLEAN;
	m_bool = value;
}

//格納している値をLuaスタックに積む
void LuaFuncParamItem::PushToStack(lua_State *l) const
{
	switch (m_type)
	{
	case LUA_TNUMBER: lua_pushnumber(l, m_number); break;
	case LUA_TSTRING: lua_pushstring(l, m_str); break;
	case LUA_TNIL: lua_pushnil(l); break;
	case LUA_TBOOLEAN: lua_pushboolean(l, m_bool); break;
	default:;
	}
}

//------------------------------------------------------------
// class LuaFuncParam : 複数のパラメータや返り値を表すクラス
//------------------------------------------------------------

//パラメータを全クリア

//LuaFuncParam::LuaFuncParam() :m_params_count(0) {}
//LuaFuncParam::~LuaFuncParam()
//{
//    Clear();
//}

void LuaFuncParam::Clear()
{
	for (int i = 0; i < m_params_count; i++)
	{
		m_params[i].ReleaseValue();
	}
	m_params_count = 0;
}

//数値パラメータの追加
LuaFuncParam& LuaFuncParam::Number(double number)
{
	m_params[m_params_count].SetNumber(number);
	m_params_count++;
	return *this;
}
//文字列パラメータの追加
LuaFuncParam& LuaFuncParam::String(const char *str)
{
	m_params[m_params_count].SetString(str);
	m_params_count++;
	return *this;
}
// nilパラメータの追加
LuaFuncParam& LuaFuncParam::Nil()
{
	m_params[m_params_count].SetNil();
	m_params_count++;
	return *this;
}
// ブール値パラメータの追加
LuaFuncParam& LuaFuncParam::Bool(bool value)
{
	m_params[m_params_count].SetBool(value);
	m_params_count++;
	return *this;
}

// 指定インデックスのパラメータのNULLチェック
// (インデックスは０ベース）
bool LuaFuncParam::IsNil(int index) const
{
	if (index < 0 || index >= m_params_count) return true;
	return m_params[index].IsNil();
}
// 指定インデックスのパラメータ数値取得
// (インデックスは０ベース）
double LuaFuncParam::GetNumber(int index) const
{
	if (index < 0 || index >= m_params_count) return 0;
	if (m_params[index].GetType() != LUA_TNUMBER) return 0;
	return m_params[index].GetNumber();
}
// 指定インデックスのパラメータ文字列取得
// (インデックスは０ベース）
const char * LuaFuncParam::GetString(int index) const
{
	if (index < 0 || index >= m_params_count) return nullptr;
	if (m_params[index].GetType() != LUA_TSTRING) return nullptr;
	return m_params[index].GetString();
}
// 指定インデックスのブール値取得
// (インデックスは０ベース）
bool LuaFuncParam::GetBool(int index) const
{
	if (index < 0 || index >= m_params_count) return false;
	if (m_params[index].GetType() != LUA_TBOOLEAN) return false;
	return m_params[index].GetBool();
}

//Luaスタックに引数をプッシュして、プッシュした引数の数を返す
int LuaFuncParam::PushToStack(lua_State *L) const
{
	for (int i = 0; i < m_params_count; i++)
	{
		m_params[i].PushToStack(L);
	}
	return m_params_count;
}

//Luaスタックから値を取得
//スタックトップからresult_count個の値を取得して格納
void LuaFuncParam::GetFromStack(lua_State *L, int resultCount)
{
	for (auto i = 0; i < resultCount; i++)
	{
		auto index = -resultCount + i;
		const auto type = lua_type(L, index);
		switch (type)
		{
		case LUA_TNIL: this->Nil(); break;
		case LUA_TSTRING: this->String(lua_tostring(L, index)); break;
		case LUA_TNUMBER: this->Number(lua_tonumber(L, index)); break;
		case LUA_TBOOLEAN: this->Bool(lua_toboolean(L, index) ? true : false); break;
		default: this->Nil(); break;
		}
	}
}

//--------------------------------------------------------------
// class LuaHelper : Lua関数呼び出しを簡単にするヘルパークラス
//--------------------------------------------------------------

// Luaステートを閉じる
void LuaHelper::Close()
{
	if (m_L) lua_close(m_L);
	m_L = nullptr;
}

// エラーメッセージをセットする
void LuaHelper::SetErr(const char *location, const char *message)
{
	sprintf_s(m_err, sizeof(m_err), "%s : %s", location, message);
}

// Luaステートをセットする
// 同時に、debug.tracebackの実装へのポインタを得る
// （このため、Luaライブラリオープン後が望ましい）
void LuaHelper::SetLua(lua_State *L)
{
	m_L = L;
	lua_getglobal(L, "debug");
	if (!lua_isnil(L, -1))
	{
		lua_getfield(L, -1, "traceback");
		m_pGetStackTraceFunc = lua_tocfunction(L, -1);
	}
}

// print関数を設定
void LuaHelper::InitPrintFunc() const
{
	// Windows以外なら標準のもの(stdout出力）で良い
#ifdef _WIN32
	lua_register(m_L, "print", LuaHelper::LuaPrintWindows);
	lua_atpanic(m_L, LuaHelper::LuaPrintWindows);
#endif
}

#ifdef _WIN32
// print関数実装：VC++のメッセージとして出力
int LuaHelper::LuaPrintWindows(lua_State *L)
{
	const int count = lua_gettop(L);
	lua_getglobal(L, "tostring");
	for (int i = 0; i < count; i++)
	{
		lua_pushvalue(L, -1);
		lua_pushvalue(L, i + 1);
		lua_call(L, 1, 1);
		const char *str = lua_tostring(L, -1);
		OutputDebugString((str) ? str : "");
		if (i != 0) OutputDebugString("?t");
		lua_pop(L, 1);
	}
	OutputDebugString("?n");
	return 0;
}
#endif

//返り値とスタックの値からエラーメッセージを設定
void LuaHelper::AnalyzeError(int res_call, const char* location)
{
	const char* reason = "";
	switch (res_call)
	{
	case LUA_ERRRUN:	reason = "SCRIPT RUNTIME ERROR"; break;
	case LUA_ERRSYNTAX:	reason = "SCRIPT SYNTAX ERROR"; break;
	case LUA_ERRMEM:	reason = "SCRIPT MEMORY ERROR"; break;
	case LUA_ERRFILE:	reason = "SCRIPT FILE ERROR"; break;
	default: break;
	}
	//エラーメッセージ取得
	const char *mes = lua_tostring(m_L, -1);
	tstring err_mes = static_cast<tstring>(reason) + static_cast<tstring>(mes);
	SetErr(location, err_mes.c_str());
}

//関数コール
//result,paramsはNULLでも良い
bool LuaHelper::CallFunc(const char *funcname, LuaFuncParam* result, int resultCount, LuaFuncParam* params)
{
	const auto oldTop = lua_gettop(m_L);

	//返り値はクリアしておく
	if (result != nullptr) result->Clear();

	//ランタイムエラー処理関数を積む
	lua_pushcfunction(m_L, m_pGetStackTraceFunc);

	//関数を見つける
	lua_getglobal(m_L, funcname);
	if (!lua_isfunction(m_L, -1))
	{
		auto location = "calling function<" + static_cast<tstring>(funcname) + ">";
		SetErr(location.c_str(), "the function not found.");
		return false;
	}

	//paramによって指定された可変個の引数をスタックに置く
	int params_count = 0;
	if (params)
		params_count = params->PushToStack(m_L);

	//Lua内の関数をコール
	const auto resCall = lua_pcall(m_L, params_count, resultCount, oldTop);

	//エラー処理
	if (resCall != 0)
	{
		//エラーメッセージ生成
		auto location = "calling function<" + static_cast<tstring>(funcname) + ">";
		AnalyzeError(resCall, location.c_str());
		//スタックを元に戻す
		lua_settop(m_L, oldTop);
		return false;
	}

	//返り値解析
	if (result != nullptr)
		result->GetFromStack(m_L, resultCount);

	//スタックを元に戻す
	lua_settop(m_L, oldTop);
	return true;
}

//ファイル実行
//result,paramsはNULLでも良い
bool LuaHelper::DoFile(const char *path, LuaFuncParam* result, int resultCount, LuaFuncParam* params)
{
	const auto oldTop = lua_gettop(m_L);

	//返り値はクリアしておく
	if (result != nullptr) result->Clear();

	//ランタイムエラー処理関数を積む
	lua_pushcfunction(m_L, m_pGetStackTraceFunc);

	//ファイルをロード
	const auto resLoad = luaL_loadfile(m_L, path);
	//エラー処理
	if (resLoad != 0)
	{
		//エラーメッセージ生成
		auto location = "loading file<" + static_cast<tstring>(path) + ">";
		AnalyzeError(resLoad, location.c_str());
		//スタックを元に戻す
		lua_settop(m_L, oldTop);
		return false;
	}

	//paramsによって指定された可変個の引数をスタックに置く
	auto paramsCount = 0;
	if (params)
		paramsCount = params->PushToStack(m_L);

	//ロードされたチャンクをコール
	const auto resCall = lua_pcall(m_L, paramsCount, resultCount, oldTop + 1);

	//エラー処理
	if (resCall != 0)
	{
		//エラーメッセージ生成
		auto location = "executing file<" + static_cast<tstring>(path) + ">";
		AnalyzeError(resCall, location.c_str());
		//スタックを元に戻す
		lua_settop(m_L, oldTop);
		return false;
	}

	//返り値解析
	if (result != nullptr) result->GetFromStack(m_L, resultCount);

	//スタックを元に戻す
	lua_settop(m_L, oldTop);

	return true;
}