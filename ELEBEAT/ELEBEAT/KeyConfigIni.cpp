#include <string>
#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include "KeyConfigIni.h"
#include "extern.h"
using namespace std;

KeyConfigIni ConfigKey;

KeyConfigIni::KeyConfigIni(void)                          
{                                                                   
    initFileName = _T("Data/KeyConfig.ini"); //Default file.    
    ConfigKey.init();                                                 
}

KeyConfigIni::KeyConfigIni(string_t fname=_T(""))                          
{                                                                   
    initFileName = _T("Data/KeyConfig.ini"); //Default file.    
    ConfigKey.init();                                                         
    ConfigKey.load(fname);                                                    
}                                                                   
                                                                        
    /**                                                                 
        INI file is read.                                               
        @param fname Filename                                           
        When there is not a file, It becomes initFileName.              
        When there is not a pass, It becomes Windows folder.            
    */                                                                  
bool KeyConfigIni::load(string_t fname=_T(""))                                  
{                                                                   
    if(fname.empty()){                                              
        fname = initFileName;                                       
    }                                                               
    loadFileName = fname;                                           
    WIN32_FIND_DATA fd;                                             
    HANDLE h = ::FindFirstFile(fname.c_str(), &fd);                 
    if (h != INVALID_HANDLE_VALUE) {                                
		ConfigKey.iniRW(fname,1);                                             
    }                                                               
    return (h != INVALID_HANDLE_VALUE);                             
}                                                                   
                                                                        
/**                                                                 
    It writes it in the INI file.                                   
    @param fname Filename                                           
    When there is not a file, It becomes open file.                 
    When there is not a pass, It becomes Windows folder.            
*/                                                                  
bool KeyConfigIni::save(string_t fname=_T(""))                                  
{                                                                   
    if(fname.empty()){                                              
        fname = loadFileName;                                       
    }                                                               
    ConfigKey.iniRW(fname,0);                                                 
    return true;                                                    
}                                                                   
bool KeyConfigIni::iniRW(string_t f, int r)    
{                                
    string_t s;                  

    s = _T("KeyConfig");
    inimoni2::inirw( r,f,s, _T("Key1             "), KeyConfig.Key1     );
    inimoni2::inirw( r,f,s, _T("Key2             "), KeyConfig.Key2     );
    inimoni2::inirw( r,f,s, _T("Key3             "), KeyConfig.Key3     );
    inimoni2::inirw( r,f,s, _T("Key4             "), KeyConfig.Key4     );
    inimoni2::inirw( r,f,s, _T("Key5             "), KeyConfig.Key5     );
    inimoni2::inirw( r,f,s, _T("Key6             "), KeyConfig.Key6     );
    inimoni2::inirw( r,f,s, _T("Key7             "), KeyConfig.Key7     );
    inimoni2::inirw( r,f,s, _T("Key8             "), KeyConfig.Key8     );
    inimoni2::inirw( r,f,s, _T("Key9             "), KeyConfig.Key9     );
    inimoni2::inirw( r,f,s, _T("Key10            "), KeyConfig.Key10    );
    inimoni2::inirw( r,f,s, _T("Key11            "), KeyConfig.Key11    );
    inimoni2::inirw( r,f,s, _T("Key12            "), KeyConfig.Key12    );
    inimoni2::inirw( r,f,s, _T("Key13            "), KeyConfig.Key13    );
    inimoni2::inirw( r,f,s, _T("Key14            "), KeyConfig.Key14    );
    inimoni2::inirw( r,f,s, _T("Key15            "), KeyConfig.Key15    );
    inimoni2::inirw( r,f,s, _T("Key16            "), KeyConfig.Key16    );
    inimoni2::inirw( r,f,s, _T("KeyLeft          "), KeyConfig.KeyLeft  );
    inimoni2::inirw( r,f,s, _T("KeyRight         "), KeyConfig.KeyRight );
    inimoni2::inirw( r,f,s, _T("KeyUp            "), KeyConfig.KeyUp    );
    inimoni2::inirw( r,f,s, _T("KeyDown          "), KeyConfig.KeyDown  );
    inimoni2::inirw( r,f,s, _T("KeyStart         "), KeyConfig.KeyStart );
    inimoni2::inirw( r,f,s, _T("KeyBack          "), KeyConfig.KeyBack  );
    return true;                                                    
}                                                                   

void KeyConfigIni::init()                                                         
{                                                            
    KeyConfig.Key1               = _T("1");
    KeyConfig.Key2               = _T("2");
    KeyConfig.Key3               = _T("3");
    KeyConfig.Key4               = _T("4");
    KeyConfig.Key5               = _T("Q");
    KeyConfig.Key6               = _T("W");
    KeyConfig.Key7               = _T("E");
    KeyConfig.Key8               = _T("R");
    KeyConfig.Key9               = _T("A");
    KeyConfig.Key10              = _T("S");
    KeyConfig.Key11              = _T("D");
    KeyConfig.Key12              = _T("F");
    KeyConfig.Key13              = _T("Z");
    KeyConfig.Key14              = _T("X");
    KeyConfig.Key15              = _T("C");
    KeyConfig.Key16              = _T("V");
    KeyConfig.KeyLeft            = _T("LEFT");
    KeyConfig.KeyRight           = _T("RIGHT");
    KeyConfig.KeyUp              = _T("UP");
    KeyConfig.KeyDown            = _T("DOWN");
    KeyConfig.KeyStart           = _T("RETURN");
    KeyConfig.KeyBack            = _T("ESCAPE");
}

template<class T>
bool inimoni2::inirw(int is_read, string_t& fname, string_t sec, string_t key, T& val_t)
{
	if(is_read){
        inimoni2::read(fname.c_str(), sec.c_str(), key.c_str(), val_t);
    }
    else{
		inimoni2::write(fname.c_str(), sec.c_str(), key.c_str(), val_t);
    }
	return true;
}

bool inimoni2::read(string_t ifn, string_t sec, string_t key, int& dst)            
{                                                                        
    dst = GetPrivateProfileInt( sec.c_str(), key.c_str(), dst, ifn.c_str() );
    return true;                                                         
}

bool inimoni2::read(string_t ifn, string_t sec, string_t key, basic_string<TCHAR>& dst)
{                                                                        
    TCHAR buf[256];                                                      
    GetPrivateProfileString(                                             
        sec.c_str(),                                                     
        key.c_str(),                                                     
        dst.c_str(),                                                     
        buf,                                                             
        sizeof(buf),                                                     
        ifn.c_str() );                                                   
    dst = buf;                                                           
    return true;                                                         
}
                                                                             
bool inimoni2::read(string_t ifn, string_t sec, string_t key, double& dst)         
{                                                                        
    string_t s;                                                          
    inimoni2::read(ifn, sec, key, s);                                     
                                                                             
    TCHAR* e;                                                            
    double x = _tcstod(s.c_str(), &e);                                   
                                                                             
    dst = 0.0;                                                           
    if (!*e) {                                                           
        dst = x;                                                         
    }                                                                    
    return true;                                                         
}

template<class T>
bool inimoni2::write(string_t ifn, string_t sec, string_t key, T val_t)
{
    TCHAR val[1024];
    inimoni2::to_string(val, val_t);                                      
    WritePrivateProfileString( sec.c_str(), key.c_str(), val, ifn.c_str() );
	return true;                                                    
}                                                                   
                                                                        
void inimoni2::to_string(TCHAR* str, int val)                                 
{
	sprintf_s( str, 256, _T("%d"),val);
}                                                                   
                                                                        
void inimoni2::to_string(TCHAR* str, double val)                              
{                                                                   
    sprintf_s( str, 256, _T("%lf"),val);                              
}                                                                   
                                                                        
void inimoni2::to_string(TCHAR* str, basic_string<TCHAR> val)                 
{                                                                   
    sprintf_s( str, 256, _T("%s"),val.c_str());                        
}

