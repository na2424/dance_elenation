#include "KeyConfig.h"
#include "DxLib.h"
#include "extern.h"
#include "JoyPad.h"
#include <string>
#include <memory>


using namespace DxLib;

KeyConfig::KeyConfig(void) :
	m_fontHandle(0),
	m_select(0),
	canMove(false),
	canEnter(false),
	confilmSelect(0),
	m_configState(ConfigState::Select)
{
	ReplaceDefaultKey();
}

void KeyConfig::SetFontHandle(int hundle)
{
	this->m_fontHandle = hundle;
}

void KeyConfig::ChangeConfigState(ConfigState state)
{
	m_configState = state;
	m_configColor = (m_configState == ConfigState::Change) ? GetColor(255, 0, 0) : GetColor(255, 255, 0);
}

void KeyConfig::GetKey()
{
	GetHitKeyStateAll(m_buf.data());
	if (m_buf[KEY_INPUT_UP] == 1) ++m_keyUp;
	else m_keyUp = 0;
	if (m_buf[KEY_INPUT_DOWN] == 1) ++m_keyDown;
	else m_keyDown = 0;
	if (m_buf[KEY_INPUT_LEFT] == 1) ++m_keyLeft;
	else m_keyLeft = 0;
	if (m_buf[KEY_INPUT_RIGHT] == 1) ++m_keyRight;
	else m_keyRight = 0;
	if (m_buf[KEY_INPUT_RETURN] == 1) ++m_keyEnter;
	else m_keyEnter = 0;
	if (m_buf[KEY_INPUT_ESCAPE] == 1) ++m_keyEsc;
	else m_keyEsc = 0;
}

//--------------------------------
// キーコンフィグ画面(キーボード)
//--------------------------------
void KeyConfig::ConfigKeySetting()
{
	ChangeConfigState(ConfigState::Select);

	const int grHandle = MakeGraph(WIN_WIDTH, WIN_HEIGHT);

	//空のグラフィックハンドルを作成する
	DxLib::GetDrawScreenGraph(0, 0, WIN_WIDTH, WIN_HEIGHT, grHandle);

	DxLib::SetDrawScreen(DX_SCREEN_BACK);
	while (ProcessMessage() != -1 && m_configState != ConfigState::Back)
	{
		this->GetKey();
		//状態更新
		UpdateState();

		//描画処理
		ClearDrawScreen();
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 128);
		DrawGraph(0, 0, grHandle, false);
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

		if (m_configState == ConfigState::Decide)
			DrawStringKey2(confilmSelect);

		DrawStringKey(m_select);
		ScreenFlip();
	}
}

void KeyConfig::UpdateState()
{
	if (m_configState == ConfigState::Select)
		MoveSelect();
	if (m_configState == ConfigState::Change)
		ChangeKeySetting();
	if (m_configState == ConfigState::Decide)
		ConfirmKeySetting();
}

void KeyConfig::MoveSelect()
{
	//移動
	if (KeyConfig::m_keyUp == 1) --m_select;
	if (KeyConfig::m_keyDown == 1) ++m_select;

	if (m_select < 0)
		m_select = INPUT_KEY_MAX - 1;
	else if (INPUT_KEY_MAX <= m_select)
		m_select = 0;

	//選択、戻る
	if (KeyConfig::m_keyEnter == 1)
	{
		ChangeConfigState(ConfigState::Change);
		KeyConfig::m_keyEnter = 2;
	}
	else if (KeyConfig::m_keyEsc == 1)
	{
		ChangeConfigState(ConfigState::Decide);
	}
}

void KeyConfig::ChangeKeySetting()
{
	for (int j = 0; j < 256; j++)
	{
		if (m_buf[j] == 1 && KeyConfig::m_keyEnter <= 1)
		{
			this->k[m_select] = j;
			ChangeConfigState(ConfigState::Select);
		}
	}
}

void KeyConfig::ConfirmKeySetting()
{
	if (CheckHitKey(KEY_INPUT_LEFT) == 1)
	{
		//「はい」を選択
		confilmSelect = 0;
	}
	else if (CheckHitKey(KEY_INPUT_RIGHT) == 1)
	{
		//「いいえ」を選択
		confilmSelect = 1;
	}
	if (confilmSelect == 0 && CheckHitKey(KEY_INPUT_RETURN) == 1)
	{
		ChangeConfigState(ConfigState::Back);
		ReplaceKey();
		Setting.Save(_T("Data\\Setting.ini"));
	}
	else if (confilmSelect == 1 && CheckHitKey(KEY_INPUT_RETURN) == 1)
	{
		ChangeConfigState(ConfigState::Back);
		ReplaceDefaultKey();
	}
}

//--------------------------------
// キーコンフィグ画面(JoyPad)
//--------------------------------
void KeyConfig::ConfigJoyPadSetting()
{
	auto tt = 0;
	auto flagJoypad = 0;
	auto flag = 1;
	auto x = 0;

	const auto joypad = std::make_shared<JoyPad>();
	//joypad = new joypad_t;
	for (auto i = 0; i < 8; i++) {
		for (auto j = 0; j < 2; j++) {
			joypad->m_Key[i][j] = Setting.JoyPad.JoyPadKey[i][j];
		}
	}
	for (auto i = 0; i < 4; i++) {
		for (auto j = 0; j < 4; j++) {
			joypad->m_K[i][j] = Setting.JoyPad.JoyPad[i][j];
		}
	}

	//空のグラフィックハンドルを作成する
	const auto grHandle = MakeGraph(WIN_WIDTH, WIN_HEIGHT);
	//現在描画されている画面を空のグラフィックハンドルに代入する
	GetDrawScreenGraph(0, 0, WIN_WIDTH, WIN_HEIGHT, grHandle);
	//裏画面に設定
	SetDrawScreen(DX_SCREEN_BACK);
	if (GetJoypadNum() == 0 && flagJoypad == 0)
	{
		flagJoypad = 1;
		while (ProcessMessage() != -1 && flagJoypad == 1)
		{
			GetHitKeyStateAll(m_buf.data()); // 全てのキーの入力状態を得る
			GetJoypadInputState(DX_INPUT_PAD1);
			ClearDrawScreen();
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);
			DrawGraph(0, 0, grHandle, false);
			SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);           //ブレンドモードをオフ
			DrawFormatStringToHandle(WIN_WIDTH / 2 - 420, WIN_HEIGHT / 2 - 114 + 40 * 8, 0xffffff, m_fontHandle, _T("ジョイパッドが１つも接続されていません。\n\n オプション画面に戻ります。"));
			ScreenFlip();

			if (CheckHitKeyAll() != 0 && m_buf[Setting.KeyConfig.KeyStart] == 0) return;
		}
	}
	while (tt <= INPUT_JOYPAD_MAX && ProcessMessage() != -1)
	{
		this->GetKey();
		GetHitKeyStateAll(m_buf.data()); // 全てのキーの入力状態を得る
		auto inputState = GetJoypadInputState(DX_INPUT_PAD1);

		//同時押し対策(2のべき乗高速判定)
		if (0 != (inputState & (inputState - 1)) && inputState != 0)
			inputState = 0;

		if (tt == INPUT_JOYPAD_MAX)
		{
			if (m_keyLeft == 1)
			{
				x = 0;
			}
			else if (m_keyRight == 1)
			{
				x = 1;
			}
			if (x == 0 && m_buf[Setting.KeyConfig.KeyStart] == 1)
			{
				tt++;
				JoyPadSubstitution(joypad);
				Setting.Save(_T("Data\\Setting.ini"));
			}
			else if (x == 1 && m_buf[Setting.KeyConfig.KeyStart] == 1)
			{
				tt++;
				JoyPadSubstitution2(joypad);
			}
		}

		if (flag == 0)
		{
			for (auto i = 0; i < 8; i++) {
				for (auto j = 0; j < 2; j++) {
					if (inputState == joypad->m_Key[i][j])
					{
						if (joypad->m_Key[i][j] != 0)
							joypad->m_Key[i][j] = 0;
						flag = 1;
						break;
					}
				}
				if (flag == 1)break;
			}
			for (auto i = 0; i < 4; i++) {
				for (auto j = 0; j < 4; j++) {
					if (inputState == joypad->m_K[i][j])
					{
						if (joypad->m_K[i][j] != 0)
							joypad->m_K[i][j] = 0;
						flag = 1;
						break;
					}
				}
				if (flag == 1)break;
			}
		}

		//キーを代入
		if (inputState != 0 && flag == 0 && tt < INPUT_JOYPAD_MAX)
		{
			if (tt < 16)
				joypad->m_Key[tt % 8][static_cast<int>(tt / 8)] = inputState;
			else if (tt >= 16)
				joypad->m_K[static_cast<int>((tt - 16) / 4)][((tt - 16) + 4) % 4] = inputState;
			tt++;
			flag = 1;
		}
		else if (tt != 0)
		{
			if ((inputState&joypad->m_Key[(tt - 1) % 8][static_cast<int>((tt - 1) / 8)]) == 0 && flag == 1 && tt < 16 && tt < INPUT_JOYPAD_MAX)
				flag = 0;
			else if ((inputState&joypad->m_K[static_cast<int>(((tt - 16) - 1) / 4)][((tt - 16) - 1) % 4]) == 0 && flag == 1 && tt >= 16 && tt < INPUT_JOYPAD_MAX)
				flag = 0;
		}
		else if (tt == 0)
		{
			flag = 0;
		}

		//カーソル移動の全パターン
		tt = MoveCasolJoyPad(tt);

		if (m_keyEsc == 1)
			tt = INPUT_JOYPAD_MAX;

		//描画処理
		ClearDrawScreen();
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);            //ブレンドモードをオン
		DrawGraph(0, 0, grHandle, false);
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);           //ブレンドモードをオフ
		DrawStringJoyPad(tt, joypad);
		if (tt == INPUT_JOYPAD_MAX)
			DrawStringKey3(x);
		ScreenFlip();
	}
	//メモリ開放
	//delete joypad;
	return;
}

void KeyConfig::DrawStringKey(int key)
{
	DrawFormatStringToHandle(WIN_WIDTH / 2 - 300, WIN_HEIGHT / 2 - 114 + 40 * -1, 0xffffff, m_fontHandle, _T("上下カーソルで移動、Enterで選択、Escで保存します。"));
	//枠
	for (int m = 0; m < 4; ++m)
	{
		for (int n = 0; n < 4; ++n)
		{
			DrawBox(WIN_WIDTH / 2 + 114 + m * 55, WIN_HEIGHT / 2 - 114 + n * 55, WIN_WIDTH / 2 + 114 + (m + 1) * 55 - 4, WIN_HEIGHT / 2 - 114 + (n + 1) * 55 - 4, 0xffffff, FALSE);
		}
	}

	///白
	for (int i = 0; i < 24; ++i)
	{
		if (i < 8)
		{
			tstring str = KEY_LIST[i] + "%s";
			const TCHAR* keyName = str.c_str();
			DrawFormatStringToHandle(WIN_WIDTH / 2 - 240, WIN_HEIGHT / 2 - 114 + 40 * i, 0xffffff, m_fontHandle, keyName, ConvertKeycodeToChar(this->k[i]));
		}
		else
		{
			DrawFormatStringToHandle(WIN_WIDTH / 2 + 114 + ((i - 8) % 4) * 55 + 4, WIN_HEIGHT / 2 - 114 + ((i - 8) / 4) * 55 + 4, 0xffffff, m_fontHandle, _T("%s"), ConvertKeycodeToChar(this->k[i]));
		}
	}

	///選択色
	for (int i = 0; i < 24; ++i)
	{
		if (i < 8 && key == i)
		{
			auto str = KEY_LIST[i] + "%s";
			const auto keyName = str.c_str();
			DrawFormatStringToHandle(WIN_WIDTH / 2 - 240, WIN_HEIGHT / 2 - 114 + 40 * i, m_configColor, m_fontHandle, keyName, ConvertKeycodeToChar(this->k[i]));
		}
		else if (key == i)
		{
			DrawFormatStringToHandle(WIN_WIDTH / 2 + 114 + ((i - 8) % 4) * 55 + 4, WIN_HEIGHT / 2 - 114 + ((i - 8) / 4) * 55 + 4, m_configColor, m_fontHandle, _T("%s"), ConvertKeycodeToChar(this->k[i]));
		}
	}
}

void KeyConfig::DrawStringKey2(int i) const
{
	DrawFormatStringToHandle(WIN_WIDTH / 2 - 240, static_cast<int>(WIN_HEIGHT / 2 - 114 + 40 * 8.7f), 0xffffff, m_fontHandle, _T("以上の設定で保存します。\n よろしいですか？"));
	DrawFormatStringToHandle(WIN_WIDTH / 2 - 240, WIN_HEIGHT / 2 - 114 + 40 * 10, 0xffffff, m_fontHandle, _T("はい"));
	DrawFormatStringToHandle(WIN_WIDTH / 2 + 55, WIN_HEIGHT / 2 - 114 + 40 * 10, 0xffffff, m_fontHandle, _T("いいえ"));
	if (i == 0)
		DrawFormatStringToHandle(WIN_WIDTH / 2 - 240, WIN_HEIGHT / 2 - 114 + 40 * 10, 0xff0000, m_fontHandle, _T("はい"));
	if (i == 1)
		DrawFormatStringToHandle(WIN_WIDTH / 2 + 55, WIN_HEIGHT / 2 - 114 + 40 * 10, 0xff0000, m_fontHandle, _T("いいえ"));
}

void KeyConfig::DrawStringKey3(int i) const
{
	DrawFormatStringToHandle(WIN_WIDTH / 2 - 240, static_cast<int>(WIN_HEIGHT / 2 - 114 + 40 * 8.7f), 0xffffff, m_fontHandle, _T("以上の設定で保存します。\n よろしいですか？"));
	DrawFormatStringToHandle(WIN_WIDTH / 2 - 240, WIN_HEIGHT / 2 - 114 + 40 * 10, 0xffffff, m_fontHandle, _T("はい"));
	DrawFormatStringToHandle(WIN_WIDTH / 2 + 55, WIN_HEIGHT / 2 - 114 + 40 * 10, 0xffffff, m_fontHandle, _T("いいえ"));
	if (i == 0)
		DrawFormatStringToHandle(WIN_WIDTH / 2 - 240, WIN_HEIGHT / 2 - 114 + 40 * 10, 0xff0000, m_fontHandle, _T("はい"));
	if (i == 1)
		DrawFormatStringToHandle(WIN_WIDTH / 2 + 55, WIN_HEIGHT / 2 - 114 + 40 * 10, 0xff0000, m_fontHandle, _T("いいえ"));
}

void KeyConfig::DrawStringJoyPad(int tt, const std::shared_ptr<JoyPad>& joypad) const
{
	DrawFormatStringToHandle(WIN_WIDTH / 2 - 300, WIN_HEIGHT / 2 - 114 + 40 * -1, 0xffffff, m_fontHandle, _T("カーソルキーで移動、JoyPad入力で変更、Escで保存します。"));
	///白
	//左側
	for (int j = 0; j < 2; ++j)
	{
		for (int i = 0; i < 8; ++i)
		{
			tstring str = (j == 0) ? _T("            %d") : _T("                   %d");
			DrawFormatStringToHandle(WIN_WIDTH / 2 - 420, WIN_HEIGHT / 2 - 114 + 40 * i, 0xffffff, m_fontHandle, KEY_LIST[i].c_str());
			DrawFormatStringToHandle(WIN_WIDTH / 2 - 420, WIN_HEIGHT / 2 - 114 + 40 * i, 0xffffff, m_fontHandle, str.c_str(), joypad->m_Key[i][j]);
		}
	}

	//右側
	for (auto m = 0; m < 4; m++)
	{
		for (auto n = 0; n < 4; n++)
		{
			DrawBox(188 + WIN_WIDTH / 2 - 228 + m * 88, 88 + WIN_HEIGHT / 2 - 204 + n * 88, 188 + WIN_WIDTH / 2 - 228 + (m + 1) * 88 - 8, 88 + WIN_HEIGHT / 2 - 204 + (n + 1) * 88 - 8, 0xffffff, FALSE);
		}
	}
	for (auto i = 0; i < 4; i++)
	{
		for (auto j = 0; j < 4; j++)
		{
			DrawFormatString(188 + WIN_WIDTH / 2 - 228 + j * 88 + 6, 88 + WIN_HEIGHT / 2 - 204 + i * 88 + 6, 0xffffff, _T("%d"), joypad->m_K[i][j]);
		}
	}

	///選択色
	for (auto j = 0; j < 2; ++j)
	{
		for (auto i = 0; i < 8; ++i)
		{
			if (tt == i + j * 8)
			{
				tstring str = (j == 0) ? _T("            %d") : _T("                   %d");
				DrawFormatStringToHandle(WIN_WIDTH / 2 - 420, WIN_HEIGHT / 2 - 114 + 40 * i, 0xff0000, m_fontHandle, str.c_str(), joypad->m_Key[i][j]);
			}
		}
	}
	if (tt >= 16 && tt != INPUT_JOYPAD_MAX) {
		DrawFormatString(188 + WIN_WIDTH / 2 - 228 + ((tt - 16) % 4) * 88 + 6, 88 + WIN_HEIGHT / 2 - 204 + static_cast<int>((tt - 16) / 4) * 88 + 6, 0xff0000, _T("%d"), joypad->m_K[static_cast<int>((tt - 16) / 4)][(tt - 16) % 4]);
	}
}

void KeyConfig::ReplaceKey()
{
	Setting.KeyConfig.KeyDown = this->k[0];
	Setting.KeyConfig.KeyLeft = this->k[1];
	Setting.KeyConfig.KeyRight = this->k[2];
	Setting.KeyConfig.KeyUp = this->k[3];
	Setting.KeyConfig.KeyStart = this->k[4];
	Setting.KeyConfig.KeyBack = this->k[5];
	Setting.KeyConfig.KeyL = this->k[6];
	Setting.KeyConfig.KeyR = this->k[7];
	Setting.KeyConfig.Key1 = this->k[8];
	Setting.KeyConfig.Key2 = this->k[9];
	Setting.KeyConfig.Key3 = this->k[10];
	Setting.KeyConfig.Key4 = this->k[11];
	Setting.KeyConfig.Key5 = this->k[12];
	Setting.KeyConfig.Key6 = this->k[13];
	Setting.KeyConfig.Key7 = this->k[14];
	Setting.KeyConfig.Key8 = this->k[15];
	Setting.KeyConfig.Key9 = this->k[16];
	Setting.KeyConfig.Key10 = this->k[17];
	Setting.KeyConfig.Key11 = this->k[18];
	Setting.KeyConfig.Key12 = this->k[19];
	Setting.KeyConfig.Key13 = this->k[20];
	Setting.KeyConfig.Key14 = this->k[21];
	Setting.KeyConfig.Key15 = this->k[22];
	Setting.KeyConfig.Key16 = this->k[23];
}

void KeyConfig::ReplaceDefaultKey()
{
	this->k[0] = Setting.KeyConfig.KeyDown;
	this->k[1] = Setting.KeyConfig.KeyLeft;
	this->k[2] = Setting.KeyConfig.KeyRight;
	this->k[3] = Setting.KeyConfig.KeyUp;
	this->k[4] = Setting.KeyConfig.KeyStart;
	this->k[5] = Setting.KeyConfig.KeyBack;
	this->k[6] = Setting.KeyConfig.KeyL;
	this->k[7] = Setting.KeyConfig.KeyR;
	this->k[8] = Setting.KeyConfig.Key1;
	this->k[9] = Setting.KeyConfig.Key2;
	this->k[10] = Setting.KeyConfig.Key3;
	this->k[11] = Setting.KeyConfig.Key4;
	this->k[12] = Setting.KeyConfig.Key5;
	this->k[13] = Setting.KeyConfig.Key6;
	this->k[14] = Setting.KeyConfig.Key7;
	this->k[15] = Setting.KeyConfig.Key8;
	this->k[16] = Setting.KeyConfig.Key9;
	this->k[17] = Setting.KeyConfig.Key10;
	this->k[18] = Setting.KeyConfig.Key11;
	this->k[19] = Setting.KeyConfig.Key12;
	this->k[20] = Setting.KeyConfig.Key13;
	this->k[21] = Setting.KeyConfig.Key14;
	this->k[22] = Setting.KeyConfig.Key15;
	this->k[23] = Setting.KeyConfig.Key16;
}

void KeyConfig::JoyPadSubstitution(const std::shared_ptr<JoyPad>& joypad)
{
	for (auto i = 0; i < 8; i++) {
		for (auto j = 0; j < 2; j++) {
			Setting.JoyPad.JoyPadKey[i][j] = joypad->m_Key[i][j];
		}
	}
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			Setting.JoyPad.JoyPad[i][j] = joypad->m_K[i][j];
		}
	}
}

void KeyConfig::JoyPadSubstitution2(const std::shared_ptr<JoyPad>& joypad)
{
	for (auto i = 0; i < 8; i++) {
		for (auto j = 0; j < 2; j++) {
			joypad->m_Key[i][j] = Setting.JoyPad.JoyPadKey[i][j];
		}
	}
	for (auto i = 0; i < 4; i++) {
		for (auto j = 0; j < 4; j++) {
			joypad->m_K[i][j] = Setting.JoyPad.JoyPad[i][j];
		}
	}
}

//キーボードの数値(Keycode)から文字列を返す関数
char* KeyConfig::ConvertKeycodeToChar(const int x) const
{
	char szBuffer[50];
	memset(szBuffer, '\0', sizeof(szBuffer));
	const auto bufText = szBuffer;
	if (x == 0x01)strcpy_s(bufText, sizeof(szBuffer), "ESC");
	if (x == 0x02)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0x03)strcpy_s(bufText, sizeof(szBuffer), "2");
	if (x == 0x04)strcpy_s(bufText, sizeof(szBuffer), "3");
	if (x == 0x05)strcpy_s(bufText, sizeof(szBuffer), "4");
	if (x == 0x06)strcpy_s(bufText, sizeof(szBuffer), "5");
	if (x == 0x07)strcpy_s(bufText, sizeof(szBuffer), "6");
	if (x == 0x08)strcpy_s(bufText, sizeof(szBuffer), "7");
	if (x == 0x09)strcpy_s(bufText, sizeof(szBuffer), "8");
	if (x == 0x0A)strcpy_s(bufText, sizeof(szBuffer), "9");
	if (x == 0x0B)strcpy_s(bufText, sizeof(szBuffer), "0");
	if (x == 0x0C)strcpy_s(bufText, sizeof(szBuffer), "-");
	if (x == 0x0D)strcpy_s(bufText, sizeof(szBuffer), "=");
	if (x == 0x0E)strcpy_s(bufText, sizeof(szBuffer), "BACKSPACE");
	if (x == 0x0F)strcpy_s(bufText, sizeof(szBuffer), "TAB");
	if (x == 0x10)strcpy_s(bufText, sizeof(szBuffer), "Q");
	if (x == 0x11)strcpy_s(bufText, sizeof(szBuffer), "W");
	if (x == 0x12)strcpy_s(bufText, sizeof(szBuffer), "E");
	if (x == 0x13)strcpy_s(bufText, sizeof(szBuffer), "R");
	if (x == 0x14)strcpy_s(bufText, sizeof(szBuffer), "T");
	if (x == 0x15)strcpy_s(bufText, sizeof(szBuffer), "Y");
	if (x == 0x16)strcpy_s(bufText, sizeof(szBuffer), "U");
	if (x == 0x17)strcpy_s(bufText, sizeof(szBuffer), "I");
	if (x == 0x18)strcpy_s(bufText, sizeof(szBuffer), "O");
	if (x == 0x19)strcpy_s(bufText, sizeof(szBuffer), "P");
	if (x == 0x1A)strcpy_s(bufText, sizeof(szBuffer), "[");
	if (x == 0x1B)strcpy_s(bufText, sizeof(szBuffer), "]");
	if (x == 0x1C)strcpy_s(bufText, sizeof(szBuffer), "RETURN");
	if (x == 0x1D)strcpy_s(bufText, sizeof(szBuffer), "LCONTROL");
	if (x == 0x1E)strcpy_s(bufText, sizeof(szBuffer), "A");
	if (x == 0x1F)strcpy_s(bufText, sizeof(szBuffer), "S");
	if (x == 0x20)strcpy_s(bufText, sizeof(szBuffer), "D");
	if (x == 0x21)strcpy_s(bufText, sizeof(szBuffer), "F");
	if (x == 0x22)strcpy_s(bufText, sizeof(szBuffer), "G");
	if (x == 0x23)strcpy_s(bufText, sizeof(szBuffer), "H");
	if (x == 0x24)strcpy_s(bufText, sizeof(szBuffer), "J");
	if (x == 0x25)strcpy_s(bufText, sizeof(szBuffer), "K");
	if (x == 0x26)strcpy_s(bufText, sizeof(szBuffer), "L");
	if (x == 0x27)strcpy_s(bufText, sizeof(szBuffer), ";");
	if (x == 0x28)strcpy_s(bufText, sizeof(szBuffer), "'");
	if (x == 0x29)strcpy_s(bufText, sizeof(szBuffer), "`");
	if (x == 0x2A)strcpy_s(bufText, sizeof(szBuffer), "LSHIFT");
	if (x == 0x2B)strcpy_s(bufText, sizeof(szBuffer), "BACKSLASH");
	if (x == 0x2C)strcpy_s(bufText, sizeof(szBuffer), "Z");
	if (x == 0x2D)strcpy_s(bufText, sizeof(szBuffer), "X");
	if (x == 0x2E)strcpy_s(bufText, sizeof(szBuffer), "C");
	if (x == 0x2F)strcpy_s(bufText, sizeof(szBuffer), "V");
	if (x == 0x30)strcpy_s(bufText, sizeof(szBuffer), "B");
	if (x == 0x31)strcpy_s(bufText, sizeof(szBuffer), "N");
	if (x == 0x32)strcpy_s(bufText, sizeof(szBuffer), "M");
	if (x == 0x33)strcpy_s(bufText, sizeof(szBuffer), ",");
	if (x == 0x34)strcpy_s(bufText, sizeof(szBuffer), ".");
	if (x == 0x35)strcpy_s(bufText, sizeof(szBuffer), "/");
	if (x == 0x36)strcpy_s(bufText, sizeof(szBuffer), "RSHIFT");
	if (x == 0x37)strcpy_s(bufText, sizeof(szBuffer), "*");
	if (x == 0x38)strcpy_s(bufText, sizeof(szBuffer), "LMENU");
	if (x == 0x39)strcpy_s(bufText, sizeof(szBuffer), "SPACE");
	if (x == 0x3A)strcpy_s(bufText, sizeof(szBuffer), "CAPSLOCK");
	if (x == 0x3B)strcpy_s(bufText, sizeof(szBuffer), "F1");
	if (x == 0x3C)strcpy_s(bufText, sizeof(szBuffer), "F2");
	if (x == 0x3D)strcpy_s(bufText, sizeof(szBuffer), "F3");
	if (x == 0x3E)strcpy_s(bufText, sizeof(szBuffer), "F4");
	if (x == 0x3F)strcpy_s(bufText, sizeof(szBuffer), "F5");
	if (x == 0x40)strcpy_s(bufText, sizeof(szBuffer), "F6");
	if (x == 0x41)strcpy_s(bufText, sizeof(szBuffer), "F7");
	if (x == 0x42)strcpy_s(bufText, sizeof(szBuffer), "F8");
	if (x == 0x43)strcpy_s(bufText, sizeof(szBuffer), "F9");
	if (x == 0x44)strcpy_s(bufText, sizeof(szBuffer), "F10");
	if (x == 0x45)strcpy_s(bufText, sizeof(szBuffer), "NUMLOCK");
	if (x == 0x46)strcpy_s(bufText, sizeof(szBuffer), "SCROLL");
	if (x == 0x47)strcpy_s(bufText, sizeof(szBuffer), "NUM7");
	if (x == 0x48)strcpy_s(bufText, sizeof(szBuffer), "NUM8");
	if (x == 0x49)strcpy_s(bufText, sizeof(szBuffer), "NUM9");
	if (x == 0x4A)strcpy_s(bufText, sizeof(szBuffer), "NUM-");
	if (x == 0x4B)strcpy_s(bufText, sizeof(szBuffer), "NUM4");
	if (x == 0x4C)strcpy_s(bufText, sizeof(szBuffer), "NUM5");
	if (x == 0x4D)strcpy_s(bufText, sizeof(szBuffer), "NUM6");
	if (x == 0x4E)strcpy_s(bufText, sizeof(szBuffer), "NUM+");
	if (x == 0x4F)strcpy_s(bufText, sizeof(szBuffer), "NUM1");
	if (x == 0x50)strcpy_s(bufText, sizeof(szBuffer), "NUM2");
	if (x == 0x51)strcpy_s(bufText, sizeof(szBuffer), "NUM3");
	if (x == 0x52)strcpy_s(bufText, sizeof(szBuffer), "NUM0");
	if (x == 0x53)strcpy_s(bufText, sizeof(szBuffer), "NUM.");

	if (x == 0x56)strcpy_s(bufText, sizeof(szBuffer), "OEM_102");
	if (x == 0x57)strcpy_s(bufText, sizeof(szBuffer), "F11");
	if (x == 0x58)strcpy_s(bufText, sizeof(szBuffer), "F12");

	if (x == 0x64)strcpy_s(bufText, sizeof(szBuffer), "F13");
	if (x == 0x65)strcpy_s(bufText, sizeof(szBuffer), "F14");
	if (x == 0x66)strcpy_s(bufText, sizeof(szBuffer), "F15");

	if (x == 0x70)strcpy_s(bufText, sizeof(szBuffer), "KANA");
	if (x == 0x73)strcpy_s(bufText, sizeof(szBuffer), "ABNT_C1");
	if (x == 0x79)strcpy_s(bufText, sizeof(szBuffer), "CONVERT");
	if (x == 0x7B)strcpy_s(bufText, sizeof(szBuffer), "NOCONVERT");
	if (x == 0x7D)strcpy_s(bufText, sizeof(szBuffer), "\\");
	if (x == 0x7E)strcpy_s(bufText, sizeof(szBuffer), "ABNT_C2");

	if (x == 0x8D)strcpy_s(bufText, sizeof(szBuffer), "NUM=");

	if (x == 0x90)strcpy_s(bufText, sizeof(szBuffer), "^");
	if (x == 0x91)strcpy_s(bufText, sizeof(szBuffer), "@");
	if (x == 0x92)strcpy_s(bufText, sizeof(szBuffer), ":");
	if (x == 0x93)strcpy_s(bufText, sizeof(szBuffer), "_");
	if (x == 0x94)strcpy_s(bufText, sizeof(szBuffer), "KANJI");
	if (x == 0x95)strcpy_s(bufText, sizeof(szBuffer), "STOP");
	if (x == 0x96)strcpy_s(bufText, sizeof(szBuffer), "AX");
	if (x == 0x97)strcpy_s(bufText, sizeof(szBuffer), "UNLABELED");

	if (x == 0x99)strcpy_s(bufText, sizeof(szBuffer), "NEXTTRACK");

	if (x == 0x9C)strcpy_s(bufText, sizeof(szBuffer), "NUMENTER");
	if (x == 0x9D)strcpy_s(bufText, sizeof(szBuffer), "RCONTROL");

	if (x == 0xA0)strcpy_s(bufText, sizeof(szBuffer), "MUTE");
	if (x == 0xA1)strcpy_s(bufText, sizeof(szBuffer), "CALCULATOR");
	if (x == 0xA2)strcpy_s(bufText, sizeof(szBuffer), "PLAYPAUSE");

	if (x == 0xA4)strcpy_s(bufText, sizeof(szBuffer), "MEDIASTOP");

	if (x == 0xAE)strcpy_s(bufText, sizeof(szBuffer), "VOLUMEDOWN");
	if (x == 0xB0)strcpy_s(bufText, sizeof(szBuffer), "VOLUMEUP");

	if (x == 0xB2)strcpy_s(bufText, sizeof(szBuffer), "WEBHOME");
	if (x == 0xB3)strcpy_s(bufText, sizeof(szBuffer), "NUM.");
	if (x == 0xB5)strcpy_s(bufText, sizeof(szBuffer), "NUM/");

	if (x == 0xB7)strcpy_s(bufText, sizeof(szBuffer), "SYSRQ");
	if (x == 0xB8)strcpy_s(bufText, sizeof(szBuffer), "RMENU");

	if (x == 0xC5)strcpy_s(bufText, sizeof(szBuffer), "PAUSE");

	if (x == 0xC7)strcpy_s(bufText, sizeof(szBuffer), "HOME");
	if (x == 0xC8)strcpy_s(bufText, sizeof(szBuffer), "UP");
	if (x == 0xC9)strcpy_s(bufText, sizeof(szBuffer), "PRIOR");

	if (x == 0xCB)strcpy_s(bufText, sizeof(szBuffer), "LEFT");

	if (x == 0xCD)strcpy_s(bufText, sizeof(szBuffer), "RIGHT");

	if (x == 0xCF)strcpy_s(bufText, sizeof(szBuffer), "END");
	if (x == 0xD0)strcpy_s(bufText, sizeof(szBuffer), "DOWN");
	if (x == 0xD1)strcpy_s(bufText, sizeof(szBuffer), "NEXT");
	if (x == 0xD2)strcpy_s(bufText, sizeof(szBuffer), "INSERT");
	if (x == 0xD3)strcpy_s(bufText, sizeof(szBuffer), "DELETE");

	if (x == 0xDB)strcpy_s(bufText, sizeof(szBuffer), "LWIN");
	if (x == 0xDC)strcpy_s(bufText, sizeof(szBuffer), "RWIN");
	if (x == 0xDD)strcpy_s(bufText, sizeof(szBuffer), "APPS");
	if (x == 0xDE)strcpy_s(bufText, sizeof(szBuffer), "POWER");
	if (x == 0xDF)strcpy_s(bufText, sizeof(szBuffer), "SLEEP");

	if (x == 0xE3)strcpy_s(bufText, sizeof(szBuffer), "WAKE");

	if (x == 0xE5)strcpy_s(bufText, sizeof(szBuffer), "WEBSEARCH");
	if (x == 0xE6)strcpy_s(bufText, sizeof(szBuffer), "WEBFAVORITES");
	if (x == 0xE7)strcpy_s(bufText, sizeof(szBuffer), "WEBREFRESH");
	if (x == 0xE8)strcpy_s(bufText, sizeof(szBuffer), "WEBSTOP");
	if (x == 0xE9)strcpy_s(bufText, sizeof(szBuffer), "WEBFORWARD");
	if (x == 0xEA)strcpy_s(bufText, sizeof(szBuffer), "WEBBACK");
	if (x == 0xEB)strcpy_s(bufText, sizeof(szBuffer), "MYCOMPUTER");
	if (x == 0xEC)strcpy_s(bufText, sizeof(szBuffer), "MAIL");
	if (x == 0xED)strcpy_s(bufText, sizeof(szBuffer), "MEDIASELECT");
	if (x == 0xEE)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0xEF)strcpy_s(bufText, sizeof(szBuffer), "1");

	if (x == 0xF1)strcpy_s(bufText, sizeof(szBuffer), "Esc");
	if (x == 0xF2)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0xF3)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0xF4)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0xF5)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0xF6)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0xF7)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0xF8)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0xF9)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0xFA)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0xFB)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0xFC)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0xFD)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0xFE)strcpy_s(bufText, sizeof(szBuffer), "1");
	if (x == 0xFF)strcpy_s(bufText, sizeof(szBuffer), "1");
	return bufText;
}

#pragma region MoveCasolJoyPad
//カーソル移動の全パターン(仮実装なので修正予定)
int KeyConfig::MoveCasolJoyPad(int tt) const
{
	if (tt == 0) {
		if (this->m_keyDown == 1)
		{
			tt = 1;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 8;
		}
	}
	else if (tt == 1) {
		if (this->m_keyDown == 1)
		{
			tt = 2;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 9;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 0;
		}
	}
	else if (tt == 2) {
		if (this->m_keyDown == 1)
		{
			tt = 3;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 10;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 1;
		}
	}
	else if (tt == 3) {
		if (this->m_keyDown == 1)
		{
			tt = 4;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 11;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 2;
		}
	}
	else if (tt == 4) {
		if (this->m_keyDown == 1)
		{
			tt = 5;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 12;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 3;
		}
	}
	else if (tt == 5) {
		if (this->m_keyDown == 1)
		{
			tt = 6;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 13;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 4;
		}
	}
	else if (tt == 6) {
		if (this->m_keyDown == 1)
		{
			tt = 7;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 14;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 5;
		}
	}
	else if (tt == 7) {
		if (this->m_keyRight == 1)
		{
			tt = 15;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 6;
		}
	}
	else if (tt == 8) {
		if (this->m_keyDown == 1)
		{
			tt = 9;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 16;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 0;
		}
	}
	else if (tt == 9) {
		if (this->m_keyDown == 1)
		{
			tt = 10;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 16;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 1;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 8;
		}
	}
	else if (tt == 10) {
		if (this->m_keyDown == 1)
		{
			tt = 11;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 20;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 2;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 9;
		}
	}
	else if (tt == 11) {
		if (this->m_keyDown == 1)
		{
			tt = 12;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 20;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 3;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 10;
		}
	}
	else if (tt == 12) {
		if (this->m_keyDown == 1)
		{
			tt = 13;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 24;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 4;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 11;
		}
	}
	else if (tt == 13) {
		if (this->m_keyDown == 1)
		{
			tt = 14;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 24;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 5;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 12;
		}
	}
	else if (tt == 14) {
		if (this->m_keyDown == 1)
		{
			tt = 15;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 28;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 6;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 13;
		}
	}
	else if (tt == 15) {
		if (this->m_keyRight == 1)
		{
			tt = 28;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 7;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 14;
		}
	}
	//4*4
	else if (tt == 16) {
		if (this->m_keyDown == 1)
		{
			tt = 20;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 17;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 8;
		}
	}
	else if (tt == 17) {
		if (this->m_keyDown == 1)
		{
			tt = 21;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 18;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 16;
		}
	}
	else if (tt == 18) {
		if (this->m_keyDown == 1)
		{
			tt = 22;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 19;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 17;
		}
	}
	else if (tt == 19) {
		if (this->m_keyDown == 1)
		{
			tt = 23;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 18;
		}
	}
	else if (tt == 20) {
		if (this->m_keyDown == 1)
		{
			tt = 24;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 21;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 10;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 16;
		}
	}
	else if (tt == 21) {
		if (this->m_keyDown == 1)
		{
			tt = 25;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 22;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 20;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 17;
		}
	}
	else if (tt == 22) {
		if (this->m_keyDown == 1)
		{
			tt = 26;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 23;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 21;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 18;
		}
	}
	else if (tt == 23) {
		if (this->m_keyDown == 1)
		{
			tt = 27;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 22;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 19;
		}
	}
	else if (tt == 24) {
		if (this->m_keyDown == 1)
		{
			tt = 28;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 25;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 12;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 20;
		}
	}
	else if (tt == 25) {
		if (this->m_keyDown == 1)
		{
			tt = 29;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 26;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 24;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 21;
		}
	}
	else if (tt == 26) {
		if (this->m_keyDown == 1)
		{
			tt = 30;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 27;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 25;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 22;
		}
	}
	else if (tt == 27) {
		if (this->m_keyDown == 1)
		{
			tt = 31;
		}
		else if (this->m_keyLeft == 1)
		{
			tt = 26;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 23;
		}
	}
	else if (tt == 28) {
		if (this->m_keyLeft == 1)
		{
			tt = 14;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 29;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 24;
		}
	}
	else if (tt == 29) {
		if (this->m_keyLeft == 1)
		{
			tt = 28;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 30;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 25;
		}
	}
	else if (tt == 30) {
		if (this->m_keyLeft == 1)
		{
			tt = 29;
		}
		else if (this->m_keyRight == 1)
		{
			tt = 31;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 26;
		}
	}
	else if (tt == 31) {
		if (this->m_keyLeft == 1)
		{
			tt = 30;
		}
		else if (this->m_keyUp == 1)
		{
			tt = 27;
		}
	}
	else if (tt == 32) {
	}
	return (tt);
}
#pragma endregion