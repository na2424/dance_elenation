#include "extern.h"
#include "JudgeCount.h"



JudgeCount::JudgeCount() :
	parfect(0),
	grate(0),
	good(0),
	missed(0),
	OK(0),
	NG(0),
	maxCombo(0),
	percent(0),
	score(0),
	HiScore(0),
	isDoneFullcombo(false) {};

void JudgeCount::Init()
{
	ResetCount();
}

void JudgeCount::ResetCount()
{
	parfect = 0;
	grate = 0;
	good = 0;
	missed = 0;
	OK = 0;
	NG = 0;
	maxCombo = 0;
	percent = 0;
	score = 0;
	HiScore = 0;
	isDoneFullcombo = 0;
}