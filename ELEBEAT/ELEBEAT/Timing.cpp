#include "Timing.h"
#include "Music.h"
#include "SettingIni.h"
#include "extern.h"
#include <boost/lexical_cast.hpp>
#include "DoubleUtils.h"



void Timing::SetTiming(const std::shared_ptr<Music> &musicP, const int musicIndex)
{
	//BPM
	SetBpm(musicP, musicIndex);
	//STOP
	SetStop(musicP, musicIndex);
	//Offset
	SetOffset(musicP, musicIndex);

	//BPM.
	SetDiffBpmPosition();
	SetDiffTimeBpmPosition();
	SetStopTime();
	SetBpmTime();
}

bool Timing::IsStopTiming(long time, int stopIndex)
{
	return m_StopPositionTime.at(stopIndex) < time && m_Stop[stopIndex] != 0;
}

bool Timing::IsBpmChangeTiming(long time, int bpmIndex)
{
	return m_Cn[bpmIndex + 1] <= time && m_Cn[bpmIndex + 1] != 0;
}

void Timing::SetBpm(const std::shared_ptr<Music> &musicP, const int musicIndex)
{
	for (tstring bpm : musicP->danceSong->m_Bpm[musicIndex])
	{
		m_Bpm.emplace_back(atof(bpm.c_str()));
	}

	for (tstring bpmPosition : musicP->danceSong->m_BpmPositions[musicIndex])
	{
		m_BpmPosition.emplace_back(atof(bpmPosition.c_str()));
	}

	m_Bpm.emplace_back(0);
	m_BpmPosition.emplace_back(-1);
}

void Timing::SetStop(const std::shared_ptr<Music> &musicP, const int musicIndex)
{
	for (tstring stop : musicP->danceSong->m_Stop[musicIndex])
	{
		m_Stop.emplace_back(boost::lexical_cast<double>(stop.c_str()));
	}

	for (tstring stopPosition : musicP->danceSong->m_StopPositions[musicIndex])
	{
		m_StopPosition.emplace_back(boost::lexical_cast<double>(stopPosition.c_str()));
	}

	m_Stop.emplace_back(0);
	m_StopPosition.emplace_back(-1);
}

void Timing::SetOffset(const std::shared_ptr<Music> &musicP, const int musicIndex)
{
	m_Offset = -boost::lexical_cast<double>(musicP->danceSong->m_Offset[musicIndex].c_str()) * 1000 + Setting.GamePlaySetting.GlobalOffset;
}

void Timing::SetDiffBpmPosition()
{
	for (unsigned n = 0; n < m_BpmPosition.size() - 1; ++n)
	{
		m_bn.emplace_back(m_BpmPosition[n + 1] - m_BpmPosition[n]);
	}
}

void Timing::SetDiffTimeBpmPosition()
{
	for (unsigned n = 0; n < m_Bpm.size() - 1; ++n)
	{
		if (m_Bpm[n] != 0)
		{
			m_dn.emplace_back(m_bn[n]);
			m_bn[n] = m_bn[n] * 60000.0 / m_Bpm[n];//+ m_TimeS;
		}
	}

	m_bn.emplace_back(0);
}

/// <summary>
/// 停止時間をセット
/// </summary>
void Timing::SetStopTime()
{
	auto stopIndex = 0;
	auto stopTime = 0.0;

	for (unsigned n = 0; n < m_Stop.size() - 1; ++n)
	{
		if (m_Stop[n] != 0)
		{
			for (unsigned i = 0; i < m_Bpm.size() - 1; ++i)
			{
				if (m_BpmPosition[i + 1] > m_StopPosition[n] || m_BpmPosition[i + 1] == -1)
				{
					if (m_StopPosition[stopIndex] < m_StopPosition[n] && m_StopPosition[stopIndex] != -1)
					{
						stopTime += m_Stop[stopIndex] * 1000.0f;
						stopIndex++;
					}
					m_StopPositionTime.emplace_back(60000.0f / m_Bpm[i] * (m_StopPosition[n] - m_BpmPosition[i]) + m_TimeN + stopTime + m_Offset);

					m_TimeN = 0;
					break;
				}
				m_TimeN += m_bn[i];
			}
		}
	}

	m_StopPositionTime.emplace_back(-1);
}

/// <summary>
/// 停止時間を含めたBPMポジションのズレ時間をセット
/// </summary>
void Timing::SetDiffTimeBpmPositionWithStop()
{
	auto stopIndex = 0;

	for (auto n = 0; n < m_BpmPosition.size() - 1; ++n)
	{
		auto time = 0.0;
		for (auto i = stopIndex; i < m_Stop.size() - 1; ++i)
		{
			if (m_StopPosition.at(i) <= m_BpmPosition[n + 1] && m_StopPosition.at(i) != -1)
			{
				time += m_Stop[i] * 1000.0f;
				stopIndex++;
			}
			if (DoubleUtils::AreSame(m_StopPosition[i], -1))
				break;
		}

		if (m_Bpm[n] != 0)
			m_dn[n] = m_dn[n] * 60000.0f / m_Bpm[n] + time;
	}
}

void Timing::SetBpmTime()
{
	int n = 0;
	for (auto bpm : m_Bpm)
	{
		if (bpm != 0)
		{
			m_Cn.emplace_back(m_TimeN + m_Offset);
			m_TimeN += m_dn[n++];
		}
	}

	m_Cn.emplace_back(0);
}