#ifndef LOADING_WINDOW_H
#define LOADING_WINDOW_H
#include <windows.h>
#include "define.h"

/* Win32 only: get an HINSTANCE; used for starting dialog boxes. */
class AppInstance
{
	HINSTANCE h;

public:
	//AppInstance();
	//~AppInstance();
	HINSTANCE Get() const { return h; }
	operator HINSTANCE () const { return h; }
};

class LoadingWindow
{
private:
	AppInstance handle;
	HWND hwnd;
	tstring text[3];

	static BOOL CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

public:

	LoadingWindow(HINSTANCE hInstance);
	~LoadingWindow();
	void Load() const;
	void SetText(tstring str);
	void Paint();
};
#endif