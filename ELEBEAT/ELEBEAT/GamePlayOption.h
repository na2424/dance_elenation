﻿#ifndef GAME_FLAG_CLASS_H
#define GAME_FLAG_CLASS_H

#include <string> 
#include <vector>
#include <array>
#include <tchar.h>

#include "Singleton.h"
#include "Enums.h"

#ifdef _UNICODE
typedef std::wstring tstring;
#else
typedef std::string tstring;
#endif

class GamePlayOption : public Singleton<GamePlayOption>
{
public:
	friend class Singleton < GamePlayOption >;
	bool isAuto;
	bool isCrap;
	bool isReverse;
	double speed;

	NoteSkinType noteSkin;

	void Init(void);

	//PlayerOption一覧
	const std::vector<tstring> playerOptionNameList = std::vector<tstring>
	{
		_T("Speed"),
		_T("Scroll"),
		_T("NoteSkin")
	};

	//Speed
	const std::vector<tstring> speedNameList = std::vector < tstring >
	{
		_T(" x1"), _T("x1.5"), _T(" x2"), _T("x2.5"), _T(" x3"), _T("x3.5"), _T(" x4"), _T("x4.5"), _T(" x5"), _T("x5.5"), _T(" x6"), _T(" x8")
	};

	const std::vector<double> speedList = std::vector<double>
	{
		1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 8.0
	};

	//Scroll
	const std::vector<tstring> scrollNameList = std::vector< tstring >
	{
		_T("STANDARD"), _T("REVERSE")
	};

	const std::vector<bool> scrollList = std::vector<bool>
	{
		false, true
	};

	//NoteSkin
	const std::vector<tstring> noteSkinNameList = std::vector<tstring>
	{
		_T("RAINBOW"), _T("NOTE"), _T("FLAT")
	};

	const std::vector<NoteSkinType> noteSkinList = std::vector<NoteSkinType>
	{
		NoteSkinType::RAINBOW,
		NoteSkinType::NOTE,
		NoteSkinType::FLAT
	};

protected:
	GamePlayOption() {}
};

#endif