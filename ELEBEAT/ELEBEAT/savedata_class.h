#ifndef SAVEDATA_CLASS_H
#define SAVEDATA_CLASS_H

#include <boost/timer.hpp>
#include "Dxlib.h"

#include "define.h"
#include "Singleton.h"

/// <summary>
/// オプションのセーブデータの構造体
/// </summary>
class OptionSaveData
{
private:
	bool m_isEventMode;
	int m_songCount;
	int m_style;
	bool m_isFullScreen;
	int m_isShowFps;

public:
	static int g_SongCount;         // 現在何曲目かを入れる変数、初期値は１。(またタイトルに戻ったら1を代入する)
	static int g_SongCountSetting;  // 1プレイで何曲まで遊べるかを入れる変数。オプションで変更可能
	static bool g_IsEventMode;      // EVENTMODEのON,OFF(ON→１プレイで何曲でも遊べる、OFF→１プレイで設定した曲数だけ遊べる)
	static int g_Style;             // 4:3(CSstyle)か5:4(ACstyle)かを入れる変数
	static bool g_IsFullScreen;     // ウィンドウモードかフルスクリーンモードかを入れる変数
	static int g_ShowFps;           // ゲーム中にFPSを表示するかの設定を行います。

	void LoadDat();
	void SaveDat();

	OptionSaveData() :
		m_isEventMode(false),
		m_songCount(3),
		m_style(0),
		m_isFullScreen(false),
		m_isShowFps(false)
	{
	}
};

//楽曲スコアセーブデータの構造体([2][3]はplaymodeとdifficult)
struct ScoreSaveData
{
	bool m_IsFullCombo[3][5];        //フルコンボ　(値の意味：false(0)→フルコンしてない　true(1)→フルコンした)
	int m_Rank[3][5];            //評価      (値の意味：-1→未プレイ 0→パーフェクト 1→AAA 2→AA 3→A 4→B 5→C 6→D 7→E)
	long m_HighScore[3][5];       //ハイスコア
};

class ScoreData :public Singleton<ScoreData>
{
public:
	friend class Singleton < ScoreData >;
	static void TranslateToSafeFileName(tstring& strSafeFileName);
	//  文字列を置換する
	static void Replace(tstring& string1, tstring string2, tstring string3);
};

#endif