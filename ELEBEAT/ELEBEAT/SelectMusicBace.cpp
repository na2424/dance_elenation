#include "SelectMusicBase.h"
#include "extern.h"
#include "stdlib.h"
#include <boost/lexical_cast.hpp>
#include "Foreach.h"
#include "Screen.h"
#include "LuaHelper.h"

using namespace std;
using namespace DxLib;

//selectmusicクラス関連
//(9keys,4keys)
//int
int SelectMusicBase::g_DifNowCenter = 0;

int SelectMusicBase::g_BeforeCenter = 0;
int SelectMusicBase::g_MarkerX[16][4] = {};
int SelectMusicBase::g_MarkerY[16][4] = {};
int SelectMusicBase::g_SelectMarker = 0;
int SelectMusicBase::g_MarkerCount = 0;

array<int, 3> SelectMusicBase::g_CurrentAlbumArt;
array<int, 2> SelectMusicBase::g_MaxMinbpm;

bool SelectMusicBase::one[10] = {};

int SelectMusicBase::g_NowCenter = CENTER_DEFAULT;
int SelectMusicBase::x_16[10] = {};

int SelectMusicBase::g_MarkerGraph16[1000] = {};
int SelectMusicBase::nowAA_16[3] = {};
int SelectMusicBase::beforecenter_16 = 0;

int SelectMusicBase::nowselectglobalAAnum = 0;
int SelectMusicBase::difselect = 0;
int SelectMusicBase::beforeselectAA = -1;
int SelectMusicBase::beforeselectmarker = -1;

int SelectMusicBase::strlength[3] = {};

bool SelectMusicBase::one_16[15] = {};
bool SelectMusicBase::stop[3] = {};
bool SelectMusicBase::selectmusicflag = false;
bool SelectMusicBase::loadendflag_16 = false;

bool SelectMusicBase::g_IsLoaded = false;
bool SelectMusicBase::g_IsLoaded4Keys = false;

SelectMusicBase::SelectMusicBase() :
	startTime(0),
	_y(0),
	randomcenternum(0),
	m_InputFrameCount(0),
	isLeft(false),
	isRight(false),
	isLeftContinue(false),
	isRightContinue(false),
	isFastLeft(false),
	isFastRight(false),
	markerflag(false),
	isLeftMarker(false),
	isRightMarker(false),
	isDownMarker(false),
	marker_goup(false),
	//16keys
	//nowcenter(CENTERDEFAULT),

	loadingImage(0),
	count(0),
	selectkeynumber(0),
	maxline_folder(0),
	maxline_AA(0),
	maxline_marker(0),
	selectfoldernum(0),
	stageNumImage(0),
	moveleft(0), //左へ
	moveright(0), //右へ
	leftcountflag_16(0), //押し始め
	rightcountflag_16(0), //押し始め
	fastleftflag_16(0), //高速移動許可
	fastrightflag_16(0), //高速移動許可
	////int 画像
	rightImage(0), //右にパネル
	leftImage(0), //左にパネル
	markerImage(0), //マーカー選択
	enterImage(0), //ok
	backImage(0), //戻る
	detailstring(0),
	backblack(0),
	numstring(0),

	//4keys
	backoverImage(0),
	setumei1(0),
	setumei2(0),
	blend_gh(0),
	black_gh(0),
	right_pic(0),
	left_pic(0),
	right_click_pic(0),
	left_click_pic(0),
	marker_waku(0),
	player_option(0),
	selecter(0),
	raderImage(0),
	wheelSound(0),
	decideSound(0),
	expandSound(0),
	mrkSound(0),
	closeSound(0),
	mrkMoveSound(0)
{
	memset(square, 0, sizeof(square));
	memset(difstring, 0, sizeof(difstring));
	memset(movedrawflag, 0, sizeof(movedrawflag));

	for (int i = 0; i < 40; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			x[i][j] = 0;
			y[i][j] = 0;
			m_x[i][j] = 0;
			m_x[i][j] = 0;
		}
	}
	//BPM数字の画像
	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/bpm_num.png"), 10, 5, 2, 50, 50, m_bpmNumberImages.data());
}

//Lua初期化
void SelectMusicBase::InitLua()
{
	// LuaのVMを生成する
	L = luaL_newstate();

	// Luaの標準ライブラリを開く
	luaL_openlibs(L);

	luaHelper.SetLua(L);
}

//lua読み込み
int SelectMusicBase::ReadLua(void)
{
	LuaFuncParam params;
	LuaFuncParam results;

	if (!luaHelper.DoFile("Themes/DANCE ELENATION/lua/_file_music_select.lua", &results, 1, &params))
	{
		//エラー
		if (!OptionSaveData::g_IsFullScreen)
			MessageBoxA(nullptr, _T("ERROR"), _T("_file_music_select.luaを開けませんでした\n"), MB_OK);
		exit(0);
	}

	vector<tstring>luaStackImageStr = {
		_T("backoverImage"),
		_T("nanidoImage"),
		_T("backfrontImage"),
		_T("setumei1Image"),
		_T("setumei2Image"),
		_T("blendImage"),
		_T("blackImage"),
		_T("rightImage"),
		_T("leftImage"),
		_T("rightClickImage"),
		_T("leftClickImage"),
		_T("markerWakuImage"),
		_T("playerOptionImage"),
		_T("bpmWakuImage"),
		_T("selecterImage"),
		_T("raderImage"),
		_T("loadingImage")
	};
	vector<tstring>luaStackSoundStr = {
		_T("selectdifsound"),
		_T("wheel_sound"),
		_T("decide_sound"),
		_T("expand_sound"),
		_T("mrk_sound"),
		_T("close_sound"),
		_T("mrk_move_sound")
	};

	//ファイル名をスタックに積む
	for each(tstring str in luaStackImageStr)
	{
		lua_getglobal(L, str.c_str());
	}
	for each(tstring str in luaStackSoundStr)
	{
		lua_getglobal(L, str.c_str());
	}

	const int num = lua_gettop(L);

	vector<tstring> filename(0);
	if (num == 0)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("エラー"), _T("No stack"), MB_OK);
	}

	for (int N = num; N > 0; N--)
	{
		if (lua_type(L, N) == LUA_TSTRING)
			filename.emplace_back(lua_tostring(L, N));
	}

	const int nameNum = static_cast<int>(filename.size());
	int count = 1;

	auto LoadImageLua = [&](int &handle)
	{
		handle = LoadGraph((_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\") + filename.at(nameNum - count)).c_str());
		++count;
	};

	auto LoadSoundLua = [&](int &handle)
	{
		handle = LoadSoundMem((_T("Themes\\DANCE ELENATION\\Sounds\\MusicSelect\\") + filename.at(nameNum - count)).c_str());
		++count;
	};

	LoadImageLua(backoverImage);
	LoadImageLua(difficultyImage);
	LoadImageLua(backFrontImage);
	LoadImageLua(setumei1);
	LoadImageLua(setumei2);
	LoadImageLua(blend_gh);
	LoadImageLua(black_gh);
	LoadImageLua(right_pic);
	LoadImageLua(left_pic);
	LoadImageLua(right_click_pic);
	LoadImageLua(left_click_pic);
	LoadImageLua(marker_waku);
	LoadImageLua(player_option);
	LoadImageLua(bpmFrameImage);
	LoadImageLua(selecter);
	LoadImageLua(raderImage);
	LoadImageLua(loadingImage);

	LoadSoundLua(selectdifsound);
	LoadSoundLua(wheelSound);
	LoadSoundLua(decideSound);
	LoadSoundLua(expandSound);
	LoadSoundLua(mrkSound);
	LoadSoundLua(closeSound);
	LoadSoundLua(mrkMoveSound);

	return 0;
}

//  文字列を置換する
void SelectMusicBase::Replace(tstring& String1, tstring String2, tstring String3) const
{
	tstring::size_type  Pos(String1.find(String2));

	while (Pos != tstring::npos)
	{
		String1.replace(Pos, String2.length(), String3);
		Pos = String1.find(String2, Pos + String3.length());
	}
}

//曲選択画面でのBPM数字
void SelectMusicBase::DrawSelectMusicBPM(int num) const
{
	// numが十進数で何桁になるか調べる
	int beamWidth = 0;
	for (auto i = 1; num >= i; i *= 10) beamWidth++;

	// 画面右上に右詰で表示
	// xは描く数字(一桁一桁各々)の左端のX座標
	int x = 565;
	for (auto i = 0; i < beamWidth; i++)
	{
		DrawGraph(x, 95, m_bpmNumberImages[num % 10], TRUE);
		num /= 10;
		x -= 18;
	}
}