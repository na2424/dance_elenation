#ifndef KEYCONFIG_H
#define KEYCONFIG_H

#include "define.h"
#include <array>
#include <vector>
#include <memory>

class JoyPad;

class KeyConfig
{
private:
	int m_fontHandle;
	std::array<char, 256> m_buf;
	std::array<int, INPUT_KEY_MAX> k;
	int s;
	int flag;

	int m_select;

	//Key
	int m_keyUp;
	int m_keyDown;
	int m_keyLeft;
	int m_keyRight;
	int m_keyEnter;
	int m_keyEsc;

	bool canMove;
	bool canEnter;
	//bool isEndKeyConfig;
	int confilmSelect;

	enum ConfigState : int
	{
		Select,
		Change,
		Decide,
		Back
	};
	ConfigState m_configState;

	int m_configColor;

	const std::vector<tstring> KEY_LIST = std::vector<tstring>
	{
		"Key Down    ",
		"Key Left    ",
		"Key Right   ",
		"Key Up      ",
		"Key Start   ",
		"Key Back    ",
		"Key L       ",
		"Key R       ",
	};

public:
	KeyConfig();
	void ChangeConfigState(ConfigState state);
	void SetFontHandle(int hundle);
	void UpdateState();
	void ConfigKeySetting();
	void ConfigJoyPadSetting();
	void DrawStringKey(int i);
	void DrawStringKey2(int i) const;
	void DrawStringKey3(int i) const;
	void DrawStringJoyPad(int tt, const std::shared_ptr<JoyPad>& joypad) const;
	void ReplaceKey();
	void ReplaceDefaultKey();
	static void JoyPadSubstitution(const std::shared_ptr<JoyPad>& joypad);
	static void JoyPadSubstitution2(const std::shared_ptr<JoyPad>& joypad);
	char* ConvertKeycodeToChar(const int x) const;

	int MoveCasolJoyPad(int tt) const;
	void MoveSelect();
	void ChangeKeySetting();
	void ConfirmKeySetting();

	void GetKey();
};

#endif