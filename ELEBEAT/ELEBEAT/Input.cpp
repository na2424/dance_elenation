#include "DxLib.h"
#include "extern.h"
#include "Input.h"
#include <tchar.h>



////array<array<int, 4>, 4> Input::GetInstance().key;
//int Input::GetInstance().key[][4] = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 } };
//int Input::GetInstance().keyUp = 0;
//int Input::GetInstance().keyDown = 0;
//int Input::GetInstance().keyRight = 0;
//int Input::GetInstance().keyLeft = 0;
//int Input::GetInstance().keyEsc = 0;
//int Input::GetInstance().keyEnter = 0;
//int Input::GetInstance().keyShift = 0;
//int Input::GetInstance().F7 = 0;
//int Input::GetInstance().F8 = 0;
//int Input::GetInstance().keyPrtsc = 0;
//int Input::GetInstance().casolPic = 0;
////マウスクリックの状態を保存する変数
//int Input::GetInstance().mouseClick = 0;
//int Input::GetInstance().mouseWheel = 0;
///////Input *Input::GetInstance().Instance = nullptr;

//キー入力取得関数
void Input::GetKey()
{
	char buf[256];
	const auto joy = GetJoypadInputState(DX_INPUT_PAD1);
	GetHitKeyStateAll(buf);
	if (joy&Setting.JoyPad.JoyPadKey[0][0] || joy&Setting.JoyPad.JoyPadKey[0][1] || buf[Setting.KeyConfig.KeyDown] == 1) keyDown++;
	else keyDown = 0;
	if (joy&Setting.JoyPad.JoyPadKey[1][0] || joy&Setting.JoyPad.JoyPadKey[1][1] || buf[Setting.KeyConfig.KeyLeft] == 1) keyLeft++;
	else keyLeft = 0;
	if (joy&Setting.JoyPad.JoyPadKey[2][0] || joy&Setting.JoyPad.JoyPadKey[2][1] || buf[Setting.KeyConfig.KeyRight] == 1) keyRight++;
	else keyRight = 0;
	if (joy&Setting.JoyPad.JoyPadKey[3][0] || joy&Setting.JoyPad.JoyPadKey[3][1] || buf[Setting.KeyConfig.KeyUp] == 1) keyUp++;
	else keyUp = 0;
	if (joy&Setting.JoyPad.JoyPadKey[4][0] || joy&Setting.JoyPad.JoyPadKey[4][1] || buf[Setting.KeyConfig.KeyStart] == 1) keyEnter++;
	else keyEnter = 0;
	if (joy&Setting.JoyPad.JoyPadKey[5][0] || joy&Setting.JoyPad.JoyPadKey[5][1] || buf[Setting.KeyConfig.KeyBack] == 1) keyEsc++;
	else keyEsc = 0;
	if (buf[Setting.KeyConfig.KeyL] == 1 || buf[Setting.KeyConfig.KeyR] == 1 ||
		joy&Setting.JoyPad.JoyPadKey[6][0] || joy&Setting.JoyPad.JoyPadKey[6][1] ||
		joy&Setting.JoyPad.JoyPadKey[7][0] || joy&Setting.JoyPad.JoyPadKey[7][1]) keyShift++;
	else keyShift = 0;
	if (buf[Setting.KeyConfig.Key1] == 1 || joy&Setting.JoyPad.JoyPad[0][0]) m_Key[0][0]++;
	else m_Key[0][0] = 0;
	if (buf[Setting.KeyConfig.Key2] == 1 || joy&Setting.JoyPad.JoyPad[0][1]) m_Key[1][0]++;
	else m_Key[1][0] = 0;
	if (buf[Setting.KeyConfig.Key3] == 1 || joy&Setting.JoyPad.JoyPad[0][2]) m_Key[2][0]++;
	else m_Key[2][0] = 0;
	if (buf[Setting.KeyConfig.Key4] == 1 || joy&Setting.JoyPad.JoyPad[0][3]) m_Key[3][0]++;
	else m_Key[3][0] = 0;
	if (buf[Setting.KeyConfig.Key5] == 1 || joy&Setting.JoyPad.JoyPad[1][0]) m_Key[0][1]++;
	else m_Key[0][1] = 0;
	if (buf[Setting.KeyConfig.Key6] == 1 || joy&Setting.JoyPad.JoyPad[1][1]) m_Key[1][1]++;
	else m_Key[1][1] = 0;
	if (buf[Setting.KeyConfig.Key7] == 1 || joy&Setting.JoyPad.JoyPad[1][2]) m_Key[2][1]++;
	else m_Key[2][1] = 0;
	if (buf[Setting.KeyConfig.Key8] == 1 || joy&Setting.JoyPad.JoyPad[1][3]) m_Key[3][1]++;
	else m_Key[3][1] = 0;
	if (buf[Setting.KeyConfig.Key9] == 1 || joy&Setting.JoyPad.JoyPad[2][0]) m_Key[0][2]++;
	else m_Key[0][2] = 0;
	if (buf[Setting.KeyConfig.Key10] == 1 || joy&Setting.JoyPad.JoyPad[2][1])m_Key[1][2]++;
	else m_Key[1][2] = 0;
	if (buf[Setting.KeyConfig.Key11] == 1 || joy&Setting.JoyPad.JoyPad[2][2]) m_Key[2][2]++;
	else m_Key[2][2] = 0;
	if (buf[Setting.KeyConfig.Key12] == 1 || joy&Setting.JoyPad.JoyPad[2][3]) m_Key[3][2]++;
	else m_Key[3][2] = 0;
	if (buf[Setting.KeyConfig.Key13] == 1 || joy&Setting.JoyPad.JoyPad[3][0]) m_Key[0][3]++;
	else m_Key[0][3] = 0;
	if (buf[Setting.KeyConfig.Key14] == 1 || joy&Setting.JoyPad.JoyPad[3][1]) m_Key[1][3]++;
	else m_Key[1][3] = 0;
	if (buf[Setting.KeyConfig.Key15] == 1 || joy&Setting.JoyPad.JoyPad[3][2]) m_Key[2][3]++;
	else m_Key[2][3] = 0;
	if (buf[Setting.KeyConfig.Key16] == 1 || joy&Setting.JoyPad.JoyPad[3][3]) m_Key[3][3]++;
	else m_Key[3][3] = 0;
	if (buf[KEY_INPUT_F7] == 1) F7++;
	else F7 = 0;
	if (buf[KEY_INPUT_F8] == 1) F8++;
	else F8 = 0;
	if (buf[KEY_INPUT_F6] == 1) keyPrtsc++;
	else keyPrtsc = 0;
	//2^0 〜 2^3 は十字キー、2^4 〜はゲームパッドの 1 ボタン、２ボタン……となります。32 ビットなので 2^31 が最大。
	//現在、押されている状態が何フレーム目かを記録する方式。
	//ボタンを離すと0になり、ボタンを押すと押しているフレーム分加算される

	/*
	clsDx();
	printfDx("key_00 = %d\n", key[0][0]);
	printfDx("key_10 = %d\n", key[1][0]);
	printfDx("key_20 = %d\n", key[2][0]);
	printfDx("key_30 = %d\n", key[3][0]);
	printfDx("key_01 = %d\n", key[0][1]);
	printfDx("key_11 = %d\n", key[1][1]);
	printfDx("key_21 = %d\n", key[2][1]);
	printfDx("key_31 = %d\n", key[3][1]);
	printfDx("key_02 = %d\n", key[0][2]);
	printfDx("key_12 = %d\n", key[1][2]);
	printfDx("key_22 = %d\n", key[2][2]);
	printfDx("key_32 = %d\n", key[3][2]);
	printfDx("key_03 = %d\n", key[0][3]);
	printfDx("key_13 = %d\n", key[1][3]);
	printfDx("key_23 = %d\n", key[2][3]);
	printfDx("key_33 = %d\n", key[3][3]);
	printfDx("keyUp = %d\n", keyUp);
	printfDx("keyDown = %d\n", keyDown);
	printfDx("keyRight = %d\n", keyRight);
	printfDx("keyLeft = %d\n", keyLeft);
	printfDx("keyEsc = %d\n", keyEsc);
	printfDx("keyEnter = %d\n", keyEnter);
	printfDx("keyShift = %d\n", keyShift);
	printfDx("F7 = %d\n", F7);
	printfDx("F8 = %d\n", F8);
	*/
}
void Input::Init()
{
	keyUp = 0;
	keyDown = 0;
	keyRight = 0;
	keyLeft = 0;
	keyEsc = 0;
	keyEnter = 0;
	keyShift = 0;
	F7 = 0;
	F8 = 0;
	keyPrtsc = 0;
	casolPic = LoadGraph(_T("img/cursor.png"));
	mouseClick = 0;
	mouseWheel = 0;
}
void Input::GetMouse()
{
	//左クリック
	const auto mouseInput = GetMouseInput();
	if ((mouseInput & MOUSE_INPUT_LEFT) == 1)
		Input::GetInstance().mouseClick++;
	else
		Input::GetInstance().mouseClick = 0;
	//マウス座標
	GetMousePoint(&mouseX, &mouseY);
	//ホイール
	mouseWheel = GetMouseWheelRotVol();
}