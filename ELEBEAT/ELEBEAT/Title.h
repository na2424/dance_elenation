#ifndef TITLE_H
#define TITLE_H
#include <array>
#include "DxLib.h"
#include "SceneManager.h"



class Rect;
class MouseClickObjectList;
class BlueLightBallList;

//三角形のポリゴン２つで四角形を描画する為の値。
class VtPm
{
public:
	float x, y;
	float u, v;
};

//一つのテクスチャーについての構造体
class ObChild_t
{
public:
	float x, y, z;//中心点
	VERTEX_3D Vertex[6];        //描画用頂点6個
};

//沢山のテクスチャーが集まった一つの情報。
//例えば左にダーっと連続で表示される壁は沢山ののObChild_tをここで管理している。
//Obchild_tの集合
class Object_t
{
public:
	int m_Type;    // 0:画面に平行、1:画面に垂直
	int Img;    //画像
	int ImgSize;
	int ImgX1, ImgX2, ImgY1, ImgY2;
	float LargeX, LargeY;//縦横の大きさ(Typeが1の時はLargeXがLargeZの役割をする)
	float Zhaba;
	float FromZ, ToZ;    //どこからどこまで奥行きを設定するか
	float FadeFromZ, FadeToZ;    //どこからどこまでフェードを設定するか(消える瞬間フェードアウト、現れる瞬間フェードインする)
	int ObchindMax;
	ObChild_t ObChild[OBCHILD_MAX];
};

//タイトル画面用クラス
class Title : public Scene
{
private:
	//----------------------
	// 定数.
	//----------------------
	static const int LINE_SPACE_PIXEL = 50;
	static const int LOOP_COUNT = 600;
	static const int PLACE_X = 380;
	static const int PLACE_Y = 545;
	static const int BALL_MAX = 24;

	const VtPm m_vtPm[6] = { { -1, 1, 0, 0 },{ 1, 1, 1, 0 },{ -1, -1, 0, 1 },{ 1, -1, 1, 1 },{ -1, -1, 0, 1 },{ 1, 1, 1, 0 } };

	std::shared_ptr<Rect> m_mouseColliderRect;
	std::shared_ptr<MouseClickObjectList> m_mouseClickObjectList;
	std::shared_ptr<BlueLightBallList> m_lightballList;

	int textImagePosY;

	//カーソルの画像のハンドル
	int cursorHigh;
	std::array<int, 4> co;

	//----------------------
	// 画像.
	//----------------------
	//Image
	std::array<Image, 2> titleImage;
	std::array<Image, 3> menuTextImage;
	Image mainObjImage;
	Image hexaImage;
	Image frameImage;
	Image lightImage;
	Image txtImage;
	Image lineImage;
	Image casolPicImage;

	//Sprite
	int lightballSprite;

	//---------------------
	// 音源.
	//---------------------
	Sound loopBgm;
	Sound moveSound;

	//---------------------
	// 関数.
	//---------------------
	void LoadTitleSound();
	void LoadTitleImage();

	void DrawBack(int textImagePosY);
	void DrawTitle(int count, int blendValue[]);
	void DrawCursor(int casol_high, int blendValue[]);
	int GetCountLoop(int count);
	void IniObj(Object_t *Ob, int ImgHandle, int ImgSize, int ImgX1, int ImgY1, int ImgX2, int ImgY2, float LargeX, float LargeY, int Type, float FromZ, float FadeFromZ, float FadeToZ, float ToZ, float GraphX, float GraphY, int ObchildMax);
	void Ini();
	void ClacObject();
	void SortObject();
	static void SwapObChild(ObChild_t *Ob1, ObChild_t *Ob2);
	static void Efect05();

public:

	int m_ObjectNum;
	Object_t m_Object[OBJECT_NUM_MAX];

	//関数
	Title();
	~Title();

	void Initialize(const std::shared_ptr<Music> &musicP) override;
	void Update(const std::shared_ptr<Music>& musicP) override;
	void Finalize(const std::shared_ptr<Music> &musicP) override;
};

#endif