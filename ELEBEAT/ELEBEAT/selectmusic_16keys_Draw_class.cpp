#include "Color.h"
#include "define.h"
#include "extern.h"
#include "SelectMusic16keys.h"

//画像描画
void SelectMusic16keys::DrawGraphAll_16(int nowcenter)
{
	//フォルダなら
	if (selectMode == SelectMode::FOLDER)
	{
		DrawModiGraph(x_16[2], LEFTY, x_16[2] + AASIZE, LEFTY,
			x_16[2] + AASIZE, LEFTY + AASIZE, x_16[2], LEFTY + AASIZE, g_KindFolder[nowcenter - 3], TRUE);
		DrawModiGraph(x_16[2], LEFTY + AABETWEEN + AASIZE, x_16[2] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[2] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[2], LEFTY + (AASIZE * 2) + AABETWEEN, g_KindFolder[nowcenter - 2], TRUE);
		DrawModiGraph(x_16[2], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[2] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[2] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[2], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_KindFolder[nowcenter - 1], TRUE);

		DrawModiGraph(x_16[3], LEFTY, x_16[3] + AASIZE, LEFTY,
			x_16[3] + AASIZE, LEFTY + AASIZE, x_16[3], LEFTY + AASIZE, g_KindFolder[nowcenter], TRUE);
		DrawModiGraph(x_16[3], LEFTY + AABETWEEN + AASIZE, x_16[3] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[3] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[3], LEFTY + (AASIZE * 2) + AABETWEEN, g_KindFolder[nowcenter + 1], TRUE);
		DrawModiGraph(x_16[3], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[3] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[3] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[3], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_KindFolder[nowcenter + 2], TRUE);

		DrawModiGraph(x_16[4], LEFTY, x_16[4] + AASIZE, LEFTY,
			x_16[4] + AASIZE, LEFTY + AASIZE, x_16[4], LEFTY + AASIZE, g_KindFolder[nowcenter + 3], TRUE);
		DrawModiGraph(x_16[4], LEFTY + AABETWEEN + AASIZE, x_16[4] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[4] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[4], LEFTY + (AASIZE * 2) + AABETWEEN, g_KindFolder[nowcenter + 4], TRUE);
		DrawModiGraph(x_16[4], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[4] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[4] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[4], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_KindFolder[nowcenter + 5], TRUE);

		DrawModiGraph(x_16[5], LEFTY, x_16[5] + AASIZE, LEFTY,
			x_16[5] + AASIZE, LEFTY + AASIZE, x_16[5], LEFTY + AASIZE, g_KindFolder[nowcenter + 6], TRUE);
		DrawModiGraph(x_16[5], LEFTY + AABETWEEN + AASIZE, x_16[5] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[5] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[5], LEFTY + (AASIZE * 2) + AABETWEEN, g_KindFolder[nowcenter + 7], TRUE);
		DrawModiGraph(x_16[5], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[5] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[5] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[5], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_KindFolder[nowcenter + 8], TRUE);

		DrawModiGraph(x_16[6], LEFTY, x_16[6] + AASIZE, LEFTY,
			x_16[6] + AASIZE, LEFTY + AASIZE, x_16[6], LEFTY + AASIZE, g_KindFolder[nowcenter + 9], TRUE);
		DrawModiGraph(x_16[6], LEFTY + AABETWEEN + AASIZE, x_16[6] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[6] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[6], LEFTY + (AASIZE * 2) + AABETWEEN, g_KindFolder[nowcenter + 10], TRUE);
		DrawModiGraph(x_16[6], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[6] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[6] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[6], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_KindFolder[nowcenter + 11], TRUE);

		DrawModiGraph(x_16[7], LEFTY, x_16[7] + AASIZE, LEFTY,
			x_16[7] + AASIZE, LEFTY + AASIZE, x_16[7], LEFTY + AASIZE, g_KindFolder[nowcenter + 12], TRUE);
		DrawModiGraph(x_16[7], LEFTY + AABETWEEN + AASIZE, x_16[7] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[7] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[7], LEFTY + (AASIZE * 2) + AABETWEEN, g_KindFolder[nowcenter + 13], TRUE);
		DrawModiGraph(x_16[7], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[7] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[7] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[7], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_KindFolder[nowcenter + 14], TRUE);
	}
	else if (selectMode == SelectMode::AA) //AAなら
	{
		DrawModiGraph(x_16[2], LEFTY, x_16[2] + AASIZE, LEFTY,
			x_16[2] + AASIZE, LEFTY + AASIZE, x_16[2], LEFTY + AASIZE, g_AA[nowcenter - 3], TRUE);
		DrawModiGraph(x_16[2], LEFTY + AABETWEEN + AASIZE, x_16[2] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[2] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[2], LEFTY + (AASIZE * 2) + AABETWEEN, g_AA[nowcenter - 2], TRUE);
		DrawModiGraph(x_16[2], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[2] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[2] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[2], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_AA[nowcenter - 1], TRUE);

		DrawModiGraph(x_16[3], LEFTY, x_16[3] + AASIZE, LEFTY,
			x_16[3] + AASIZE, LEFTY + AASIZE, x_16[3], LEFTY + AASIZE, g_AA[nowcenter], TRUE);
		DrawModiGraph(x_16[3], LEFTY + AABETWEEN + AASIZE, x_16[3] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[3] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[3], LEFTY + (AASIZE * 2) + AABETWEEN, g_AA[nowcenter + 1], TRUE);
		DrawModiGraph(x_16[3], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[3] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[3] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[3], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_AA[nowcenter + 2], TRUE);

		DrawModiGraph(x_16[4], LEFTY, x_16[4] + AASIZE, LEFTY,
			x_16[4] + AASIZE, LEFTY + AASIZE, x_16[4], LEFTY + AASIZE, g_AA[nowcenter + 3], TRUE);
		DrawModiGraph(x_16[4], LEFTY + AABETWEEN + AASIZE, x_16[4] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[4] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[4], LEFTY + (AASIZE * 2) + AABETWEEN, g_AA[nowcenter + 4], TRUE);
		DrawModiGraph(x_16[4], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[4] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[4] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[4], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_AA[nowcenter + 5], TRUE);

		DrawModiGraph(x_16[5], LEFTY, x_16[5] + AASIZE, LEFTY,
			x_16[5] + AASIZE, LEFTY + AASIZE, x_16[5], LEFTY + AASIZE, g_AA[nowcenter + 6], TRUE);
		DrawModiGraph(x_16[5], LEFTY + AABETWEEN + AASIZE, x_16[5] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[5] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[5], LEFTY + (AASIZE * 2) + AABETWEEN, g_AA[nowcenter + 7], TRUE);
		DrawModiGraph(x_16[5], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[5] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[5] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[5], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_AA[nowcenter + 8], TRUE);

		DrawModiGraph(x_16[6], LEFTY, x_16[6] + AASIZE, LEFTY,
			x_16[6] + AASIZE, LEFTY + AASIZE, x_16[6], LEFTY + AASIZE, g_AA[nowcenter + 9], TRUE);
		DrawModiGraph(x_16[6], LEFTY + AABETWEEN + AASIZE, x_16[6] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[6] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[6], LEFTY + (AASIZE * 2) + AABETWEEN, g_AA[nowcenter + 10], TRUE);
		DrawModiGraph(x_16[6], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[6] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[6] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[6], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_AA[nowcenter + 11], TRUE);

		DrawModiGraph(x_16[7], LEFTY, x_16[7] + AASIZE, LEFTY,
			x_16[7] + AASIZE, LEFTY + AASIZE, x_16[7], LEFTY + AASIZE, g_AA[nowcenter + 12], TRUE);
		DrawModiGraph(x_16[7], LEFTY + AABETWEEN + AASIZE, x_16[7] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[7] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[7], LEFTY + (AASIZE * 2) + AABETWEEN, g_AA[nowcenter + 13], TRUE);
		DrawModiGraph(x_16[7], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[7] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[7] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[7], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_AA[nowcenter + 14], TRUE);
	}
	else //markerなら
	{
		DrawModiGraph(x_16[2], LEFTY, x_16[2] + AASIZE, LEFTY,
			x_16[2] + AASIZE, LEFTY + AASIZE, x_16[2], LEFTY + AASIZE, g_MarkerGraph16[nowcenter - 3], TRUE);
		DrawModiGraph(x_16[2], LEFTY + AABETWEEN + AASIZE, x_16[2] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[2] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[2], LEFTY + (AASIZE * 2) + AABETWEEN, g_MarkerGraph16[nowcenter - 2], TRUE);
		DrawModiGraph(x_16[2], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[2] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[2] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[2], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_MarkerGraph16[nowcenter - 1], TRUE);

		DrawModiGraph(x_16[3], LEFTY, x_16[3] + AASIZE, LEFTY,
			x_16[3] + AASIZE, LEFTY + AASIZE, x_16[3], LEFTY + AASIZE, g_MarkerGraph16[nowcenter], TRUE);
		DrawModiGraph(x_16[3], LEFTY + AABETWEEN + AASIZE, x_16[3] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[3] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[3], LEFTY + (AASIZE * 2) + AABETWEEN, g_MarkerGraph16[nowcenter + 1], TRUE);
		DrawModiGraph(x_16[3], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[3] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[3] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[3], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_MarkerGraph16[nowcenter + 2], TRUE);

		DrawModiGraph(x_16[4], LEFTY, x_16[4] + AASIZE, LEFTY,
			x_16[4] + AASIZE, LEFTY + AASIZE, x_16[4], LEFTY + AASIZE, g_MarkerGraph16[nowcenter + 3], TRUE);
		DrawModiGraph(x_16[4], LEFTY + AABETWEEN + AASIZE, x_16[4] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[4] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[4], LEFTY + (AASIZE * 2) + AABETWEEN, g_MarkerGraph16[nowcenter + 4], TRUE);
		DrawModiGraph(x_16[4], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[4] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[4] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[4], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_MarkerGraph16[nowcenter + 5], TRUE);

		DrawModiGraph(x_16[5], LEFTY, x_16[5] + AASIZE, LEFTY,
			x_16[5] + AASIZE, LEFTY + AASIZE, x_16[5], LEFTY + AASIZE, g_MarkerGraph16[nowcenter + 6], TRUE);
		DrawModiGraph(x_16[5], LEFTY + AABETWEEN + AASIZE, x_16[5] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[5] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[5], LEFTY + (AASIZE * 2) + AABETWEEN, g_MarkerGraph16[nowcenter + 7], TRUE);
		DrawModiGraph(x_16[5], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[5] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[5] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[5], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_MarkerGraph16[nowcenter + 8], TRUE);

		DrawModiGraph(x_16[6], LEFTY, x_16[6] + AASIZE, LEFTY,
			x_16[6] + AASIZE, LEFTY + AASIZE, x_16[6], LEFTY + AASIZE, g_MarkerGraph16[nowcenter + 9], TRUE);
		DrawModiGraph(x_16[6], LEFTY + AABETWEEN + AASIZE, x_16[6] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[6] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[6], LEFTY + (AASIZE * 2) + AABETWEEN, g_MarkerGraph16[nowcenter + 10], TRUE);
		DrawModiGraph(x_16[6], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[6] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[6] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[6], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_MarkerGraph16[nowcenter + 11], TRUE);

		DrawModiGraph(x_16[7], LEFTY, x_16[7] + AASIZE, LEFTY,
			x_16[7] + AASIZE, LEFTY + AASIZE, x_16[7], LEFTY + AASIZE, g_MarkerGraph16[nowcenter + 12], TRUE);
		DrawModiGraph(x_16[7], LEFTY + AABETWEEN + AASIZE, x_16[7] + AASIZE, LEFTY + AABETWEEN + AASIZE,
			x_16[7] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[7], LEFTY + (AASIZE * 2) + AABETWEEN, g_MarkerGraph16[nowcenter + 13], TRUE);
		DrawModiGraph(x_16[7], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[7] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
			x_16[7] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[7], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), g_MarkerGraph16[nowcenter + 14], TRUE);

		MarkerMove();
	}

	DrawBox(795, 0, WIN_WIDTH, WIN_HEIGHT, Color::GetInstance().Black(), TRUE);
}

//音楽詳細（曲名）とかを書く スクロールもする
void SelectMusic16keys::DrawMusicDetail(int nowselectAAnum, bool clear, const std::shared_ptr<Music> &musicP)
{
	//座標変更用
	int moveX[3] = {};
	//一周したとき一秒カウント用
	int stoptime[3] = {};
	//文字列スクロールの際、前のスクロール文字に使う
	TCHAR drawstring[3][260] = {};

	//クリアフラグ(AAを再選択の際移動してる座標などを0に戻す)
	if (clear == 1)
	{
		moveX[0] = moveX[1] = moveX[2] = 0;
		stoptime[0] = stoptime[1] = stoptime[2] = 0;
		goto clearflag;
	}

	//スクロール処理 ここは計算のみ
	for (int i = 0; i <= 2; i++)
	{
		if (movedrawflag[i] == 1)
		{
			if (moveX[i] == -strlength[i] - 10)
			{
				moveX[i] = 0;
			}
			if (moveX[i] == 0)
			{
				stoptime[i]++;
				if (stoptime[i] >= 300)
				{
					moveX[i]--;
					stoptime[i] = 0;
				}
			}
			else
			{
				moveX[i]--;
			}
		}
	}

	//文字列描画
	//スクロールの際はみ出て文字描画しないように
	SetDrawArea(795, 0, WIN_WIDTH, WIN_HEIGHT);
	//強制的にマイナス部分は描画させない
	if (nowselectAAnum >= 0)
	{
		auto& color = Color::GetInstance();
		if (movedrawflag[0] == 1)
		{
			//曲名
			DrawFormatString(800 + moveX[0], 110, color.White(),
				(_T("%s")), musicP->elebeatSong->m_Name[nowselectAAnum + nowAA_16[0]].c_str());
			//中心
			DrawFormatString(800 + moveX[0] + 10 + strlength[0], 110, color.White(),
				(_T("%s")), musicP->elebeatSong->m_Name[nowselectAAnum + nowAA_16[0]].c_str());
		}
		else
		{
			//スクロールしない
			DrawFormatString(800, 110, color.White(),
				(_T("%s")), musicP->elebeatSong->m_Name[nowselectAAnum + nowAA_16[0]].c_str());
		}

		if (movedrawflag[1] == 1)
		{
			//サブ曲名
			DrawFormatString(800 + moveX[1], 170, color.White(),
				(_T("%s")), musicP->elebeatSong->m_SubName[nowselectAAnum + nowAA_16[0]].c_str());
			DrawFormatString(800 + moveX[1] + 10 + strlength[1], 170, color.White(),
				_T("%s"), musicP->elebeatSong->m_SubName[nowselectAAnum + nowAA_16[0]].c_str());
		}
		else
		{
			//スクロールしない
			DrawFormatString(800, 170, color.White(),
				_T("%s"), musicP->elebeatSong->m_SubName[nowselectAAnum + nowAA_16[0]].c_str());
		}

		if (movedrawflag[2] == 1)
		{
			//作曲者
			DrawFormatString(800 + moveX[2], 230, color.White(),
				_T("%s"), musicP->elebeatSong->m_Artist[nowselectAAnum + nowAA_16[0]].c_str());
			DrawFormatString(800 + moveX[2] + 10 + strlength[2], 230, color.White(),
				_T("%s"), musicP->elebeatSong->m_Artist[nowselectAAnum + nowAA_16[0]].c_str());
		}
		else
		{
			//スクロールしない
			DrawFormatString(800, 230, color.White(),
				_T("%s"), musicP->elebeatSong->m_Artist[nowselectAAnum + nowAA_16[0]].c_str());
		}
	}
	//描画可能領域を再設定
	SetDrawArea(0, 0, WIN_WIDTH, WIN_HEIGHT);
clearflag:;
}
//文字列長さ
void SelectMusic16keys::ReCalculateStringLength(int nowselectAAnum, const std::shared_ptr<Music> &musicP)
{
	strlength[0] = GetDrawStringWidth(musicP->elebeatSong->m_Name[nowselectAAnum + nowAA_16[0]].c_str(), static_cast<int>(_tcslen(musicP->elebeatSong->m_Name[nowselectAAnum + nowAA_16[0]].c_str())));
	strlength[1] = GetDrawStringWidth(musicP->elebeatSong->m_SubName[nowselectAAnum + nowAA_16[0]].c_str(), static_cast<int>(_tcslen(musicP->elebeatSong->m_SubName[nowselectAAnum + nowAA_16[0]].c_str())));
	strlength[2] = GetDrawStringWidth(musicP->elebeatSong->m_Artist[nowselectAAnum + nowAA_16[0]].c_str(), static_cast<int>(_tcslen(musicP->elebeatSong->m_Artist[nowselectAAnum + nowAA_16[0]].c_str())));

	for (int i = 0; i <= 2; i++)
	{
		if (strlength[i] > 165)
		{
			movedrawflag[i] = 1;
		}
		else
		{
			movedrawflag[i] = 0;
			strlength[i] = 0;
		}
	}
}
//その他文字列描画
void SelectMusic16keys::DrawOtherDetail(int nowselectAAnum, const std::shared_ptr<Music> &musicP)
{
	if (nowselectAAnum == -1000)
		return;

	DrawRotaGraph(920, 700, 1, 0, difficultIconImage[difselect], TRUE);
	DrawDifficulty(musicP->elebeatSong->m_ElebeatDifficulty[1][difselect][nowselectAAnum + nowAA_16[0]]);
}
//リテラル文章を書く
void SelectMusic16keys::DrawLiteralString(void) const
{
	int rgb[3] = {};
	//緑
	rgb[0] = GetColor(0, 255, 0);
	//黄色
	rgb[1] = GetColor(255, 255, 0);
	//赤
	rgb[2] = GetColor(255, 20, 0);

	DrawRotaGraph(870, 112, 1, 0, selectMusicImage, TRUE);

	//難易度文字
	if (difselect == 0)
		DrawRotaGraph(873, 555, 1, 0, basicImage, TRUE);
	else if (difselect == 1)
		DrawRotaGraph(873, 555, 1, 0, normalImage, TRUE);
	else
		DrawRotaGraph(873, 555, 1, 0, advancedImage, TRUE);
}

//背景斜め
void SelectMusic16keys::DrawMusicScoreChar(void) const
{
}

//選択パネル描画
void SelectMusic16keys::DrawPanelAll(void)
{
	//左右パネルとか
	DrawModiGraph(0, LEFTY + (AABETWEEN * 3) + (AASIZE * 3), AASIZE, LEFTY + (AABETWEEN * 3) + (AASIZE * 3)    //左矢印
		, AASIZE, LEFTY + (AABETWEEN * 3) + (AASIZE * 4), 0, LEFTY + (AABETWEEN * 3) + (AASIZE * 4), leftImage, TRUE);
	DrawModiGraph(200, LEFTY + (AABETWEEN * 3) + (AASIZE * 3), 200 + AASIZE, LEFTY + (AABETWEEN * 3) + (AASIZE * 3)    //右矢印
		, 200 + AASIZE, LEFTY + (AABETWEEN * 3) + (AASIZE * 4), 200, LEFTY + (AABETWEEN * 3) + (AASIZE * 4), rightImage, TRUE);
	if (selectMode == SelectMode::FOLDER)
	{
		//folder
		DrawModiGraph(400, LEFTY + (AABETWEEN * 3) + (AASIZE * 3), 400 + AASIZE, LEFTY + (AABETWEEN * 3) + (AASIZE * 3)    //マーカーセレクト
			, 400 + AASIZE, LEFTY + (AABETWEEN * 3) + (AASIZE * 4), 400, LEFTY + (AABETWEEN * 3) + (AASIZE * 4), markerImage, TRUE);
		DrawModiGraph(600, LEFTY + (AABETWEEN * 3) + (AASIZE * 3), 600 + AASIZE, LEFTY + (AABETWEEN * 3) + (AASIZE * 3)    //戻る
			, 600 + AASIZE, LEFTY + (AABETWEEN * 3) + (AASIZE * 4), 600, LEFTY + (AABETWEEN * 3) + (AASIZE * 4), backImage, TRUE);
	}
	else if (selectMode == SelectMode::AA)
	{
		//AA
		DrawModiGraph(400, LEFTY + (AABETWEEN * 3) + (AASIZE * 3), 400 + AASIZE, LEFTY + (AABETWEEN * 3) + (AASIZE * 3)    //戻る
			, 400 + AASIZE, LEFTY + (AABETWEEN * 3) + (AASIZE * 4), 400, LEFTY + (AABETWEEN * 3) + (AASIZE * 4), backImage, TRUE);
		DrawModiGraph(600, LEFTY + (AABETWEEN * 3) + (AASIZE * 3), 600 + AASIZE, LEFTY + (AABETWEEN * 3) + (AASIZE * 3)    //ゲームに
			, 600 + AASIZE, LEFTY + (AABETWEEN * 3) + (AASIZE * 4), 600, LEFTY + (AABETWEEN * 3) + (AASIZE * 4), enterImage, TRUE);
	}
	else
	{
		//marker
		DrawModiGraph(600, LEFTY + (AABETWEEN * 3) + (AASIZE * 3), 600 + AASIZE, LEFTY + (AABETWEEN * 3) + (AASIZE * 3)    //戻る
			, 600 + AASIZE, LEFTY + (AABETWEEN * 3) + (AASIZE * 4), 600, LEFTY + (AABETWEEN * 3) + (AASIZE * 4), backImage, TRUE);
	}
}

//ハイスコア計算
void SelectMusic16keys::DrawHiScore_16(int nowselectAAnum, const std::shared_ptr<Music> &musicP)
{
	//fopen_sで使用
	FILE *fp1;
	errno_t error;

	//構造体宣言
	ScoreSaveData save_data;

	//その他の変数
	TCHAR name[256] = {};

	//構造体初期化
	for (int m = 0; m < 2; m++)
	{
		for (int n = 0; n < 3; n++)
		{
			save_data.m_IsFullCombo[m][n] = false;
			save_data.m_Rank[m][n] = 10;
			save_data.m_HighScore[m][n] = 0;
		}
	}
	if (nowselectAAnum >= 0)
	{
		//曲名からオープンdatを決める
		_stprintf_s(name, sizeof(name), _T("Data\\%s.dat"), musicP->elebeatSong->m_Name[nowselectAAnum].c_str());
	}
	//ファイルの内容からデータをロード
	if ((error = _tfopen_s(&fp1, name, _T("rb"))) != 0)
	{
	}
	//ファイルがあればデータを読み込む
	else
	{
		fread(&save_data, sizeof(ScoreSaveData), 1, fp1);
		fclose(fp1);
	}

	auto playModeNumber = static_cast<int>(SceneManager::g_PlayMode);
	for(auto i=0; i<3; i++)
	{
		if (save_data.m_HighScore[playModeNumber][i] != 0)
			DrawFormatString(800, 24, 0xffffff, _T("%d"), save_data.m_HighScore[playModeNumber][i]);
	}
}

//マーカーを動かす
void SelectMusic16keys::MarkerMove(void)
{
	Color& color = Color::GetInstance();

	//計算
	int betweenTime = startTime % 1000;
	markernum = nowselectmarker;

	//場所決め
	for (int j = 0; j < marker_numb[nowselectmarker]; j++)
	{
		if (marker_time[nowselectmarker] * j <= betweenTime && betweenTime < marker_time[nowselectmarker] * (j + 1))
		{
			switch (nowselectmarker)
			{
			case 0:
				DrawBox(x_16[3], LEFTY, x_16[3] + AASIZE, LEFTY + AASIZE, color.Black(), TRUE);
				DrawModiGraph(x_16[3], LEFTY, x_16[3] + AASIZE, LEFTY,
					x_16[3] + AASIZE, LEFTY + AASIZE, x_16[3], LEFTY + AASIZE, noteMarkerImage[nowselectmarker][j], TRUE);
				break;
			case 1:
				DrawBox(x_16[3], LEFTY + AABETWEEN + AASIZE, x_16[3] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, color.Black(), TRUE);
				DrawModiGraph(x_16[3], LEFTY + AABETWEEN + AASIZE, x_16[3] + AASIZE, LEFTY + AABETWEEN + AASIZE,
					x_16[3] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[3], LEFTY + (AASIZE * 2) + AABETWEEN, noteMarkerImage[nowselectmarker][j], TRUE);
				break;
			case 2:
				DrawBox(x_16[3], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[3] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), color.Black(), TRUE);
				DrawModiGraph(x_16[3], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[3] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
					x_16[3] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[3], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), noteMarkerImage[nowselectmarker][j], TRUE);
				break;
			case 3:
				DrawBox(x_16[4], LEFTY, x_16[4] + AASIZE, LEFTY + AASIZE, color.Black(), TRUE);
				DrawModiGraph(x_16[4], LEFTY, x_16[4] + AASIZE, LEFTY,
					x_16[4] + AASIZE, LEFTY + AASIZE, x_16[4], LEFTY + AASIZE, noteMarkerImage[nowselectmarker][j], TRUE);
				break;
			case 4:
				DrawBox(x_16[4], LEFTY + AABETWEEN + AASIZE, x_16[4] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, color.Black(), TRUE);
				DrawModiGraph(x_16[4], LEFTY + AABETWEEN + AASIZE, x_16[4] + AASIZE, LEFTY + AABETWEEN + AASIZE,
					x_16[4] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[4], LEFTY + (AASIZE * 2) + AABETWEEN, noteMarkerImage[nowselectmarker][j], TRUE);
				break;
			case 5:
				DrawBox(x_16[4], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[4] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), color.Black(), TRUE);
				DrawModiGraph(x_16[4], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[4] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
					x_16[4] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[4], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), noteMarkerImage[nowselectmarker][j], TRUE);
				break;
			case 6:
				DrawBox(x_16[5], LEFTY, x_16[5] + AASIZE, LEFTY + AASIZE, color.Black(), TRUE);
				DrawModiGraph(x_16[5], LEFTY, x_16[5] + AASIZE, LEFTY,
					x_16[5] + AASIZE, LEFTY + AASIZE, x_16[5], LEFTY + AASIZE, noteMarkerImage[nowselectmarker][j], TRUE);
				break;
			case 7:
				DrawBox(x_16[5], LEFTY + AABETWEEN + AASIZE, x_16[5] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, color.Black(), TRUE);
				DrawModiGraph(x_16[5], LEFTY + AABETWEEN + AASIZE, x_16[5] + AASIZE, LEFTY + AABETWEEN + AASIZE,
					x_16[5] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[5], LEFTY + (AASIZE * 2) + AABETWEEN, noteMarkerImage[nowselectmarker][j], TRUE);
				break;
			case 8:
				DrawBox(x_16[5], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[5] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), color.Black(), TRUE);
				DrawModiGraph(x_16[5], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[5] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
					x_16[5] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[5], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), noteMarkerImage[nowselectmarker][j], TRUE);
				break;
			case 9:
				DrawBox(x_16[6], LEFTY, x_16[6] + AASIZE, LEFTY + AASIZE, color.Black(), TRUE);
				DrawModiGraph(x_16[6], LEFTY, x_16[6] + AASIZE, LEFTY,
					x_16[6] + AASIZE, LEFTY + AASIZE, x_16[6], LEFTY + AASIZE, noteMarkerImage[nowselectmarker][j], TRUE);
				break;
			case 10:
				DrawBox(x_16[6], LEFTY + AABETWEEN + AASIZE, x_16[6] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, color.Black(), TRUE);
				DrawModiGraph(x_16[6], LEFTY + AABETWEEN + AASIZE, x_16[6] + AASIZE, LEFTY + AABETWEEN + AASIZE,
					x_16[6] + AASIZE, LEFTY + (AASIZE * 2) + AABETWEEN, x_16[6], LEFTY + (AASIZE * 2) + AABETWEEN, noteMarkerImage[nowselectmarker][j], TRUE);
				break;
			case 11:
				DrawBox(x_16[6], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[6] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), color.Black(), TRUE);
				DrawModiGraph(x_16[6], LEFTY + (AABETWEEN * 2) + (AASIZE * 2), x_16[6] + AASIZE, LEFTY + (AABETWEEN * 2) + (AASIZE * 2),
					x_16[6] + AASIZE, LEFTY + (AASIZE * 3) + (AABETWEEN * 2), x_16[6], LEFTY + (AASIZE * 3) + (AABETWEEN * 2), noteMarkerImage[nowselectmarker][j], TRUE);
				break;
			}
			break;
		}
	}
}

//16keys難易度
void SelectMusic16keys::DrawDifficulty(int num)
{
	// numが十進数で何桁になるか調べる
	int BeamWidth = 0;
	for (int i = 1; num >= i; i *= 10) BeamWidth++;

	// 画面右上に右詰で表示
	// xは描く数字(一桁一桁各々)の左端のX座標
	int x = 875;
	for (int i = 0; i < BeamWidth; i++)
	{
		if (num >= 10)
			DrawGraph(915, 685, d16Num20x30Images[num % 10], TRUE);
		else
			DrawGraph(910, 685, d16Num20x30Images[num % 10], TRUE);

		num /= 10;
		x -= 10;
	}
}