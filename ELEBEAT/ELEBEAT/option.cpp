#include "Option.h"
#include "extern.h"
#include "KeyConfig.h"
#include "FileManager.h"
#include "Input.h"
#include "Screen.h"
#include "Color.h"
#include "LuaHelper.h"
#include "DxHelper.h"
#include "savedata_class.h"
#include "MouseClickObject.h"
#include "SelectMusicBase.h"

using namespace DxLib;

Option::Option() :
	m_optionMessageFrameImage(0),
	m_optionTitleImage(0),
	m_paddingImage(0),
	m_optionRightCasolImage(0),
	m_decideSound(0),
	m_moveSound(0),
	m_backBgm(0),
	m_isHideCasol3(false),
	m_displayCasolX(590),
	m_displayCasolY(315),
	m_fontHandle(0),//(１層目→ -1 、２層目→ 0,1,2,3,4,5)
	m_optionFlag(-1),
	m_casolHigh(0),
	m_casolHigh2(0),
	m_casolHigh3(),
	m_casolFlag()
{
	m_pFunc[0] = &Option::OnInputStageSettings;
	m_pFunc[1] = &Option::OnInputDisplaySettings;
	m_pFunc[2] = &Option::OnInputKeyConfig;
	m_pFunc[3] = &Option::OnInputReloadSongs;
	m_pFunc[4] = &Option::OnInputPlayerSettings;

	InitLua();

	m_mouseF = std::make_unique<MouseClickObject>();
	m_saveDataOption = std::make_unique<OptionSaveData>();
	m_keyConfig = std::make_unique<KeyConfig>();
}

void Option::Initialize(const std::shared_ptr<Music> &musicP)
{
	//描画可能領域セット
	SetDrawArea(0, 0, WIN_WIDTH, WIN_HEIGHT);
	SetFontSize(18);

	LoadData();

	m_keyConfig->SetFontHandle(m_fontHandle);

	//セーブデータの読み込み(無ければ書き出し)
	m_saveDataOption->LoadDat();

	//TODO:リストから取得する.
	m_listSize =
	{
		{2,5,0},
		{2,2,0},
		{0,0,0,0},
		{0},
		{0}
	};

	Screen::FadeIn();
	PlaySoundMem(m_backBgm, DX_PLAYTYPE_LOOP);
	//裏画面に設定
	SetDrawScreen(DX_SCREEN_BACK);
}

void Option::Update(const std::shared_ptr<Music>& musicP)
{
	//シングルトン参照
	Input& input = Input::GetInstance();

	m_fps->Update();
	m_count++;

	//キー入力の取得
	input.GetKey();

	//マウスを取得
	input.GetMouse();

	OptionCasol();
	OptionCasol2();

	MouseFunction(m_listSize);

	if (m_optionFlag != -1)
		OptionCasol2Detail(m_listSize[m_casolHigh][m_casolHigh2]);

	//１層目で決定キーが押されたら
	if (m_optionFlag == -1)
	{
		if (input.keyEnter == 1)
		{
			PlaySoundMem(m_decideSound, DX_PLAYTYPE_BACK, TRUE);
			input.keyEnter = 2;
			m_optionFlag = m_casolHigh;
		}
	}

	//１層目でESCキーが押されたら、あとモードがタイトルならループ脱出
	if ((input.keyEsc == 1 && m_optionFlag == -1) || SceneManager::g_CurrentScene == Scenes::TITLE)
	{
		SceneManager::SetNextScene(Scenes::TITLE);
		return;
	}

	if (m_optionFlag != -1)
		UpDateOption();

	if (m_optionFlag == 3)
		return;

	if (m_optionFlag == 4)
	{
		SceneManager::SetNextScene(Scenes::TITLE);
		return;
	}

	/*----------ここから描画関係---------------*/
	// 画面の初期化
	ClearDrawScreen();
	DxLib::DrawGraph(0, 0, g_BackImage, TRUE);
	DxLib::DrawGraph(0, 0, m_paddingImage, TRUE);

	//option画像
	DrawRotaGraph2(402, 204 - 32, 210, 40, 1, 0, m_optionTitleImage, TRUE);
	//メッセージ処理
	DrawOptionMessage();

	//カーソル描画
	DrawCasol(m_count);

	switch (m_optionFlag)
	{
	case 0:
		DrawListName(m_stageSettingList);
		break;
	case 1:
		DrawListName(m_displaySettingList);
		break;
	case 2:
		DrawListName(m_keyConfigList);
		break;
	default:
		DrawListName(m_optionNameList);
		break;
	}

	//設定項目の描画(右側)
	switch (m_optionFlag)
	{
	case 0:
		//Event
		DrawSelectOption(m_inputOffOnList, 0, static_cast<int>(OptionSaveData::g_IsEventMode));
		//Stage Num
		DrawSelectOption(m_inputStageNumList, 1, static_cast<int>(OptionSaveData::g_SongCountSetting));
		break;
	case 1:
		//Full Screen
		DrawSelectOption(m_inputOffOnList, 0, static_cast<int>(OptionSaveData::g_IsFullScreen));
		//Show Fps
		DrawSelectOption(m_inputOffOnList, 1, static_cast<int>(OptionSaveData::g_ShowFps));
		break;
	case 2:
		//KeyConfig
		break;
	case 3:
		//Reload Songs
		break;
	default:
		//Exit
		break;
	}

	//マウスカーソルの表示
	DrawRotaGraph2(input.mouseX, input.mouseY, 50, 50, 1.0f, m_count / 24.0, Input::GetInstance().casolPic, TRUE);

	m_fps->Draw();
	// 描画した画面の反映
	ScreenFlip();

	if (input.keyPrtsc == 1)
		m_ssc->CaptureScreenshot();

	m_fps->Wait();
}

void Option::Finalize(const std::shared_ptr<Music> &musicP)
{
	m_saveDataOption->SaveDat();
	DxLib::StopSoundMem(m_backBgm);
	DxLib::InitSoundMem();
	DxLib::DeleteFontToHandle(m_fontHandle);

	Screen::FadeOut();
}

//--------------------------
// オプションリスト描画
//--------------------------

/// <summary>
/// オプションリストの描画(Topと2階層で使う)
/// </summary>
void Option::DrawListName(const std::vector<tstring> &nameList) const
{
	int i = 0;
	for each (tstring s in nameList)
	{
		DxLib::DrawStringToHandle(PLACE_X + 2, PLACE_Y + 2 + LINE_HEIGHT * i, s.c_str(), Color::GetInstance().Black(), m_fontHandle);
		DxLib::DrawStringToHandle(PLACE_X, PLACE_Y + LINE_HEIGHT * i, s.c_str(), Color::GetInstance().White(), m_fontHandle);
		++i;
	}
}

/// <summary>
/// 設定項目の描画(2階層で使う)
/// </summary>
void Option::DrawSelectOption(const std::vector<tstring> &select, int num, int savedata) const
{
	int i = 0;
	for each(tstring s in select)
	{
		//黒
		DxLib::DrawStringToHandle(PLACE_X + 2 + TEXT_SPACING + SPACING * i, PLACE_Y + 2 + LINE_HEIGHT * num, s.c_str(), Color::GetInstance().Black(), m_fontHandle);
		//白
		DxLib::DrawStringToHandle(PLACE_X + TEXT_SPACING + SPACING * i, PLACE_Y + LINE_HEIGHT * num, s.c_str(), Color::GetInstance().White(), m_fontHandle);
		//赤
		if (savedata == i)
			DxLib::DrawStringToHandle(PLACE_X + TEXT_SPACING + SPACING * i, PLACE_Y + LINE_HEIGHT * num, s.c_str(), Color::GetInstance().Red(), m_fontHandle);
		++i;
	}
}

//-----------------------
// カーソル移動
//-----------------------

/// <summary>
/// １層目のカーソル処理
/// </summary>
void Option::OptionCasol()
{
	// カーソルを上に移動
	if ((Input::GetInstance().keyUp == 1 || 0 < Input::GetInstance().mouseWheel) && m_optionFlag == -1) {
		m_casolHigh--;
		PlaySoundMem(m_moveSound, DX_PLAYTYPE_BACK, TRUE);
	}
	if (10 < Input::GetInstance().keyUp && Input::GetInstance().keyUp % 5 == 0 && m_optionFlag == -1) {
		m_casolHigh--;
		PlaySoundMem(m_moveSound, DX_PLAYTYPE_BACK, TRUE);
	}
	// カーソルを下に移動
	if ((Input::GetInstance().keyDown == 1 || Input::GetInstance().mouseWheel < 0) && m_optionFlag == -1) {
		m_casolHigh++;
		PlaySoundMem(m_moveSound, DX_PLAYTYPE_BACK, TRUE);
	}
	if (10 < Input::GetInstance().keyDown && Input::GetInstance().keyDown % 5 == 0 && m_optionFlag == -1) {
		m_casolHigh++;
		PlaySoundMem(m_moveSound, DX_PLAYTYPE_BACK, TRUE);
	}
	// 下端に達したら上に移動
	if (m_casolHigh >= 5)m_casolHigh = 0;
	// 上端に達したら下に移動
	if (m_casolHigh <= -1)m_casolHigh = 4;
}

/// <summary>
/// ２層目のカーソル処理
/// </summary>
void Option::OptionCasol2()
{
	auto& input = Input::GetInstance();

	// カーソルを上に移動
	if ((input.keyUp == 1 || 0 < input.mouseWheel) && m_optionFlag != -1)
	{
		m_casolHigh2--;
		PlaySoundMem(m_moveSound, DX_PLAYTYPE_BACK, TRUE);
	}
	if (10 < input.keyUp && input.keyUp % 5 == 0 && m_optionFlag != -1)
	{
		m_casolHigh2--;
		PlaySoundMem(m_moveSound, DX_PLAYTYPE_BACK, TRUE);
	}
	// カーソルを下に移動
	if ((input.keyDown == 1 || input.mouseWheel < 0) && m_optionFlag != -1)
	{
		m_casolHigh2++;
		PlaySoundMem(m_moveSound, DX_PLAYTYPE_BACK, TRUE);
	}
	if (10 < input.keyDown && input.keyDown % 5 == 0 && m_optionFlag != -1)
	{
		m_casolHigh2++;
		PlaySoundMem(m_moveSound, DX_PLAYTYPE_BACK, TRUE);
	}

	//カーソル移動をループさせる
	if (m_optionFlag == 0)
	{
		if (m_casolHigh2 >= 3)  m_casolHigh2 = 0;
		if (m_casolHigh2 <= -1) m_casolHigh2 = 2;
	}
	if (m_optionFlag == 1)
	{
		if (m_casolHigh2 >= 3)  m_casolHigh2 = 0;
		if (m_casolHigh2 <= -1) m_casolHigh2 = 2;
	}
	if (m_optionFlag == 2)
	{
		if (m_casolHigh2 >= 4)  m_casolHigh2 = 0;
		if (m_casolHigh2 <= -1) m_casolHigh2 = 3;
	}
	if (m_optionFlag == 3) {}
	if (m_optionFlag == 4) {}
}

/// <summary>
/// 2層目の右側
/// </summary>
void Option::OptionCasol2Detail(int size /*最大数*/)
{
	if (m_optionFlag == -1 || size == 0)
	{
		m_isHideCasol3 = true;
		return;
	}

	m_isHideCasol3 = false;
	auto& input = Input::GetInstance();

	if (input.keyLeft == 1)
	{
		m_casolHigh3[m_casolHigh2]--;
		PlaySoundMem(m_moveSound, DX_PLAYTYPE_BACK, TRUE);
	}
	if (10 < input.keyLeft && input.keyLeft % 5 == 0)
	{
		m_casolHigh3[m_casolHigh2]--;
		PlaySoundMem(m_moveSound, DX_PLAYTYPE_BACK, TRUE);
	}

	if (input.keyRight == 1)
	{
		m_casolHigh3[m_casolHigh2]++;
		PlaySoundMem(m_moveSound, DX_PLAYTYPE_BACK, TRUE);
	}
	if (10 < input.keyRight && input.keyRight % 5 == 0)
	{
		m_casolHigh3[m_casolHigh2]++;
		PlaySoundMem(m_moveSound, DX_PLAYTYPE_BACK, TRUE);
	}

	//カーソル移動をループさせる
	if (m_casolHigh3[m_casolHigh2] >= size)  m_casolHigh3[m_casolHigh2] = 0;
	if (m_casolHigh3[m_casolHigh2] <= -1) m_casolHigh3[m_casolHigh2] = size - 1;
}

/// <summary>
/// カーソルを描画する
/// </summary>
void Option::DrawCasol(int count)
{
	constexpr float OFFSET_X = 447.0f;
	constexpr float OFFSET_Y = 398.0f;

	if (OptionSaveData::g_IsFullScreen == 1 /* 4:3の時 */)
	{
		if (m_optionFlag == -1)
			MV1SetPosition(ModelHandle, VGet(OFFSET_X + 30.0f, OFFSET_Y - m_casolHigh * 7, -576.0f));
		else
			MV1SetPosition(ModelHandle, VGet(OFFSET_X + 30.0f, OFFSET_Y - m_casolHigh2 * 7, -576.0f));
	}
	if (!OptionSaveData::g_IsFullScreen /* 5:4の時 */)
	{
		if (m_optionFlag == -1)
			MV1SetPosition(ModelHandle, VGet(OFFSET_X, OFFSET_Y - m_casolHigh * 7, -576.0f));
		else
			MV1SetPosition(ModelHandle, VGet(OFFSET_X, OFFSET_Y - m_casolHigh2 * 7, -576.0f));
	}
	// 立方体３ＤモデルのY軸を回転させる
	DxLib::MV1SetRotationXYZ(ModelHandle, VGet(0.0f, count * 4.0f * PI_F / 180.0f, 0.0f));

	// ３Ｄモデル(カーソル)の描画
	DxLib::MV1DrawModel(ModelHandle);

	if (m_optionFlag == -1 || m_isHideCasol3) return;
	const auto x = PLACE_X + TEXT_SPACING + SPACING * m_casolHigh3[m_casolHigh2] + 0;
	const auto y = PLACE_Y + LINE_HEIGHT * m_casolHigh2 + 31;

	DxLib::DrawGraph(x, y, m_optionRightCasolImage, TRUE);
}

//-------------------
// 入力イベント
//-------------------
//２層目
void Option::OnInputStageSettings()
{
	if (Input::GetInstance().keyEnter == 1)
	{
		switch (m_casolHigh2)
		{
		case 0:
			PlaySoundMem(m_decideSound, DX_PLAYTYPE_BACK, TRUE);
			OptionSaveData::g_IsEventMode = m_casolHigh3[m_casolHigh2];
			break;

		case 1:
			PlaySoundMem(m_decideSound, DX_PLAYTYPE_BACK, TRUE);
			OptionSaveData::g_SongCountSetting = m_casolHigh3[m_casolHigh2];
			break;

		case 2:
			BackToTopOptions();
			break;

		default:;
		}
	}
	if (Input::GetInstance().keyEsc == 1)
		BackToTopOptions();
}

void Option::OnInputDisplaySettings()
{
	if (Input::GetInstance().keyEnter == 1)
	{
		switch (m_casolHigh2)
		{
		case 0:
			if (static_cast<bool>(m_casolHigh3[0]) == OptionSaveData::g_IsFullScreen) break;
			OptionSaveData::g_IsFullScreen = m_casolHigh3[0];
			PlaySoundMem(m_decideSound, DX_PLAYTYPE_BACK, TRUE);
			ChangeScreenMode(m_casolHigh3[0] == 1);
			break;

		case 1:
			if (static_cast<bool>(m_casolHigh3[1]) == OptionSaveData::g_ShowFps) break;
			OptionSaveData::g_ShowFps = m_casolHigh3[1];
			PlaySoundMem(m_decideSound, DX_PLAYTYPE_BACK, TRUE);
			break;

		case 2:
			BackToTopOptions();
			break;

		default:;
		}
	}
	if (Input::GetInstance().keyEsc == 1)
		BackToTopOptions();
}

void Option::OnInputKeyConfig()
{
	if (Input::GetInstance().keyEnter == 1)
	{
		switch (m_casolHigh2)
		{
		case 0:
			PlaySoundMem(m_decideSound, DX_PLAYTYPE_BACK, TRUE);
			m_keyConfig->ConfigKeySetting();
			break;

		case 1:
			PlaySoundMem(m_decideSound, DX_PLAYTYPE_BACK, TRUE);
			m_keyConfig->ConfigJoyPadSetting();
			break;

		case 2:
			Setting.Init();
			m_keyConfig->ReplaceDefaultKey();

			Setting.Save(_T("Data\\Setting.ini"));
			PlaySoundMem(m_decideSound, DX_PLAYTYPE_BACK, TRUE);
			break;

		case 3:
			BackToTopOptions();
			break;

		default:;
		}
	}

	if (Input::GetInstance().keyEsc == 1)
		BackToTopOptions();
}

/// <summary>
/// 楽曲のリロードを選択.
/// </summary>
void Option::OnInputReloadSongs()
{
	FileManager::GetInstance().IsLoaded(true, false);
	SelectMusicBase::g_IsLoaded = false;
	SelectMusicBase::g_IsLoaded4Keys = false;
	SceneManager::SetNextScene(Scenes::LOGO);
}

void Option::OnInputPlayerSettings()
{
	//TODO:スコアをサーバーに保存する時の名前の設定と通信On/Off
}

void Option::MouseFunction(const std::vector<std::vector<int> > &list)
{
	Input& input = Input::GetInstance();
	//マウス処理関係
	for (int i = 0; i < static_cast<int>(list.size()); ++i)
	{
		if (((PLACE_Y + LINE_HEIGHT * i) - 5 < input.mouseY &&
			input.mouseY < (PLACE_Y + LINE_HEIGHT * i) + 34) &&
			(PLACE_X < input.mouseX && input.mouseX < PLACE_X + 248))
		{
			if (input.mouseClick == 1)
			{
				//1層目の時
				if (m_optionFlag == -1)
				{
					m_casolFlag[i] = 1;
					m_optionFlag = i;
					m_casolHigh = i;
				}
				else //2層目の時
				{
					m_casolHigh2 = i;
					if (m_casolHigh2 == static_cast<int>(list[m_optionFlag].size()) - 1)
						BackToTopOptions();
				}
				PlaySoundMem(m_moveSound, DX_PLAYTYPE_BACK, TRUE);
			}
		}
		else
		{
			m_casolFlag[i] = 0;
		}
	}
	for (size_t u = 0; u < list.size(); ++u)
	{
		if (m_casolFlag[u] == 1)
			m_casolHigh = u;
	}
}

void Option::Set3DModel()
{
	//3Dカーソル読み込み
	ModelHandle = MV1LoadModel(_T("MMD\\二重立方体.x"));
	// ３Ｄモデルのスケールを変更
	MV1SetScale(ModelHandle, VGet(0.5f, 0.5f, 0.5f));
	// 標準ライトをポイントライトにする
	ChangeLightTypePoint(VGet(0.0f, 0.0f, 0.0f), 2000.0f, 0.0f, 0.002f, 0.0f);
	// 標準ライトの位置をモデルの上に移動する
	//SetLightPosition(VGet(446.0f, 855.0f, -776.0f));
	// 標準ライトの位置をモデルの上に移動する
	SetLightPosition(VGet(446.0f, 549.0f - 100, -976.0f + 165));
}

void Option::UpDateOption()
{
	(this->*m_pFunc[this->m_optionFlag])();
}

/// <summary>
/// オプション画面のTopに戻る.
/// (値をリセットする)
/// </summary>
void Option::BackToTopOptions()
{
	PlaySoundMem(m_closeSound, DX_PLAYTYPE_BACK, TRUE);
	m_casolFlag[m_optionFlag] = 0;
	m_optionFlag = -1;
	m_casolHigh2 = 0;
	m_casolHigh3.fill(0);
}

//メッセージ処理
void Option::DrawOptionMessage()
{
	if (m_optionFlag == -1)
		DrawOptionMessage(m_optionMessageList);
	else if (m_optionFlag == 0)
		DrawOptionMessage(m_stageMessageList);
	else if (m_optionFlag == 1)
		DrawOptionMessage(m_displayMessageList);
	else if (m_optionFlag == 2)
		DrawOptionMessage(m_keyConfigMessageList);
}

void Option::DrawOptionMessage(const std::vector< std::vector<tstring> > &textList)
{
	DxLib::DrawGraph(26, 55, m_optionMessageFrameImage, TRUE);
	const auto casol = (m_optionFlag == -1) ? m_casolHigh : m_casolHigh2;
	const auto line = static_cast<int>(textList[casol].size());
	for (auto i = 0; i < line; i++)
	{
		DxLib::DrawStringToHandle(166, PLACE_Y + LINE_HEIGHT * 8 + 30 * i - (30 / 2 * line), textList[casol][i].c_str(), Color::GetInstance().White(), m_fontHandle);
	}
}

/// <summary>
/// ウィンドウ・フルスクリーンモード切り替え
/// </summary>
void Option::ChangeScreenMode(bool isFullScreen)
{
	if (isFullScreen)
	{
		SetGraphMode(WIN_WIDTH + 64, WIN_HEIGHT, 32);
		ChangeWindowMode(FALSE);
	}
	else
	{
		ChangeWindowMode(TRUE);
		SetGraphMode(WIN_WIDTH, WIN_HEIGHT, 32);
	}
}

void Option::LoadData()
{
	//背景画像の読み込み
	g_BackImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/back.jpg"));

	//3Dカーソルと3Dライト設定
	Set3DModel();

	//フォント
	m_fontHandle = CreateFontToHandle(_T("Fontn"), 24, 3, DX_FONTTYPE_EDGE);

	//lua読み込み
	ReadLua();
}

/// <summary>
/// Lua初期化
/// </summary>
void Option::InitLua()
{
	// LuaのVMを生成する
	L = luaL_newstate();

	// Luaの標準ライブラリを開く
	luaL_openlibs(L);
	m_luaHelper.SetLua(L);
}

/// <summary>
/// Lua読み込み
/// </summary>
void Option::ReadLua()
{
	if (!m_luaHelper.DoFile("Themes/DANCE ELENATION/lua/_file_options.lua"))
	{
		printfDx(_T("_file_options.luaを開けませんでした\n"));
		printfDx(_T("error : %s\n"), lua_tostring(L, -1));
		lua_close(L);
		return;
	}
	//画像ファイル名をスタックに積む
	lua_getglobal(L, "frame");
	lua_getglobal(L, "options");
	lua_getglobal(L, "optionsPage");
	lua_getglobal(L, "OptionMessage");
	lua_getglobal(L, "OptionRightCasol");
	lua_getglobal(L, "backSound");
	lua_getglobal(L, "moveSound");
	lua_getglobal(L, "decideSound");
	lua_getglobal(L, "closeSound");
	const int num = lua_gettop(L);
	std::vector<tstring> cg_name(0);
	if (num == 0)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("エラー"), _T("No stack"), MB_OK);
	}
	for (int N = num; N > 0; N--)
	{
		if (lua_type(L, N) == LUA_TSTRING)
			cg_name.emplace_back(lua_tostring(L, N));
	}
	const int nameNum = static_cast<int>(cg_name.size());
	const tstring imagePass = _T("Themes/DANCE ELENATION/Graphics/Options/%s");
	const tstring soundPass = _T("Themes/DANCE ELENATION/Sounds/Options/%s");

	int count = 1;

	DxHelper::SetObject(m_frameImage, IMAGE_TYPE, imagePass, cg_name[nameNum - (count++)].c_str());
	DxHelper::SetObject(m_optionTitleImage, IMAGE_TYPE, imagePass, cg_name[nameNum - (count++)].c_str());
	DxHelper::SetObject(m_paddingImage, IMAGE_TYPE, imagePass, cg_name[nameNum - (count++)].c_str());
	DxHelper::SetObject(m_optionMessageFrameImage, IMAGE_TYPE, imagePass, cg_name[nameNum - (count++)].c_str());
	DxHelper::SetObject(m_optionRightCasolImage, IMAGE_TYPE, imagePass, cg_name[nameNum - (count++)].c_str());

	DxHelper::SetObject(m_backBgm, SOUND_TYPE, soundPass, cg_name[nameNum - (count++)].c_str());
	DxHelper::SetObject(m_moveSound, SOUND_TYPE, soundPass, cg_name[nameNum - (count++)].c_str());
	DxHelper::SetObject(m_decideSound, SOUND_TYPE, soundPass, cg_name[nameNum - (count++)].c_str());
	DxHelper::SetObject(m_closeSound, SOUND_TYPE, soundPass, cg_name[nameNum - (count++)].c_str());

	//lua閉じる
	lua_settop(L, 0);
}