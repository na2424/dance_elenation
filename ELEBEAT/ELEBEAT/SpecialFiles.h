#ifndef SPECIAL_FILES_H
#define SPECIAL_FILES_H

//#include "define.h"

/** @brief The listing of the special files and directories in use. */
class SpecialFiles
{
public:
	/**
	* @brief The user packages directory.
	*
	* This should be separate from system packages so that
	* we can write to it (installing a package).
	*/
	static constexpr char DATA_DIR[] = "DATA/";
	static constexpr char PACKAGES_DIR[] = "Packages/";
	static constexpr char KEYMAPS_PATH[] = "Save/Keymaps.ini";
	static constexpr char EDIT_MODE_KEYMAPS_PATH[] = "Save/EditMode_Keymaps.ini";
	static constexpr char PREFERENCES_INI_PATH[] = "Save/Preferences.ini";
	static constexpr char THEMES_DIR[] = "Themes/";
	static constexpr char DEFAULT_THEMES_DIR[] = "Themes/DANCE ELENATION/";
	static constexpr char BASE_LANGUAGE[] = "en";
	static constexpr char METRICS_FILE[] = "metrics.ini";
	static constexpr char MARKERS_DIR[] = "Markers/";
	static constexpr char BASE_THEME_NAME[] = "_fallback";
	static constexpr char DEFAULTS_INI_PATH[] = "Data/Defaults.ini";
	static constexpr char STATIC_INI_PATH[] = "Data/Static.ini";
	static constexpr char TYPE_TXT_FILE[] = "Data/Type.txt";
	static constexpr char SONGS_DIR[] = "Songs/";
	static constexpr char SONGS_4KEYS_DIR[] = "Songs4keys/";
	static constexpr char COURSES_DIR[] = "Courses/";
	static constexpr char NOTESKINS_DIR[] = "NoteSkins/";
};
#endif