#include <boost/timer.hpp>
#include "DxLib.h"
#include "NoteELEBEAT.h"
#include "Combo.h"
#include "Input.h"
#include "JudgeCount.h"
#include "KeyInput.h"
#include "extern.h"

using namespace DxLib;

NoteElebeat::NoteElebeat(void)
{
	for (int m = 0; m < 4; m++)
	{
		for (int n = 0; n < 4; n++)
		{
			m_LongFlag[m][n] = 0;
			m_LongEndFlag[m][n] = 0;
			m_U[m][n] = 0;
			m_NoteCount[m][n] = 0;

			for (int i = 0; i < MAXX; i++)
			{
				m_NoteKind[m][n][i] = -1;
				note[m][n][i] = -1000;
				note_time[m][n][i] = -10000;
			}
		}
	}
}

//キーを押した時の時間を取得する関数
void NoteElebeat::GetKeyTime(const std::shared_ptr<KeyInput>& f_keyinput, long int *now, int matrix) const
{
	if (matrix == 3)
	{
		for (auto m = 0; m < matrix; m++)
		{
			for (auto n = 0; n < matrix; n++)
			{
				if (Input::GetInstance().m_Key[m + 1][n + 1] == 1)
				{
					f_keyinput->m_KeyTime[m][n] = static_cast<int>(*now);
					if (f_keyinput->m_B[m][n] != 1)
						f_keyinput->m_B[m][n] = 1;
				}
			}
		}
	}
	if (matrix == 4)
	{
		for (int m = 0; m < matrix; m++)
		{
			for (int n = 0; n < matrix; n++)
			{
				if (Input::GetInstance().m_Key[m][n] == 1)
				{
					f_keyinput->m_KeyTime[m][n] = static_cast<int>(*now);
					if (f_keyinput->m_B[m][n] != 1)
						f_keyinput->m_B[m][n] = 1;
				}
			}
		}
	}
}

//キーを押したとき最も近いノートが各方向の上から何番目かを求める関数(「basyo[横][縦]」に結果が入る。)
void NoteElebeat::GetKeyNote(const std::shared_ptr<KeyInput>& f_keyinput, int matrix)
{
	for (int m = 0; m < matrix; m++)
	{
		for (int n = 0; n < matrix; n++)
		{
			if (f_keyinput->m_B[m][n] == 1)
			{
				for (int a = 0; a < MAXX; a++)
				{
					f_keyinput->m_Diff[m][n] = f_keyinput->m_KeyTime[m][n] - static_cast<int>(this->note_time[m][n][a]);
					if (f_keyinput->m_Diff[m][n] < 0)
					{
						f_keyinput->m_Diff[m][n] *= -1;
					}
					if (a == 0)
						f_keyinput->m_Min[m][n] = f_keyinput->m_Diff[m][n];
					if (f_keyinput->m_Diff[m][n] <= f_keyinput->m_Min[m][n])
					{
						f_keyinput->m_Min[m][n] = f_keyinput->m_Diff[m][n];
						f_keyinput->m_Place[m][n] = a;
					}
				}
				f_keyinput->m_B[m][n] = 0;
			}
		}
	}
}

//ノートの判定処理を行う関数
void NoteElebeat::NoteJudgement(const std::shared_ptr<ScoreSt>& f_score, const std::shared_ptr<Combo>& f_combo, const std::shared_ptr<KeyInput>& f_keyinput, int m, int n, int *k, long int now, int i, bool Auto_flag)
{
	JudgeCount& judge = JudgeCount::GetInstance();
	//パーフェクト判定
	if (this->note_time[m][n][f_keyinput->m_Place[m][n]] - Setting.Judge.JudgePerfect <= now && now <= this->note_time[m][n][f_keyinput->m_Place[m][n]] + Setting.Judge.JudgePerfect)
	{
		if (this->note_time[m][n][f_keyinput->m_Place[m][n]] - Setting.Judge.JudgePerfect <= f_keyinput->m_KeyTime[m][n] && f_keyinput->m_KeyTime[m][n] <= this->note_time[m][n][f_keyinput->m_Place[m][n]] + Setting.Judge.JudgePerfect)
		{
			if (this->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] == -1)//通常ノートの場合
			{
				if (Auto_flag == 0)
					judge.score += 1000000 / this->m_Maxnote * 1.00;

				f_score->m_ScoreSa = judge.score - f_score->m_ScoreNum;        //本命のスコアと目に見えるスコアの差を求める
				*k = 0;                                            //スコアのアニメーションで使うフラグカウンタを０にする
				f_combo->m_AnimationFrame = 0;                                    //コンボのアニメーションのリセット
				this->m_NoteCount1++;                            //押されたノート数をカウント
				f_combo->m_ComboNum++;                                        //現在のコンボ数をカウント
				judge.parfect++;                                            //パーフェクト数をカウント
				this->m_JudgmentResult = 1;                            //パーフェクトの判定がされたことを記録する
				this->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] = 1;        //フラグに１を代入(押されたと判定)
				this->m_NoteHitTime[m][n][f_keyinput->m_Place[m][n]] = static_cast<int>(now);//フラグに１を代入したときの時間を取得
				this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;    //ミスしたノートと押されたノートを足すと総ノート数になる。
			}
			else if (this->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] == -2)//ロングノートの場合
			{
				if (Auto_flag == 0) {
					judge.score += 1000000 / this->m_Maxnote*1.00;                    //スコアを加算
				}
				f_score->m_ScoreSa = judge.score - f_score->m_ScoreNum;        //本命のスコアと目に見えるスコアの差を求める
				*k = 0;                                            //スコアのアニメーションのリセット
				f_combo->m_AnimationFrame = 0;                                    //コンボのアニメーションのリセット
				this->m_NoteCount1++;                            //押されたノート数をカウント
				f_combo->m_ComboNum++;                                        //現在のコンボ数をカウント
				judge.parfect++;                                            //パーフェクト数をカウント
				this->m_JudgmentResult = 1;                            //パーフェクトの判定がされたことを記録する
				this->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] = 2;            //フラグに２を代入(押されたと判定)
				this->m_LongFlag[m][n] = 2;
				this->m_NoteHitTime[m][n][f_keyinput->m_Place[m][n]] = static_cast<int>(now);//フラグに１を代入したときの時間を取得
				this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;    //ミスしたノートと押されたノートを足すと総ノート数になる。
			}
		}
	}

	//グレ判定
	else if (this->note_time[m][n][f_keyinput->m_Place[m][n]] - Setting.Judge.JudgeGreat <= now && now <= this->note_time[m][n][f_keyinput->m_Place[m][n]] - Setting.Judge.JudgePerfect || this->note_time[m][n][f_keyinput->m_Place[m][n]] + Setting.Judge.JudgePerfect <= now &&  now <= this->note_time[m][n][f_keyinput->m_Place[m][n]] + Setting.Judge.JudgeGreat)
	{
		if (this->note_time[m][n][f_keyinput->m_Place[m][n]] - Setting.Judge.JudgeGreat <= f_keyinput->m_KeyTime[m][n] && f_keyinput->m_KeyTime[m][n] <= this->note_time[m][n][f_keyinput->m_Place[m][n]] - Setting.Judge.JudgePerfect || this->note_time[m][n][f_keyinput->m_Place[m][n]] + Setting.Judge.JudgePerfect <= f_keyinput->m_KeyTime[m][n] && f_keyinput->m_KeyTime[m][n] <= this->note_time[m][n][f_keyinput->m_Place[m][n]] + Setting.Judge.JudgeGreat)
		{
			if (this->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] == -1)
			{
				if (Auto_flag == 0) {
					judge.score += 1000000 / this->m_Maxnote * 0.70;                    //スコアを加算
				}
				f_score->m_ScoreSa = judge.score - f_score->m_ScoreNum;        //本命のスコアと目に見えるスコアの差を求める
				*k = 0;                                            //スコアのアニメーションのリセット
				f_combo->m_AnimationFrame = 0;                                    //コンボのアニメーションのリセット
				m_NoteCount1++;                            //押されたノート数をカウント
				f_combo->m_ComboNum++;                                        //現在のコンボ数をカウント
				judge.grate++;                                            //グレ数をカウント
				m_JudgmentResult = 2;                        //グレの判定がされたことを記録する
				m_NoteKind[m][n][f_keyinput->m_Place[m][n]] = 1;        //フラグに１を代入(押されたと判定)
				m_NoteHitTime[m][n][f_keyinput->m_Place[m][n]] = static_cast<int>(now);//フラグに１を代入したときの時間を取得
				m_NoteTotle = m_NoteCount0 + m_NoteCount1;    //ミスしたノートと押されたノートを足すと総ノート数になる。
			}
			else if (this->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] == -2)//ロングノートの場合
			{
				if (Auto_flag == 0) {
					judge.score += 1000000 / this->m_Maxnote * 0.70;                    //スコアを加算
				}
				f_score->m_ScoreSa = judge.score - f_score->m_ScoreNum;        //本命のスコアと目に見えるスコアの差を求める
				*k = 0;                                            //スコアのアニメーションのリセット
				f_combo->m_AnimationFrame = 0;                                    //コンボのアニメーションのリセット
				this->m_NoteCount1++;                            //押されたノート数をカウント
				f_combo->m_ComboNum++;                                        //現在のコンボ数をカウント
				judge.grate++;                                            //グレ数をカウント
				this->m_JudgmentResult = 2;                        //グレの判定がされたことを記録する
				this->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] = 2;        //フラグに２を代入(押されたと判定)
				this->m_LongFlag[m][n] = 2;
				this->m_NoteHitTime[m][n][f_keyinput->m_Place[m][n]] = static_cast<int>(now);//フラグに２を代入したときの時間を取得
				this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;    //ミスしたノートと押されたノートを足すと総ノート数になる。
			}
		}
	}

	//グッド判定
	else if (this->note_time[m][n][f_keyinput->m_Place[m][n]] - Setting.Judge.JudgeGood <= now && now <= this->note_time[m][n][f_keyinput->m_Place[m][n]] - Setting.Judge.JudgeGreat || this->note_time[m][n][f_keyinput->m_Place[m][n]] + Setting.Judge.JudgeGreat <= now &&  now <= this->note_time[m][n][f_keyinput->m_Place[m][n]] + Setting.Judge.JudgeGood && this->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] != -3)
	{
		if (this->note_time[m][n][f_keyinput->m_Place[m][n]] - Setting.Judge.JudgeGood <= f_keyinput->m_KeyTime[m][n] && f_keyinput->m_KeyTime[m][n] <= this->note_time[m][n][f_keyinput->m_Place[m][n]] - Setting.Judge.JudgeGreat || this->note_time[m][n][f_keyinput->m_Place[m][n]] + Setting.Judge.JudgeGreat <= f_keyinput->m_KeyTime[m][n] && f_keyinput->m_KeyTime[m][n] <= this->note_time[m][n][f_keyinput->m_Place[m][n]] + Setting.Judge.JudgeGood)
		{
			if (this->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] == -1)
			{
				if (Auto_flag == 0)
					judge.score += 1000000 / this->m_Maxnote * 0.40;

				f_score->m_ScoreSa = judge.score - f_score->m_ScoreNum;        //本命のスコアと目に見えるスコアの差を求める
				*k = 0;                                            //スコアのアニメーションのリセット
				this->m_NoteCount0++;
				judge.UpdateMaxComboOrNone(f_combo->m_ComboNum);
				f_combo->m_ComboNum++;
				judge.good++;

				this->m_JudgmentResult = 3;                        //グッドの判定がされたことを記録する
				this->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] = 1;        //フラグに０を代入(切ったと判定)
				this->m_NoteHitTime[m][n][f_keyinput->m_Place[m][n]] = static_cast<int>(now);//フラグに０を代入したときの時間を取得
				this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;    //ミスしたノートと押されたノートを足すと総ノート数になる。
			}
			else if (this->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] == -2)
			{
				if (Auto_flag == 0)
					judge.score += 1000000 / this->m_Maxnote * 0.40;

				f_score->m_ScoreSa = judge.score - f_score->m_ScoreNum;        //本命のスコアと目に見えるスコアの差を求める
				*k = 0;                                            //スコアのアニメーションのリセット
				this->m_NoteCount0++;
				judge.UpdateMaxComboOrNone(f_combo->m_ComboNum);
				f_combo->m_ComboNum = 0;
				judge.good++;

				this->m_JudgmentResult = 3;                            //グッドの判定がされたことを記録する
				this->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] = 1;        //フラグに1を代入
				this->m_NoteHitTime[m][n][f_keyinput->m_Place[m][n]] = static_cast<int>(now);//フラグを代入したときの時間を取得
				this->m_LongFlag[m][n] = 0;
				this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;    //ミスしたノートと押されたノートを足すと総ノート数になる。
			}
		}
	}

	/*------------フラグに０を代入する判定-------------*/
	/*ミスの処理を行なう */
	else if ((this->note_time[m][n][i] + 300 < now) && (now < this->note_time[m][n][i] + 400) && this->m_NoteKind[m][n][i] == -1)
	{
		this->m_NoteCount0++;
		judge.UpdateMaxComboOrNone(f_combo->m_ComboNum);
		f_combo->m_ComboNum = 0;
		judge.missed++;
		this->m_NoteKind[m][n][i] = 0;
		this->m_NoteHitTime[m][n][i] = static_cast<int>(now);                    //フラグに０を代入したときの時間を取得
		this->m_JudgmentResult = 4;                                    //ミスの判定がされたことを記録する

		this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;
	}
	//LONGNOTEのミスの場合
	else if (this->note_time[m][n][i] + 300 < now  && now < this->note_time[m][n][i] + 350 && this->m_NoteKind[m][n][i] == -2 && this->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] != -3)
	{
		this->m_NoteCount0++;
		judge.UpdateMaxComboOrNone(f_combo->m_ComboNum);
		f_combo->m_ComboNum = 0;
		judge.missed++;
		judge.NG++;
		this->m_NoteKind[m][n][i] = 0;
		this->m_NoteHitTime[m][n][i] = static_cast<int>(now);                    //フラグに０を代入したときの時間を取得
		this->m_LongFlag[m][n] = 0;
		this->m_JudgmentResult = 4;                                    //ミスの判定がされたことを記録する

		this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;
		this->m_U[m][n] = 0;
	}
}

//LONGNOTEホールド時の処理を行う関数
void NoteElebeat::LongNoteHold(NoteElebeat *note_mn, ScoreSt *f_score, KeyInput *f_keyinput, int m, int n, int *k, long int now, bool Auto_flag)
{
	//途中で離した
	if (note_mn->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] == 2 && note_mn->m_NoteKind[m][n][f_keyinput->m_Place[m][n] + 1] == -3 && Input::GetInstance().m_Key[m + 1][n + 1] == 0) {
		note_mn->m_U[m][n] = 0;                                //OK,NGアニメーションのリセット
		note_mn->m_NoteKind[m][n][f_keyinput->m_Place[m][n] + 1] = 0;
		note_mn->m_NoteHitTime[m][n][f_keyinput->m_Place[m][n] + 1] = static_cast<int>(now);
		note_mn->m_LongFlag[m][n] = 0;
		JudgeCount::GetInstance().NG++;
	}
	//最後まで押せた
	else if (note_mn->m_NoteKind[m][n][f_keyinput->m_Place[m][n]] == 2 && note_mn->m_NoteKind[m][n][f_keyinput->m_Place[m][n] + 1] == -3 && note_mn->note_time[m][n][f_keyinput->m_Place[m][n] + 1] < now && Input::GetInstance().m_Key[m + 1][n + 1] > 0)
	{
		//スコアを加算
		if (Auto_flag == 0)
			JudgeCount::GetInstance().score += 1000000 / note_mn->m_Maxnote*1.00;
		//本命のスコアと目に見えるスコアの差を求める
		f_score->m_ScoreSa = JudgeCount::GetInstance().score - f_score->m_ScoreNum;
		//スコアのアニメーションのリセット
		*k = 0;
		//OK,NGアニメーションのリセット
		note_mn->m_U[m][n] = 0;
		JudgeCount::GetInstance().OK++;
		note_mn->m_NoteKind[m][n][f_keyinput->m_Place[m][n] + 1] = 3;
		note_mn->m_NoteHitTime[m][n][f_keyinput->m_Place[m][n] + 1] = static_cast<int>(now);
		note_mn->m_LongEndFlag[m][n] = 1;
	}
}

//Autoさんのキーを押した時の時間を取得する関数
void NoteElebeat::GetKeyTimeAuto(const std::shared_ptr<KeyInput>& f_keyinput, long int *now, int matrix, const std::shared_ptr<NoteElebeat>& f_NoteClass)
{
	if (matrix == 3) {
		for (int m = 0; m < matrix; m++)
		{
			for (int n = 0; n < matrix; n++)
			{
				for (int i = MAXX - 1; i >= 0; i--)
				{
					if (f_NoteClass->note_time[m][n][i] < *now && f_NoteClass->note_time[m][n][i]>0)
					{
						f_keyinput->m_KeyTime[m][n] = static_cast<int>(f_NoteClass->note_time[m][n][i]);
						if (f_keyinput->m_B[m][n] != 1)
							f_keyinput->m_B[m][n] = 1;
						break;
					}
				}
			}
		}
	}
	if (matrix == 4) {
		for (int m = 0; m < matrix; m++)
		{
			for (int n = 0; n < matrix; n++)
			{
				for (int i = MAXX - 1; i >= 0; i--)
				{
					if (f_NoteClass->note_time[m][n][i] < *now && f_NoteClass->note_time[m][n][i]>0)
					{
						f_keyinput->m_KeyTime[m][n] = static_cast<int>(f_NoteClass->note_time[m][n][i]);
						if (f_keyinput->m_B[m][n] != 1)
							f_keyinput->m_B[m][n] = 1;
						break;
					}
				}
			}
		}
	}
}

//Autoさんのキーを押したとき最も近いノートが各方向の上から何番目かを求める関数(「place[横][縦]」に結果が入る。)
void NoteElebeat::GetKeyNoteAuto(const std::shared_ptr<KeyInput>& f_keyinput, const std::shared_ptr<NoteElebeat>& note_mn, int matrix)
{
	for (int m = 0; m < matrix; m++)
	{
		for (int n = 0; n < matrix; n++)
		{
			if (f_keyinput->m_B[m][n] != 1) continue;

			for (int a = 0; a < MAXX; a++)
			{
				f_keyinput->m_Diff[m][n] = f_keyinput->m_KeyTime[m][n] - static_cast<int>(note_mn->note_time[m][n][a]);
				if (f_keyinput->m_Diff[m][n] < 0)
					f_keyinput->m_Diff[m][n] *= -1;
				if (a == 0)
					f_keyinput->m_Min[m][n] = f_keyinput->m_Diff[m][n];
				if (f_keyinput->m_Diff[m][n] <= f_keyinput->m_Min[m][n])
				{
					f_keyinput->m_Min[m][n] = f_keyinput->m_Diff[m][n];
					f_keyinput->m_Place[m][n] = a;
				}
			}
			f_keyinput->m_B[m][n] = 0;
		}
	}
}