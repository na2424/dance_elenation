#include "SelectMusic16keys.h"
#include "DxLib.h"
#include "define.h"
#include "stdlib.h"
#include "Input.h"
#include "extern.h"
#include <random>
#include "Foreach.h"
#include "Marker.h"
#include "Color.h"


using namespace DxLib;

bool SelectMusic16keys::LoadGraphAll(const std::shared_ptr<Music> &musicP)
{
	int i = 0;
	int j = CENTER_DEFAULT_16;
	int centernum = 0;
	Marker& marker = Marker::GetInstance();

	while (i != musicP->elebeatSong->m_TotalMusicNum)
	{
		if (LoadGraph(musicP->elebeatSong->m_AlbumArtPath[i].c_str()) == -1)
			g_TempAA[i] = LoadGraph(_T("img\\noalbumart.png"));
		else
			g_TempAA[i] = LoadGraph(musicP->elebeatSong->m_AlbumArtPath[i].c_str());

		i++;
		j++;
	}

	//FOLDER
	i = 0;
	j = CENTER_DEFAULT_16;

	while (i != musicP->elebeatSong->m_FolderNum)
	{
		if (LoadGraph(musicP->elebeatSong->m_FolderAlbumArtPath[i].c_str()) == -1)
			g_KindFolder[j] = LoadGraph(_T("img\\nofolder.png"));
		else
			g_KindFolder[j] = LoadGraph(musicP->elebeatSong->m_FolderAlbumArtPath[i].c_str());

		i++;
		j++;
	}

	if (stop[0] != 1)
	{
		//判定
		if (musicP->elebeatSong->m_FolderNum % 3 == 1)
		{
			centernum = j;
			j += 2;
		}
		else if (musicP->elebeatSong->m_FolderNum % 3 == 2)
		{
			centernum = j;
			j += 1;
		}
		else
		{
			centernum = j;
		}

		i = CENTER_DEFAULT_16;

		while (j != (CENTER_DEFAULT_16 * 2) + 10)
		{
			if (i == centernum) break;

			g_KindFolder[j] = g_KindFolder[i];
			i++;
			j++;
		}

		if (musicP->elebeatSong->m_FolderNum % 3 == 1)
			j = CENTER_DEFAULT_16 - 2;
		else if (musicP->elebeatSong->m_FolderNum % 3 == 2)
			j = CENTER_DEFAULT_16 - 1;
		else
			j = CENTER_DEFAULT_16;

		i = CENTER_DEFAULT_16 + musicP->elebeatSong->m_FolderNum;

		while (j != 0)
		{
			//0ikaになったとき
			if (i == CENTER_DEFAULT_16 - 1)
				break;

			g_KindFolder[j] = g_KindFolder[i];

			i--;
			j--;
		}
	}

	//マーカー
	i = 0;
	j = CENTER_DEFAULT_16;

	while (i != marker.marker_num)
	{
		g_MarkerGraph16[j] = noteMarkerImage[i][20];
		i++;
		j++;
	}

	//12個以下じゃないとき
	if (stop[2] != 1)
	{
		//判定
		if (marker.marker_num % 3 == 1)///上にいっこ
		{
			centernum = j;
			j += 2;
		}
		else if (marker.marker_num % 3 == 2)///二個
		{
			centernum = j;
			j += 1;
		}
		else
		{
			centernum = j;
		}

		//右はじまで
		while (j != (CENTER_DEFAULT_16 * 2) + 10)
		{
			//最大数になったとき
			if (i == centernum)
			{
				break;
			}
			g_MarkerGraph16[j] = g_MarkerGraph16[i];
			i++;
			j++;
		}

		//判定
		if (marker.marker_num % 3 == 1)
			j = CENTER_DEFAULT_16 - 2;
		else if (marker.marker_num % 3 == 2)
			j = CENTER_DEFAULT_16 - 1;
		else
			j = CENTER_DEFAULT_16;

		i = CENTER_DEFAULT_16 + musicP->elebeatSong->m_FolderNum;

		//左はじまで
		while (j != 0)
		{
			//0ikaになったとき
			if (i == CENTER_DEFAULT_16 - 1)
			{
				break;
			}
			g_MarkerGraph16[j] = g_MarkerGraph16[i];
			i--;
			j--;
		}
	}

	//パネル選択画像
	rightImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\16select01.png"));
	leftImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\16select02.png"));
	markerImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\16select03.png"));
	enterImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\16select04.png"));
	backImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\16select05.png"));

	//右側画像
	selectMusicImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\selectmusic16.png"));
	basicImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\basic_16.png"));
	normalImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\normal_16.png"));
	advancedImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\advanced_16.png"));
	difficultyNumImage = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\d-16num20x30.png"));

	difficultIconImage[0] = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\d-b.png"));
	difficultIconImage[1] = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\d-n.png"));
	difficultIconImage[2] = LoadGraph(_T("Themes\\DANCE ELENATION\\Graphics\\MusicSelect\\d-a.png"));

	this->loadendflag_16 = 1;

	return 0;
}

//座標設定 (yはいらん)
void SelectMusic16keys::GraphCoordinatesSet(void) const
{
	/*************************見えるx座標*************************/
	x_16[3] = LEFTX;
	x_16[4] = LEFTX + AASIZE + AABETWEEN;
	x_16[5] = LEFTX + (AASIZE * 2) + (AABETWEEN * 2);
	x_16[6] = LEFTX + (AASIZE * 3) + (AABETWEEN * 3);
	/*************************見えないx座標*************************/
	x_16[2] = LEFTX - AABETWEEN - AASIZE;
	x_16[7] = LEFTX + (AASIZE * 4) + (AABETWEEN * 4);
}
//画像動かす
bool SelectMusic16keys::MoveGraph(void)
{
	//panelplace++
	if (moveright == 1)
	{
		if (this->count < 6)
		{
			x_16[2] -= 25;
			x_16[3] -= 25;
			x_16[4] -= 25;
			x_16[5] -= 25;
			x_16[6] -= 25;
			x_16[7] -= 25;
		}
		if (this->count >= 6 && this->count < 9)
		{
			x_16[2] -= 10;
			x_16[3] -= 10;
			x_16[4] -= 10;
			x_16[5] -= 10;
			x_16[6] -= 10;
			x_16[7] -= 10;
		}
		if (this->count >= 9 && this->count < 12)
		{
			x_16[2] -= 5;
			x_16[3] -= 5;
			x_16[4] -= 5;
			x_16[5] -= 5;
			x_16[6] -= 5;
			x_16[7] -= 5;
		}
		this->count++;
	}
	//panelplace--
	if (moveleft == 1)
	{
		if (this->count < 6)
		{
			x_16[2] += 25;
			x_16[3] += 25;
			x_16[4] += 25;
			x_16[5] += 25;
			x_16[6] += 25;
			x_16[7] += 25;
		}
		if (this->count >= 6 && this->count < 9)
		{
			x_16[2] += 10;
			x_16[3] += 10;
			x_16[4] += 10;
			x_16[5] += 10;
			x_16[6] += 10;
			x_16[7] += 10;
		}
		if (this->count >= 9 && this->count < 12)
		{
			x_16[2] += 5;
			x_16[3] += 5;
			x_16[4] += 5;
			x_16[5] += 5;
			x_16[6] += 5;
			x_16[7] += 5;
		}
		this->count++;
	}
	if (this->count == 11) {
		return 1;
	}
	return 0;
}
//マーカーセレクト overselectも処理として入れておく
void SelectMusic16keys::SelectMarker(void) const
{
	//ストップしないとき 12枚以上
	if (stop[2] != 0)
	{
	}
}

//key操作
int SelectMusic16keys::CatchKey(void)
{
	Input &input = Input::GetInstance();
	int Mouse_flag = -1;

	if (Input::GetInstance().mouseClick == 1)
	{
		for (int Ml = 0; Ml < 4 && Mouse_flag == -1; Ml++)
		{
			for (int Mu = 0; Mu < 4 && Mouse_flag == -1; Mu++)
			{
				if (SQUARE_SIZE*Ml < input.mouseX && input.mouseX < SQUARE_SIZE*Ml + 177)
				{
					if (SQUARE_SIZE*Mu < input.mouseY && input.mouseY < SQUARE_SIZE*Mu + 177)
					{
						Mouse_flag = Ml + Mu * 4;
					}
				}
			}
		}
	}
	else
	{
		Mouse_flag = -1;
	}
	selectkeynumber = -1;
	///////////////////////////////////////////////////
	if ((Mouse_flag == 0 || input.m_Key[0][0] == 1) && one_16[0] == 0 && this->count == 0)
	{
		one_16[0] = 1;
		selectmusicflag = 1;
		selectkeynumber = 0;
	}
	if (Mouse_flag != 0 && input.m_Key[0][0] == 0)
		one_16[0] = 0;
	///////////////////////////////////////////////////
	if ((Mouse_flag == 1 || input.m_Key[1][0] == 1) && one_16[1] == 0 && this->count == 0)
	{
		one_16[1] = 1;
		selectmusicflag = 1;
		selectkeynumber = 3;
	}
	if (Mouse_flag != 1 && input.m_Key[1][0] == 0)
		one_16[1] = 0;
	///////////////////////////////////////////////////
	if ((Mouse_flag == 2 || input.m_Key[2][0] == 1) && one_16[2] == 0 && this->count == 0)
	{
		one_16[2] = 1;
		selectmusicflag = 1;
		selectkeynumber = 6;
	}
	if (Mouse_flag != 2 && input.m_Key[2][0] == 0)
		one_16[2] = 0;
	///////////////////////////////////////////////////
	if ((Mouse_flag == 3 || input.m_Key[3][0] == 1) && one_16[3] == 0 && this->count == 0)
	{
		one_16[3] = 1;
		selectmusicflag = 1;
		selectkeynumber = 9;
	}
	if (Mouse_flag != 3 && input.m_Key[3][0] == 0)
		one_16[3] = 0;
	///////////////////////////////////////////////////
	if ((Mouse_flag == 4 || input.m_Key[0][1] == 1) && one_16[4] == 0 && this->count == 0)
	{
		one_16[4] = 1;
		selectmusicflag = 1;
		selectkeynumber = 1;
	}
	if (Mouse_flag != 4 && input.m_Key[0][1] == 0)
		one_16[4] = 0;
	///////////////////////////////////////////////////
	if ((Mouse_flag == 5 || input.m_Key[1][1] == 1) && one_16[5] == 0 && this->count == 0)
	{
		one_16[5] = 1;
		selectmusicflag = 1;
		selectkeynumber = 4;
	}
	if (Mouse_flag != 5 && input.m_Key[1][1] == 0)
		one_16[5] = 0;
	///////////////////////////////////////////////////
	if ((Mouse_flag == 6 || input.m_Key[2][1] == 1) && one_16[6] == 0 && this->count == 0)
	{
		one_16[6] = 1;
		selectmusicflag = 1;
		selectkeynumber = 7;
	}
	if (Mouse_flag != 6 && input.m_Key[2][1] == 0)
		one_16[6] = 0;
	///////////////////////////////////////////////////
	if ((Mouse_flag == 7 || input.m_Key[3][1] == 1) && one_16[7] == 0 && this->count == 0)
	{
		one_16[7] = 1;
		selectmusicflag = 1;
		selectkeynumber = 10;
	}
	if (Mouse_flag != 7 && input.m_Key[3][1] == 0)
		one_16[7] = 0;
	///////////////////////////////////////////////////
	if ((Mouse_flag == 8 || input.m_Key[0][2] == 1) && one_16[8] == 0 && this->count == 0) {
		one_16[8] = 1;
		selectmusicflag = 1;
		selectkeynumber = 2;
	}
	if (Mouse_flag != 8 && input.m_Key[0][2] == 0)
		one_16[8] = 0;
	///////////////////////////////////////////////////
	if ((Mouse_flag == 9 || input.m_Key[1][2] == 1) && one_16[9] == 0 && this->count == 0)
	{
		one_16[9] = 1;
		selectmusicflag = 1;
		selectkeynumber = 5;
	}
	if (Mouse_flag != 9 && input.m_Key[1][2] == 0)
		one_16[9] = 0;
	///////////////////////////////////////////////////
	if ((Mouse_flag == 10 || input.m_Key[2][2] == 1) && one_16[10] == 0 && this->count == 0)
	{
		one_16[10] = 1;
		selectmusicflag = 1;
		selectkeynumber = 8;
	}
	if (Mouse_flag != 10 && input.m_Key[2][2] == 0)
		one_16[10] = 0;
	///////////////////////////////////////////////////
	if ((Mouse_flag == 11 || input.m_Key[3][2] == 1) && one_16[11] == 0 && this->count == 0)
	{
		one_16[11] = 1;
		selectmusicflag = 1;
		selectkeynumber = 11;
	}
	if (Mouse_flag != 11 && input.m_Key[3][2] == 0)
		one_16[11] = 0;
	///////////////////////////////////////////////////
	//右へ
	if ((input.m_Key[0][3] > 0 || Mouse_flag == 12) && moveleft == 0)
	{
		leftcountflag_16 = 1;
		moveright = 1;
	}
	if (input.m_Key[0][3] == 0 && Mouse_flag != 12) {
		leftcountflag_16 = 0;
	}
	///////////////////////////////////////////////////
	//左へ
	if ((input.m_Key[1][3] > 0 || Mouse_flag == 13) && moveright == 0)
	{
		rightcountflag_16 = 1;
		moveleft = 1;
	}
	if (input.m_Key[1][3] == 0 && Mouse_flag != 13) {
		rightcountflag_16 = 0;
	}
	///////////////////////////////////////////////////
	//マーカーセレクト to return AA
	if ((Mouse_flag == 14 || input.m_Key[2][3] == 1) && one_16[12] == 0)
	{
		one_16[12] = 1;
		//folderの時
		if (selectMode == SelectMode::FOLDER)
		{
			//マーカー選択に
			selectMode = SelectMode::MARKER;
			g_NowCenter = CENTER_DEFAULT_16;
		}
		//AAの時
		else if (selectMode == SelectMode::AA)
		{
			PlaySoundMem(closeSound, DX_PLAYTYPE_BACK);
			//folderに戻る
			selectMode = SelectMode::FOLDER;
			//元のラインに戻す
			g_NowCenter = beforecenter_16;
			//前セレクトしてたAA番号をなかったことに
			beforeselectAA = -1;
			//上に同じく
			nowselectglobalAAnum = -1;
		}
		//markerの時
		else
		{
			//前セレクトしてたmarkerをなかったことに
			beforeselectmarker = -1;
		}
	}
	if (Mouse_flag != 14 && input.m_Key[2][3] == 0)
		one_16[12] = 0;

	//gameへ
	if ((Mouse_flag == 15 || input.m_Key[3][3] == 1) && one_16[13] == 0)
	{
		one_16[13] = 1;

		if (selectMode == SelectMode::FOLDER)
			return -1;
		else if (selectMode == SelectMode::AA && selectmusicflag == 1)
			return 1;
		else
			selectMode = SelectMode::FOLDER;
	}

	if (Mouse_flag != 14 && input.m_Key[3][3] == 0)
		one_16[13] = 0;

	if (input.keyEsc == 1)
		return -1;

	return 0;
}
// folder番号酒盗 左上の描画番号を返す
int SelectMusic16keys::NowFolderNumber_16(int nowcenter)
{
	int num = nowcenter - CENTER_DEFAULT_16;

	if (num < 0)
	{
		num = maxline_folder - ((abs(num) / 3) - 1);
		num = (num * 3) - 3;
	}
	return num;
}

//AAの番号を調べる
int SelectMusic16keys::NowAANumber_16(const int nowcenter) const
{
	//AAの詳細番号を出す
	int num = nowcenter - CENTER_DEFAULT_16;

	if (num < 0)
	{
		num = maxline_AA - ((abs(num) / 3) - 1);
		num = (num * 3) - 3;
	}
	return num;
}
//マーカー番号調べる
int SelectMusic16keys::NowMarkernumber_16(const int nowcenter) const
{
	int num = nowcenter - CENTER_DEFAULT_16;

	if (num < 0)
	{
		num = maxline_marker - ((abs(num) / 3) - 1);
		num = (num * 3) - 3;
	}
	return num;
}

//選択AAを表示
void SelectMusic16keys::ShowSelectAA(void)
{
}

//音楽並べる
int SelectMusic16keys::SetSelectAA(int nowcenter, const std::shared_ptr<Music> &musicP)
{
	int centernum = 0;
	//一般化したフォルダ番号
	int foldernumber = 0;
	//総計からフォルダを決定する
	int musicsum = 0;
	//AA表示に変更
	selectMode = SelectMode::AA;
	//開いたフォルダ番号取得
	foldernumber = NowFolderNumber_16(nowcenter);
	//選択した音楽番号
	foldernumber += selectkeynumber;
	//選択したフォルダ番号（一桁）を保持　グローバル
	selectfoldernum = foldernumber;
	//0からそのフォルダまでの音楽総計
	for (auto i = 0; i <= foldernumber; ++i)
	{
		//曲数
		musicsum += musicP->elebeatSong->m_MusicNumAt[i];
	}

	//グローバルに渡す
	//最初の音楽番号
	nowAA_16[0] = musicsum - musicP->elebeatSong->m_MusicNumAt[foldernumber];
	//最後の音楽番号
	nowAA_16[1] = musicsum - 1;
	//音楽総計
	nowAA_16[2] = nowAA_16[1] - nowAA_16[0] + 1;

	//ラインを得る
	if (nowAA_16[2] % 3 == 0)
	{
		maxline_AA = nowAA_16[2] / 3;
	}
	else
	{
		maxline_AA = (nowAA_16[2] / 3) + 1;
	}

	for (size_t i = 0; i < g_AA.size(); i++)
	{
		g_AA[i] = NULL;
	}

	//フォルダ内部の曲が12以下の時
	if (musicP->elebeatSong->m_MusicNumAt[foldernumber] <= 12)
	{
		//ストップAA
		stop[1] = 1;
		int i = CENTER_DEFAULT_16;
		int j = nowAA_16[0];

		while (i != g_AA.size())
		{
			//最大数になったとき
			if (j == nowAA_16[1] + 1)
			{
				return CENTER_DEFAULT_16;
			}
			g_AA[i] = g_TempAA[j];
			i++;
			j++;
		}
	}
	else
	{
		//十二個以上で動けるときの処理
		stop[1] = 0;

		int i = CENTER_DEFAULT_16;
		int j = nowAA_16[0];
		//真ん中分
		while (i != g_AA.size())
		{
			//最大数になったとき
			if (j == nowAA_16[1] + 1)
			{
				break;
			}
			g_AA[i] = g_TempAA[j];
			i++;
			j++;
		}

		//判定
		if (nowAA_16[2] % 3 == 1)//上にいっこ
		{
			i += 2;
			centernum = i + nowAA_16[2];
		}
		else if (nowAA_16[2] % 3 == 2)//二個
		{
			i += 1;
			centernum = i + nowAA_16[2];
		}
		else
		{
			centernum = i + nowAA_16[2];
		}
		//右側設定
		j = nowAA_16[0];

		while (j != g_AA.size())
		{
			//最大数になったとき
			if (i == centernum)
			{
				break;
			}
			g_AA[i] = g_TempAA[j];
			i++;
			j++;
		}

		//判定
		if (nowAA_16[2] % 3 == 1)//上にいっこ
		{
			//音楽開始番号
			j = CENTER_DEFAULT_16 - 3;
		}
		else if (nowAA_16[2] % 3 == 2)//二個
		{
			j = CENTER_DEFAULT_16 - 2;
		}
		else {
			j = CENTER_DEFAULT_16 - 1;
		}

		i = nowAA_16[1];

		//左側設定
		while (j != 0)
		{
			//0以下になったとき
			if (i == nowAA_16[0] - 1)
			{
				break;
			}
			g_AA[j] = g_TempAA[i];
			i--;
			j--;
		}
	}
	return CENTER_DEFAULT_16;
}

int SelectMusic16keys::RestorationAA(int *close_sound, const std::shared_ptr<Music> &musicP)
{
	//変数
	int i = 0;
	int j = 0;
	int centernum = 0;
	//総計からフォルダを決定する
	int musicsum = 0;
	//AA表示に変更
	selectMode = SelectMode::AA;
	//0からそのフォルダまでの音楽総計
	while (i != selectfoldernum + 1)
	{
		//曲数
		musicsum += musicP->elebeatSong->m_MusicNumAt[i];
		i++;
	}

	//グローバルに渡す
	//最初の音楽番号
	nowAA_16[0] = musicsum - musicP->elebeatSong->m_MusicNumAt[selectfoldernum];
	//最後の音楽番号
	nowAA_16[1] = musicsum - 1;
	//音楽総計
	nowAA_16[2] = nowAA_16[1] - nowAA_16[0] + 1;

	if (nowAA_16[2] % 3 == 0)
		maxline_AA = nowAA_16[2] / 3;
	else
		maxline_AA = (nowAA_16[2] / 3) + 1;

	for (size_t i = 0; i < g_AA.size(); ++i)
	{
		g_AA[i] = NULL;
	}

	//フォルダ内部の曲が12以下の時
	if (musicP->elebeatSong->m_MusicNumAt[selectfoldernum] <= 12)
	{
		stop[1] = 1;
		i = CENTER_DEFAULT_16;
		j = nowAA_16[0];

		while (i != g_AA.size())
		{
			if (j == nowAA_16[1] + 1)
				return CENTER_DEFAULT_16;

			g_AA[i] = g_TempAA[j];
			i++;
			j++;
		}
	}
	else
	{
		stop[1] = 0;

		i = CENTER_DEFAULT_16;
		j = nowAA_16[0];

		while (i != g_AA.size())
		{
			if (j == nowAA_16[1] + 1) break;
			g_AA[i] = g_TempAA[j];
			i++;
			j++;
		}

		if (nowAA_16[2] % 3 == 1)
		{
			i += 2;
			centernum = i + nowAA_16[2];
		}
		else if (nowAA_16[2] % 3 == 2)
		{
			i += 1;
			centernum = i + nowAA_16[2];
		}
		else
		{
			centernum = i + nowAA_16[2];
		}
		j = nowAA_16[0];

		while (j != g_AA.size())
		{
			if (i == centernum)
			{
				break;
			}
			g_AA[i] = g_TempAA[j];
			i++;
			j++;
		}

		if (nowAA_16[2] % 3 == 1)
			j = CENTER_DEFAULT_16 - 3;
		else if (nowAA_16[2] % 3 == 2)
			j = CENTER_DEFAULT_16 - 2;
		else
			j = CENTER_DEFAULT_16 - 1;

		i = nowAA_16[1];

		while (j != 0)
		{
			if (i == nowAA_16[0] - 1) break;
			g_AA[j] = g_TempAA[i];
			i--;
			j--;
		}
	}

	PlaySoundMem(*close_sound, DX_PLAYTYPE_BACK);
	return CENTER_DEFAULT_16;
}

//デバッグ
void SelectMusic16keys::debug(int nowcenter, int nowselectAAnum)
{
	Color &color = Color::GetInstance();
	DrawFormatString(0, 690, color.White(), _T("nowcenter %d"), nowcenter);

	if (selectMode == SelectMode::FOLDER)
	{
		DrawFormatString(0, 750, color.White(), _T("number %d"), this->NowFolderNumber_16(nowcenter));
		DrawFormatString(0, 730, color.White(), _T("maxline_folder %d"), maxline_folder);
		DrawFormatString(0, 710, color.White(), _T("nowline %d"), (nowcenter - CENTER_DEFAULT_16) / 3);
	}
	else
	{
		DrawFormatString(0, 750, color.White(), _T("number %d"), this->NowAANumber_16(nowcenter));
		DrawFormatString(0, 730, color.White(), _T("セレクト描画番号%d 音楽番号%d"), nowselectAAnum, nowselectAAnum + nowAA_16[0]);
		DrawFormatString(0, 710, color.White(), _T("nowAA[0] %d nowAA[1] %d nowAA[2] %d"), nowAA_16[0], nowAA_16[1], nowAA_16[2]);
	}
	musicNumber = nowselectglobalAAnum + nowAA_16[0];
}

//ライン数を取得
int SelectMusic16keys::GetMaxLineFolder(const std::shared_ptr<Music> &musicP)
{
	auto maxLine = 0;
	//ライン数を出す
	if (musicP->elebeatSong->m_FolderNum % 3 == 0)
		maxLine = musicP->elebeatSong->m_FolderNum / 3;
	else
		maxLine = (musicP->elebeatSong->m_FolderNum / 3) + 1;
	return maxLine;
}

//音楽存在確認 必要なのはnowcenterと選択番号 (場合によるのでglobal nowcenterに任せない)
bool SelectMusic16keys::SelectFolderMusicExist(int nowcenter, const std::shared_ptr<Music> &musicP)
{
	//開いたフォルダ番号取得
	int num = this->NowFolderNumber_16(nowcenter);
	//選択した音楽番号
	num += selectkeynumber;
	if (static_cast<int>(musicP->elebeatSong->m_MusicNumAt.size()) <= num)
		return 0;
	//フォルダ内部の曲が0の時
	if (musicP->elebeatSong->m_MusicNumAt[num] == 0)
		return 0;
	else
		return 1;
}

//音楽存在確認　folder内部でのAA選択ver 音楽存在でreturn1
bool SelectMusic16keys::SelectAAMusicExist(const int nowcenter) const
{
	int num = this->NowAANumber_16(nowcenter) + selectkeynumber + nowAA_16[0];
	if (num <= nowAA_16[1])
		return 1;
	else
		return 0;
}

int SelectMusic16keys::MaxNum(const int num[]) const
{
	int max = 0;
	int length = sizeof(num) / sizeof(num[0]);
	for (auto i = 0; i < length; i++)
	{
		if (max < num[i])
			max = num[i];
	}
	return max;
}

int SelectMusic16keys::GetRandomMusicNum(SelectMode selectMode, int *difselect, const std::shared_ptr<Music> &musicP)
{
	std::random_device rnd;
	std::mt19937 mt(rnd());
	std::uniform_int_distribution<int> dist;

	bool canPlay = false;
	bool canPlayDif[3] = {};
	int num = 0;
	int count = 1;

	for (int i = 0; i < 3; i++)
	{
		if (MaxNum(musicP->elebeatSong->m_ElebeatDifficulty[1][i].data()) != 0)
			canPlayDif[i] = true;
	}

	while (!canPlay)
	{
		if (!canPlayDif[*difselect])
		{
			*difselect = ((*difselect + 1) + 3) % 3;
			count = 1;
		}

		if (count % 100 == 0)
		{
			*difselect = ((*difselect + 1) + 3) % 3;
		}
		++count;

		//フォルダ選択中ランダムの範囲(現在未使用)
		if (selectMode == SelectMode::FOLDER)
		{
			dist = std::uniform_int_distribution<int>(0, musicP->elebeatSong->m_TotalMusicNum - 1);
		}
		//AA選択中ランダムの範囲
		else
		{
			dist = std::uniform_int_distribution<int>(nowAA_16[0], nowAA_16[1]);
		}

		canPlay = true;
		num = dist(mt);

		if (musicP->elebeatSong->m_ElebeatDifficulty[1][*difselect][num] == 0)
			canPlay = false;
	}
	return num;
}

//ランダムでフォルダを開く
int SelectMusic16keys::GetRandomFolderNum(int nowcenter, const std::shared_ptr<Music> &musicP)
{
	int num = 0;
	std::random_device rnd;
	std::mt19937 mt(rnd());
	std::uniform_int_distribution<int> dist(1, musicP->elebeatSong->m_FolderNum - 1);
	bool canOpen = false;

	while (!canOpen)
	{
		canOpen = true;
		num = dist(mt);

		int count = (num + nowcenter - CENTER_DEFAULT_16) / 12;
		for (int i = 0; i < count; i++)
		{
			nowcenter += 12;
			num -= 12;
		}

		selectkeynumber = num;

		if (!SelectFolderMusicExist(nowcenter, musicP))
			canOpen = false;
	}

	return nowcenter;
}

//folder -> AA overも入れてるのでこの関数で完成
int SelectMusic16keys::OpenFolder(int *open_sound, const std::shared_ptr<Music> &musicP)
{
	//int
	int centernum = 0;
	//nowcenter の内部的保持変数
	int selecfoldernum = 0;

	//前のフォルダを保持
	//centernum はtempのnowcenter
	beforecenter_16 = g_NowCenter;

	//正常な時があるのでまず正常な時のを保持しておく
	centernum = g_NowCenter;

	//ストップしないのでoverselect処理
	if (stop[0] == 0)
	{
		//マイナスの時
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == -1 && selectkeynumber > 2) //右端over
		{
			//セレクト位置変更
			selectkeynumber -= 3;
			if (selectkeynumber >= 0 && selectkeynumber <= 2)
			{
				//3はdef追加番号　以下このように増えていく
				centernum = CENTER_DEFAULT_16;
			}
			if (selectkeynumber >= 3 && selectkeynumber <= 5)
			{
				centernum = CENTER_DEFAULT_16 + 3;
				selectkeynumber -= 3;
			}
			if (selectkeynumber >= 6 && selectkeynumber <= 8)
			{
				centernum = CENTER_DEFAULT_16 + 6;
				selectkeynumber -= 6;
			}
		}
		//右端２個over
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == -2 && selectkeynumber > 5)
		{
			//セレクト位置変更
			selectkeynumber -= 6;
			if (selectkeynumber >= 0 && selectkeynumber <= 2)
			{
				//3はdef追加番号　以下このように増えていく
				centernum = CENTER_DEFAULT_16;
			}
			if (selectkeynumber >= 3 && selectkeynumber <= 5)
			{
				centernum = CENTER_DEFAULT_16 + 3;
				selectkeynumber -= 3;
			}
		}
		//右端３個over
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == -3 && selectkeynumber > 8)
		{
			//セレクト位置変更
			selectkeynumber -= 9;
			//右端はdef番号なのでこれ
			centernum = CENTER_DEFAULT_16;
		}

		//プラスの時処理
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == maxline_folder - 1 && selectkeynumber >= 4)
		{
			//セレクト位置変更
			selectkeynumber -= 3;

			if (selectkeynumber >= 0 && selectkeynumber <= 2)
			{
				//3はdef追加番号　以下このように増えていく
				centernum = CENTER_DEFAULT_16;
			}
			if (selectkeynumber >= 3 && selectkeynumber <= 5)
			{
				centernum = CENTER_DEFAULT_16 + 3;
				selectkeynumber -= 3;
			}
			if (selectkeynumber >= 6 && selectkeynumber <= 8)
			{
				centernum = CENTER_DEFAULT_16 + 6;
				selectkeynumber -= 6;
			}
		}

		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == maxline_folder - 2 && selectkeynumber >= 7) //２個over
		{
			//セレクト位置変更
			selectkeynumber -= 6;
			if (selectkeynumber >= 0 && selectkeynumber <= 2)
			{
				//3はdef追加番号　以下このように増えていく
				centernum = CENTER_DEFAULT_16;
			}
			if (selectkeynumber >= 3 && selectkeynumber <= 5)
			{
				centernum = CENTER_DEFAULT_16 + 3;
				selectkeynumber -= 3;
			}
		}
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == maxline_folder - 3 && selectkeynumber >= 10) //一個
		{
			//セレクト位置変更
			selectkeynumber -= 9;
			//右端はdef番号なのでこれ
			centernum = CENTER_DEFAULT_16;
		}
	}

	//TODO:フォルダをランダムで開く
	if (centernum == CENTER_DEFAULT_16 && selectkeynumber == 0)
	{
		centernum = GetRandomFolderNum(centernum, musicP);
		//AA設定
		g_NowCenter = this->SetSelectAA(centernum, musicP);
		PlayMusicMem(*open_sound, DX_PLAYTYPE_BACK);
		return 0;
	}

	//音楽存在確認
	//音楽存在しないなら
	if (this->SelectFolderMusicExist(centernum, musicP) == 0)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("nomusic"), _T("nomusic"), MB_OK);
		//AAを元に戻す
		g_NowCenter = centernum;
	}
	else //音楽存在時
	{
		//AA設定
		g_NowCenter = this->SetSelectAA(centernum, musicP);
		PlayMusicMem(*open_sound, DX_PLAYTYPE_BACK);
	}
	return 0;
}
//AAセレクト時
int SelectMusic16keys::AASelect(void)
{
	int centernum = 0;
	//通常時の処理を追記 通常じゃなかったら後で上書き
	centernum = g_NowCenter;

	//12枚以下じゃないとき ストップしないとき
	if (stop[1] == 0)
	{
		//マイナスの時の処理
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == -1 && selectkeynumber > 2)
		{
			//右端over
			//セレクト位置変更
			selectkeynumber -= 3;
			if (selectkeynumber >= 0 && selectkeynumber <= 2)
			{
				//3はdef追加番号　以下このように増えていく
				centernum = CENTER_DEFAULT_16;
			}
			if (selectkeynumber >= 3 && selectkeynumber <= 5)
			{
				centernum = CENTER_DEFAULT_16 + 3;
				selectkeynumber -= 3;
			}
			if (selectkeynumber >= 6 && selectkeynumber <= 8)
			{
				centernum = CENTER_DEFAULT_16 + 6;
				selectkeynumber -= 6;
			}
		}
		//右端２個over
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == -2 && selectkeynumber > 5)
		{
			//セレクト位置変更
			selectkeynumber -= 6;
			if (selectkeynumber >= 0 && selectkeynumber <= 2)
			{
				//3はdef追加番号　以下このように増えていく
				centernum = CENTER_DEFAULT_16;
			}
			if (selectkeynumber >= 3 && selectkeynumber <= 5)
			{
				centernum = CENTER_DEFAULT_16 + 3;
				selectkeynumber -= 3;
			}
		}
		//右端３個over
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == -3 && selectkeynumber > 8)
		{
			//セレクト位置変更
			selectkeynumber -= 9;
			//右端はdef番号なのでこれ
			centernum = CENTER_DEFAULT_16;
		}

		//プラスの時処理
		//3個over
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == maxline_AA - 1 && selectkeynumber >= 3)
		{
			//セレクト位置変更
			selectkeynumber -= 3;
			if (selectkeynumber >= 0 && selectkeynumber <= 2)
			{
				//3はdef追加番号　以下このように増えていく
				centernum = CENTER_DEFAULT_16;
			}
			if (selectkeynumber >= 3 && selectkeynumber <= 5)
			{
				centernum = CENTER_DEFAULT_16 + 3;
				selectkeynumber -= 3;
			}
			if (selectkeynumber >= 6 && selectkeynumber <= 8)
			{
				centernum = CENTER_DEFAULT_16 + 6;
				selectkeynumber -= 6;
			}
		}
		//２個over
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == maxline_AA - 2 && selectkeynumber >= 6)
		{
			//セレクト位置変更
			selectkeynumber -= 6;
			if (selectkeynumber >= 0 && selectkeynumber <= 2)
			{
				//3はdef追加番号　以下このように増えていく
				centernum = CENTER_DEFAULT_16;
			}
			if (selectkeynumber >= 3 && selectkeynumber <= 5)
			{
				centernum = CENTER_DEFAULT_16 + 3;
				selectkeynumber -= 3;
			}
		}
		//一個
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == maxline_AA - 3 && selectkeynumber >= 9)
		{
			//セレクト位置変更
			selectkeynumber -= 9;
			//右端はdef番号なのでこれ
			centernum = CENTER_DEFAULT_16;
		}
	}

	//前と同じのセレクトしたら難易度変更
	if (this->NowAANumber_16(centernum) + selectkeynumber == beforeselectAA)
	{    //同じAA選択したら　難易度変更
		if (difselect >= 2)
			difselect = 0;
		else
			difselect++;
	}
	else
	{
		//前と同じじゃないとき前のAA保持
		beforeselectAA = this->NowAANumber_16(centernum) + selectkeynumber;
	}

	//Randの時
	if (centernum == 300 && selectkeynumber == 0)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("rand"), _T("rand"), MB_OK);
		return -1;
	}

	//音楽存在しない
	if (SelectAAMusicExist(centernum) == 0)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("nomusic"), _T("nomusic"), MB_OK);
		return -1;
	}

	nowselectglobalAAnum = this->NowAANumber_16(centernum) + selectkeynumber;

	return 0;
}

//マーカーセレクト　これで動かす
int SelectMusic16keys::MarkerTouch(void)
{
	int centernum = 0;

	//通常時の処理を追記 通常じゃなかったら後で上書き
	centernum = g_NowCenter;

	if (stop[2] == 0) {//12枚以下じゃないとき ストップしないとき
		//マイナスの時の処理
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == -1 && selectkeynumber > 2)
		{
			//セレクト位置変更
			selectkeynumber -= 3;
			if (selectkeynumber >= 0 && selectkeynumber <= 2)
			{
				//3はdef追加番号　以下このように増えていく
				centernum = CENTER_DEFAULT_16;
			}
			if (selectkeynumber >= 3 && selectkeynumber <= 5)
			{
				centernum = CENTER_DEFAULT_16 + 3;
				selectkeynumber -= 3;
			}
			if (selectkeynumber >= 6 && selectkeynumber <= 8)
			{
				centernum = CENTER_DEFAULT_16 + 6;
				selectkeynumber -= 6;
			}
		}
		//右端２個over
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == -2 && selectkeynumber > 5)
		{
			//セレクト位置変更
			selectkeynumber -= 6;
			if (selectkeynumber >= 0 && selectkeynumber <= 2)
			{
				//3はdef追加番号　以下このように増えていく
				centernum = CENTER_DEFAULT_16;
			}
			if (selectkeynumber >= 3 && selectkeynumber <= 5) {
				centernum = CENTER_DEFAULT_16 + 3;
				selectkeynumber -= 3;
			}
		}
		//右端３個over
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == -3 && selectkeynumber > 8)
		{
			//セレクト位置変更
			selectkeynumber -= 9;
			//右端はdef番号なのでこれ
			centernum = CENTER_DEFAULT_16;
		}

		//プラスの時処理
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == maxline_marker - 1 && selectkeynumber >= 3)
		{
			//セレクト位置変更
			selectkeynumber -= 3;
			if (selectkeynumber >= 0 && selectkeynumber <= 2)
			{
				//3はdef追加番号　以下このように増えていく
				centernum = CENTER_DEFAULT_16;
			}
			if (selectkeynumber >= 3 && selectkeynumber <= 5)
			{
				centernum = CENTER_DEFAULT_16 + 3;
				selectkeynumber -= 3;
			}
			if (selectkeynumber >= 6 && selectkeynumber <= 8)
			{
				centernum = CENTER_DEFAULT_16 + 6;
				selectkeynumber -= 6;
			}
		}
		//２個over
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == maxline_marker - 2 && selectkeynumber >= 6)
		{
			//セレクト位置変更
			selectkeynumber -= 6;
			if (selectkeynumber >= 0 && selectkeynumber <= 2)
			{
				//3はdef追加番号　以下このように増えていく
				centernum = CENTER_DEFAULT_16;
			}
			if (selectkeynumber >= 3 && selectkeynumber <= 5)
			{
				centernum = CENTER_DEFAULT_16 + 3;
				selectkeynumber -= 3;
			}
		}
		//一個
		if ((g_NowCenter - CENTER_DEFAULT_16) / 3 == maxline_marker - 3 && selectkeynumber >= 9)
		{
			//セレクト位置変更
			selectkeynumber -= 9;
			//右端はdef番号なのでこれ
			centernum = CENTER_DEFAULT_16;
		}
	}

	//前と同じのセレクトしたら難易度変更
	if (NowAANumber_16(centernum) + selectkeynumber == selectMerker)
	{
		//同じAA選択したら　難易度変動
		//同じマーカー選択なので判定がどんなふうか書く
		return 1;
	}
	else //前と同じじゃないとき
	{
		//前のAA保持
		beforeselectmarker = NowAANumber_16(centernum) + selectkeynumber;
	}
	//選択したマーカー番号をglobalに
	selectMerker = beforeselectmarker;

	return 0;
}

//デストラクタ
void SelectMusic16keys::DelSelectMusic_16(int& loopsound, int& back_sound)
{
	//音楽
	DeleteSoundMem(loopsound);
	DeleteSoundMem(back_sound);
}