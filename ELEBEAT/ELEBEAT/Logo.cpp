#include "DxLib.h"
#include "Logo.h"
#include "extern.h"
#include "SceneManager.h"

//ロゴ表示画面
void Logo::Initialize(const std::shared_ptr<Music> &musicP)
{
	//描画可能領域セット.
	SetDrawArea(0, 0, WIN_WIDTH, WIN_HEIGHT);
	logoImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Logo/elebeat.png"));
	//裏画面に設定.
	DxLib::SetDrawScreen(DX_SCREEN_BACK);
}

void Logo::Update(const std::shared_ptr<Music>& musicP)
{
	m_count++;

	if (WAIT_COUNT - m_count < 0)
	{
		SceneManager::SetNextScene(Scenes::TITLE);
		return;
	}

	DxLib::ClearDrawScreen();

	if (m_count <= 255 / 5)
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, m_count * 5);

	if (m_count > 255 / 5)
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, (WAIT_COUNT - m_count) * 5);

	DxLib::DrawRotaGraph(480, 384, 1.0f, 0, logoImage, TRUE);
	DxLib::ScreenFlip();
}

void Logo::Finalize(const std::shared_ptr<Music> &musicP)
{
	DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
}