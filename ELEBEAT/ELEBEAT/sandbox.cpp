﻿#include "DxLib.h"
#include "extern.h"

#define PI2 (3.141592654f*2)



int m_imgMask = 0;
int m_imgOverLay = 0;
int m_Count = 0;

void drawmask(void)
{
	//画像ロード
	m_imgMask = LoadMask(_T("img/menu_back_mask.png"));
	m_imgOverLay = LoadGraph(_T("img/menu_back_over.png"));
	m_Count = 0;

	//main768 384
	while (1) {
		ClearDrawScreen();
		m_Count++;
		CreateMaskScreen();     // マスクここから
		DrawMask(0, 0, m_imgMask, DX_MASKTRANS_BLACK); // 黒い所のみを描画領域に
		SetDrawBlendMode(DX_BLENDMODE_ADD, 255);               // 加算ブレンドに設定
		DrawRotaGraph2(512, 384, 250, 110, 2.45, PI2 / 240 * m_Count, m_imgOverLay, TRUE);
		DrawRotaGraph2(512, 384, 250, 110, 2.45, PI2 / 240 * (m_Count - 120), m_imgOverLay, TRUE);
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);     // ブレンドモードリセット
		DeleteMaskScreen(); // マスクここまで
		ScreenFlip();
	}
}
/*
void netconnect(void){
	WSADATA wsaData;//構造体らしい　しらんがな(´・ω・`)　次の文でつかう
	int err;                    //WSAStartupが戻り値としてだした変数を保持　んでswitchで判断

	err = WSAStartup(MAKEWORD(2,0), &wsaData);//1はver　2は初期化状態を保存する変数　これでwinsocketつかえるで

	if (err != 0) {//エラー内容によって変わる処理
		switch (err) {
		case WSASYSNOTREADY://ネットワークサブシステムがネットワークへの接続を準備できていない
			printf("WSASYSNOTREADY\n");
			break;
		case WSAVERNOTSUPPORTED://要求されたwinsockのバージョンがサポートされていない
			printf("WSAVERNOTSUPPORTED\n");
			break;
		case WSAEINPROGRESS://ブロッキング操作の実行中であるか、またはサービスプロバイダがコールバック関数を処理している
			printf("WSAEINPROGRESS\n");
			break;
		case WSAEPROCLIM://    winsockが処理できる最大プロセス数に達した
			printf("WSAEPROCLIM\n");
			break;
		case WSAEFAULT://    第二引数であるlpWSAData は有効なポインタではない
			printf("WSAEFAULT\n");
			break;
		}
	}

	WSACleanup();//winsocketを終了する
}
*/

//////----------------------エフェクトテスト
/*
	//debug
	char *cstring = TEXT("The border land was wrapped in Scarlet Magic. Girls believe that you solve this mystory...﻿");
	//char *string = "Girls believe that you solve this mystory...";
	int color = GetColor(255,255,255);
	int red = GetColor(255,36,0);
	int i_de = 0;
	int strlen_hen = 0;
	int strnum = 24;        //元文字大きさ

	while(1){
		if(GetDrawStringWidth(cstring,strlen(cstring)) < WIN_WIDTH + 20){
			break;
		}else{
			strnum--;
			SetFontSize(strnum);
		}
	}

	SetFontSize(strnum);

	strnum = GetDrawStringWidth(cstring,strlen(cstring));

	ClearDrawScreen() ;

	DrawBox(0,0,WIN_WIDTH,WIN_HEIGH,GetColor(255,255,255),TRUE);

	ScreenFlip();

	while(1){
		DrawBox(0,WIN_HEIGH-200-50,i_de,WIN_HEIGH-200+50,red,TRUE);

		DrawFormatString(WIN_WIDTH - strnum -10,WIN_HEIGH-200+10,color,cstring);

		i_de += 50;

		ScreenFlip();

		if(i_de >= 7000)
			break;
	}

	ClearDrawScreen() ;

	DrawBox(0,0,WIN_WIDTH,WIN_HEIGH,GetColor(255,255,255),TRUE);

	ScreenFlip();

	char randstr[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char drawstr[3] = {};
	i_de = -100;

	while(1){
		ClearDrawScreen() ;

		if(i_de <= 15){
			DrawFormatString(100,10,color,"%c",randstr[rand()%27]);
		}else{
			DrawFormatString(100,10,color,"E");
		}

		if(i_de <= 30){
			DrawFormatString(120,10,color,"%c",randstr[rand()%27]);
		}else{
			DrawFormatString(120,10,color,"L");
		}

		if(i_de <= 45){
			DrawFormatString(140,10,color,"%c",randstr[rand()%27]);
		}else{
			DrawFormatString(140,10,color,"E");
		}

		if(i_de <= 60){
			DrawFormatString(160,10,color,"%c",randstr[rand()%27]);
		}else{
			DrawFormatString(160,10,color,"B");
		}

		if(i_de <= 75){
			DrawFormatString(180,10,color,"%c",randstr[rand()%27]);
		}else{
			DrawFormatString(180,10,color,"E");
		}

		if(i_de <= 90){
			DrawFormatString(200,10,color,"%c",randstr[rand()%27]);
		}else{
			DrawFormatString(200,10,color,"A");
		}

		if(i_de <= 105){
			DrawFormatString(220,10,color,"%c",randstr[rand()%27]);
		}else{
			DrawFormatString(220,10,color,"T");
		}
		i_de++;

		ScreenFlip();

		if(i_de >= 150)
			break;
	}
*/
////-----------------------------------------------------------------------------------