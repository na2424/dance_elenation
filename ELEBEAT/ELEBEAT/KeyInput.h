#ifndef KEYINPUT_CLASS_H
#define KEYINPUT_CLASS_H
#include <array>
#include <boost/timer.hpp>

//キー入力時に変動する変数の構造体
class KeyInput
{
private:
	static const int MAX_NUM = 4;
public:
	int m_KeyTime[MAX_NUM][MAX_NUM];            //左キーを押した時の時間を入れる変数(初期値は0)
	int m_Diff[MAX_NUM][MAX_NUM];
	int m_Min[MAX_NUM][MAX_NUM];
	int m_B[MAX_NUM][MAX_NUM];    //キーを押したとき、押されるべき時間に近いノートを反応させるのに使う変数。時間距離を入れる。(bの初期値は0)
	int m_Place[MAX_NUM][MAX_NUM];

	KeyInput() :
		m_KeyTime(),
		m_B(),
		m_Place()
	{
		std::fill(m_Diff[0], m_Diff[MAX_NUM - 1], 800);
		std::fill(m_Min[0], m_Min[MAX_NUM - 1], 1000);
	}
};

//キー入力時に変動する変数の構造体
class KeyInput4Key
{
private:
	static const int MAX_NUM = 4;
public:
	std::array<int, MAX_NUM> m_KeyTime;            //左キーを押した時の時間を入れる変数(初期値は0)
	std::array<int, MAX_NUM> m_Diff;
	std::array<int, MAX_NUM> m_Min;
	std::array<int, MAX_NUM> m_B;    //キーを押したとき、押されるべき時間に近いノートを反応させるのに使う変数。時間距離を入れる。(bの初期値は0)
	std::array<int, MAX_NUM> m_Row;
	std::array<int, MAX_NUM> m_RFlag;
	std::array<double, MAX_NUM> m_ReleaseTimes;
	std::array<boost::timer, MAX_NUM> m_ReleaseTime;    //longnote

	KeyInput4Key() :
		m_KeyTime(),
		m_B(),
		m_Row(),
		m_RFlag(),
		m_ReleaseTimes()

	{
		m_Diff.fill(800);
		m_Min.fill(1000);
	}
};

#endif