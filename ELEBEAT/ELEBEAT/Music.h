#ifndef MUSIC_CLASS_H
#define MUSIC_CLASS_H

#include <array>
#include <vector>
#include <memory>

#include "define.h"

class ElebeatSong;
class DanceSong;

class Music
{
public:
	Music();
	//文字変数　音楽のパスとかAAパスとか
	std::unique_ptr<ElebeatSong> elebeatSong;
	std::unique_ptr<DanceSong> danceSong;
};

class ElebeatSong
{
public:
	ElebeatSong();
	std::vector<tstring> m_Name;
	std::vector<tstring> m_SubName;
	std::vector<tstring> m_Artist;
	std::vector<tstring> m_AlbumArtPath;
	std::vector<tstring> m_MusicPath;
	std::vector<tstring> m_Offset;
	std::vector<std::vector<tstring> > m_Bpm;
	std::vector<std::vector<tstring> > m_BpmPositions;

	std::vector<tstring> m_FolderName;
	std::vector<tstring> m_SequencePath;

	std::vector<int> m_ElebeatDifficulty[2][3];

	std::vector<tstring> m_FolderAlbumArtPath;

	std::vector<int> m_MusicNumAt;

	int m_FolderNum;
	int m_TotalMusicNum;

	void PushBackEmpty(void);
};

class DanceSong
{
public:
	DanceSong();
	std::vector<tstring> m_Name;
	std::vector<tstring> m_SubName;
	std::vector<tstring> m_Artist;
	std::vector<tstring> m_BannerPath;
	std::vector<tstring> m_BackGroundPath;
	std::vector<tstring> m_MusicPath;
	std::vector<tstring> m_Offset;
	std::vector<std::vector<tstring> > m_Bpm;
	std::vector<std::vector<tstring> > m_BpmPositions;
	std::vector<std::vector<tstring> > m_Stop;
	std::vector<std::vector<tstring> > m_StopPositions;

	std::vector<tstring> m_FolderName;
	std::vector<tstring> m_SequencePath;

	std::vector<int> m_Difficulty[5];
	std::vector<std::array<std::array<double, 5>, 5> > m_RadarChart;

	//その他画像
	std::vector<tstring> m_FolderBannerPath;
	std::vector<int> m_MusicNumAt;

	int m_FolderNum;
	int m_TotalMusicNum;

	void Reserve(int num);
	void PushBackEmpty(void);
};

#endif