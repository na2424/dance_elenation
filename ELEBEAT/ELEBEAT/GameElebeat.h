﻿#ifndef GAME_ELEBEAT_H
#define GAME_ELEBEAT_H
#include "GameBase.h"
#include "SceneManager.h"
#include "NoteELEBEAT.h"

class ScoreSt;
class Combo;
class KeyInput;

class GameElebeat : public GameBase
{
private:

    SelectKeys m_enumKeys;

	std::shared_ptr<NoteElebeat> m_note;
	std::shared_ptr<ScoreSt> m_sScore;
	std::shared_ptr<Combo> m_sCombo;
	std::shared_ptr<KeyInput> m_sKeyinput;

	std::vector<std::vector<std::vector<bool> > > m_isCraped;

    //--------------------
    // 定数.
    //--------------------
    const int MASU_Y = 86;

    //--------------------
    // 画像.
    //--------------------
    std::array<int, 2> m_judgeAreaImages;
    std::array<std::array<int, 10>, 3> m_difficultyNumberImages;

    Image startImage;
	Image comboImage;
	Image elebeatImage;
	Image readyImage;
	Image goImage;

    //--------------------
    // 音源.
    //--------------------
    Sound readyGoSound;
	Sound backSound;
	Sound musicSound;
	Sound handCrapSound;

    //--------------------
    // その他変数.
    //--------------------
    int m_matrix;
    int m_fontHandle;
    double m_musicEndTime;
	std::array<int,3> m_difColors;
    int m_scoreFrame;
    int tempClapedTime;
    int m1, m2, m3, m4;
    int keysPosition;
    int st;
    //Timing
	std::vector<double> m_bpm;
	std::vector<double> m_bpmPosition;
    double m_offset;
	std::vector<double> m_bpmTimeSpan;

    //---------------------
    // 関数.
    //---------------------
    void InitMatrix();
    void LoadGameImage();
    void LoadGameSound();
    void SetBpmList(const std::shared_ptr<Music>& musicP);
    void SetOffset(const std::shared_ptr<Music>& musicP);
    void SetBpmTimeSpan();
    bool LoadNote(const tstring& lineString);
	void DrawDifficulty(const std::shared_ptr<Music>& musicP, int dif) const;
	void DrawReadyGo(const int nowTime) const;
    void DrawCombo(const std::shared_ptr<Combo>& fCombo) override;
    void FadeInScene() const;

public:
    GameElebeat();
    void Initialize(const std::shared_ptr<Music> &musicP) override;
    void Update(const std::shared_ptr<Music>& musicP) override;
    void Finalize(const std::shared_ptr<Music> &musicP) override;
};


#endif