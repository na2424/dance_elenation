#ifndef OPTION_H
#define OPTION_H
#include <vector>
#include <array>
#include <memory>
#include "DxLib.h"
#include "SceneManager.h"
#include "LuaHelper.h"

class Music;
class KeyConfig;
class OptionSaveData;
class MouseClickObject;

class Option : public Scene
{
private:

	//Image
	Image m_optionMessageFrameImage;
	Image m_frameImage;
	Image m_optionTitleImage;
	Image m_paddingImage;
	Image m_optionRightCasolImage;

	//Sound
	Sound m_decideSound;
	Sound m_moveSound;
	Sound m_backBgm;
	Sound m_closeSound;

	//Lua
	lua_State *L;
	LuaHelper m_luaHelper;

	//クラス参照
	std::unique_ptr<KeyConfig> m_keyConfig;
	std::unique_ptr<MouseClickObject> m_mouseF;
	std::unique_ptr<OptionSaveData> m_saveDataOption;

	//メンバ変数
	bool m_isHideCasol3;
	int m_displayCasolX;
	int m_displayCasolY;
	int m_fontHandle;
	int m_optionFlag;
	int m_casolHigh;
	int m_casolHigh2;
	std::array<int, 8> m_casolHigh3;
	std::array<int, 8> m_casolFlag;

	std::vector<std::vector<int> > m_listSize;

	//定数
	const int PLACE_X = 260;
	const int PLACE_Y = 260;
	const int LINE_HEIGHT = 55;
	const int TEXT_SPACING = 330;
	const int SPACING = 55;

	///TODO:Luaで読み込む形式にする
	const std::vector<tstring> m_optionNameList = std::vector<std::string>
	{
		"Stage Settings",
		"Display Settings",
		"Key Config",
		"Reload Songs",
		"Exit"
	};

	const std::vector<tstring>  m_stageSettingList = std::vector<std::string>
	{
		"Event",
		"Stage",
		"Back To Top Options"
	};

	const std::vector<tstring>  m_displaySettingList = std::vector < std::string >
	{
		"FullScreen",
		"Show FPS",
		"Back To Top Options"
	};

	const std::vector<tstring> m_keyConfigList = std::vector<std::string>
	{
		"KeyConfig",
		"JoyPad",
		"Default",
		"Back To Top Options"
	};

	const std::vector<tstring> m_inputOffOnList = std::vector<tstring>
	{
		"OFF",
		"ON"
	};

	const std::vector<tstring> m_inputStageNumList = std::vector<tstring>
	{
		" 1",
		" 2",
		" 3",
		" 4",
		" 5"
	};

	const std::vector<std::vector<tstring> > m_optionMessageList = std::vector<std::vector<std::string> >
	{
		{"ステージに関する設定を行います。"}
		,
		{"ディスプレイに関する設定を行います。"}
		,
		{"キーコンフィグ。",
			"キーボードやジョイパッドで使用するキーを設定します。"}
		,
		{"曲データをロードし直します。"}
		,
		{"TITLE画面に戻ります。"}
	};

	const std::vector<std::vector<tstring> > m_stageMessageList = std::vector<std::vector<std::string> >
	{
		{"EVENT MODEにするかの設定を行います。",
			"OFF…無し、ON…１ゲームで何曲もプレイできます。"}
		,
		{ "EVENT MODE設定がOFFの時、",
		"１クレジットでプレイできる曲数を設定します。" }
		,
		{ "Option Topに戻ります。" }
	};

	const std::vector<std::vector<tstring> > m_displayMessageList = std::vector < std::vector<std::string> >
	{
		{"Full Screenモードにするかの設定を行います。",
		"OFF…Window、 ON…Full Screen"}
		,
		{ "ゲーム中FPSを表示するかの設定を行います。"}
		,
		{ "Option Topに戻ります。" }
	};

	const std::vector<std::vector<tstring> > m_keyConfigMessageList = std::vector < std::vector<std::string> >
	{
		{"キーボードのキーコンフィグを設定します。",
			"(キーコンフィグ設定中はカーソルキー、",
			"エンター、エスケープキーを使用します。)"}
		,
		{ "ゲームパッドのボタンの割り当てを行います。" ,
			"(キーコンフィグ設定中はカーソルキー、",
			"エンター、エスケープキーを使用します。)" }
		,
		{ "キーコンフィグを初期状態にします。" }
		,
		{ "Option Topに戻ります。" }
	};

public:

	//関数
	Option();
	void Initialize(const std::shared_ptr<Music> &musicP) override;
	void Update(const std::shared_ptr<Music>& musicP) override;
	void Finalize(const std::shared_ptr<Music> &musicP) override;

	void OptionCasol(void);
	void OptionCasol2(void);
	void OptionCasol2Detail(int size);

	void OnInputStageSettings(void);
	void OnInputDisplaySettings(void);
	void OnInputKeyConfig(void);
	void OnInputReloadSongs(void);
	void OnInputPlayerSettings(void);

	void MouseFunction(const std::vector<std::vector<int> > &size);
	void BackToTopOptions(void);

	void DrawCasol(int count);
	void DrawListName(const std::vector<tstring> &nameList) const;
	void DrawSelectOption(const std::vector<tstring> &select, int num, int savedata) const;
	void DrawOptionMessage(void);
	void DrawOptionMessage(const std::vector< std::vector<tstring> > &textList);
	void DrawOptionMessage(const std::vector<tstring> &textList);

	void InitLua();

	static void ChangeScreenMode(bool isWindow);
	void LoadData(void);
	void ReadLua(void);
	static void Set3DModel();

	void (Option::*m_pFunc[5 + 1])();

	void UpDateOption();
};

#endif