#ifndef LONG_NOTE_IMAGES_H
#define LONG_NOTE_IMAGES_H

#include <array>



//ロングノートの画像
class LongNotePic
{
public:

	std::array<int, 35> longnote_pic1;        //in画像
	std::array<int, 25> longnote_pic2[25];        //out画像
	std::array<int, 10> hold_pic[10];            //くるくるまわるアレ
	std::array<int, 6> hold_pic2[6];            //end(爆発)画像
	std::array<int, 4> judgeImage[4];                //ロングノートの判定画像を入れるための配列
	std::array<int, 2> longJudge[2];            //ロングノートのOK、NGの判定画像を入れるための配列

	LongNotePic()
	{
		longnote_pic1.fill(0);
		longnote_pic2->fill(0);
		hold_pic->fill(0);
		hold_pic2->fill(0);
		judgeImage->fill(0);
		longJudge->fill(0);
	}
};

#endif