#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <direct.h>
#include <Shlwapi.h>
#include <iostream>

#include "FileManager.h"
#include "DxLib.h"
#include "extern.h"
#include "Marker.h"

#include "Music.h"
#include "MarkerIni.h"
#include "GameBase.h"
#include "savedata_class.h"
#include "SelectMusic16keys.h"

#include "SpecialFiles.h"

#pragma comment( lib, "Shlwapi.lib" )
#pragma warning(disable:4996)


using namespace DxLib;

void FileManager::Init()
{
	m_isLoaded = false;
	m_isLoadedSmFile = false;
}

/*
unsigned __stdcall LoadMarkerPass(void *);   //マーカーロードLoadMarker
unsigned __stdcall LoadMusic(void *);        //音楽ロード
*/

void FileManager::CheckDirectory()
{
	const std::vector<tstring> passNames =
	{
		_T("Data"),
		_T("img"),
		_T("Markers"),
		_T("MMD"),
		_T("NoteSkins"),
		_T("ScreenShot"),
		_T("Songs"),
		_T("Songs4keys"),
		_T("Themes")
	};

	for each (tstring pass in passNames)
	{
		if (!PathFileExists(pass.c_str()))
			_tmkdir(pass.c_str());
	}
}

void FileManager::LoadMmdFile() const
{
	if (m_isLoaded) return;
	//MMDはファイルが大きい(主にテクスチャ)ので非同梱
	DxLib::printfDx(_T("3Dの読み込み"));
	ModelHandle = MV1LoadModel(_T("MMD/二重立方体.x"));
	DxLib::printfDx(_T("    [完了]\n"));
	DxLib::ScreenFlip();
}

void FileManager::LoadGlobalImage() const
{
	g_underBarImage = LoadGraph((SpecialFiles::DEFAULT_THEMES_DIR + tstring("Graphics/underbar.png")).c_str());
}

void FileManager::LoadNumberImage() const
{
	if (m_isLoaded) return;

	//リテラル読み込み
	DxLib::printfDx(_T("数字画像の読み込み"));

	//タイマー数字の画像
	LoadDivGraph(_T("Themes\\DANCE ELENATION\\Numbers\\timer_num.png"), 10, 10, 1, 80, 80, timerImage);

	//コンボ数字
	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/combo_num_pa.png"), 10, 10, 1, 80, 80, GameBase::g_ComboParfect4KeysImages.data());
	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/combo_num_gr.png"), 10, 10, 1, 80, 80, GameBase::g_ComboGrate4KeysImages.data());
	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/combo_num_go.png"), 10, 10, 1, 80, 80, GameBase::g_ComboGood4KeysImages.data());

	DxLib::printfDx(_T("    [完了]\n"));
	DxLib::ScreenFlip();
}

void FileManager::LoadMarker() const
{
	if (m_isLoaded) return;
	auto& marker = Marker::GetInstance();

	std::vector<MarkerIni> markerIni(30);

	DxLib::printfDx(_T("マーカー検索開始"));
	DxLib::ScreenFlip();

	WIN32_FIND_DATA fd;

	//マーカー検索
	//検索ディレクトリ指定
	auto hFind = FindFirstFile(_T("Markers/*"), &fd);
	auto markerNum = 0;

	do
	{
		//DOS文字を排除
		if (_tcschr(fd.cFileName, _T('.')) == nullptr)
		{
			//通常マーカー
			marker_handle_normal.emplace_back(std::vector<tstring>());
			//前を一個足す
			const auto markerIniPath = SpecialFiles::MARKERS_DIR + tstring(fd.cFileName) + _T("/Marker.ini");

			markerIni[markerNum].Load(markerIniPath);

			for (auto i = 0; i < markerIni[markerNum].Marker.m1; i++)
			{
				std::ostringstream ostr;
				ostr << SpecialFiles::MARKERS_DIR << fd.cFileName << _T("/") << i << _T(".png");
				//マーカー順に追加
				marker_handle_normal[markerNum].emplace_back(ostr.str());
			}

			//ほかマーカー
			marker_handle_perfect.emplace_back(std::vector<tstring>());
			marker_handle_grate.emplace_back(std::vector<tstring>());
			marker_handle_good.emplace_back(std::vector<tstring>());

			for (auto i = 0; i < markerIni[markerNum].Marker.m2; i++)
			{
				std::ostringstream ostr;
				ostr << SpecialFiles::MARKERS_DIR << fd.cFileName << "/perfect/" << i << ".png";
				marker_handle_perfect[markerNum].emplace_back(ostr.str());
				ostr.clear();
				ostr << SpecialFiles::MARKERS_DIR << fd.cFileName << "/grate/" << i << ".png";
				marker_handle_grate[markerNum].emplace_back(ostr.str());
				ostr.clear();
				ostr << SpecialFiles::MARKERS_DIR << fd.cFileName << "/good/" << i << ".png";
				marker_handle_good[markerNum].emplace_back(ostr.str());
			}

			//種類配列
			++markerNum;
		}
	} while (FindNextFile(hFind, &fd)); //次のファイルさがすよ！

	//マーカーの数
	marker.marker_num = markerNum;
	//検索終了
	FindClose(hFind);

	DxLib::printfDx(_T("    [完了]\n"));
	DxLib::ScreenFlip();
	DxLib::printfDx(_T("マーカーの読み込み"));
	DxLib::ScreenFlip();

	//title代入
	//-------------------------------------------------------------------------------------

	std::vector <int> row2;
	std::vector < std::vector < int > > row3;

	row3.emplace_back(row2);
	row3.emplace_back(row2);

	for (auto i = 0; i < marker.marker_num; i++)
	{
		noteMarkerImage.emplace_back(row2);
		noteJudgeImage.emplace_back(row3);
	}

	for (auto i = 0; i < marker.marker_num; i++)
	{
		noteJudgeImage[i].push_back(row2);
	}

	//検索ディレクトリ指定
	hFind = FindFirstFile(_T("Markers/*"), &fd);

	markerNum = 0;
	while (markerNum != marker.marker_num)
	{
		//DOS文字を排除
		if (_tcschr(fd.cFileName, '.') == nullptr)
		{
			//メインマーカー
			for (int j = 0; j < markerIni[markerNum].Marker.m1; j++)
			{
				noteMarkerImage[markerNum].emplace_back(LoadGraph(marker_handle_normal[markerNum][j].c_str()));
			}
			//判定マーカー
			for (int j = 0; j < markerIni[markerNum].Marker.m2; j++)
			{
				noteJudgeImage[markerNum][0].emplace_back(LoadGraph(marker_handle_perfect[markerNum][j].c_str()));
				noteJudgeImage[markerNum][1].emplace_back(LoadGraph(marker_handle_grate[markerNum][j].c_str()));
				noteJudgeImage[markerNum][2].emplace_back(LoadGraph(marker_handle_good[markerNum][j].c_str()));
			}
			marker_numb.emplace_back(markerIni[markerNum].Marker.m1);
			marker_numj.emplace_back(markerIni[markerNum].Marker.m2);
			marker_just.emplace_back(markerIni[markerNum].Marker.m3);
			marker_time.emplace_back(markerIni[markerNum].Marker.m4);

			++markerNum;
		}
		//次のファイルさがすよ！
		FindNextFile(hFind, &fd);
	}

	//検索終了
	FindClose(hFind);
	DxLib::printfDx(_T("　　[完了]\n"));
	DxLib::ScreenFlip();
}

void FileManager::LoadElebeatFile(std::shared_ptr<Music> &musicP) const
{
	auto& song = musicP->elebeatSong;
	constexpr auto STR_NUM = 10000;

	//UTF8日本語ロケールに変更.
	_wsetlocale(LC_ALL, L"");
	setlocale(LC_ALL, "ja_JP.UTF-8");

	WIN32_FIND_DATA fd;

	//音楽フォルダパスから音楽とか画像とか探すパスまでを保持
	std::vector<tstring> music_folder_handle;

	//txt内部変数
	std::vector<tstring> kind;                        //音ゲーの題名パスを保持
	std::vector<tstring> music_txt;                        //音ゲーのtxtを探す為のパス保存変数前が音ゲー種類真ん中音楽数
	std::vector<tstring> music_txt_front;                //\songs\jubeat\ + (曲名フォルダ)\ を保持する変数
	std::vector<tstring> music_handle;                    //音ゲー種類までのパス保持変数

	music_txt.emplace_back(tstring("\0"));
	music_txt_front.emplace_back(tstring("\0"));
	music_handle.emplace_back(tstring("\0"));
	music_folder_handle.emplace_back(_T("\0"));
	kind.emplace_back(_T("\0"));

	song->m_MusicNumAt.emplace_back(0);
	RandAAReplace.emplace_back(0);

	//folderここに追加
	song->m_FolderAlbumArtPath.emplace_back(_T("img/random-aa.png"));
	song->m_FolderName.emplace_back(_T("Random"));
	//folder情報を次へ
	auto folderCount = 0;
	folderCount++;
	//folderここまで追加

	//検索ディレクトリ指定
	auto hFind = FindFirstFile(_T("Songs/*"), &fd);

	//ファイル名保持
	do
	{
		//DOS文字を排除
		if (_tcschr(fd.cFileName, _T('.')) == nullptr)
		{
			if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				song->m_FolderName.emplace_back(fd.cFileName);
				//folderAA
				song->m_FolderAlbumArtPath.emplace_back(SpecialFiles::SONGS_DIR + tstring(fd.cFileName) + "/" + tstring(fd.cFileName) + ".png");
				music_handle.emplace_back(SpecialFiles::SONGS_DIR + tstring(fd.cFileName) + "/");
				kind.emplace_back(SpecialFiles::SONGS_DIR + tstring(fd.cFileName) + "/*");
				folderCount++;
			}
		}
	} while (FindNextFile(hFind, &fd)); //次のファイル

	//検索終了
	FindClose(hFind);

	//種類はkind[]で確認0がnull　なのでこれが12なら11配列まで存在する
	//フォルダ種類が何種類あるか保持しとくため
	song->m_FolderNum = folderCount;

	auto folderMusicCount = 1;
	auto musicCount = 0;

	//そしてtxtを探す為のパスを作る
	//音楽種類終了までいったんループ
	for (auto i = 1; i < song->m_FolderNum; ++i)
	{
		//検索ディレクトリ指定
		hFind = FindFirstFile(kind.at(i).c_str(), &fd);
		do
		{
			if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (_T('.') == fd.cFileName[0])
				{
					if ((_T('\0') == fd.cFileName[1]) || (_T('.') == fd.cFileName[1] && _T('\0') == fd.cFileName[2])) continue;
				}
				DxLib::clsDx();
				DxLib::ClearDrawScreen();
				DxLib::printfDx(_T(".eleファイルの検索\n"));
				DxLib::printfDx(_T("%s %s\n"), music_handle[i].c_str(), fd.cFileName);
				DxLib::ScreenFlip();
				music_txt.emplace_back(music_handle[i] + tstring(fd.cFileName) + "/*.ele");
				music_txt_front.emplace_back(music_handle[i].c_str() + tstring(fd.cFileName) + "/");
				tstring front = music_handle[i].c_str() + tstring(fd.cFileName) + "/";
				music_folder_handle.emplace_back(std::move(front));
				folderMusicCount++;
			}
		} while (FindNextFile(hFind, &fd));//次のファイルさがすよ！

		//最初のフォルダの時はそのまま音楽数代入
		if (musicCount == 0)
		{
			//フォルダの中身が0じゃないとき 初期化が1なのでこうなる
			if (folderMusicCount != 1)
			{
				song->m_MusicNumAt.emplace_back(folderMusicCount);
				musicCount = song->m_MusicNumAt[i];

				music_txt.emplace_back(_T("\0"));
				music_txt_front.emplace_back(_T("\0"));
				music_folder_handle.emplace_back(_T("\0"));
				folderMusicCount++;
			}
		}
		else
		{
			//フォルダの中身が0じゃないとき
			if (folderMusicCount != musicCount + 1)
			{
				song->m_MusicNumAt.emplace_back(folderMusicCount - musicCount);
				musicCount += song->m_MusicNumAt[i];

				music_txt.emplace_back(_T("\0"));
				music_txt_front.emplace_back(_T("\0"));
				music_folder_handle.emplace_back(_T("\0"));
				folderMusicCount++;
			}
		}
	}

	DxLib::printfDx(_T("    [完了]\n"));
	DxLib::ScreenFlip();

	//音楽総数
	for (auto num : song->m_MusicNumAt)
	{
		song->m_TotalMusicNum += num;
	}

	//検索終了
	FindClose(hFind);

	SelectMusic16keys::g_AA.resize(song->m_TotalMusicNum + 24);
	SelectMusic16keys::g_TempAA.resize(song->m_TotalMusicNum + 24);
	SelectMusic16keys::g_KindFolder.resize(song->m_TotalMusicNum + 24);

	////////////////////////////////////////////////////////////////////////////////////////

	//できたもの
	//music_txt[i]に\songs\jubeat\Evans\*.txt
	//music_txt_front[j]に\songs\jubeat\Evans\
	//music_folder_handle[j]にフォルダハンドル \songs\jubeat\Evans\

	try
	{
		FILE *fp;

		wchar_t utf8Str[STR_NUM] = {};
		TCHAR sjisStr[STR_NUM] = {};    //行読み込み用
		//wchar_t wchartxt[STR_NUM] = {};
		//フォルダ最後が音楽数0の時breakするフラグ　一回しか使用しない
		auto isLoadedMusic = false;

		std::vector<tstring> txt4(0);

		std::vector<tstring> temp(0);
		TCHAR tempchar2[420] = {};

		//フォルダ内のランダムをフォルダ画像にする変数
		auto folderinrand = 2;

		//まず0のmusicAA
		song->PushBackEmpty();

		//k;曲数 i:文字進める変数
		//txtを探しだしtxt内を読み込む offsetとか代入
		for (auto k = 1; k < song->m_TotalMusicNum; ++k)  //音楽の総数
		{
			while (true)
			{
				if (music_txt[k] == _T("\0"))
				{
					song->PushBackEmpty();
					RandAAReplace.emplace_back(k);
					++folderinrand;
					//次の曲へ
					++k;
				}
				else //正常なデータ数ならbreak;
				{
					break;
				}
				//もし曲数カウントが曲数超えたとき（最後のフォルダが音楽数0）
				if (k > song->m_TotalMusicNum)
				{
					//breakフラグ立ててここからbreak;
					isLoadedMusic = true;
					break;
				}
			}

			if (isLoadedMusic) break;

			/*$$$ folder先頭書き換えここまで*/
			//検索ディレクトリ指定/*.txtを検索
			hFind = FindFirstFile(music_txt[k].c_str(), &fd);
			//フラグ
			bool isChecked[3] = {};
			//まずはとりあえずtxtまでのフルパスを作る
			do
			{
				//\songs\jubeat\Evans\Evans.txt Evansが+された
				music_txt_front[k] += tstring(fd.cFileName);

				//ファイルパス保持
				song->m_SequencePath.emplace_back(music_txt_front[k]);
				song->m_SequencePath[k].push_back(_T('\0'));

				for (auto i = 0; i < 2; i++)
				{
					for (auto j = 0; j < 5; j++)
					{
						song->m_ElebeatDifficulty[i][j].emplace_back(0);
					}
				}

				wchar_t ws[STR_NUM];
				std::mbstowcs(ws, music_txt_front[k].c_str(), STR_NUM);

				//UTF-8テキストの読み込み
				if (_wfopen_s(&fp, ws, L"r,ccs=UTF-8") != 0)
				{
					auto message = music_txt_front[k] + _T("\nの指定ファイルが読み込めませんでした。\nファイル名が正しくないか、前後のフォルダに\n.smファイルが含まれていない可能性があります。");
					if (!OptionSaveData::g_IsFullScreen)
						MessageBoxA(nullptr, message.c_str(), _T("読み込みエラー"), MB_OK);
					std::exit(0);
				}

				std::fill_n(isChecked, 3, false);

				//行読み込み(fstreamより高速)
				while (std::fgetws(utf8Str, sizeof(utf8Str), fp) != nullptr)
				{
					size_t ret;
					if (wcstombs_s(&ret, sjisStr, sizeof(sjisStr), utf8Str, _TRUNCATE) == ERANGE)
					{
						if (!OptionSaveData::g_IsFullScreen)
							MessageBox(nullptr, (_T("行の最大読み込み文字数") + boost::lexical_cast<tstring>(STR_NUM) + _T("を超えています。\n") + music_txt_front[k]).c_str(), _T("読み込みエラー"), MB_OK);
						std::exit(0);
					}
					tstring string_txt_st = tstring(sjisStr);
					boost::algorithm::trim(string_txt_st);

					if (string_txt_st.find(_T("#TITLE:")) != std::string::npos)
					{
						boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
						song->m_Name.emplace_back(txt4[1]);
					}

					if (string_txt_st.find(_T("#SUBTITLE:")) != std::string::npos)
					{
						boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
						song->m_SubName.emplace_back(txt4[1]);
					}

					if (string_txt_st.find(_T("#ARTIST:")) != std::string::npos)
					{
						boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
						song->m_Artist.emplace_back(txt4[1]);
					}

					if (string_txt_st.find(_T("#ALBUMART:")) != std::string::npos)
					{
						boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
						_tcscpy_s(tempchar2, sizeof(tempchar2), music_folder_handle[k].c_str());
						_tcscat_s(tempchar2, sizeof(tempchar2), txt4[1].c_str());
						song->m_AlbumArtPath.emplace_back(tempchar2);
					}

					//if (_tcsstr(string_txt_st.c_str(), _T("#BACKGROUND:")) != NULL){
					//    boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
					//    _tcscpy_s(tempchar2[k], sizeof(tempchar2[k]), music_folder_handle[k].c_str());
					//    _tcscat_s(tempchar2[k], sizeof(tempchar2[k]), txt4[1].c_str());//んで本命に代入
					//    musicP->musicAAG.push_back(tempchar2[k]);
					//    musicP->musicAAG[k].push_back(_T('\0'));
					//}

					if (string_txt_st.find(_T("#MUSIC:")) != std::string::npos)
					{
						boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
						_tcscpy_s(tempchar2, sizeof(tempchar2), music_folder_handle[k].c_str());
						_tcscat_s(tempchar2, sizeof(tempchar2), txt4[1].c_str());
						song->m_MusicPath.emplace_back(tempchar2);
					}

					if (string_txt_st.find(_T("#OFFSET:")) != std::string::npos)
					{
						boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
						song->m_Offset.emplace_back(txt4[1].c_str());
					}

					if (string_txt_st.find(_T("#BPMS:")) != std::string::npos)
					{
						boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
						std::vector<tstring> txt42(0);
						boost::algorithm::split(txt42, txt4[1], boost::is_any_of(_T(",")));
						song->m_Bpm.emplace_back(temp);
						for (unsigned i = 0; i < txt42.size(); ++i)
						{
							song->m_Bpm[k].emplace_back(txt42[i]);
						}
					}

					if (string_txt_st.find(_T("#BPMPOSITIONS:")) != std::string::npos)
					{
						boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
						std::vector<tstring> txt42(0);
						boost::algorithm::split(txt42, txt4[1], boost::is_any_of(_T(",")));
						song->m_BpmPositions.emplace_back(temp);
						for (unsigned i = 0; i < txt42.size(); ++i)
						{
							song->m_BpmPositions[k].emplace_back(txt42[i]);
						}
					}

					if (string_txt_st.find(_T("#NOTE:")) != std::string::npos || isChecked[0])
					{
						isChecked[0] = true;
						if (string_txt_st.find(_T("keys:9")) != std::string::npos || isChecked[1])
						{
							isChecked[1] = true;
							isChecked[2] = false;
							if (string_txt_st.find(_T("BASIC:")) != std::string::npos)
							{
								//難易度
								boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
								song->m_ElebeatDifficulty[0][0][k] = _ttoi(txt4[1].c_str());
							}
							else if (string_txt_st.find(_T("NORMAL:")) != std::string::npos)
							{
								boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
								song->m_ElebeatDifficulty[0][1][k] = (_ttoi(txt4[1].c_str()));
							}
							else if (string_txt_st.find(_T("ADVANCED:")) != std::string::npos)
							{
								boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
								song->m_ElebeatDifficulty[0][2][k] = (_ttoi(txt4[1].c_str()));
							}
						}
						if (string_txt_st.find(_T("keys:16")) != std::string::npos || isChecked[2])
						{
							isChecked[1] = false;
							isChecked[2] = true;
							if (string_txt_st.find(_T("BASIC:")) != std::string::npos)
							{
								//難易度
								boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
								song->m_ElebeatDifficulty[1][0][k] = _ttoi(txt4[1].c_str());
							}
							else if (string_txt_st.find(_T("NORMAL:")) != std::string::npos)
							{
								boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
								song->m_ElebeatDifficulty[1][1][k] = _ttoi(txt4[1].c_str());
							}
							else if (string_txt_st.find(_T("ADVANCED:")) != std::string::npos)
							{
								boost::algorithm::split(txt4, string_txt_st, boost::is_from_range(_T(':'), _T(';')));
								song->m_ElebeatDifficulty[1][2][k] = _ttoi(txt4[1].c_str());
							}
						}
					}
				}

				DxLib::clsDx();
				DxLib::ClearDrawScreen();
				DxLib::printfDx(_T(".eleファイルの読み込み\n"), music_txt_front[k], fd.cFileName);
				DxLib::printfDx(_T("%s %s\n"), music_txt_front[k].c_str(), fd.cFileName);
				DxLib::ScreenFlip();
				std::fclose(fp);
			} while (FindNextFile(hFind, &fd)); //次のファイルさがすよ！

			FindClose(hFind);
		}

		//譜面が一個でも存在するかの確認
		bool hasSequence = false;

		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < musicCount; ++j)
			{
				if (song->m_ElebeatDifficulty[1][i][j] != 0)
				{
					//一個でも譜面があれば通す
					hasSequence = true;
					break;
				}
			}
			if (hasSequence) break;
		}

		if (!hasSequence)
		{
			if (!OptionSaveData::g_IsFullScreen)
				MessageBox(nullptr, _T(".ele譜面が一個もないようです。強制終了します。"), _T("譜面エラー"), MB_OK);

			std::exit(0);
		}

		DxLib::printfDx(_T("[完了]\n"));

		DxLib::ScreenFlip();
		DxLib::clsDx();
		DxLib::ClearDrawScreen();
	}
	catch (...)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("std::exception"), _T(".eleファイルの読み込みエラー"), MB_OK);
		std::exit(0);
	}
}

void FileManager::LoadDanceElenationFile(std::shared_ptr<Music> &musicP)
{
	//ロードするファイルのパスを取得.
	GetDanceElenationSongPaths(musicP);

	auto& song = musicP->danceSong;

	_wsetlocale(LC_ALL, L"");
	setlocale(LC_ALL, "ja_JP.UTF-8");

	try
	{
		FILE *fp;
		WIN32_FIND_DATA fd;
		tstring termString;

		std::array<std::array<double, 5>, 5> chartTemps{};
		//読み込み時間計測
		const auto startTime = GetNowCount();
		//フォルダ最後が音楽数0の時breakするフラグ　一回しか使用しない
		auto isLoadedMusic = false;

		//行読み込み最大文字数
		constexpr auto STR_NUM = 110000;
		//難易度数
		constexpr auto DIFFICULT_NUM = 5;
		bool isMatch[2] = {};            //#NOTEで使う

		song->PushBackEmpty();

		//k;曲数 i:文字進める変数
		//txtを探しだしtxt内を読み込む offsetとか代入
		for (auto k = 1; k < song->m_TotalMusicNum; ++k)  //音楽の総数
		{
			while (true)
			{
				if (musicText[k] == _T("\0"))
				{
					song->PushBackEmpty();
					RandAA4Replace.emplace_back(k);
					//次の曲へ
					++k;
				}
				else //正常なデータ数ならbreak;
				{
					break;
				}
				//もし曲数カウントが曲数超えたとき（最後のフォルダが音楽数0）
				if (k > song->m_TotalMusicNum)
				{
					//breakフラグ立ててここからbreak;
					isLoadedMusic = true;
					break;
				}
			}

			if (isLoadedMusic) break;

			/*$$$ folder先頭書き換えここまで*/

			//検索ディレクトリ指定/*.txtを検索
			auto hFind = FindFirstFile(musicText[k].c_str(), &fd);

			//まずはtxtまでのフルパスを作る
			do
			{
				//ファイルパス保持 \songs\jubeat\Evans\Evans.txt Evansが+された
				musicTextFront[k] += tstring(fd.cFileName);
				song->m_SequencePath.emplace_back(musicTextFront[k]);

				for (auto di = 0; di < DIFFICULT_NUM; di++)
				{
					song->m_Difficulty[di].emplace_back(0);
				}
				song->m_RadarChart.emplace_back(chartTemps);

				auto bitFlagSong = 0;
				wchar_t ws[STR_NUM];
				std::mbstowcs(ws, musicTextFront[k].c_str(), STR_NUM);

				//UTF-8テキストの読み込み
				if (_wfopen_s(&fp, ws, L"r,ccs=UTF-8") != 0)
				{
					auto message = musicTextFront[k] + _T("\nの指定ファイルが読み込めませんでした。\nファイル名が正しくないか、前後のフォルダに\n.smファイルが含まれていない可能性があります。");
					if (!OptionSaveData::g_IsFullScreen)
						MessageBoxA(nullptr, message.c_str(), _T("読み込みエラー"), MB_OK);
					std::exit(0);
				}

				//行読み込み用
				wchar_t utf8Str[STR_NUM] = {};
				TCHAR sjisStr[STR_NUM] = {};
				wchar_t wchartxt[STR_NUM] = {};
				std::vector<tstring> temptxt(0);
				auto isLoadingBpm = false;
				auto isLoadingStop = false;

				auto ClipText = [&temptxt](tstring text)
				{
					boost::algorithm::split(temptxt, text, boost::is_from_range(_T(':'), _T(';')));
					return temptxt[1];
				};

				//行読み込み
				while (std::fgetws(utf8Str, sizeof(utf8Str), fp) != nullptr)
				{
					size_t ret;
					if (wcstombs_s(&ret, sjisStr, sizeof(sjisStr), utf8Str, _TRUNCATE) == ERANGE)
					{
						if (!OptionSaveData::g_IsFullScreen)
							MessageBox(nullptr, (_T("行の最大読み込み文字数") + boost::lexical_cast<tstring>(STR_NUM) + _T("を超えています。\n") + musicTextFront[k]).c_str(), _T("読み込みエラー"), MB_OK);
						std::exit(0);
					}
					auto stringTextSt = tstring(std::move(sjisStr));
					boost::algorithm::trim(stringTextSt);

					if (stringTextSt.find(_T("#TITLE:")) != std::string::npos)
					{
						DxLib::printfDx(_T("%s"), stringTextSt.c_str());

						song->m_Name.emplace_back(ClipText(stringTextSt));
						bitFlagSong |= TITLE;
					}
					else if (stringTextSt.find(_T("#SUBTITLE:")) != std::string::npos)
					{
						song->m_SubName.emplace_back(ClipText(stringTextSt));
						bitFlagSong |= SUBTITLE;
					}
					else if (stringTextSt.find(_T("#ARTIST:")) != std::string::npos)
					{
						song->m_Artist.emplace_back(ClipText(stringTextSt));
						bitFlagSong |= ARTIST;
					}
					else if (stringTextSt.find(_T("#BANNER:")) != std::string::npos /*|| string_txt_st.find(_T("#JACKET:")) != string::npos*/)
					{
						tstring filePath = musicFolderHandle[k].c_str() + ClipText(stringTextSt);
						song->m_BannerPath.emplace_back(std::move(filePath));
						bitFlagSong |= BANNER;
					}
					else if (stringTextSt.find(_T("#BACKGROUND:")) != std::string::npos)
					{
						tstring filePath = musicFolderHandle[k].c_str() + ClipText(stringTextSt);
						song->m_BackGroundPath.emplace_back(std::move(filePath));
						bitFlagSong |= BACKGROUND;
					}
					else if (stringTextSt.find(_T("#MUSIC:")) != std::string::npos)
					{
						tstring filePath = musicFolderHandle[k].c_str() + ClipText(stringTextSt);
						song->m_MusicPath.emplace_back(std::move(filePath));
						bitFlagSong |= MUSIC;
					}
					else if (stringTextSt.find(_T("#OFFSET:")) != std::string::npos)
					{
						song->m_Offset.emplace_back(ClipText(stringTextSt));
						bitFlagSong |= OFFSET;
					}
					else if (stringTextSt.find(_T("#BPMS:")) != std::string::npos || isLoadingBpm)
					{
						//false → 未読み込み状態または読み込み完了.
						//true  → 読み込み中.
						isLoadingBpm = true;

						termString += stringTextSt;

						if (_tcsstr(stringTextSt.c_str(), _T(";")) != nullptr)
							isLoadingBpm = false;

						if (!isLoadingBpm)
						{
							std::vector<tstring> clipedText(0);
							boost::algorithm::split(clipedText, ClipText(termString), boost::is_any_of(_T("=,")), boost::token_compress_on);

							std::vector <tstring> temp(0);
							song->m_Bpm.emplace_back(temp);
							song->m_BpmPositions.emplace_back(temp);

							for (size_t i = 0; i < (clipedText.size() / 2); ++i)
							{
								//BPMPOSITION
								song->m_BpmPositions[k].emplace_back(clipedText[i * 2]);
								//BPM
								song->m_Bpm[k].emplace_back(clipedText[i * 2 + 1]);
							}
							termString.clear();
							bitFlagSong |= BPMS;
						}
					}
					else if (stringTextSt.find(_T("#STOPS:")) != std::string::npos || isLoadingStop)
					{
						// false → 未読み込み状態または読み込み完了.
						// true  → 読み込み中.
						isLoadingStop = true;
						termString += stringTextSt;

						if (_tcsstr(stringTextSt.c_str(), _T(";")) != nullptr)
							isLoadingStop = false;

						if (isLoadingStop == false)
						{
							std::vector<tstring> clipedText(0);
							boost::algorithm::split(clipedText, ClipText(termString), boost::is_any_of(_T("=,")), boost::token_compress_on);

							std::vector <tstring> temp(0);
							song->m_Stop.emplace_back(temp);
							song->m_StopPositions.emplace_back(temp);

							for (size_t i = 0; i < clipedText.size() / 2; ++i)
							{
								//STOPPOSITION
								song->m_StopPositions[k].emplace_back(clipedText[i * 2]);
								//STOP
								song->m_Stop[k].emplace_back(clipedText[i * 2 + 1]);
							}
							termString.clear();
							bitFlagSong |= STOPS;
						}
					}

					//#NOTE部分の難易度とレーダ読み込み
					else if (stringTextSt.find(_T("#NOTES:")) != std::string::npos || isMatch[0])
					{
						isMatch[0] = true;
						if (stringTextSt.find(_T("dance-single:")) != std::string::npos || isMatch[1])
						{
							isMatch[1] = true;
							auto difficultType = 0;
							if (_tcsstr(stringTextSt.c_str(), _T("Beginner:")) != nullptr)
								difficultType = 0;
							else if (_tcsstr(stringTextSt.c_str(), _T("Easy:")) != nullptr)
								difficultType = 1;
							else if (_tcsstr(stringTextSt.c_str(), _T("Medium:")) != nullptr)
								difficultType = 2;
							else if (_tcsstr(stringTextSt.c_str(), _T("Hard:")) != nullptr)
								difficultType = 3;
							else if (_tcsstr(stringTextSt.c_str(), _T("Challenge:")) != nullptr)
								difficultType = 4;
							/*else if (_tcsstr(stringTextSt.c_str(), _T("Edit:")) != nullptr)
								difficultType = 5;*/
							else
								continue;

							//難易度
							std::fgetws(wchartxt, sizeof(wchartxt), fp);

							if (wcstombs_s(&ret, sjisStr, sizeof(sjisStr), std::move(wchartxt), _TRUNCATE) == ERANGE)
							{
								if (!OptionSaveData::g_IsFullScreen)
									MessageBox(nullptr, (_T("行の最大読み込み文字数") + boost::lexical_cast<tstring>(STR_NUM) + ("を超えています。")).c_str(), _T("読み込みエラー"), MB_OK);
								std::exit(0);
							}
							stringTextSt = tstring(sjisStr);
							boost::algorithm::trim(stringTextSt);
							boost::algorithm::split(temptxt, stringTextSt, boost::is_punct());
							song->m_Difficulty[difficultType][k] = std::_ttoi(temptxt[0].c_str());

							//レーダ
							std::fgetws(wchartxt, sizeof(wchartxt), fp);
							std::wcstombs(sjisStr, std::move(wchartxt), STR_NUM);
							stringTextSt = tstring(sjisStr);
							boost::algorithm::trim(stringTextSt);
							boost::algorithm::split(temptxt, stringTextSt, boost::is_any_of(",:"), boost::token_compress_on);

							for (auto n = 0; n < 5; ++n)
							{
								song->m_RadarChart.at(k)[difficultType][n] = boost::lexical_cast<double>(temptxt[n].c_str());
							}
							isMatch[0] = false;
							isMatch[1] = false;
						}
						else if (stringTextSt.find(_T("dance-double:")) != std::string::npos)
						{
							isMatch[0] = false;
						}
					}
				}

				CheckBitFlagSong(bitFlagSong, musicTextFront[k], musicP);

				DxLib::clsDx();
				DxLib::ClearDrawScreen();
				DxLib::printfDx(_T(".smファイルの読み込み\n"), musicTextFront[k].c_str(), fd.cFileName);
				DxLib::printfDx(_T("%s\n"), musicTextFront[k].c_str());
				DxLib::ScreenFlip();
				//ファイルを閉じる
				std::fclose(fp);
				//次のファイルさがすよ！
			} while (FindNextFile(hFind, &fd));

			FindClose(hFind);
		}

		//譜面が一個でも存在するかの確認
		auto hasSequence = false;
		for (auto i = 0; i < song->m_TotalMusicNum; ++i)
		{
			//一個でも譜面があれば通す
			for (auto j = 0; j < DIFFICULT_NUM; j++)
			{
				if (song->m_Difficulty[j][i] != 0)
				{
					hasSequence = true;
					break;
				}
			}
			if (hasSequence) break;
		}

		if (hasSequence == false)
		{
			if (!OptionSaveData::g_IsFullScreen)
				MessageBox(nullptr, _T("4Keys譜面が一個もないようです。強制終了します。"), _T("譜面エラー"), MB_OK);
			std::exit(0);
		}
		DxLib::printfDx(_T("[完了]\n"));
		DxLib::clsDx();

		m_mux.lock();
		m_isLoadedSmFile = true;
		m_mux.unlock();
		std::string TTT = boost::lexical_cast<std::string>(GetNowCount() - startTime) + _T(" :LoadTime\n");
		OutputDebugStringA(TTT.c_str());
	}
	catch (std::bad_alloc bad_alloc)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("error: std::bad_alloc "), bad_alloc.what(), MB_OK);
		std::exit(0);
	}
	catch (tstring &e)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("std::exception::what : "), e.c_str(), MB_OK);
		std::exit(0);
	}
	catch (TCHAR* e)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("std::exception::what : "), e, MB_OK);
		std::exit(0);
	}
	catch (...)
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("std::exception::what : "), _T("Unknown error"), MB_OK);
		std::exit(0);
	}
}

//ロードするファイルのパスを取得.
void FileManager::GetDanceElenationSongPaths(std::shared_ptr<Music> &musicP)
{
	auto& song = musicP->danceSong;
	WIN32_FIND_DATA fd;

	musicText.emplace_back(tstring("\0"));
	musicTextFront.emplace_back(tstring("\0"));
	musicHandle.emplace_back(tstring("\0"));
	musicFolderHandle.emplace_back(_T("\0"));
	kind.emplace_back(_T("\0"));
	song->m_MusicNumAt.emplace_back(0);
	RandAA4Replace.emplace_back(0);

	//最初のフォルダはランダムを入れる.
	song->m_FolderBannerPath.emplace_back(_T("img/random-aa.png"));
	song->m_FolderName.emplace_back(_T("Random"));

	//フォルダ検索
	auto folderCount = 0;
	//folder情報を次へ
	folderCount++;

	//検索ディレクトリ指定
	auto hFind = FindFirstFile((SpecialFiles::SONGS_4KEYS_DIR + tstring("*")).c_str(), &fd);

	do
	{
		if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			if (_T('.') == fd.cFileName[0])
			{
				if ((_T('\0') == fd.cFileName[1]) || (_T('.') == fd.cFileName[1] && _T('\0') == fd.cFileName[2])) continue;
			}
			song->m_FolderName.emplace_back(fd.cFileName);
			//folderAA
			song->m_FolderBannerPath.emplace_back(SpecialFiles::SONGS_4KEYS_DIR + tstring(fd.cFileName) + _T("/") + tstring(fd.cFileName) + _T(".png"));
			musicHandle.emplace_back(SpecialFiles::SONGS_4KEYS_DIR + tstring(fd.cFileName) + _T("/"));
			kind.emplace_back(SpecialFiles::SONGS_4KEYS_DIR + tstring(fd.cFileName) + _T("/*"));
			++folderCount;
		}
	} while (FindNextFile(hFind, &fd));//次のファイルさがすよ！

	//検索終了
	FindClose(hFind);

	//種類はkind[]で確認0がnull　なのでこれが12なら11配列まで存在する
	//種類が何種類あるか保持
	song->m_FolderNum = folderCount;

	///////////////////////////////////////////////////////////////////////////
	/*$$$ randomのため1で初期化 このあとのi == 1 とかは変更したものの可能性大*/
	//smファイル検索
	auto folderMusicCount = 1;
	int musicCount = 0;

	//そしてtxtを探す為のパスを作る
	//音楽種類終了までいったんループ
	for (int i = 1; i < song->m_FolderNum; ++i)
	{
		//検索ディレクトリ指定
		auto hFind = FindFirstFile(kind.at(i).c_str(), &fd);
		do
		{
			if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (_T('.') == fd.cFileName[0])
				{
					if ((_T('\0') == fd.cFileName[1]) || (_T('.') == fd.cFileName[1] && _T('\0') == fd.cFileName[2])) continue;
				}
				DxLib::clsDx();
				DxLib::ClearDrawScreen();
				DxLib::printfDx(_T(".smファイルの検索\n"));
				DxLib::printfDx(_T("%s %s\n"), musicHandle[i].c_str(), fd.cFileName);
				DxLib::ScreenFlip();

				musicText.emplace_back(musicHandle[i] + tstring(fd.cFileName) + _T("/*.sm"));
				musicTextFront.emplace_back(musicHandle[i] + tstring(fd.cFileName) + _T("/"));
				auto front = musicHandle[i].c_str() + tstring(fd.cFileName) + _T("/");

				musicFolderHandle.emplace_back(front.c_str());
				//music_txt[j]はテキスト検索　music_folder_handle[j]はハンドル
				++folderMusicCount;
			}
			//次のファイルさがすよ！
		} while (FindNextFile(hFind, &fd));

		//最初のフォルダの時はそのまま音楽数代入
		if (musicCount == 0)
		{
			//フォルダの中身が0じゃないとき 初期化が1なのでこうなる
			if (folderMusicCount != 1)
			{
				song->m_MusicNumAt.emplace_back(folderMusicCount);
				musicCount = song->m_MusicNumAt[i];
				musicText.emplace_back(_T("\0"));
				musicTextFront.emplace_back(_T("\0"));
				musicFolderHandle.emplace_back(_T("\0"));
				++folderMusicCount;
			}
		}
		//それ以降は追加しながら音楽数加算
		else
		{
			//フォルダの中身が0じゃないとき
			if (folderMusicCount != musicCount + 1)
			{
				song->m_MusicNumAt.emplace_back(folderMusicCount - musicCount);
				musicCount += song->m_MusicNumAt[i];
				musicText.emplace_back(_T("\0"));
				musicTextFront.emplace_back(_T("\0"));
				musicFolderHandle.emplace_back(_T("\0"));
				++folderMusicCount;
			}
		}
	}

	//音楽数総数
	for (auto num : song->m_MusicNumAt)
	{
		song->m_TotalMusicNum += num;
	}
}

//文字列を置換する
tstring Replace(tstring str1, tstring str2, tstring str3)
{
	auto pos(str1.find(str2));

	while (pos != tstring::npos)
	{
		str1.replace(pos, str2.length(), str3);
		pos = str1.find(str2, pos + str3.length());
	}

	return str1;
}

void FileManager::CheckBitFlagSong(const int bitFlagSong, const tstring& str, const std::shared_ptr<Music>& musicP)
{
	auto& song = musicP->danceSong;
	constexpr char ERRER_MESSAGE_UTF8[] = _T("\nまたは文字コードがUTF-8でない可能性があります。");
	constexpr char ERROR_TITLE[] = _T("読み込みエラー");

	if ((bitFlagSong & TITLE) != TITLE)
	{
		auto message = str + _T("\nの#TITLEが未定義です。") + ERRER_MESSAGE_UTF8;
		if (!OptionSaveData::g_IsFullScreen)
			MessageBoxA(nullptr, message.c_str(), ERROR_TITLE, MB_OK);
		std::exit(0);
		return;
	}
	if ((bitFlagSong & SUBTITLE) != SUBTITLE)
	{
		song->m_SubName.emplace_back(_T("\0"));
	}
	if ((bitFlagSong & ARTIST) != ARTIST)
	{
		song->m_Artist.emplace_back(_T("Unknown Artist"));
	}
	if ((bitFlagSong & BANNER) != BANNER)
	{
		auto message = str + _T("\nの#BANNERが未定義です。") + ERRER_MESSAGE_UTF8;
		if (!OptionSaveData::g_IsFullScreen)
			MessageBoxA(nullptr, message.c_str(), ERROR_TITLE, MB_OK);
		std::exit(0);
	}
	if ((bitFlagSong & BACKGROUND) != BACKGROUND)
	{
		auto message = str + _T("\nの#BACKGROUNDが未定義です。") + ERRER_MESSAGE_UTF8;
		if (!OptionSaveData::g_IsFullScreen)
			MessageBoxA(nullptr, message.c_str(), ERROR_TITLE, MB_OK);
		std::exit(0);
	}
	if ((bitFlagSong & MUSIC) != MUSIC)
	{
		auto message = str + _T("\nの#MUSICが未定義です。") + ERRER_MESSAGE_UTF8;
		if (!OptionSaveData::g_IsFullScreen)
			MessageBoxA(nullptr, message.c_str(), ERROR_TITLE, MB_OK);
		std::exit(0);
	}
	if ((bitFlagSong & OFFSET) != OFFSET)
	{
		song->m_Offset.emplace_back("0");
	}
	if ((bitFlagSong & BPMS) != BPMS)
	{
		auto message = str + _T("\nの#BPMSが未設定です。");
		MessageBoxA(nullptr, message.c_str(), ERROR_TITLE, MB_OK);
		std::exit(0);
	}
	if ((bitFlagSong & STOPS) != STOPS)
	{
		std::vector <tstring> temp(0);
		song->m_StopPositions.emplace_back(temp);
		song->m_Stop.emplace_back(temp);
	}
}

void FileManager::IsLoaded(bool isLoaded, bool isLoadedSequence)
{
	this->m_isLoaded = isLoaded;
	this->m_isLoadedSequence = isLoadedSequence;
}

bool FileManager::IsLoaded(void) const
{
	return this->m_isLoaded && this->m_isLoadedSequence;
}

void FileManager::LoadScoreData()
{
	//ScoreData::GetInstance().Init(musicP);
}

void FileManager::LoadFile(std::shared_ptr<Music> &musicP)
{
	DxLib::printfDx(_T("**********************************\n"));
	DxLib::printfDx(_T("*         Dance Elenation        *\n"));
	DxLib::printfDx(_T("**********************************\n"));

	//ディレクトリが無ければ生成
	CheckDirectory();

	//MMD(3D)読み込み
	LoadMmdFile();

	//ini読み込み
	Setting.Load(_T("Data/Setting.ini"));

	//グローバル画像読み込み.
	LoadGlobalImage();

	//数字画像の読み込み
	LoadNumberImage();

	//マーカー読み込み
	LoadMarker();

	//ELEファイル読み込み
	LoadElebeatFile(musicP);

	//SMファイル読み込み
	LoadDanceElenationFile(musicP);

	///TODO:スコアデータ読み込み
	LoadScoreData();

	//読み込み完了
	IsLoaded(true, true);
}