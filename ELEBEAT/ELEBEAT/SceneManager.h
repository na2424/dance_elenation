#ifndef SCENEMANAGER_CLASS_H
#define SCENEMANAGER_CLASS_H

#include <memory>
#include "Enums.h"
#include "Fps.h"
#include "Music.h"
#include "ScreenShot.h"



class Scene
{
protected:
	std::unique_ptr<Fps> m_fps;
	std::unique_ptr<ScreenShot> m_ssc;

	int m_count;

public:
	virtual ~Scene() = default;

	Scene() :
		m_count(0)
	{
		m_fps = std::make_unique<Fps>();
		m_ssc = std::make_unique<ScreenShot>();
	}

	virtual void Initialize(const std::shared_ptr<Music>&) abstract;
	virtual void Update(const std::shared_ptr<Music>&) abstract;
	virtual void Finalize(const std::shared_ptr<Music>&) abstract;
};

class SceneManager
{
public:
	static Scenes g_CurrentScene;
	static SelectKeys g_PlayMode;
	[[nodiscard]] static std::unique_ptr<Scene> CreateScene();
	static void SetNextScene(Scenes nextScene);
};

#endif