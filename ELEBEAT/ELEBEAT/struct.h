#ifndef STRUCT_H
#define STRUCT_H
#include "header.h"
#include "define.h"
#include "Dxlib.h"
#include "vector"
#include <array>
#include <boost/timer.hpp>
#include "savedata_class.h"
#include "KeyInput.h"
#include "JudgeCount.h"
#include "Input.h"
#include "GamePlayOption.h"
#include "JoyPad.h"
#include "KeysSelect.h"
#include "KeyConfig.h"

class CMyException : public std::exception
{
public:
	CMyException() {}
	CMyException(const tstring in_message,
		const char*  in_fileName,
		const char*  in_functionName,
		const int    in_line)
	{
		message = in_message;
		fileName = in_fileName;
		functionName = in_functionName;
		line = in_line;
	}
	void DumpInfo() const
	{
		if (!OptionSaveData::g_IsFullScreen)
			MessageBox(nullptr, _T("message"), message.c_str(), MB_OK);
	}
private:
	tstring      message;
	const char* fileName;
	const char* functionName;
	int         line;
};

#endif