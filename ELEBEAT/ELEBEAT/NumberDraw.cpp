#include "NumberDraw.h"



//曲選択画面以外のタイマー数字
void TimerDrawer::DrawMenuTimer(int num, int addY)
{
	// numが十進数で何桁になるか調べる
	auto beamWidth = 0;
	if (num == 0) {
		beamWidth = 1;
	}
	else {
		for (auto i = 1; num >= i; i *= 10) beamWidth++;
	}
	// 画面右上に右詰で表示
	// xは描く数字(一桁一桁各々)の左端のX座標
	auto x = 855;
	for (auto i = 0; i < beamWidth; i++)
	{
		if (num == 0) {
			DrawGraph(855, 90, m_TimerImages[0], TRUE);
		}
		else {
			DrawGraph(x, 90 + addY, m_TimerImages[num % 10], TRUE);
		}
		num /= 10;
		x -= 80;
	}
}