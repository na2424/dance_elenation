#include <string>
#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <boost/lexical_cast.hpp>
#include "SettingIni.h"
#include "extern.h"



SettingIni::SettingIni(void)
{
	initFileName = _T("Data/Setting.ini"); //Default file.
	Setting.Init();
}

SettingIni::SettingIni(string_t fname = _T(""))
{
	initFileName = _T("Data/Setting.ini"); //Default file.
	Setting.Init();
	Setting.Load(fname);
}

/**
	INI file is read.
	@param fname Filename
	When there is not a file, It becomes initFileName.
	When there is not a pass, It becomes Windows folder.
	*/
bool SettingIni::Load(string_t fname = _T(""))
{
	if (fname.empty()) {
		fname = initFileName;
	}
	loadFileName = fname;
	WIN32_FIND_DATA fd;
	HANDLE h = ::FindFirstFile(fname.c_str(), &fd);
	if (h != INVALID_HANDLE_VALUE) {
		Setting.IniRw(fname, 1);
	}
	return (h != INVALID_HANDLE_VALUE);
}

/**
	It writes it in the INI file.
	@param fname Filename
	When there is not a file, It becomes open file.
	When there is not a pass, It becomes Windows folder.
	*/
bool SettingIni::Save(string_t fname = _T(""))
{
	if (fname.empty()) {
		fname = loadFileName;
	}
	IniRw(fname, 0);
	return true;
}

bool SettingIni::IniRw(string_t f, int r)
{
	string_t s = _T("Judge");
	Inirw(r, f, s, _T("Pa_h             "), Judge.JudgePerfect);
	Inirw(r, f, s, _T("Gr_h             "), Judge.JudgeGreat);
	Inirw(r, f, s, _T("Go_h             "), Judge.JudgeGood);

	s = _T("Life");
	Inirw(r, f, s, _T("Pa_life          "), Life.LifePerfect);
	Inirw(r, f, s, _T("Gr_life          "), Life.LifeGreat);
	Inirw(r, f, s, _T("Go_life          "), Life.LifeGood);
	Inirw(r, f, s, _T("Miss_life        "), Life.LifeMiss);

	s = _T("KeyConfig");
	Inirw(r, f, s, _T("KeyDown          "), KeyConfig.KeyDown);
	Inirw(r, f, s, _T("KeyLeft          "), KeyConfig.KeyLeft);
	Inirw(r, f, s, _T("KeyRight         "), KeyConfig.KeyRight);
	Inirw(r, f, s, _T("KeyUp            "), KeyConfig.KeyUp);
	Inirw(r, f, s, _T("KeyStart         "), KeyConfig.KeyStart);
	Inirw(r, f, s, _T("KeyBack          "), KeyConfig.KeyBack);
	Inirw(r, f, s, _T("KeyL             "), KeyConfig.KeyL);
	Inirw(r, f, s, _T("KeyR             "), KeyConfig.KeyR);
	Inirw(r, f, s, _T("Key1             "), KeyConfig.Key1);
	Inirw(r, f, s, _T("Key2             "), KeyConfig.Key2);
	Inirw(r, f, s, _T("Key3             "), KeyConfig.Key3);
	Inirw(r, f, s, _T("Key4             "), KeyConfig.Key4);
	Inirw(r, f, s, _T("Key5             "), KeyConfig.Key5);
	Inirw(r, f, s, _T("Key6             "), KeyConfig.Key6);
	Inirw(r, f, s, _T("Key7             "), KeyConfig.Key7);
	Inirw(r, f, s, _T("Key8             "), KeyConfig.Key8);
	Inirw(r, f, s, _T("Key9             "), KeyConfig.Key9);
	Inirw(r, f, s, _T("Key10            "), KeyConfig.Key10);
	Inirw(r, f, s, _T("Key11            "), KeyConfig.Key11);
	Inirw(r, f, s, _T("Key12            "), KeyConfig.Key12);
	Inirw(r, f, s, _T("Key13            "), KeyConfig.Key13);
	Inirw(r, f, s, _T("Key14            "), KeyConfig.Key14);
	Inirw(r, f, s, _T("Key15            "), KeyConfig.Key15);
	Inirw(r, f, s, _T("Key16            "), KeyConfig.Key16);

	s = _T("JoyPad");
	Inirw(r, f, s, _T("JoyPadDown1     "), JoyPad.JoyPadKey[0][0]);
	Inirw(r, f, s, _T("JoyPadDown2     "), JoyPad.JoyPadKey[0][1]);
	Inirw(r, f, s, _T("JoyPadLeft1     "), JoyPad.JoyPadKey[1][0]);
	Inirw(r, f, s, _T("JoyPadLeft2     "), JoyPad.JoyPadKey[1][1]);
	Inirw(r, f, s, _T("JoyPadRight1    "), JoyPad.JoyPadKey[2][0]);
	Inirw(r, f, s, _T("JoyPadRight2    "), JoyPad.JoyPadKey[2][1]);
	Inirw(r, f, s, _T("JoyPadUp1       "), JoyPad.JoyPadKey[3][0]);
	Inirw(r, f, s, _T("JoyPadUp2       "), JoyPad.JoyPadKey[3][1]);
	Inirw(r, f, s, _T("JoyPadStart1    "), JoyPad.JoyPadKey[4][0]);
	Inirw(r, f, s, _T("JoyPadStart2    "), JoyPad.JoyPadKey[4][1]);
	Inirw(r, f, s, _T("JoyPadBack1     "), JoyPad.JoyPadKey[5][0]);
	Inirw(r, f, s, _T("JoyPadBack2     "), JoyPad.JoyPadKey[5][1]);
	Inirw(r, f, s, _T("JoyPadL1        "), JoyPad.JoyPadKey[6][0]);
	Inirw(r, f, s, _T("JoyPadL2        "), JoyPad.JoyPadKey[6][1]);
	Inirw(r, f, s, _T("JoyPadR1        "), JoyPad.JoyPadKey[7][0]);
	Inirw(r, f, s, _T("JoyPadR2        "), JoyPad.JoyPadKey[7][1]);
	Inirw(r, f, s, _T("JoyPad1          "), JoyPad.JoyPad[0][0]);
	Inirw(r, f, s, _T("JoyPad2          "), JoyPad.JoyPad[0][1]);
	Inirw(r, f, s, _T("JoyPad3          "), JoyPad.JoyPad[0][2]);
	Inirw(r, f, s, _T("JoyPad4          "), JoyPad.JoyPad[0][3]);
	Inirw(r, f, s, _T("JoyPad5          "), JoyPad.JoyPad[1][0]);
	Inirw(r, f, s, _T("JoyPad6          "), JoyPad.JoyPad[1][1]);
	Inirw(r, f, s, _T("JoyPad7          "), JoyPad.JoyPad[1][2]);
	Inirw(r, f, s, _T("JoyPad8          "), JoyPad.JoyPad[1][3]);
	Inirw(r, f, s, _T("JoyPad9          "), JoyPad.JoyPad[2][0]);
	Inirw(r, f, s, _T("JoyPad10         "), JoyPad.JoyPad[2][1]);
	Inirw(r, f, s, _T("JoyPad11         "), JoyPad.JoyPad[2][2]);
	Inirw(r, f, s, _T("JoyPad12         "), JoyPad.JoyPad[2][3]);
	Inirw(r, f, s, _T("JoyPad13         "), JoyPad.JoyPad[3][0]);
	Inirw(r, f, s, _T("JoyPad14         "), JoyPad.JoyPad[3][1]);
	Inirw(r, f, s, _T("JoyPad15         "), JoyPad.JoyPad[3][2]);
	Inirw(r, f, s, _T("JoyPad16         "), JoyPad.JoyPad[3][3]);

	s = _T("GamePlaySetting");
	Inirw(r, f, s, _T("GlobalOffset     "), GamePlaySetting.GlobalOffset);

	s = _T("ScreenShot");
	Inirw(r, f, s, _T("PRTSC_ELE        "), ScreenShot.PRTSC_ELE);
	return true;
}

void SettingIni::Init()
{
	Judge.JudgePerfect = 60;
	Judge.JudgeGreat = 100;
	Judge.JudgeGood = 160;
	Life.LifePerfect = 2;
	Life.LifeGreat = 1;
	Life.LifeGood = 0;
	Life.LifeMiss = 6;
	KeyConfig.KeyDown = 208;
	KeyConfig.KeyLeft = 203;
	KeyConfig.KeyRight = 205;
	KeyConfig.KeyUp = 200;
	KeyConfig.KeyStart = 28;
	KeyConfig.KeyBack = 1;
	KeyConfig.KeyL = 42;
	KeyConfig.KeyR = 54;
	KeyConfig.Key1 = 2;
	KeyConfig.Key2 = 3;
	KeyConfig.Key3 = 4;
	KeyConfig.Key4 = 5;
	KeyConfig.Key5 = 16;
	KeyConfig.Key6 = 17;
	KeyConfig.Key7 = 18;
	KeyConfig.Key8 = 19;
	KeyConfig.Key9 = 30;
	KeyConfig.Key10 = 31;
	KeyConfig.Key11 = 32;
	KeyConfig.Key12 = 33;
	KeyConfig.Key13 = 44;
	KeyConfig.Key14 = 45;
	KeyConfig.Key15 = 46;
	KeyConfig.Key16 = 47;
	JoyPad.JoyPadKey[0][0] = 0;
	JoyPad.JoyPadKey[0][1] = 0;
	JoyPad.JoyPadKey[1][0] = 0;
	JoyPad.JoyPadKey[1][1] = 0;
	JoyPad.JoyPadKey[2][0] = 0;
	JoyPad.JoyPadKey[2][1] = 0;
	JoyPad.JoyPadKey[3][0] = 0;
	JoyPad.JoyPadKey[3][1] = 0;
	JoyPad.JoyPadKey[4][0] = 0;
	JoyPad.JoyPadKey[4][1] = 0;
	JoyPad.JoyPadKey[5][0] = 0;
	JoyPad.JoyPadKey[5][1] = 0;
	JoyPad.JoyPadKey[6][0] = 0;
	JoyPad.JoyPadKey[6][1] = 0;
	JoyPad.JoyPadKey[7][0] = 0;
	JoyPad.JoyPadKey[7][1] = 0;
	JoyPad.JoyPad[0][0] = 0;
	JoyPad.JoyPad[0][1] = 0;
	JoyPad.JoyPad[0][2] = 0;
	JoyPad.JoyPad[0][3] = 0;
	JoyPad.JoyPad[1][0] = 0;
	JoyPad.JoyPad[1][1] = 0;
	JoyPad.JoyPad[1][2] = 0;
	JoyPad.JoyPad[1][3] = 0;
	JoyPad.JoyPad[2][0] = 0;
	JoyPad.JoyPad[2][1] = 0;
	JoyPad.JoyPad[2][2] = 0;
	JoyPad.JoyPad[2][3] = 0;
	JoyPad.JoyPad[3][0] = 0;
	JoyPad.JoyPad[3][1] = 0;
	JoyPad.JoyPad[3][2] = 0;
	JoyPad.JoyPad[3][3] = 0;
	GamePlaySetting.GlobalOffset = 0;
}

template<class T>
bool Inirw(int isRead, string_t& fname, string_t sec, string_t key, T& val_t)
{
	if (isRead) {
		read(fname.c_str(), sec.c_str(), key.c_str(), val_t);
	}
	else {
		Write(fname.c_str(), sec.c_str(), key.c_str(), val_t);
	}
	return true;
}

bool read(string_t ifn, string_t sec, string_t key, int& dst)
{
	dst = GetPrivateProfileInt(sec.c_str(), key.c_str(), dst, ifn.c_str());
	return true;
}

bool read(string_t ifn, string_t sec, string_t key, std::basic_string<TCHAR>& dst)
{
	TCHAR buf[256];
	GetPrivateProfileString(
		sec.c_str(),
		key.c_str(),
		dst.c_str(),
		buf,
		sizeof(buf),
		ifn.c_str());
	dst = buf;
	return true;
}

bool read(string_t ifn, string_t sec, string_t key, double& dst)
{
	string_t s;
	read(ifn, sec, key, s);

	TCHAR* e;
	double x = _tcstod(s.c_str(), &e);

	dst = 0.0;
	if (!*e) {
		dst = x;
	}
	return true;
}

template<class T>
bool Write(string_t ifn, string_t sec, string_t key, T val_t)
{
	string_t str;
	ToString(str, val_t);
	WritePrivateProfileString(sec.c_str(), key.c_str(), str.c_str(), ifn.c_str());
	return true;
}

template<class T>
void ToString(string_t &str, T val)
{
	str = boost::lexical_cast<tstring>(val);
}