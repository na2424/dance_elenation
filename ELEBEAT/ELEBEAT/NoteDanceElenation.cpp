#include <boost/timer.hpp>
#include <vector>
#include <tchar.h>

#include "NoteDanceElenation.h"
#include "DxLib.h"
#include "extern.h"

#include "GameBase.h"
#include "header.h"
#include "JudgeCount.h"
#include "KeyInput.h"
#include "struct.h"
#include "Combo.h"



NoteDanceElenation::NoteDanceElenation() :
	m_cycle(0),
	m_cycle2(0),
	pixelPerBeat(0),
	m_movedPixel(0),
	m_offsetPix(0), m_JudgeLinePositionY(86), m_BeatTime(0), m_BeatNowTime(0), m_Fps(0),
	m_Speed(0),
	m_InvBps16(0),
	GAPn(0),
	GAPs(0)
{
	m_rote[0] = PI * 1 / 2, //←
	m_rote[1] = PI * 0 / 2, //↓
	m_rote[2] = PI * 2 / 2, //↑
	m_rote[3] = PI * 3 / 2; //→

	m_holdBottomCapInactiveImages[2] = LoadGraph(_T("NoteSkins/hold/Up Hold BottomCap Inactive.png"));
	m_holdBottomCapActiveImages[2] = LoadGraph(_T("NoteSkins/hold/Up Hold BottomCap Active.png"));
	m_holdBodyInactiveImages[2] = LoadGraph(_T("NoteSkins/hold/Up Hold Body Inactive.png"));
	m_holdBodyActiveImages[2] = LoadGraph(_T("NoteSkins/hold/Up Hold Body Active.png"));
	m_holdBottomCapInactiveImages[0] = LoadGraph(_T("NoteSkins/hold/Left Hold BottomCap Inactive.png"));
	m_holdBottomCapActiveImages[0] = LoadGraph(_T("NoteSkins/hold/Left Hold BottomCap Active.png"));
	m_holdBodyInactiveImages[0] = LoadGraph(_T("NoteSkins/hold/Left Hold Body Inactive.png"));
	m_holdBodyActiveImages[0] = LoadGraph(_T("NoteSkins/hold/Left Hold Body Active.png"));
	m_holdBottomCapInactiveImages[3] = LoadGraph(_T("NoteSkins/hold/Right Hold BottomCap Inactive.png"));
	m_holdBottomCapActiveImages[3] = LoadGraph(_T("NoteSkins/hold/Right Hold BottomCap Active.png"));
	m_holdBodyInactiveImages[3] = LoadGraph(_T("NoteSkins/hold/Right Hold Body Inactive.png"));
	m_holdBodyActiveImages[3] = LoadGraph(_T("NoteSkins/hold/Right Hold Body Active.png"));
	m_downHoldHeadInactiveImage = LoadGraph(_T("NoteSkins/hold/Down Hold Head Inactive.png"));
	m_downHoldHeadActiveImage = LoadGraph(_T("NoteSkins/hold/Down Hold Head Active.png"));
	m_holdBottomCapInactiveImages[1] = LoadGraph(_T("NoteSkins/hold/Down Hold BottomCap Inactive.png"));
	m_holdBottomCapActiveImages[1] = LoadGraph(_T("NoteSkins/hold/Down Hold BottomCap Active.png"));
	m_holdBodyInactiveImages[1] = LoadGraph(_T("NoteSkins/hold/Down Hold Body Inactive.png"));
	m_holdBodyActiveImages[1] = LoadGraph(_T("NoteSkins/hold/Down Hold Body Active.png"));
	m_note8Image = LoadGraph(_T("NoteSkins/dance/Down Receptor Waiting.png"));

	for (auto m = 0; m < 16; m++)
	{
		m_Note4Images[m] = 0;
		m_Note8[m] = 0;
		m_Note12[m] = 0;
		m_Note16[m] = 0;
	}
	for (auto m = 0; m < 4; m++)
	{
		m_KeyoutTime[m] = 0;
		m_KeyState[m] = 0;
		m_LongFlag[m] = 0;
		m_LongEndFlag[m] = 0;
		m_U[m] = 0;
		m_NoteCount[m] = 0;
		m_NoteFlag[m].resize(0);
		m_NoteFlagTime[m].resize(0);
		m_Note[m].resize(0);
		m_NoteTime[m].resize(0);
	}
}

void NoteDanceElenation::LoadNoteSkin(NoteSkinType noteSkinType)
{
	switch (noteSkinType)
	{
	case NoteSkinType::RAINBOW:
		LoadDivGraph(_T("NoteSkins/rainbow/Down Tap Note 4th 4x4.png"), 16, 4, 4, 64, 64, m_Note4Images);
		LoadDivGraph(_T("NoteSkins/rainbow/Down Tap Note 8th 4x4.png"), 16, 4, 4, 64, 64, m_Note8);
		LoadDivGraph(_T("NoteSkins/rainbow/Down Tap Note 16th 4x4.png"), 16, 4, 4, 64, 64, m_Note12);
		LoadDivGraph(_T("NoteSkins/rainbow/Down Tap Note 16th 4x4.png"), 16, 4, 4, 64, 64, m_Note16);
		break;

	case NoteSkinType::NOTE:
		LoadDivGraph(_T("NoteSkins/note/Down Tap Note 4th 4x4.png"), 16, 4, 4, 64, 64, m_Note4Images);
		LoadDivGraph(_T("NoteSkins/note/Down Tap Note 8th 4x4.png"), 16, 4, 4, 64, 64, m_Note8);
		LoadDivGraph(_T("NoteSkins/note/Down Tap Note 12th 4x4.png"), 16, 4, 4, 64, 64, m_Note12);
		LoadDivGraph(_T("NoteSkins/note/Down Tap Note 16th 4x4.png"), 16, 4, 4, 64, 64, m_Note16);
		break;

	case NoteSkinType::FLAT:
		LoadDivGraph(_T("NoteSkins/flat/Down Tap Note 4x4.png"), 16, 4, 4, 64, 64, m_Note4Images);
		break;
	}
}

//キーを押したとき最も近いノートが各方向の上から何番目かを求める関数(「row[列]」に結果が入る。)
void NoteDanceElenation::GetKeyNote(const std::shared_ptr<KeyInput4Key>& f_keyinput, int matrix)
{
	for (auto m = 0; m < matrix; m++)
	{
		if (f_keyinput->m_B[m] == 1)
		{
			auto begin = m_NoteCount[m] - 5;
			auto end = m_NoteCount[m] + 5;

			if (begin < 0)
				begin = 0;

			if (end > static_cast<int>(m_Note[m].size()))
				end = static_cast<int>(m_Note[m].size());

			for (int a = begin; a < end; a++)
			{
				f_keyinput->m_Diff[m] = abs(f_keyinput->m_KeyTime[m] - m_NoteTime[m][a]);

				if (a == begin)
					f_keyinput->m_Min[m] = f_keyinput->m_Diff[m];

				if (f_keyinput->m_Diff[m] <= f_keyinput->m_Min[m])
				{
					f_keyinput->m_Min[m] = f_keyinput->m_Diff[m];
					f_keyinput->m_Row[m] = a;
				}
			}
			f_keyinput->m_B[m] = 0;
		}

		if (this->m_NoteFlag[m][f_keyinput->m_Row[m]] == -3)
			f_keyinput->m_Row[m]--;
	}
}

//ノートの判定処理を行う関数
void NoteDanceElenation::NoteJudgement(const std::shared_ptr<ScoreSt>& fScore, const std::shared_ptr<Combo>& f_combo, const std::shared_ptr<KeyInput4Key>& f_keyinput, const std::shared_ptr<LifeSt>& f_life, int m, int *k, long int now, int i, int Auto_flag)
{
	JudgeCount& judge = JudgeCount::GetInstance();
	//パーフェクト判定
	if (this->m_NoteTime[m][f_keyinput->m_Row[m]] - Setting.Judge.JudgePerfect <= now && now <= this->m_NoteTime[m][f_keyinput->m_Row[m]] + Setting.Judge.JudgePerfect)
	{
		if (this->m_NoteTime[m][f_keyinput->m_Row[m]] - Setting.Judge.JudgePerfect <= f_keyinput->m_KeyTime[m] && f_keyinput->m_KeyTime[m] <= this->m_NoteTime[m][f_keyinput->m_Row[m]] + Setting.Judge.JudgePerfect)
		{
			if (this->m_NoteFlag[m][f_keyinput->m_Row[m]] == -1)//通常ノートの場合
			{
				if (Auto_flag == false)
				{
					judge.parfect++;                                            //パーフェクト数をカウント
					for (sc = 0; sc < Setting.Life.LifePerfect; sc++)
					{
						if (f_life->m_Life < 500)
							f_life->m_Life++;
					}
					*k = 0;                                            //スコアのアニメーションで使うフラグカウンタを０にする
				}
				f_combo->m_AnimationFrame = 0;                                    //コンボのアニメーションのリセット
				this->m_NoteCount1++;                            //押されたノート数をカウント
				f_combo->m_ComboNum++;                                        //現在のコンボ数をカウント

				if (f_combo->m_ComboColorState < 1)                //コンボ画像のフラグ
					f_combo->m_ComboColorState = 1;

				this->m_JudgmentResult = 1;                            //パーフェクトの判定がされたことを記録する
				this->m_NoteFlag[m][f_keyinput->m_Row[m]] = 1;        //フラグに１を代入(押されたと判定)
				this->m_NoteFlagTime[m][f_keyinput->m_Row[m]] = static_cast<int>(now);//フラグに１を代入したときの時間を取得
				this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;    //ミスしたノートと押されたノートを足すと総ノート数になる。
			}
			else if (this->m_NoteFlag[m][f_keyinput->m_Row[m]] == -2)//ロングノートの場合
			{
				if (Auto_flag == false) {
					judge.parfect++;                                            //パーフェクト数をカウント
					for (sc = 0; sc < Setting.Life.LifePerfect; sc++)
					{
						if (f_life->m_Life < 500)
							f_life->m_Life++;
					}
					*k = 0;                                            //スコアのアニメーションで使うフラグカウンタを０にする
				}
				f_combo->m_AnimationFrame = 0;                                    //コンボのアニメーションのリセット
				this->m_NoteCount1++;                            //押されたノート数をカウント
				f_combo->m_ComboNum++;                                        //現在のコンボ数をカウント

				if (f_combo->m_ComboColorState < 1)                //コンボ画像のフラグ
					f_combo->m_ComboColorState = 1;

				this->m_JudgmentResult = 1;                            //パーフェクトの判定がされたことを記録する
				this->m_NoteFlag[m][f_keyinput->m_Row[m]] = 2;            //フラグに２を代入(押されたと判定)
				this->m_LongFlag[m] = 2;
				this->m_LongEndFlag[m] = 1;
				this->m_NoteFlagTime[m][f_keyinput->m_Row[m]] = static_cast<int>(now);//フラグに１を代入したときの時間を取得
				this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;    //ミスしたノートと押されたノートを足すと総ノート数になる。
			}
		}
	}

	//グレ判定
	else if (this->m_NoteTime[m][f_keyinput->m_Row[m]] - Setting.Judge.JudgeGreat <= now && now <= this->m_NoteTime[m][f_keyinput->m_Row[m]] - Setting.Judge.JudgePerfect ||
		this->m_NoteTime[m][f_keyinput->m_Row[m]] + Setting.Judge.JudgePerfect <= now &&  now <= this->m_NoteTime[m][f_keyinput->m_Row[m]] + Setting.Judge.JudgeGreat)
	{
		if (this->m_NoteTime[m][f_keyinput->m_Row[m]] - Setting.Judge.JudgeGreat <= f_keyinput->m_KeyTime[m] && f_keyinput->m_KeyTime[m] <= this->m_NoteTime[m][f_keyinput->m_Row[m]] - Setting.Judge.JudgePerfect ||
			this->m_NoteTime[m][f_keyinput->m_Row[m]] + Setting.Judge.JudgePerfect <= f_keyinput->m_KeyTime[m] && f_keyinput->m_KeyTime[m] <= this->m_NoteTime[m][f_keyinput->m_Row[m]] + Setting.Judge.JudgeGreat)
		{
			if (this->m_NoteFlag[m][f_keyinput->m_Row[m]] == -1)
			{
				if (Auto_flag == false)
				{
					judge.grate++;                                            //グレ数をカウント
					for (sc = 0; sc < Setting.Life.LifeGreat; sc++)
					{
						if (f_life->m_Life < 500)
							f_life->m_Life++;
					}
					*k = 0;                                            //スコアのアニメーションのリセット
				}
				f_combo->m_AnimationFrame = 0;                                    //コンボのアニメーションのリセット
				this->m_NoteCount1++;                            //押されたノート数をカウント
				f_combo->m_ComboNum++;                                        //現在のコンボ数をカウント

				if (f_combo->m_ComboColorState < 2)                //コンボ画像のフラグ
					f_combo->m_ComboColorState = 2;

				//this->note_count[m]++;
				this->m_JudgmentResult = 2;                        //グレの判定がされたことを記録する
				this->m_NoteFlag[m][f_keyinput->m_Row[m]] = 1;        //フラグに１を代入(押されたと判定)
				this->m_NoteFlagTime[m][f_keyinput->m_Row[m]] = static_cast<int>(now);//フラグに１を代入したときの時間を取得
				this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;    //ミスしたノートと押されたノートを足すと総ノート数になる。
			}
			else if (this->m_NoteFlag[m][f_keyinput->m_Row[m]] == -2)//ロングノートの場合
			{
				if (Auto_flag == false)
				{
					judge.grate++;                                            //グレ数をカウント
					for (sc = 0; sc < Setting.Life.LifeGreat; sc++)
					{
						if (f_life->m_Life < 500)
							f_life->m_Life++;
					}
					*k = 0;                                            //スコアのアニメーションのリセット
				}
				f_combo->m_AnimationFrame = 0;                                    //コンボのアニメーションのリセット
				this->m_NoteCount1++;                            //押されたノート数をカウント
				f_combo->m_ComboNum++;                                        //現在のコンボ数をカウント

				if (f_combo->m_ComboColorState < 2)                //コンボ画像のフラグ
					f_combo->m_ComboColorState = 2;

				//this->note_count[m]++;
				this->m_JudgmentResult = 2;                        //グレの判定がされたことを記録する
				this->m_NoteFlag[m][f_keyinput->m_Row[m]] = 2;        //フラグに２を代入(押されたと判定)
				this->m_LongFlag[m] = 2;
				this->m_LongEndFlag[m] = 1;
				this->m_NoteFlagTime[m][f_keyinput->m_Row[m]] = static_cast<int>(now);//フラグに２を代入したときの時間を取得
				this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;    //ミスしたノートと押されたノートを足すと総ノート数になる。
			}
		}
	}

	//グッド判定
	else if (this->m_NoteTime[m][f_keyinput->m_Row[m]] - Setting.Judge.JudgeGood <= now && now <= this->m_NoteTime[m][f_keyinput->m_Row[m]] - Setting.Judge.JudgeGreat || this->m_NoteTime[m][f_keyinput->m_Row[m]] + Setting.Judge.JudgeGreat <= now &&  now <= this->m_NoteTime[m][f_keyinput->m_Row[m]] + Setting.Judge.JudgeGood && this->m_NoteFlag[m][f_keyinput->m_Row[m]] != -3)
	{
		if (this->m_NoteTime[m][f_keyinput->m_Row[m]] - Setting.Judge.JudgeGood <= f_keyinput->m_KeyTime[m] && f_keyinput->m_KeyTime[m] <= this->m_NoteTime[m][f_keyinput->m_Row[m]] - Setting.Judge.JudgeGreat || this->m_NoteTime[m][f_keyinput->m_Row[m]] + Setting.Judge.JudgeGreat <= f_keyinput->m_KeyTime[m] && f_keyinput->m_KeyTime[m] <= this->m_NoteTime[m][f_keyinput->m_Row[m]] + Setting.Judge.JudgeGood)
		{
			if (this->m_NoteFlag[m][f_keyinput->m_Row[m]] == -1)
			{
				if (Auto_flag == false)
				{
					judge.good++;                                            //グッド数をカウント
					for (sc = 0; sc < Setting.Life.LifeGood; sc++)
					{
						if (f_life->m_Life < 500)
							f_life->m_Life++;
					}
					*k = 0;                                            //スコアのアニメーションのリセット
				}
				//this->noteCount1++;                            //押されたノート数をカウント
				judge.UpdateMaxComboOrNone(f_combo->m_ComboNum);
				f_combo->m_ComboNum++;

				if (f_combo->m_ComboColorState < 3)                //コンボ画像のフラグ
					f_combo->m_ComboColorState = 3;

				this->m_NoteCount[m]++;
				f_life->m_Life += 0;
				this->m_JudgmentResult = 3;                        //グッドの判定がされたことを記録する
				this->m_NoteFlag[m][f_keyinput->m_Row[m]] = 1;        //フラグに０を代入(切ったと判定)
				this->m_NoteFlagTime[m][f_keyinput->m_Row[m]] = static_cast<int>(now);//フラグに０を代入したときの時間を取得
				this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;    //ミスしたノートと押されたノートを足すと総ノート数になる。
			}
			else if (this->m_NoteFlag[m][f_keyinput->m_Row[m]] == -2)
			{
				if (Auto_flag == false)
				{
					judge.good++;                                            //グッド数をカウント
					for (sc = 0; sc < Setting.Life.LifeGood; sc++)
					{
						if (f_life->m_Life < 500)
							f_life->m_Life++;
					}
					*k = 0;                                            //スコアのアニメーションのリセット
				}                                //スコアのアニメーションのリセット
				this->m_NoteCount1++;                            //押されたノート数をカウント
				judge.UpdateMaxComboOrNone(f_combo->m_ComboNum);
				f_combo->m_ComboNum++;

				if (f_combo->m_ComboColorState < 3)                //コンボ画像のフラグ
					f_combo->m_ComboColorState = 3;

				//this->note_count[m]++;
				this->m_JudgmentResult = 3;                            //グッドの判定がされたことを記録する
				this->m_NoteFlag[m][f_keyinput->m_Row[m]] = 2;
				this->m_NoteFlagTime[m][f_keyinput->m_Row[m]] = static_cast<int>(now);//フラグに０を代入したときの時間を取得
				this->m_LongFlag[m] = 2;
				this->m_LongEndFlag[m] = 1;
				this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;    //ミスしたノートと押されたノートを足すと総ノート数になる。
			}
		}
	}
	/*------------フラグに０を代入する判定-------------*/
	/*ミスの処理を行なう */
	else if ((this->m_NoteTime[m][i] + 300 < now) && (now < this->m_NoteTime[m][i] + 400) && this->m_NoteFlag[m][i] == -1)
	{
		if (Auto_flag == false)
		{
			judge.missed++;
			for (sc = 0; sc < Setting.Life.LifeMiss; sc++)
			{
				if (f_life->m_Life <= 500)
					f_life->m_Life--;
			}
		}
		//this->note_count[m]++;
		this->m_NoteCount0++;
		judge.UpdateMaxComboOrNone(f_combo->m_ComboNum);
		f_combo->m_ComboNum = 0;

		if (f_combo->m_ComboColorState < 4)                //コンボ画像のフラグ
			f_combo->m_ComboColorState = 0;

		this->m_NoteFlag[m][i] = 0;
		this->m_NoteFlagTime[m][i] = static_cast<int>(now);                //フラグに０を代入したときの時間を取得
		this->m_JudgmentResult = 4;                                    //ミスの判定がされたことを記録する
		this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;
	}
	//LONGNOTEのミスの場合
	else if (this->m_NoteTime[m][i] + 300 < now  && now < this->m_NoteTime[m][i] + 350 && this->m_NoteFlag[m][i] == -2 && this->m_NoteFlag[m][f_keyinput->m_Row[m]] != -3)
	{
		if (Auto_flag == false)
		{
			judge.missed++;
			judge.NG++;

			for (sc = 0; sc < Setting.Life.LifeMiss; sc++)
			{
				if (f_life->m_Life <= 500)
					f_life->m_Life--;
			}
		}
		this->m_NoteCount[m]++;
		this->m_NoteCount0++;
		judge.UpdateMaxComboOrNone(f_combo->m_ComboNum);
		f_combo->m_ComboNum = 0;

		if (f_combo->m_ComboColorState < 4)                //コンボ画像のフラグ
			f_combo->m_ComboColorState = 0;

		//this->noteFlag[m][i] = 0;
		this->m_NoteFlagTime[m][i] = static_cast<int>(now);      //フラグに０を代入したときの時間を取得
		this->m_LongFlag[m] = 0;
		this->m_JudgmentResult = 4;                                    //ミスの判定がされたことを記録する
		this->m_LongEndFlag[m] = 0;
		this->m_NoteTotle = this->m_NoteCount0 + this->m_NoteCount1;
		this->m_U[m] = 0;
	}
}

//LONGNOTEホールド時の処理を行う関数
void NoteDanceElenation::LongNoteHold4Key(const std::shared_ptr<ScoreSt>& fScore, const std::shared_ptr<KeyInput4Key>& f_keyinput, int m, int *k, const long int now, int i, bool isAuto)
{
	if (isAuto)
	{
		m_KeyState[m] = 1;
	}
	else
	{
		if (m == 0)
			m_KeyState[0] = Input::GetInstance().keyLeft;
		else if (m == 1)
			m_KeyState[1] = Input::GetInstance().keyDown;
		else if (m == 2)
			m_KeyState[2] = Input::GetInstance().keyUp;
		else if (m == 3)
			m_KeyState[3] = Input::GetInstance().keyRight;
	}

	//途中で離した
	if (this->m_NoteFlag[m][f_keyinput->m_Row[m]] == 2 && this->m_NoteFlag[m][f_keyinput->m_Row[m] + 1] == -3 && m_KeyState[m] == 0 && f_keyinput->m_RFlag[m] == 0)
	{
		f_keyinput->m_ReleaseTime[m].restart();
		f_keyinput->m_RFlag[m] = 1;
	}
	//途中で離して押したとき
	if (f_keyinput->m_ReleaseTime[m].elapsed() <= 0.25 && f_keyinput->m_RFlag[m] == 1 && m_KeyState[m] > 0)
	{
		f_keyinput->m_RFlag[m] = 0;
	}
	//途中で離して一定時間経過
	if (f_keyinput->m_ReleaseTime[m].elapsed() > 0.25 && f_keyinput->m_RFlag[m] == 1)
	{
		this->m_U[m] = 0;                                //OK,NGアニメーションのリセット
		this->m_NoteFlag[m][f_keyinput->m_Row[m] + 1] = 0;
		this->m_NoteFlagTime[m][f_keyinput->m_Row[m] + 1] = static_cast<int>(now);
		this->m_LongFlag[m] = 0;
		this->m_LongEndFlag[m] = 0;
		f_keyinput->m_RFlag[m] = 0;
		if (!isAuto) JudgeCount::GetInstance().NG++;
	}
	//最後まで押せた
	if ((f_keyinput->m_ReleaseTime[m].elapsed() <= 0.25 || f_keyinput->m_RFlag[m] == 0) &&
		(this->m_NoteFlag[m][f_keyinput->m_Row[m]] == 2 && this->m_NoteFlag[m][f_keyinput->m_Row[m] + 1] == -3) &&
		this->m_NoteTime[m][f_keyinput->m_Row[m] + 1] < now)
	{
		//OK,NGアニメーションのリセット
		this->m_U[m] = 0;
		if (!isAuto)
		{
			JudgeCount::GetInstance().OK++;
			//スコアのアニメーションのリセット
			*k = 0;
		}
		this->m_NoteCount[m]++;
		this->m_NoteFlag[m][f_keyinput->m_Row[m] + 1] = 3;
		this->m_NoteFlagTime[m][f_keyinput->m_Row[m] + 1] = static_cast<int>(now);
		this->m_LongEndFlag[m] = 0;
		f_keyinput->m_RFlag[m] = 0;
		return;
	}

	if (this->m_NoteTime[m][i] < now && this->m_NoteFlag[m][i] == -3)
	{
		//OK,NGアニメーションのリセット
		this->m_U[m] = 0;
		this->m_NoteCount[m]++;
		this->m_NoteFlag[m][i] = 0;
		this->m_NoteFlagTime[m][i] = static_cast<int>(now);
		this->m_LongEndFlag[m] = 0;
		f_keyinput->m_RFlag[m] = 0;
		if (!isAuto) JudgeCount::GetInstance().NG++;
	}
}
//LONGNOTEのBODY画像の描画処理
void NoteDanceElenation::DrawHoldBody(const std::shared_ptr<KeyInput4Key>& fKeyinput, int m, int i, double calculater, double calculater2, double offsetPix, long int now, int picHandles[], int picHandles2[])
{
	auto length = 0;
	auto rLen = 0;
	auto n = 0;

	if (GamePlayOption::GetInstance().isReverse)
		this->m_JudgeLinePositionY = 555;

	if (this->m_NoteFlag[m][i] == -2 && this->m_NoteFlag[m][i + 1] == -3)
	{
		length = abs(static_cast<int>(this->m_Note[m][i + 1] * calculater - this->m_Note[m][i] * calculater));
		n = static_cast<int>((length - 29) / 128);
		rLen = (length - 29) % 128;
	}

	if (this->m_NoteFlag[m][i] == 2 && this->m_NoteFlag[m][i + 1] == -3)
	{
		length = abs(static_cast<int>((this->m_Note[m][i + 1] * calculater + offsetPix) - calculater2) - this->m_JudgeLinePositionY);
		n = static_cast<int>((length - 29) / 128);
		rLen = (length - 29) % 128;
	}

	//非ホールド時
	if (this->m_NoteFlag[m][i] == -2 && this->m_NoteFlag[m][i + 1] == -3)
	{
		if (!GamePlayOption::GetInstance().isReverse)
		{
			for (auto f = n - 1; f >= 0; f--)
			{
				DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i + 1] * calculater + offsetPix) - calculater2) - ONE_BEAT_PX - 29 - (f * 128), 1.0f, 0, picHandles2[m], TRUE);
			}
			DxLib::DrawRectGraph(MASU_X4 + ONE_BEAT_PX * (m)-(ONE_BEAT_PX / 2) + 2, static_cast<int>((this->m_Note[m][i] * calculater + offsetPix) - calculater2), 0, 128 - rLen, 60, rLen, picHandles2[m], TRUE, FALSE);
		}
		//Reverse
		else
		{
			for (auto f = n - 1; f >= 0; f--)
			{
				DrawRectYMirrorGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i + 1] * calculater + offsetPix) - calculater2) + 29 + (f * 128), 0, 0, 60, 128, (m == 1 || m == 2) ? picHandles[3 - m] : picHandles[m]);
			}
			DrawRectYMirrorGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i] * calculater + offsetPix) - calculater2) - rLen, 0, 128 - rLen, 60, rLen, (m == 1 || m == 2) ? picHandles[3 - m] : picHandles[m]);
		}
	}
	//ホールド時
	if (this->m_NoteFlag[m][i] == 2 && this->m_NoteFlag[m][i + 1] == -3)
	{
		if (!GamePlayOption::GetInstance().isReverse)
		{
			for (auto f = n - 1; f >= 0; f--)
			{
				DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i + 1] * calculater + offsetPix) - calculater2) - ONE_BEAT_PX - 29 - (f * 128), 1.0f, 0, picHandles2[m], TRUE);
			}
			DxLib::DrawRectGraph(MASU_X4 + ONE_BEAT_PX * (m)-(ONE_BEAT_PX / 2) + 2, this->m_JudgeLinePositionY, 0, 128 - rLen, 60, rLen, picHandles2[m], TRUE, FALSE);
			DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, static_cast<int>(255 - (255.0 / 0.25 * fKeyinput->m_ReleaseTimes[m])));
			for (auto f = n - 1; f >= 0; f--)
			{
				DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i + 1] * calculater + offsetPix) - calculater2) - ONE_BEAT_PX - 29 - (f * 128), 1.0f, 0, picHandles[m], TRUE);
			}
			DxLib::DrawRectGraph(MASU_X4 + ONE_BEAT_PX * (m)-(ONE_BEAT_PX / 2) + 2, this->m_JudgeLinePositionY, 0, 128 - rLen, 60, rLen, picHandles[m], TRUE, FALSE);
			//ブレンドモードをオフ
			DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		}
		//Reverse
		else
		{
			for (auto f = n - 1; f >= 0; f--)
			{
				DrawRectYMirrorGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i + 1] * calculater + offsetPix) - calculater2) + 29 + (f * 128), 0, 0, 60, 128, (m == 1 || m == 2) ? picHandles2[3 - m] : picHandles2[m]);
			}
			DrawRectYMirrorGraph(MASU_X4 + ONE_BEAT_PX * (m), this->m_JudgeLinePositionY - rLen, 0, 128 - rLen, 60, rLen, (m == 1 || m == 2) ? picHandles2[3 - m] : picHandles2[m]);

			DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, static_cast<int>(255 - (255.0 / 0.25 * fKeyinput->m_ReleaseTimes[m])));
			for (auto f = n - 1; f >= 0; f--)
			{
				DrawRectYMirrorGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i + 1] * calculater + offsetPix) - calculater2) + 29 + (f * 128), 0, 0, 60, 128, (m == 1 || m == 2) ? picHandles[3 - m] : picHandles[m]);
			}
			DrawRectYMirrorGraph(MASU_X4 + ONE_BEAT_PX * (m), this->m_JudgeLinePositionY - rLen, 0, 128 - rLen, 60, rLen, (m == 1 || m == 2) ? picHandles[3 - m] : picHandles[m]);
			//ブレンドモードをオフ
			DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		}
	}
}

//Auto使用でキーを押した時の時間を取得する関数
void NoteDanceElenation::GetKeyTimeAuto4Key(const std::shared_ptr<KeyInput4Key>& f_keyinput, long int now, int matrix)
{
	for (int m = 0; m < matrix; m++)
	{
		for (int i = static_cast<int>(this->m_Note[m].size()) - 1; i >= 0; i--)
		{
			if (this->m_NoteTime[m][i] < now && this->m_NoteTime[m][i] > 0 && this->m_NoteFlag[m][i] == -3)
			{
				f_keyinput->m_KeyTime[m] = static_cast<int>(now);

				if (f_keyinput->m_B[m] != 1)
					f_keyinput->m_B[m] = 1;
				break;
			}
			else if (this->m_NoteTime[m][i] < now && this->m_NoteTime[m][i]>0)
			{
				f_keyinput->m_KeyTime[m] = static_cast<int>(this->m_NoteTime[m][i]);

				if (f_keyinput->m_B[m] != 1)
					f_keyinput->m_B[m] = 1;
				break;
			}
		}
	}
}

//Auto使用でキーを押したとき最も近いノートが各方向の上から何番目かを求める関数(「basyo[横]」に結果が入る。)
void NoteDanceElenation::GetKeyNoteAuto4Key(const std::shared_ptr<KeyInput4Key>& f_keyinput, int matrix)
{
	for (auto m = 0; m < matrix; m++)
	{
		const auto sizeNote = static_cast<int>(this->m_Note[m].size());
		if (f_keyinput->m_B[m] == 1)
		{
			for (auto a = sizeNote - 1; a >= 0; a--)
			{
				f_keyinput->m_Diff[m] = abs(f_keyinput->m_KeyTime[m] - static_cast<int>(this->m_NoteTime[m][a]));

				if (a == sizeNote - 1)
					f_keyinput->m_Min[m] = f_keyinput->m_Diff[m];
				if (f_keyinput->m_Diff[m] <= f_keyinput->m_Min[m])
				{
					f_keyinput->m_Min[m] = f_keyinput->m_Diff[m];
					f_keyinput->m_Row[m] = a;
				}
			}
			f_keyinput->m_B[m] = 0;
		}
		if (this->m_NoteFlag[m][f_keyinput->m_Row[m]] == -3)
			f_keyinput->m_Row[m]--;
	}
}

void NoteDanceElenation::NoteBeatCycle(int startTime)
{
	for (auto i = 1; i < 16; i++)
	{
		if (static_cast<int>(m_InvBps16 / 16 * i * SY) <= m_BeatNowTime && m_BeatNowTime <= static_cast<int>(m_InvBps16 / 16 * (i + 1) * SY))
		{
			m_cycle = i;
			break;
		}
		else if (static_cast<int>(m_InvBps16 / 16 * 16 * SY) <= m_BeatNowTime)
		{
			m_cycle = 0;
			m_BeatTime = DxLib::GetNowCount();
			break;
		}
	}
	//裏16分用のcycle((cycle+8+16)%16)
	m_cycle2 = (m_cycle + 24) % 16;
}

//Note描画系関数
void NoteDanceElenation::NoteDraw(int m, int i)
{
	//(1P側)ノートの描画処理は各フラグが１以外のとき
	if (this->m_NoteFlag[m][i] != -1) return;
	//4分
	if (MyMod((this->m_Note[m][i] * MEASURE), MEASURE) == 0)
		DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel), 1.0f, this->m_rote[m], this->m_Note4Images[m_cycle], TRUE);
	//8分
	else if (MyMod((this->m_Note[m][i] * MEASURE * 2), MEASURE) == 0)
		DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel), 1.0f, this->m_rote[m], this->m_Note8[m_cycle], TRUE);
	//12分
	else if (MyMod((this->m_Note[m][i] * MEASURE * 3), MEASURE) == 0)
		DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel), 1.0f, this->m_rote[m], this->m_Note12[m_cycle], TRUE);
	//16分
	else if (MyMod((this->m_Note[m][i] * MEASURE * 4), MEASURE) == 0)
	{
		Image image = (this->m_Note[m][i] - static_cast<int>(this->m_Note[m][i]) == 0.25f) ? this->m_Note16[m_cycle] : this->m_Note16[m_cycle2];
		DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel), 1.0f, this->m_rote[m], std::move(image), TRUE);
	}
	//その他
	else
		DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel), 1.0f, this->m_rote[m], this->m_Note12[m_cycle], TRUE);
}

void NoteDanceElenation::NoteFlatDraw(int m, int i)
{
	if (this->m_NoteFlag[m][i] == -1)
		DxLib::DrawRotaGraph(MASU_X4 + 64 * (m), static_cast<int>((this->m_Note[m][i] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel), 1.0f, this->m_rote[m], this->m_Note4Images[m_cycle], TRUE);
}

void NoteDanceElenation::SetSpeed(double bpm)
{
	const auto pixbeat = ONE_BEAT_PX * GamePlayOption::GetInstance().speed;                         //等速の時、１拍の間隔を64ピクセルにしたい
	const auto bps = bpm / 60.0f;        //1秒間あたりの拍数
	const auto invBps = 1.0f / bps;                     //↑の逆数(1拍あたりの時間)
	const double invFps = 1.0f / 60.0f;                 //1秒で60フレーム描くから1フレームあたりの時間は逆数
	const auto fpb = invBps / invFps;              //(1拍あたりの時間/1フレームあたりの時間 = 1拍あたりのフレーム数)
	this->m_Speed = pixbeat / fpb;                       //1フレームあたりに動かすピクセル→speedに代入
	this->m_InvBps16 = invBps * 1000.0f;      //1拍あたりの時間(inv_bpsと違い、単位はﾐﾘ秒)
}

void NoteDanceElenation::SetPixelPerBeat()
{
	this->pixelPerBeat = ONE_BEAT_PX * GamePlayOption::GetInstance().speed;
}

void NoteDanceElenation::SetMovedPixel(double now)
{
	this->m_movedPixel = this->m_Speed * 60.0f / 1000.0f * now;
}

void NoteDanceElenation::DrawLongNoteBottom(int m, int i)
{
	if (GamePlayOption::GetInstance().isReverse == false)
	{
		if (this->m_NoteFlag[m][i] == -2 && this->m_NoteFlag[m][i + 1] == -3)
		{
			const auto dif = abs(static_cast<int>((this->m_Note[m][i + 1] * pixelPerBeat) - (this->m_Note[m][i] * pixelPerBeat))) + 30;
			if (dif > 58)
				DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i + 1] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel), 1.0f, 0, this->m_holdBottomCapInactiveImages[m], TRUE);
			else
				DxLib::DrawRectGraph(MASU_X4 + ONE_BEAT_PX * (m)-32 + 2, static_cast<int>((this->m_Note[m][i + 1] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel - dif / 2), 0, 58 - dif, 60, dif, this->m_holdBottomCapInactiveImages[m], TRUE, FALSE);
		}
	}
	if (GamePlayOption::GetInstance().isReverse == true)
	{
		if (this->m_NoteFlag[m][i] == -2 && this->m_NoteFlag[m][i + 1] == -3)
		{
			const auto dif = abs(static_cast<int>((this->m_Note[m][i + 1] * pixelPerBeat) - (this->m_Note[m][i] * pixelPerBeat))) + 30;
			if (dif > 58)
			{
				DxLib::DrawRotaGraph(MASU_X4 + 64 * (m), static_cast<int>((this->m_Note[m][i + 1] * this->pixelPerBeat + this->m_offsetPix) - m_movedPixel), 1.0f, PI, this->m_holdBottomCapInactiveImages[3 - m], TRUE);
			}
			else
			{
				const auto yPos = static_cast<int>((this->m_Note[m][i + 1] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel);
				DxLib::DrawRectRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>(yPos + 15.0f / 2.0f * GamePlayOption::GetInstance().speed * 0.5f), 0, 58 - dif, 60, dif, 1.0f, PI, (m == 0 || m == 3) ? this->m_holdBottomCapInactiveImages[m] : this->m_holdBottomCapInactiveImages[3 - m], TRUE, TRUE);
			}
		}
	}
}

void NoteDanceElenation::DrawLongNoteBody(int m, int i, const std::shared_ptr<KeyInput4Key>& f_keyinput)
{
	if (this->m_NoteFlag[m][i] == -2 && this->m_NoteFlag[m][i + 1] == -3)
		this->DrawHoldBody(f_keyinput, m, i, pixelPerBeat, m_movedPixel, m_offsetPix, GameBase::g_Now, m_holdBodyInactiveImages, m_holdBodyInactiveImages);
	if (this->m_NoteFlag[m][i] == 2 && this->m_NoteFlag[m][i + 1] == -3)
		this->DrawHoldBody(f_keyinput, m, i, pixelPerBeat, m_movedPixel, m_offsetPix, GameBase::g_Now, m_holdBodyActiveImages, m_holdBodyInactiveImages);
}

void NoteDanceElenation::DrawLongNoteTop(int m, int i)
{
	if (this->m_NoteFlag[m][i] == -2)
		DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel), 1.0f, this->m_rote[m], this->m_downHoldHeadInactiveImage, TRUE);
}

void NoteDanceElenation::DrawLongNoteBottomActive(int m, int i, const std::shared_ptr<KeyInput4Key>& f_keyinput)
{
	if (GamePlayOption::GetInstance().isReverse == false)
	{
		if (this->m_NoteFlag[m][i] == 2 && this->m_NoteFlag[m][i + 1] == -3 && this->m_LongEndFlag[m] == 1)
		{
			const auto rLen = static_cast<int>((this->m_Note[m][i + 1] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel) + 30 - this->m_JudgeLinePositionY;
			//下地
			if (rLen <= 58)
				DxLib::DrawRectGraph(MASU_X4 + ONE_BEAT_PX * (m)-32 + 2, this->m_JudgeLinePositionY, 0, 58 - rLen, 60, rLen, this->m_holdBottomCapInactiveImages[m], TRUE, FALSE);
			else
				DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i + 1] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel), 1.0f, 0, this->m_holdBottomCapInactiveImages[m], TRUE);
			//上乗せ
			DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, static_cast<int>(255 - (255.0 / 0.25 * f_keyinput->m_ReleaseTimes[m])));
			if (rLen <= 58)
				DxLib::DrawRectGraph(MASU_X4 + ONE_BEAT_PX * (m)-32 + 2, this->m_JudgeLinePositionY, 0, 58 - rLen, 60, rLen, this->m_holdBottomCapActiveImages[m], TRUE, FALSE);
			else
				DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i + 1] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel), 1.0f, 0, this->m_holdBottomCapActiveImages[m], TRUE);
			DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		}
	}
	if (GamePlayOption::GetInstance().isReverse == true)
	{
		if (this->m_NoteFlag[m][i] == 2 && this->m_NoteFlag[m][i + 1] == -3 && this->m_LongEndFlag[m] == 1)
		{
			const auto rLen = abs(static_cast<int>((this->m_Note[m][i + 1] * pixelPerBeat + m_offsetPix) - this->m_movedPixel) - 30 - this->m_JudgeLinePositionY);
			//下地
			if (rLen <= 58)
				DrawRectRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>(555 - rLen / 2.0), 0, 58 - rLen, 60, 58, 1.0f, PI, (m == 0 || m == 3) ? this->m_holdBottomCapInactiveImages[m] : this->m_holdBottomCapInactiveImages[3 - m], TRUE, TRUE);
			else
				DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i + 1] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel), 1.0f, PI, this->m_holdBottomCapInactiveImages[3 - m], TRUE);

			//上乗せ
			DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, static_cast<int>(255 - (255.0 / 0.25 * f_keyinput->m_ReleaseTimes[m])));
			if (rLen <= 58)
				DrawRectRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>(555 - rLen / 2.0), 0, 58 - rLen, 60, 58, 1.0f, PI, (m == 0 || m == 3) ? this->m_holdBottomCapActiveImages[m] : this->m_holdBottomCapActiveImages[3 - m], TRUE, TRUE);
			else
				DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * (m), static_cast<int>((this->m_Note[m][i + 1] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel), 1.0f, PI, this->m_holdBottomCapActiveImages[3 - m], TRUE);

			DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		}
	}
}

void NoteDanceElenation::DrawLongNoteTopActive(int m, int i, const std::shared_ptr<KeyInput4Key>& f_keyinput)
{
	if (this->m_NoteFlag[m][i] == 2 && this->m_NoteFlag[m][i + 1] == -3 && this->m_LongEndFlag[m] == 1)
	{
		DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * m, this->m_JudgeLinePositionY, 1.0f, this->m_rote[m], this->m_downHoldHeadInactiveImage, TRUE);
		DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, static_cast<int>(255 - (255.0 / 0.25 * (f_keyinput->m_ReleaseTimes[m]))));
		DxLib::DrawRotaGraph(MASU_X4 + ONE_BEAT_PX * m, this->m_JudgeLinePositionY, 1.0f, this->m_rote[m], this->m_downHoldHeadActiveImage, TRUE);
		DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}
}

void NoteDanceElenation::DrawNoteBehind(int m, int i)
{
	//フラグが0の時
	if (this->m_NoteFlag[m][i] == 0)
		DxLib::DrawRotaGraph(MASU_X4 + 64 * (m), static_cast<int>((this->m_Note[m][i] * this->pixelPerBeat + this->m_offsetPix) - this->m_movedPixel), 1.0f, this->m_rote[m], this->m_note8Image, TRUE);
}

void NoteDanceElenation::DrawRectYMirrorGraph(int x, int y, int srcX, int srcY, int srcWidth, int srcHeight, int graphHandle)
{
	DxLib::DrawRectExtendGraph(x - srcWidth / 2, y + srcHeight, x + srcWidth / 2, y, srcX, srcY, srcWidth, srcHeight, graphHandle, TRUE);
}

void NoteDanceElenation::SetOffsetPix(double offset, double bpm)
{
	//オフセット値から譜面のずれるピクセル数を計算する.
	m_offsetPix = GamePlayOption::GetInstance().speed * ((ONE_BEAT_PX * offset * bpm / ONE_MINUTE) + this->GAPn + this->GAPs) + this->m_JudgeLinePositionY;
}

//キーを押した時の時間を取得する関数
void NoteDanceElenation::GetKeyTime(const std::shared_ptr<KeyInput4Key>& f_keyinput, const long int now, const int matrix)
{
	if (Input::GetInstance().keyLeft == 1)
	{
		f_keyinput->m_KeyTime[0] = static_cast<int>(now);
		if (f_keyinput->m_B[0] != 1)
			f_keyinput->m_B[0] = 1;
	}
	if (Input::GetInstance().keyDown == 1)
	{
		f_keyinput->m_KeyTime[1] = static_cast<int>(now);
		if (f_keyinput->m_B[1] != 1)
			f_keyinput->m_B[1] = 1;
	}
	if (Input::GetInstance().keyUp == 1)
	{
		f_keyinput->m_KeyTime[2] = static_cast<int>(now);
		if (f_keyinput->m_B[2] != 1)
			f_keyinput->m_B[2] = 1;
	}
	if (Input::GetInstance().keyRight == 1)
	{
		f_keyinput->m_KeyTime[3] = static_cast<int>(now);
		if (f_keyinput->m_B[3] != 1)
			f_keyinput->m_B[3] = 1;
	}
}