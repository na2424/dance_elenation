#include <random>
#include <boost/lexical_cast.hpp>
#include "define.h"
#include "header.h"
#include "extern.h"
#include "Screen.h"
#include "SceneManager.h"
#include "BlueLightBall.h"
#include "savedata_class.h"
#include "Color.h"


using namespace DxLib;

//画面切り替えとかのエフェクトはこのcppにまとめておく
//フェードアウト
void Screen::FadeOut()
{
	//画像から黒へ
	const auto pic = DxLib::MakeGraph(WIN_WIDTH, WIN_HEIGHT);
	DxLib::GetDrawScreenGraph(0, 0, WIN_WIDTH, WIN_HEIGHT, pic);

	for (auto i = 255; i >= 0; i -= 4)
	{
		//輝度をだんだんさげていく
		DxLib::SetDrawBright(i, i, i);
		//画像描画
		DxLib::DrawGraph(0, 0, pic, FALSE);
		DxLib::ScreenFlip();
	}
	DxLib::SetDrawBright(0, 0, 0);
}

//フェードイン
void Screen::FadeIn()
{
	//黒から画像表示へ
	for (auto i = 0; i <= 255; i += 4)
	{
		//輝度をだんだんあげていく
		DxLib::SetDrawBright(i, i, i);
		//画像描画
		DrawGraph(0, 0, g_BackImage, TRUE);
		DxLib::ScreenFlip();
	}
	DxLib::SetDrawBright(255, 255, 255);
}

//タイトル画面⇒KEYSセレクト画面間のエフェクト
void Screen::TransitionTitleToKeysSelect()
{
	Fps fps;
	//フレームカウンタの初期化
	auto effectCount = 0;
	const auto leftM = LoadGraph(TEXT("Themes/DANCE ELENATION/Graphics/Screen/left_m.png"));
	const auto rightM = LoadGraph(TEXT("Themes/DANCE ELENATION/Graphics/Screen/right_m.png"));
	const auto centerM = LoadGraph(TEXT("Themes/DANCE ELENATION/Graphics/Screen/bar_dance_elenation.png"));
	const auto inSound = LoadSoundMem(TEXT("Themes/DANCE ELENATION/Sounds/Title/decide.mp3"));

	DxLib::SetDrawScreen(DX_SCREEN_BACK);
	DxLib::PlaySoundMem(inSound, DX_PLAYTYPE_BACK);

	while (ProcessMessage() != -1)
	{
		ClearDrawScreen();
		fps.Update();    //更新

		DxLib::DrawBox(0, 0, WIN_WIDTH, WIN_HEIGHT, Color::GetInstance().White(), TRUE);
		if (effectCount < 20)
		{
			DxLib::DrawRotaGraph2(WIN_WIDTH / 2 - 400 + effectCount * 20, WIN_HEIGHT / 2 + 400 - effectCount * 20, WIN_WIDTH / 2, WIN_HEIGHT / 2, 1.0, 0, leftM, TRUE);
			DxLib::DrawRotaGraph2(WIN_WIDTH / 2 + 400 - effectCount * 20, WIN_HEIGHT / 2 - 400 + effectCount * 20, WIN_WIDTH / 2, WIN_HEIGHT / 2, 1.0, 0, rightM, TRUE);
		}
		else if (effectCount < 80)
		{
			DxLib::DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT / 2, WIN_WIDTH / 2, WIN_HEIGHT / 2, 1.0, 0, leftM, TRUE);
			DxLib::DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT / 2, WIN_WIDTH / 2, WIN_HEIGHT / 2, 1.0, 0, rightM, TRUE);
		}
		else if (effectCount < 90)
		{
			DxLib::DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT / 2, WIN_WIDTH / 2, WIN_HEIGHT / 2, 1.0, (PI / 4.0)*((effectCount - 80) / 10.0), leftM, TRUE);
			DxLib::DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT / 2, WIN_WIDTH / 2, WIN_HEIGHT / 2, 1.0, (PI / 4.0)*((effectCount - 80) / 10.0), rightM, TRUE);
		}
		else if (effectCount < 100)
		{
			DxLib::DrawRotaGraph2(WIN_WIDTH / 2 + 7200 - effectCount * 80, WIN_HEIGHT / 2, WIN_WIDTH / 2, WIN_HEIGHT / 2, 1.0, PI / 4.0, leftM, TRUE);
			DxLib::DrawRotaGraph2(WIN_WIDTH / 2 - 7200 + effectCount * 80, WIN_HEIGHT / 2, WIN_WIDTH / 2, WIN_HEIGHT / 2, 1.0, PI / 4.0, rightM, TRUE);
		}
		DxLib::DrawGraph(0, WIN_HEIGHT / 2 - 50, centerM, TRUE);

		effectCount++;
		//描画
		fps.Draw();
		DxLib::ScreenFlip();
		if (effectCount > 100) break;
		//待機
		fps.Wait();
	}

	//指定のグラフィックをメモリ上から削除する
	DxLib::DeleteGraph(leftM);
	DxLib::DeleteGraph(rightM);
	DxLib::DeleteGraph(centerM);
}

//KEYSセレクト画面⇒曲選択画面間のエフェクト
void Screen::TransitionKeysSelectToMusicSelect()
{
	//フレームカウンタの初期化
	auto effectCount = 0;
	auto pic = 0;
	const auto m = 16;
	int efectAo[50];

	BlueLightBall lightball[m];
	//乱数生成クラス
	std::mt19937 engine;

	//範囲調節クラス 0から100までの乱数を作る
	const std::uniform_int_distribution<int> distribution(0, 100);
	//範囲調節クラス 0から360までの乱数を作る
	const std::uniform_int_distribution<int> distribution2(0, 360);

	//画像の読み込み
	const auto decideline = DxLib::LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Screen/decideline.png"));
	const auto circle = DxLib::LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Screen/circle.png"));

	//const auto lightballPic = DxLib::LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Title/lightball.png"));

	LoadDivGraph(_T("Themes/DANCE ELENATION/Graphics/Screen/efect01.png"), 50, 10, 5, 256, 256, efectAo);

	if (g_PlayMode == SelectKeys::KEYS_9)
		pic = DxLib::LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Screen/decide_9keys.png"));
	else if (g_PlayMode == SelectKeys::KEYS_4)
		pic = DxLib::LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Screen/decide_4keys.png"));
	else if (g_PlayMode == SelectKeys::KEYS_16)
		pic = DxLib::LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Screen/decide_16keys.png"));

	//音の読み込み
	const auto sound = DxLib::LoadSoundMem(_T("Themes/DANCE ELENATION/Sounds/KeysSelect/decide.wav"));

	//for (auto j = 0; j < m; ++j)
	//{
	//	lightball[j].x = (WIN_WIDTH / 2) - 50 + distribution(engine);
	//	lightball[j].y = (WIN_HEIGH / 2) - 50 + distribution(engine);
	//	lightball[j].vx = 1 + (GetRand(32) / 4.0);
	//	lightball[j].size = 0.20 + (GetRand(8) / 10.0);
	//	TH[j] = distribution2(engine);
	//}

	DxLib::SetDrawScreen(DX_SCREEN_BACK);

	//音を鳴らす
	DxLib::PlaySoundMem(sound, DX_PLAYTYPE_BACK);
	DrawGraph(0, 0, g_BackImage, TRUE);

	constexpr auto WIN_WIDTH_HARF = WIN_WIDTH / 2;
	constexpr auto WIN_HEIGHT_HARF = WIN_HEIGHT / 2;

	while (ProcessMessage() != -1)
	{
		const auto co = static_cast<int>(60 - ((effectCount + 600) % 600) / 60.0 * 60);

		/*for (auto j = 0; j < m; ++j)
		{
			lightball[j].x += static_cast<int>(lightball[j].vx * cos(2 * PI / 360.0 * TH[j]));
			lightball[j].y += static_cast<int>(lightball[j].vx * sin(2 * PI / 360.0 * TH[j]));
		}*/

		// 画面の初期化
		DxLib::ClearDrawScreen();
		DxLib::DrawBox(0, 0, WIN_WIDTH, WIN_HEIGHT, 0xffffff, TRUE);
		DxLib::DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT_HARF, 666 / 2, 666 / 2, 0.68 + effectCount*0.11, effectCount / 30.0, circle, TRUE);

		//DxLib::SetDrawBlendMode(DX_BLENDMODE_ADD, 200);             //ブレンドモードを加算(255/255)に設定
		//for (auto j = 0; j < m; j++)
		//	DrawRotaGraph2(static_cast<int>(lightball[j].x), static_cast<int>(lightball[j].y), 75, 75, lightball[j].size, 0, lightballPic, TRUE);
		//DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);           //ブレンドモードをオフ

		DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT_HARF, 128, 128, 1.0, 0, efectAo[effectCount], TRUE);

		if (effectCount < 25)
		{
			DxLib::DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT_HARF + 66, 400, 100, 1.0, 0, decideline, TRUE);
			DxLib::DrawGraph(0, 0, pic, TRUE);
		}
		else if (effectCount < 50)
		{
			DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, static_cast<int>(255 - (255.0 / 25.0*(effectCount - 25))));
			DxLib::DrawRotaGraph2(WIN_WIDTH / 2, WIN_HEIGHT_HARF + 66, 400, 100, 1.0, 0, decideline, TRUE);
			DxLib::DrawGraph(0, 0, pic, TRUE);
			DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		}

		if (0 < (effectCount + 600) % 600 && (effectCount + 600) % 600 < 60)
		{
			DxLib::SetDrawBlendMode(DX_BLENDMODE_ALPHA, co);
			DxLib::DrawRotaGraph2(WIN_WIDTH_HARF, WIN_HEIGHT_HARF, WIN_WIDTH_HARF, WIN_HEIGHT_HARF, 1 + static_cast<double>((effectCount + 600) % 600 / 160.0*2.4), 0, pic, TRUE);
			DxLib::DrawRotaGraph2(WIN_WIDTH_HARF, WIN_HEIGHT_HARF, WIN_WIDTH_HARF, WIN_HEIGHT_HARF, 1 + static_cast<double>((effectCount + 600) % 600 / 110.0*2.4), 0, pic, TRUE);
			DxLib::DrawRotaGraph2(WIN_WIDTH_HARF, WIN_HEIGHT_HARF, WIN_WIDTH_HARF, WIN_HEIGHT_HARF, 1 + static_cast<double>((effectCount + 600) % 600 / 80.0*2.4), 0, pic, TRUE);
			DxLib::SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		}

		DxLib::ScreenFlip(); //描画した画面の反映.

		if (effectCount >= 50)
			break;

		effectCount++;
	}

	//削除
	for (auto i = 0; i <= 50; ++i)
	{
		DxLib::DeleteGraph(efectAo[i]);
	}

	DxLib::DeleteGraph(pic);
	//DxLib::DeleteGraph(lightballPic);
}

//曲決定画面
void Screen::TransitionToGame()
{
	//フレームカウンタの初期化
	auto count = 0;
	Image stageImage = 0;
	const Sound sound = 0;

	if (OptionSaveData::g_IsEventMode)
	{
		stageImage = LoadGraph(_T("Themes/DANCE ELENATION/Graphics/Screen/eventmode.png"));
	}
	else
	{
		tstring fileName = OptionSaveData::g_SongCount == OptionSaveData::g_SongCountSetting + 1 ?
			_T("Themes/DANCE ELENATION/Graphics/Screen/finalstage.png") :
			(_T("Themes/DANCE ELENATION/Graphics/Screen/stage") + boost::lexical_cast<tstring>(OptionSaveData::g_SongCount) + _T(".png"));
		
		stageImage = LoadGraph(fileName.c_str());
	}

	//光度をもとに戻す
	DxLib::SetDrawBright(255, 255, 255);
	ClearDrawScreen();
	DxLib::SetDrawScreen(DX_SCREEN_BACK);
	//音を鳴らす
	DxLib::PlaySoundMem(sound, DX_PLAYTYPE_BACK);
	while (ProcessMessage() != -1)
	{
		if (count > 100)
			break;

		ClearDrawScreen();
		DrawRotaGraph(WIN_WIDTH / 2, WIN_HEIGHT / 2, 1.0, 0, stageImage, TRUE);

		DxLib::ScreenFlip();
		count++;
	}
	DxLib::DeleteGraph(stageImage);
}

//ゲーム画面のフェードイン
void Screen::GameFadeIn(const std::unique_ptr<Music> &musicP)
{
	if (g_PlayMode == SelectKeys::KEYS_9)
		g_BackImage = LoadGraph(TEXT("Themes/DANCE ELENATION/Graphics/Game/game_back9.png"));
	if (g_PlayMode == SelectKeys::KEYS_16)
		g_BackImage = LoadGraph(TEXT("Themes/DANCE ELENATION/Graphics/Game/game_back16.png"));

	musicAAImage = LoadGraph(musicP->elebeatSong->m_AlbumArtPath[musicNumber].c_str());

	//黒から画像表示へ
	for (auto i = 0; i <= 255; i += 4)
	{
		ClearDrawScreen();
		//輝度をだんだんあげていく
		SetDrawBright(i, i, i);
		//背景描画
		DrawGraph(0, 0, g_BackImage, TRUE);
		//アルバムアート画像の描画
		DrawModiGraph(876 - 64, 520 - 64, 876 + 64, 520 - 64, 876 + 64, 520 + 64, 876 - 64, 520 + 64, musicAAImage, FALSE);
		ScreenFlip();
	}
	SetDrawBright(255, 255, 255);
	DeleteGraph(g_BackImage);
}

//リザルト画面から他の画面に移るとき
void Screen::TransitionFromResult()
{
	const auto pic = MakeGraph(WIN_WIDTH, WIN_HEIGHT);
	GetDrawScreenGraph(0, 0, WIN_WIDTH, WIN_HEIGHT, pic);
	const auto centerM = LoadGraph(TEXT("Themes/DANCE ELENATION/Graphics/Screen/bar_dance_elenation.png"));
	SetDrawScreen(DX_SCREEN_BACK);
	for (auto i = 0; i <= WIN_HEIGHT / 48; i++)
	{
		// 画面の初期化
		ClearDrawScreen();
		//画像描画
		DrawModiGraph(0, i * 24, WIN_WIDTH, i * 24, WIN_WIDTH, WIN_HEIGHT - i * 24, 0, WIN_HEIGHT - i * 24, pic, FALSE);
		DrawGraph(0, WIN_HEIGHT / 2 - 50, centerM, TRUE);
		ScreenFlip();
	}
	DeleteGraph(centerM);
}