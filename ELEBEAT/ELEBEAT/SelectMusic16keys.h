#ifndef SELECTMUSIC_16KEYS_H
#define SELECTMUSIC_16KEYS_H

#include "SelectMusicBase.h"
#include "define.h"

//-----------------------------
//  16keys
//-----------------------------
class SelectMusic16keys : public SelectMusicBase
{
public:
	void Initialize(const std::shared_ptr<Music> &musicP) override;
	void Update(const std::shared_ptr<Music>& musicP) override;
	void Finalize(const std::shared_ptr<Music> &musicP) override;

	std::array<int, 10> d16Num20x30Images;
	int startTime;                //始まる時間
	int nowselectmarker;            //選択マーカーアニメーション用

	static std::vector<Image> g_AA;                            //アルバムアート    5000から代入していく
	static std::vector<Image> g_TempAA;                        //読み込んだAAをハンドルのみ保持する変数
	static std::vector<Image> g_KindFolder;                    //folder画像

	static SelectMode selectMode;                //0ならフォルダ1ならAA 2ならマーカーセレクト

	SelectMusic16keys();

private:
	int moveCounter;                //高速移動カウンタ
	int nowselectAAnum;                    //現在選択しているAAの音楽番号（１桁からの）
	int nowselectmarkernum;                //現在選択しているマーカー番号（一桁からの）
	int debug_AA;
	int switchkey;                        //キーのreturnでswitch用
	int replace;                        //AA置き換え用

	//----------------------
	// 画像
	//----------------------
	Image selectMusicImage;
	Image basicImage;
	Image normalImage;
	Image advancedImage;
	Image difficultyNumImage;
	Image difficultIconImage[3];

	//---------------------
	// 音声
	//---------------------
	Sound roop_music;

	//関数(16keys)
	void LoadSceneImage(void) const;
	void LoadSceneSound(void);

	bool LoadGraphAll(const std::shared_ptr<Music> &musicP);
	void GraphCoordinatesSet(void) const;
	bool MoveGraph(void);
	void SelectMarker(void) const;
	int CatchKey(void);
	int NowFolderNumber_16(int nowcenter);
	int NowAANumber_16(const int nowcenter) const;
	int NowMarkernumber_16(const int nowcenter) const;
	void ShowSelectAA(void);
	int SetSelectAA(int nowcenter, const std::shared_ptr<Music> &musicP);
	bool SelectFolderMusicExist(int nowcenter, const std::shared_ptr<Music> &musicP);
	bool SelectAAMusicExist(const int nowcenter) const;
	int MaxNum(const int num[]) const;
	int GetRandomMusicNum(SelectMode selectMode, int* difselect, const std::shared_ptr<Music> &musicP);
	int GetRandomFolderNum(int nowcenter, const std::shared_ptr<Music> &musicP);
	static int GetMaxLineFolder(const std::shared_ptr<Music> &musicP);
	int OpenFolder(int *, const std::shared_ptr<Music> &musicP);
	int AASelect(void);
	int RestorationAA(int *, const std::shared_ptr<Music> &musicP);
	void DelSelectMusic_16(int& loop_sound, int& back_sound);
	int MarkerTouch(void);
	void ReCalculateStringLength(int nowselectAAnum, const std::shared_ptr<Music> &musicP);

	void DrawDifficulty(int num);
	void DrawGraphAll_16(int nowcenter);

	void DrawMusicDetail(int nowselectAAnum, bool clear, const std::shared_ptr<Music> &musicP);
	void DrawLiteralString(void) const;
	void DrawMusicScoreChar(void) const;

	void DrawOtherDetail(int nowselectAAnum, const std::shared_ptr<Music> &musicP);
	void DrawPanelAll(void);
	void DrawHiScore_16(int nowselectAAnum, const std::shared_ptr<Music> &musicP);
	void MarkerMove(void);

	void debug(int nowcenter, int nowselectAAnum);
};

#endif