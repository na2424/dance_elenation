#ifndef SELECTMUSIC_9KEYS_H
#define SELECTMUSIC_9KEYS_H

#include <array>
#include "SelectMusicBase.h"
#include "define.h"
#include "Color.h"

class BlueLightBall;

//-----------------------------
//  9keys
//-----------------------------
class SelectMusic9keys : public SelectMusicBase
{
public:

	SelectMusic9keys();

	void Initialize(const std::shared_ptr<Music> &musicP) override;
	void Update(const std::shared_ptr<Music> &musicP) override;
	void Finalize(const std::shared_ptr<Music> &musicP) override;

	static std::vector<int> g_AA;            //アルバムアート    5000から代入していく
	static std::vector<int> g_TempAA;        //読み込んだAAをハンドルのみ保持する変数
	static std::vector<int> g_KindFolder;    //folder画像

private:

	std::vector<int> m_FolderMusicNum; //各フォルダ内楽曲数
	std::unique_ptr<TimerDrawer> m_NumberDraw;
	std::vector<BlueLightBall> m_LightBalls;

	int m_MusicKind;             //フォルダ数
	int m_MusicNum;              //楽曲数
	int m_SelectFolderNum;        //現在選択中のフォルダ番号

	int m_CurrentDisplayTime;                                            //現在のタイムを入れるための変数
	int m_time;
	int m_StartTime;                                        //スタートタイム
	int m_Key;
	int m_SelecterCounter;                                //セレクター回転カウンタ
	std::array<std::array<int, 3>, 3> m_difNumberTemp;    //難易度表示の計算用変数 前が三種類の難易度　後ろが番号用
	int m_LRfrag;
	int m_Counter;
	int m_FontHandle[4];
	int m_MarkerOptionPositionY;

	int m_GraphCache;

	EscapeState m_EscapeState;

	bool isLoadScore;
	bool isBack;
	bool isBreak;

	//---------------------
	// 定数.
	//---------------------
	static const int BALL_MAX = 39;
	const int DIFFICULT_BOX_COLOR[3] =
	{
		Color::GetInstance().Green(),
		Color::GetInstance().Yellow(),
		Color::GetInstance().Red()
	};

	//---------------------
	// フォント.
	//---------------------
	std::array<int, 4> m_fontHandle;

	//---------------------
	// 画像.
	//---------------------
	std::array<Image, 5> footImage;  //足画像
	int selecterImage = 0;
	int difnumberColorImage[4][12];
	int fullcombo[5];
	int hyouka[8];
	int setumei1;
	int setumei2;
	int player_option;

	//---------------------
	// 音声.
	//---------------------

	//---------------------
	// 関数.
	//---------------------

	//関数(9keys)
	void LoadSceneImage();
	void LoadSceneSound() const;
	void CreateSelectMusic(void) const;                                //コンストラクタ
	void GraphCoordinatesSet(void);                                //座標設定関数
	void MarkerCoordinatesSet(void);                            //マーカー座標指定関数
	void DrawGraphAll(int nowcenter);                            //画像を描画
	int CatchKey(int nowcenter, int& selectdifsound);            //難易度変更の上下キー受け取り
	int ChangeMode(int nowcenter, const std::shared_ptr<Music> &musicP);                //folder<-->AAの相互表示変換関数
	void SetMode(int nowcenter, const std::shared_ptr<Music> &musicP);
	void LoadGraphAll(const std::shared_ptr<Music> &musicP);                            //苦渋の決断
	int GetCurrentFolderNumber(int nowcenter, const std::shared_ptr<Music> &musicP) const;            //一般化の番号取得
	[[nodiscard]] int GetNowAANumber(int nowcenter) const;
	void SetAAGraph(int, int, int);
	void Delselectmusic(void);                                    //デストラクタ
	void CutBPM(int num, int nowcenter, const std::shared_ptr<Music> &musicP);                                        //bpm最大最小を得る
	void EscapeSelect(EscapeState escapeState, int nowcenter);
	int GetRandom(int min, int max, const std::shared_ptr<Music> &musicP);
	void DifNumberCalculate(std::array<std::array<int, 3>, 3> number, int nowcenter, const std::shared_ptr<Music> &musicP);        //難易度のアレ計算用
	void DrawHiScore(int nowcenter, int count, int fullcombo_gazou[], int hyoka_gazou[], int FontHandle[], bool *isLoadScore, const std::shared_ptr<Music> &musicP);
	void LoadHiScore(int nowcenter, const std::shared_ptr<Music> &musicP);                            //ハイスコア読み込み処理
	void LayerStringDraw(int stringX, int stirngY, const TCHAR drawstring[], int FontHandle) const;
	void DelSelectMusic_9(int& loop_sound, int& back_sound);        ///デストラクタ
	void DrawLR(int *right_pic, int *left_pic);
	void DrawLR_click(int *right_click_pic, int *left_click_pic);
	void DrawMusicString(int musicNumber, int FontHandle[], int counter, const std::shared_ptr<Music> &musicP) const;        //曲名とか出す

	void ShowMarker(int time, int selectmarker, int marker_y);

	void OnTimeUpFolder(const std::shared_ptr<Music> &musicP);
	void OnTimeUpAA();
	void ClearDifNum();
};

#endif