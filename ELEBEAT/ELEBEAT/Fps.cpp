#include "Fps.h"
#include <windows.h>
#include <math.h>
#include "DxLib.h"
#include "savedata_class.h"

bool Fps::Update()
{
	//1フレーム目なら時刻を記憶
	if (m_count == 0)
	{
		m_startTime = GetNowCount();
	}
	//60フレーム目なら平均を計算する
	if (m_count == N)
	{
		const auto t = GetNowCount();
		m_fps = 1000.f / ((t - m_startTime) / static_cast<float>(N));
		m_count = 0;
		m_startTime = t;
	}
	m_count++;
	return true;
}

void Fps::Draw() const
{
	if (OptionSaveData::g_ShowFps)
		DrawFormatString(0, 0, GetColor(255, 255, 255), _T("FPS %2.0f/60"), round(m_fps));
}

void Fps::Wait() const
{
	const auto tookTime = GetNowCount() - m_startTime;    //かかった時間
	const auto waitTime = m_count * 1000 / FPS - tookTime;    //待つべき時間
	if (waitTime > 0)
	{
		Sleep(waitTime);    //待機
	}
}