//
// Generated on 2013/06/30 by inimoni 
//

/*
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Documentation
  Struct = Section name, Field = Key name.
  The function used is only a declaration, load(), and save().
  The constructor automatically reads initialization file [initFileName].
  Please rewrite [initFileName] in the source freely.
  It becomes Windows folder when there is not path. The relative path can be used.

Example
#include "SampleIni.h"
void CTestDlg::OnButton1()
{
    //initFileName = "./sample.ini";
    SampleIni data1;                   // Open ./sample.ini
    int v = data1.Sample.value;        // [Sample]-value Get
    data1.Sample.value = v+1;          // [Sample]-value Modify
    data1.save();                      // Save ./Sample.ini (Opened file)

    SampleIni data2("sample.ini");     // Open C:/WINDOWS/Sample.ini
    data2.load("./sample.ini");        // Open Specified file.
    data2.save();                      // Save Opened file (Making at not being)
}

Supplementation
  Reading and writing becomes the unit of the file.
  [initFileName] is the same as the argument of GetPrivateProfileString() of SDK.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
*/

#ifndef KEYCONFIGINI_H
#define KEYCONFIGINI_H

#include <string>
using namespace std;

#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#ifdef _UNICODE
	typedef std::wstring string_t;
#else
	typedef std::string string_t;
#endif

namespace inimoni{
    template<class T>
    static bool inirw(int is_read, string_t& fname, string_t sec, string_t key, T& val_t);
};

/**
    KeyConfig.ini input output class.
*/
class KeyConfigIni
{
public:                                                                 
                                                                        
    KeyConfigIni(string_t fname=_T(""))                          
    {                                                                   
        initFileName = _T("./KeyConfig.ini"); //Default file.    
        init();                                                         
        load(fname);                                                    
    }                                                                   
                                                                        
    /**                                                                 
        INI file is read.                                               
        @param fname Filename                                           
        When there is not a file, It becomes initFileName.              
        When there is not a pass, It becomes Windows folder.            
    */                                                                  
    bool load(string_t fname=_T(""))                                  
    {                                                                   
        if(fname.empty()){                                              
            fname = initFileName;                                       
        }                                                               
        loadFileName = fname;                                           
        WIN32_FIND_DATA fd;                                             
        HANDLE h = ::FindFirstFile(fname.c_str(), &fd);                 
        if (h != INVALID_HANDLE_VALUE) {                                
            iniRW(fname,1);                                             
        }                                                               
        return (h != INVALID_HANDLE_VALUE);                             
    }                                                                   
                                                                        
    /**                                                                 
        It writes it in the INI file.                                   
        @param fname Filename                                           
        When there is not a file, It becomes open file.                 
        When there is not a pass, It becomes Windows folder.            
    */                                                                  
    bool save(string_t fname=_T(""))                                  
    {                                                                   
        if(fname.empty()){                                              
            fname = loadFileName;                                       
        }                                                               
        iniRW(fname,0);                                                 
        return true;                                                    
    }                                                                   

public:                                                                 

    // KeyConfig
    struct _KeyConfig
    {
        int      KeyDown;
        int      KeyLeft;
        int      KeyRight;
        int      KeyUp;
        int      KeyStart;
        int      KeyBack;
        int      KeyL;
        int      KeyR;
        int      Key1;
        int      Key2;
        int      Key3;
        int      Key4;
        int      Key5;
        int      Key6;
        int      Key7;
        int      Key8;
        int      Key9;
        int      Key10;
        int      Key11;
        int      Key12;
        int      Key13;
        int      Key14;
        int      Key15;
        int      Key16;
    } KeyConfig;

protected:

    string_t initFileName;
    string_t loadFileName;

    bool iniRW(string_t f, int r)    
    {                                
        string_t s;                  

        s = _T("KeyConfig");
        inimoni::inirw( r,f,s, _T("KeyDown          "), KeyConfig.KeyDown  );
        inimoni::inirw( r,f,s, _T("KeyLeft          "), KeyConfig.KeyLeft  );
        inimoni::inirw( r,f,s, _T("KeyRight         "), KeyConfig.KeyRight );
        inimoni::inirw( r,f,s, _T("KeyUp            "), KeyConfig.KeyUp    );
        inimoni::inirw( r,f,s, _T("KeyStart         "), KeyConfig.KeyStart );
        inimoni::inirw( r,f,s, _T("KeyBack          "), KeyConfig.KeyBack  );
        inimoni::inirw( r,f,s, _T("KeyL             "), KeyConfig.KeyL     );
        inimoni::inirw( r,f,s, _T("KeyR             "), KeyConfig.KeyR     );
        inimoni::inirw( r,f,s, _T("Key1             "), KeyConfig.Key1     );
        inimoni::inirw( r,f,s, _T("Key2             "), KeyConfig.Key2     );
        inimoni::inirw( r,f,s, _T("Key3             "), KeyConfig.Key3     );
        inimoni::inirw( r,f,s, _T("Key4             "), KeyConfig.Key4     );
        inimoni::inirw( r,f,s, _T("Key5             "), KeyConfig.Key5     );
        inimoni::inirw( r,f,s, _T("Key6             "), KeyConfig.Key6     );
        inimoni::inirw( r,f,s, _T("Key7             "), KeyConfig.Key7     );
        inimoni::inirw( r,f,s, _T("Key8             "), KeyConfig.Key8     );
        inimoni::inirw( r,f,s, _T("Key9             "), KeyConfig.Key9     );
        inimoni::inirw( r,f,s, _T("Key10            "), KeyConfig.Key10    );
        inimoni::inirw( r,f,s, _T("Key11            "), KeyConfig.Key11    );
        inimoni::inirw( r,f,s, _T("Key12            "), KeyConfig.Key12    );
        inimoni::inirw( r,f,s, _T("Key13            "), KeyConfig.Key13    );
        inimoni::inirw( r,f,s, _T("Key14            "), KeyConfig.Key14    );
        inimoni::inirw( r,f,s, _T("Key15            "), KeyConfig.Key15    );
        inimoni::inirw( r,f,s, _T("Key16            "), KeyConfig.Key16    );
        return true;                                                    
    }                                                                   

    void init()                                                         
    {                                                                   
        KeyConfig.KeyDown            = 208;
        KeyConfig.KeyLeft            = 203;
        KeyConfig.KeyRight           = 205;
        KeyConfig.KeyUp              = 200;
        KeyConfig.KeyStart           = 1;
        KeyConfig.KeyBack            = 28;
        KeyConfig.KeyL               = 42;
        KeyConfig.KeyR               = 54;
        KeyConfig.Key1               = 2;
        KeyConfig.Key2               = 3;
        KeyConfig.Key3               = 4;
        KeyConfig.Key4               = 5;
        KeyConfig.Key5               = 16;
        KeyConfig.Key6               = 17;
        KeyConfig.Key7               = 18;
        KeyConfig.Key8               = 19;
        KeyConfig.Key9               = 30;
        KeyConfig.Key10              = 31;
        KeyConfig.Key11              = 32;
        KeyConfig.Key12              = 33;
        KeyConfig.Key13              = 44;
        KeyConfig.Key14              = 45;
        KeyConfig.Key15              = 46;
        KeyConfig.Key16              = 47;
    }                                                                   
};
typedef KeyConfigIni KeyConfigFile; //�V���݊�

//---------------------------------------------------------------------------
// Common method                                                             
//---------------------------------------------------------------------------
#ifndef INIMONI_INIRW                                                        
#define INIMONI_INIRW                                                        
namespace inimoni2                                                            
{                                                                            
    /*                                                                       
    Read and Write INI file                                                  
      int     is_read  1=Read mode, 0=Write mode                             
      string  fname    Filename (The Windows folder when there is not path)  
      string  sec      Section name                                          
      string  key      Key name                                              
      T       val_t    [Read]Init+Output, [Write]Input                       
    */                                                                       
    template<class T>                                                        
    bool inirw(int is_read, string_t& fname, string_t sec, string_t key, T& val_t)
	{                                                                        
		if(is_read){                                                         
            inimoni2::read(fname.c_str(), sec.c_str(), key.c_str(), val_t);   
        }                                                                    
        else{                                                                
			inimoni2::write(fname.c_str(), sec.c_str(), key.c_str(), val_t);  
        }                                                                    
		return true;                                                         
    }                                                                        
                                                                             
    bool read(string_t ifn, string_t sec, string_t key, int& dst)            
    {                                                                        
        dst = GetPrivateProfileInt( sec.c_str(), key.c_str(), dst, ifn.c_str() );
        return true;                                                         
    }                                                                        
                                                                             
    bool read(string_t ifn, string_t sec, string_t key, basic_string<TCHAR>& dst)
    {                                                                        
        TCHAR buf[256];                                                      
        GetPrivateProfileString(                                             
            sec.c_str(),                                                     
            key.c_str(),                                                     
            dst.c_str(),                                                     
            buf,                                                             
            sizeof(buf),                                                     
            ifn.c_str() );                                                   
        dst = buf;                                                           
        return true;                                                         
    }                                                                        
                                                                             
    bool read(string_t ifn, string_t sec, string_t key, double& dst)         
    {                                                                        
        string_t s;                                                          
        inimoni2::read(ifn, sec, key, s);                                     
                                                                             
        TCHAR* e;                                                            
        double x = _tcstod(s.c_str(), &e);                                   
                                                                             
        dst = 0.0;                                                           
        if (!*e) {                                                           
            dst = x;                                                         
        }                                                                    
        return true;                                                         
    }                                                                        
                                                                             
    template<class T>                                                        
    bool write(string_t ifn, string_t sec, string_t key, T val_t)            
    {                                                                        
        TCHAR val[1024];                                                     
        inimoni2::to_string(val, val_t);                                      
        WritePrivateProfileString( sec.c_str(), key.c_str(), val, ifn.c_str() );
		return true;                                                    
	}                                                                   
                                                                        
    void to_string(TCHAR* str, int val)                                 
    {                                                                   
        _stprintf( str, _T("%d"), val );                                
    }                                                                   
                                                                        
    void to_string(TCHAR* str, double val)                              
    {                                                                   
        _stprintf( str, _T("%f"), val );                                
    }                                                                   
                                                                        
    void to_string(TCHAR* str, basic_string<TCHAR> val)                 
    {                                                                   
        _stprintf( str, _T("%s"), val.c_str() );                        
    }                                                                   
};                                                                      
                                                                        
#endif                                                                  
#endif                                                                  

