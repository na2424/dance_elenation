#include "SelectMusic16keys.h"
#include "extern.h"
#include "define.h"
#include "stdlib.h"
#include "header.h"
#include "Input.h"
#include "Marker.h"
#include "Screen.h"
#include "Color.h"
#include "Foreach.h"
#include "MouseClickObject.h"
#include "ScreenShot.h"
#include "GameBase.h"


using namespace DxLib;

std::vector<Image> SelectMusic16keys::g_AA;                //描画用
std::vector<Image> SelectMusic16keys::g_TempAA;            //保持用の変数
std::vector<Image> SelectMusic16keys::g_KindFolder;        //folderAA

SelectMode SelectMusic16keys::selectMode = SelectMode::FOLDER;

SelectMusic16keys::SelectMusic16keys() :
	startTime(0),
	nowselectmarker(0),
	selectMusicImage(0),
	basicImage(0),
	normalImage(0),
	advancedImage(0),
	difficultyNumImage(0)
{
	//16keys難易度画像
	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/d-16num20x30.png"), 10, 10, 1, 20, 30, d16Num20x30Images.data());
	m_mouseClickObjectList = std::make_unique<MouseClickObjectList>();
}

void SelectMusic16keys::LoadSceneImage() const
{
}

void SelectMusic16keys::LoadSceneSound()
{
	if (rand() % 2 == 1)
		roop_music = LoadSoundMem(_T("Themes\\DANCE ELENATION\\Sounds\\MusicSelect\\roop.ogg"));
	else
		roop_music = LoadSoundMem(_T("Themes\\DANCE ELENATION\\Sounds\\MusicSelect\\roop.mp3"));

	backSound = LoadSoundMem(_T("Themes\\DANCE ELENATION\\Sounds\\Common back.ogg"));
	decideSound = LoadSoundMem(_T("Themes\\DANCE ELENATION\\Sounds\\MusicSelect\\decide.mp3"));
	expandSound = LoadSoundMem(_T("Themes\\DANCE ELENATION\\Sounds\\MusicSelect\\MusicWheel expand.mp3"));
	closeSound = LoadSoundMem(_T("Themes\\DANCE ELENATION\\Sounds\\MusicSelect\\cursor1.ogg"));
}

//16Keys曲選択画面
void SelectMusic16keys::Initialize(const std::shared_ptr<Music> &musicP)
{
	setlocale(LC_ALL, "ja_JP.UTF-8");
	Marker& marker = Marker::GetInstance();

	loadendflag_16 = 0;
	maxline_folder = GetMaxLineFolder(musicP);

	if (beforeSelectMusicNum_16 == -1)
	{
		//フラグ初期化
		this->count = 0;
		g_NowCenter = CENTER_DEFAULT_16;
		moveleft = 0;                    //左へ
		moveright = 0;                    //右へ
		leftcountflag_16 = 0;            //押し始め
		rightcountflag_16 = 0;            //押し始め
		fastleftflag_16 = 0;            //高速移動許可
		fastrightflag_16 = 0;            //高速移動許可
		difselect = GameBase::g_MusicDif;            //選択音楽の難易度
		nowselectAAnum = musicNumber;    //選択中の音楽設定
	}
	else
	{
		//初回なら 主にstaticロードを入れとく
		g_NowCenter = beforecenter_16;
		nowselectglobalAAnum = beforeselectAA;
		this->count = 0;

		if (marker.marker_num % 3 == 0)
			maxline_marker = marker.marker_num / 3;
		else
			maxline_marker = (marker.marker_num / 3) + 1;
	}

	//フォントサイズ
	SetFontSize(18);

	//枚数設定
	if (musicP->elebeatSong->m_FolderNum <= 12) //12枚以下で動けなく
	{
		stop[0] = 1;
	}
	if (marker.marker_num <= 12) //marker移動判別
	{
		stop[2] = 1;
	}

	//AAの最初をRand画像に変更
	TCHAR random__aa[20] = _T("img\\random-aa.png");

	while (1)
	{
		auto isEmpty = musicP->elebeatSong->m_AlbumArtPath[RandAAReplace[replace]].empty();

		for (int i = 0; i < 20; i++) 
		{
			musicP->elebeatSong->m_AlbumArtPath[RandAAReplace[replace]].push_back(random__aa[i]);
		}
		replace++;
		if (replace == RandAAReplace.size())
			break;
	}

	//ロードと座標
	if (loadendflag_16 == 0)
	{
		//ロード完了フラグ立ってなかったら
		LoadGraphAll(musicP);
	}
	GraphCoordinatesSet();

	// 音声をループ再生する
	PlaySoundMem(roop_music, DX_PLAYTYPE_LOOP);

	//黒から画像表示へ
	for (int i = 0; i <= 255; i += 4)
	{
		//輝度をだんだんあげていく
		SetDrawBright(i, i, i);
		//画像描画
		DrawGraphAll_16(g_NowCenter);
		DrawPanelAll();
		ScreenFlip();
	}
	SetDrawBright(255, 255, 255);

	//時間取得
	startTime = GetNowCount();
}

void SelectMusic16keys::Update(const std::shared_ptr<Music>& musicP)
{
	//シングルトン参照
	auto& marker = Marker::GetInstance();
	auto& input = Input::GetInstance();

	//カウント
	count++;

	//キー操作
	input.GetInstance().GetKey();

	//マウスを取得
	input.GetMouse();

	m_mouseClickObjectList->Update();

	//画面消去
	ClearDrawScreen();

	//時間取得
	startTime = GetNowCount();

	//かきかき
	DrawGraphAll_16(g_NowCenter);

	if (input.keyEsc > 0)
	{
		SceneManager::SetNextScene(Scenes::TITLE);
		PlaySoundMem(backSound, DX_PLAYTYPE_BACK);
		StopSoundMem(roop_music);
		ClearDrawScreen();
		Screen::FadeOut();
		ClearDrawScreen();
		DelSelectMusic_16(roop_music, backSound);

		return;
	}

	//キー
	switch (switchkey = CatchKey())
	{
	case 1:
		//keyのreturnで操作
		PlaySoundMem(decideSound, DX_PLAYTYPE_BACK);
		SceneManager::SetNextScene(Scenes::GAME);
		DelSelectMusic_16(roop_music, backSound);

		//ランダム選曲の場合
		if (nowselectAAnum == 0)
		{
			musicNumber = GetRandomMusicNum(selectMode, &difselect, musicP);
			return;
		}
		//ゲームへ
		return;
		break;

	case -1:
		//esc押してタイトルに戻る
		SceneManager::SetNextScene(Scenes::TITLE);
		PlaySoundMem(backSound, DX_PLAYTYPE_BACK);
		StopSoundMem(roop_music);
		ClearDrawScreen();
		Screen::FadeOut();
		ClearDrawScreen();
		DelSelectMusic_16(roop_music, backSound);
		break;
	}

	if (SceneManager::g_CurrentScene != Scenes::MUSIC_SELECT)
	{
		return;
	}

	//表示なうによって変動
	//folder表示
	if (selectMode == SelectMode::FOLDER)
	{
		//folderなので選択権なし
		selectmusicflag = 0;
		//folder選択
		if (selectkeynumber >= 0 && selectkeynumber <= 11)
		{
			//folder開く関数
			OpenFolder(&expandSound, musicP);
			PlaySoundMem(expandSound, DX_PLAYTYPE_BACK);
			//音楽情報出さない苦肉の策
			nowselectAAnum = NULL;

			//選択してない状態に戻す
			selectkeynumber = -1;
			//長さ初期化
			strlength[0] = strlength[1] = strlength[2] = 0;
			//初期化
			movedrawflag[0] = movedrawflag[1] = movedrawflag[2] = 0;
		}
	}
	else if (selectMode == SelectMode::AA) //AA表示
	{
		if (selectkeynumber >= 0 && selectkeynumber <= 11) //AA選択
		{
			//クリアフラグ
			DrawMusicDetail(0, 1, musicP);
			//音楽存在するならば設定
			if (AASelect() != -1)
			{
				nowselectAAnum = nowselectglobalAAnum;
				//選択したAAがスクロールするかの処理
				ReCalculateStringLength(nowselectAAnum, musicP);
			}
			//選択してない状態に戻す
			selectkeynumber = -1;
		}
	}
	else  //marker
	{
		//markerなので選択権なし
		selectmusicflag = 0;
		//marker選択
		if (selectkeynumber >= 0 && selectkeynumber < marker.marker_num)
		{
			//前と同じマーカー選択した時
			if (MarkerTouch() == 1)
				MarkerMove();

			if (selectkeynumber < marker.marker_num)
			{
				//マーカー選択を渡す
				nowselectmarker = selectkeynumber;
			}
			selectkeynumber = -1;
		}
		//マーカー動かす
	}
	GameBase::g_MusicDif = difselect;
	//移動 止めるのも代入
	if (moveleft || moveright)
	{
		if (selectMode == SelectMode::FOLDER)
		{
			if (!stop[0])
				MoveGraph();
			else if (stop[0])
				moveright = moveleft = 0;
		}
		else if (selectMode == SelectMode::AA)
		{
			if (!stop[1])
				MoveGraph();
			else if (stop[1])
				moveright = moveleft = 0;
		}
	}

	//左押し続け
	if (leftcountflag_16)
	{
		moveCounter++;
		//もし一定数以上押されたら
		if (moveCounter >= 60)
		{
			//高速フラグを立てる
			fastleftflag_16 = 1;
			//int over flowをなくす
			if (moveCounter >= 10000)
				moveCounter = 60;
		}
	}

	//右押し続け
	if (rightcountflag_16)
	{
		moveCounter++;
		//もし一定数以上押されたら
		if (moveCounter >= 60)
		{
			//高速フラグを立てる
			fastrightflag_16 = 1;
			//int over flowをなくす
			if (moveCounter >= 10000)
				moveCounter = 60;
		}
	}

	//どっちも押してすらないとき全部初期化
	if (!leftcountflag_16 && !rightcountflag_16)
	{
		moveCounter = 0;
		fastleftflag_16 = 0;
		fastrightflag_16 = 0;
	}

	//i,goによる高速化移動
	if (this->count == 6)
	{
		if (fastleftflag_16 == 1 || fastrightflag_16 == 1)
		{
			if (moveleft == 1)
			{
				g_NowCenter -= 3;
				moveleft = 0;
			}
			if (moveright == 1)
			{
				g_NowCenter += 3;
				moveright = 0;
			}
			//座標設定
			GraphCoordinatesSet();

			if (selectMode == SelectMode::FOLDER)
			{
				if (g_NowCenter >= CENTER_DEFAULT_16 + (musicP->elebeatSong->m_FolderNum) || g_NowCenter <= CENTER_DEFAULT_16 - (musicP->elebeatSong->m_FolderNum))
				{
					g_NowCenter = CENTER_DEFAULT_16;
				}
			}
			else if (selectMode == SelectMode::AA)
			{
				if (g_NowCenter >= CENTER_DEFAULT_16 + nowAA_16[2] || g_NowCenter <= CENTER_DEFAULT_16 - nowAA_16[2])
				{
					g_NowCenter = CENTER_DEFAULT_16;
					DrawFormatString(300, 100, Color::GetInstance().White(), _T("reset"));
				}
			}
			else
			{
				//marker
			}
			this->count = 0;
		}
	}

	//move AA
	if (this->count == 12)
	{
		this->count = 0;
		if (moveleft == 1)
		{
			g_NowCenter -= 3;
			moveleft = 0;
		}
		if (moveright == 1)
		{
			g_NowCenter += 3;
			moveright = 0;
		}

		GraphCoordinatesSet();

		if (selectMode == SelectMode::FOLDER)
		{
			if (g_NowCenter >= CENTER_DEFAULT_16 + (musicP->elebeatSong->m_FolderNum) || g_NowCenter <= CENTER_DEFAULT_16 - (musicP->elebeatSong->m_FolderNum))
			{
				g_NowCenter = CENTER_DEFAULT_16;
				DrawFormatString(0, 710, Color::GetInstance().White(), _T("reset11111111111111111111111111111"));
			}
		}
		else if (selectMode == SelectMode::AA)
		{
			//AA時
			if (g_NowCenter >= CENTER_DEFAULT_16 + nowAA_16[2] || g_NowCenter <= CENTER_DEFAULT_16 - nowAA_16[2]) {
				g_NowCenter = CENTER_DEFAULT_16;
				DrawFormatString(300, 100, Color::GetInstance().White(), _T("reset"));
			}
		}
		else
		{
			//marker
		}
	}

	//AAの時
	if (selectMode == SelectMode::AA)
	{
		//選択AA描画
		DrawModiGraph(805, 580, 950, 580, 950, 725, 805, 725, g_AA[CENTER_DEFAULT_16 + nowselectAAnum], TRUE);
		//固定短調のリテラルを書く
		DrawLiteralString();
		//作曲者とかいろいろ書いてみる (クリアでないので後ろの引数は0)
		DrawMusicDetail(nowselectAAnum, 0, musicP);
		//ハイスコア描画
		DrawHiScore_16(nowselectAAnum, musicP);
		//色替え　難易度
		DrawOtherDetail(nowselectAAnum, musicP);
	}

	//左右パネルとか
	DrawPanelAll();

	debug(g_NowCenter, debug_AA);
	clsDx();
	//debug
	DrawBox(795, 0, 796, 768, Color::GetInstance().White(), TRUE);
	//マウスカーソルの表示
	DxLib::DrawRotaGraph2(input.mouseX, input.mouseY, 50, 50, 1.0f, count / 42.0, input.casolPic, TRUE);

	//マウスクリックのリングを描画
	m_mouseClickObjectList->Draw();

	//反転
	DxLib::ScreenFlip();
	if (input.keyPrtsc == 1)
		m_ssc->CaptureScreenshot();
}

void SelectMusic16keys::Finalize(const std::shared_ptr<Music> &musicP)
{
}