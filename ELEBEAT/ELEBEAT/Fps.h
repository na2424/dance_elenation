#ifndef FPS_H
#define FPS_H

//FPS制御クラス
class Fps
{
private:
	int m_startTime;
	int m_count;
	float m_fps;
	static const int N = 60;
	static const int FPS = 60;

public:
	Fps() = default;

	bool Fps::Update();

	void Fps::Draw() const;

	void Fps::Wait() const;
};

#endif //FPS_H