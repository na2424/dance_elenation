#include "BlueLightBall.h"
#include "define.h"
#include "DxLib.h"
#include <memory>
#include <functional>

#include "SpecialFiles.h"

using namespace std;
using namespace DxLib;

Image BlueLightBall::m_image = 0;

void BlueLightBall::Init(function<void(BlueLightBall*)> f, tstring imagePath = "Graphics/Common/particle.png")
{
	this->m_image = LoadGraph((SpecialFiles::DEFAULT_THEMES_DIR + imagePath).c_str());

	if (f == nullptr)
	{
		this->x = GetRand(WIN_WIDTH);
		this->y = GetRand(WIN_HEIGHT - 200) + 100;
		this->vx = 1 + static_cast<double>(GetRand(50) / 70.0);
		this->vy = -3.0 / 7.0 + static_cast<double>(GetRand(30) / 70.0);
		this->size = 0.07 + static_cast<double>(GetRand(50) / 300.0);
	}
	else
	{
		f(this);
	}
}

void BlueLightBall::Update(function<void(BlueLightBall*)> f)
{
	if (f == nullptr)
	{
		this->x += this->vx;
		this->y += this->vy;
		if (this->x > WIN_WIDTH + 30)
		{
			this->x = -40;
			this->y = GetRand(WIN_HEIGHT - 400) + 200;
			this->vx = 1 + static_cast<double>(GetRand(50) / 70.0);
			this->vy = -(3.0 / 7.0) / 2.0 + static_cast<double>(GetRand(30) / 70.0);
			this->size = 0.07 + static_cast<double>(GetRand(50) / 300.0);
		}
	}
	else
	{
		f(this);
	}
}

void BlueLightBall::Draw() const
{
	DrawRotaGraph(static_cast<int>(this->x), static_cast<int>(this->y), this->size, 0, this->m_image, TRUE);
}

BlueLightBallList::BlueLightBallList()
{
	m_blueLightBall.resize(BALL_MAX);
}

void BlueLightBallList::Init(function<void(BlueLightBall*)> f)
{
	for (auto& b : m_blueLightBall)
	{
		b.Init(f);
	}
}

void BlueLightBallList::Init(function<void(BlueLightBall*)> f, tstring imagePath)
{
	for (auto& b : m_blueLightBall)
	{
		b.Init(f, imagePath);
	}
}

void BlueLightBallList::Update(function<void(BlueLightBall*)> f)
{
	for (auto& b : m_blueLightBall)
	{
		b.Update(f);
	}
}

void BlueLightBallList::Update()
{
	for (auto& b : m_blueLightBall)
	{
		b.Update(nullptr);
	}
}

void BlueLightBallList::Draw()
{
	for (auto& b : m_blueLightBall)
	{
		b.Draw();
	}
}