#pragma once

struct Vector2D
{
	int x;
	int y;
};

class Rect
{
private:
	Vector2D _1;
	Vector2D _2;
public:
	explicit Rect(int x1, int y1, int x2, int y2)
	{
		_1.x = x1;
		_1.y = y1;
		_2.x = x2;
		_2.y = y2;
	}
	Vector2D GetPoint1() const
	{
		return _1;
	}
	Vector2D GetPoint2() const
	{
		return _2;
	}
	int GetWidth() const
	{
		return _2.x - _1.x;
	}
	int GetHeight() const
	{
		return _2.y - _2.y;
	}
};