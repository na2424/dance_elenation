/*========================================================================
____                         _____ _                  _   _
|  _ `  __ _ _ __   ___ ___  | ____| | ___ _ __   __ _| |_(_) ___  _ __
| | | |/ _` | '_ ` / __/ _ * |  _| | |/ _ ` '_ ` / _` | __| |/ _ `| '_ `
| |_| | (_| | | | | (_|  __/ | |___| |  __/ | | | (_| | |_| | (_) | | | |
|____/ *__,_|_| |_|*___*___| |_____|_|*___|_| |_|*__,_|___|_|*___/|_| |_|

 *Dance Elenation ver.1.65
 *2012/04/01--2017/04/27
 *Lib(BOOST,LUA,DxLib)
 *========================================================================*/

#define WIN32_LEAN_AND_MEAN

//#include <WinSock2.h>
//#include <windows.h>
#include <time.h>
#include "global.h"
#include "FileManager.h"
#include "Marker.h"
#include "Color.h"

/*================================
 * 開始
 *================================*/
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	//_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_DELAY_FREE_MEM_DF | _CRTDBG_CHECK_ALWAYS_DF | _CRTDBG_LEAK_CHECK_DF);

	// DxLibのログ出力しない
	SetOutApplicationLogValidFlag(FALSE);
	//タイトルを設定
	SetMainWindowText(_T("Dance Elenation"));
	//ランダムセット
	srand((unsigned int)time(NULL));
	//マルチスレッド許可
	SetMultiThreadFlag(TRUE);
	//非アクティブでも動く
	SetAlwaysRunFlag(TRUE);
	//描画先を裏画面に設定
	SetDrawScreen(DX_SCREEN_BACK);
	//マウスを表示させる
	SetMouseDispFlag(TRUE);
	//画面モード変更時にグラフィックハンドルをリセットしない
	SetChangeScreenModeGraphicsSystemResetFlag(FALSE);

	SetTransColor(255, 0, 255);
	SetUseTransColor(DX_MASKTRANS_NONE);
	//セーブデータの読み込み(無ければ書き出し)
	OptionSaveData saveDataOption;
	saveDataOption.LoadDat();
	// 描画可能領域セット
	SetDrawArea(0, 0, WIN_WIDTH, WIN_HEIGHT);

	constexpr TCHAR CHECK_WINDOW_MODE[] = _T("フルスクリーンモードに設定されています。\nウィンドウモードに変更しますか？");
	constexpr TCHAR FONT_PATH[] = _T("Themes\\DANCE ELENATION\\Fonts\\KGCG_NAA.otf");

	//スクリーンの変更
	if (OptionSaveData::g_IsFullScreen)
	{
		if (MessageBox(nullptr, CHECK_WINDOW_MODE, _T("起動オプション"), MB_YESNO) == IDYES)
		{
			SetGraphMode(WIN_WIDTH, WIN_HEIGHT, 32);
			ChangeWindowMode(TRUE);
			OptionSaveData::g_IsFullScreen = false;
			saveDataOption.SaveDat();
		}
		else
		{
			SetGraphMode(WIN_WIDTH + 64, WIN_HEIGHT, 32);
			ChangeWindowMode(FALSE);
			OptionSaveData::g_IsFullScreen = true;
		}
	}
	else
	{
		SetGraphMode(WIN_WIDTH, WIN_HEIGHT, 32);
		ChangeWindowMode(TRUE);
	}

	//拡大率
	SetWindowSizeExtendRate(0.8);
	SetWindowSizeChangeEnableFlag(TRUE);

	//ＤＸライブラリ初期化処理
	if (DxLib_Init() == -1) return -1;

	//フォント読み込み
	ChangeFont(FONT_PATH, DX_CHARSET_DEFAULT);
	ChangeFontType(DX_FONTTYPE_ANTIALIASING);

	//シングルトン初期化
	GamePlayOption::GetInstance().Init();
	FileManager::GetInstance().Init();
	JudgeCount::GetInstance().Init();
	Marker::GetInstance().Init();
	Input::GetInstance().Init();
	Color::GetInstance().Init();

	Scenes currentScene = SceneManager::g_CurrentScene;

	//シーンのサイクル.
	const auto startSceneCycle = [&currentScene, &musicP = std::make_shared<Music>()]()
	{
		//ファイル読み込み
		if (!FileManager::GetInstance().IsLoaded())
		{
			musicP.reset();
			musicP = std::make_shared<Music>();
			FileManager::GetInstance().LoadFile(musicP);
		}

		//次のシーン.
		currentScene = SceneManager::g_CurrentScene;
		//生成.
		auto scene = SceneManager::CreateScene();
		//初期化.
		scene->Initialize(musicP);
		//ループ.
		while (ProcessMessage() != -1 && currentScene == SceneManager::g_CurrentScene)
		{
			scene->Update(musicP);
		}
		//終了時.
		scene->Finalize(musicP);
	};

	//全体ループ
	while (ProcessMessage() != -1)
	{
		//シーン開始.
		startSceneCycle();

		if (SceneManager::g_CurrentScene == Scenes::FINISH) break;
	}

	//ＤＸライブラリ使用の終了処理
	DxLib_End();
	return 0;
}