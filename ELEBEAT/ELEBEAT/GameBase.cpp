#include "GameBase.h"
#include "Color.h"
#include "DxHelper.h"
#include "JudgeCount.h"


using namespace DxLib;

//Div画像用変数
std::array<Image, 2> GameBase::g_JudgeAreaImages = {};            //判定ゾーン画像を入れるための変数 判定の枠画像
std::array<Image, 6> GameBase::g_JudgeImages = {};                //判定画像
std::array<Image, 8> GameBase::g_GradeImages = {};                //評価画像を入れるための変数AAAからEまで
std::array<Image,10> GameBase::g_ComboElebeatImages = {};        //コンボ数字の画像
std::array<Image,10> GameBase::g_ComboParfect4KeysImages = {};   //コンボ数字の画像
std::array<Image,10> GameBase::g_ComboGrate4KeysImages = {};     //コンボ数字の画像
std::array<Image,10> GameBase::g_ComboGood4KeysImages = {};      //コンボ数字の画像
int GameBase::g_MusicDif = 0;
long int GameBase::g_Now = -1;

GameBase::GameBase() :
	m_funcCount(0),
	L(nullptr),
	m_isAuto(false),
	m_isCrap(false)
{
	m_DifImage.fill(0);
	//(ゲーム中の)スコア数字の画像
	LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/scoregame_num.png"), 10, 10, 1, 21, 35, m_scoreNumImage.data());
}

void GameBase::InitLua()
{
	// LuaのVMを生成する
	L = luaL_newstate();

	// Luaの標準ライブラリを開く
	luaL_openlibs(L);
	m_luaHelper.SetLua(L);
}

//経過時間と判定カウントを表示させる関数
void GameBase::DrawFormatStrings(long now) const
{
	auto& judge = JudgeCount::GetInstance();
	auto& color = Color::GetInstance();

	const auto lineHeight = 20;

	DxHelper::DrawOutlineString(JUDGE_X_PX, JUDGE_Y_PX + lineHeight * 0, color.White(), color.Black(), _T("PERFECT %4d"), judge.parfect);
	DxHelper::DrawOutlineString(JUDGE_X_PX, JUDGE_Y_PX + lineHeight * 1, color.White(), color.Black(), _T("GREAT   %4d"), judge.grate);
	DxHelper::DrawOutlineString(JUDGE_X_PX, JUDGE_Y_PX + lineHeight * 2, color.White(), color.Black(), _T("GOOD    %4d"), judge.good);
	DxHelper::DrawOutlineString(JUDGE_X_PX, JUDGE_Y_PX + lineHeight * 3, color.White(), color.Black(), _T("MISSED  %4d"), judge.missed);

	DxHelper::DrawOutlineString(JUDGE_X_PX, JUDGE_Y_PX + lineHeight * 5, color.White(), color.Black(), _T("OK      %4d"), judge.OK);
	DxHelper::DrawOutlineString(JUDGE_X_PX, JUDGE_Y_PX + lineHeight * 6, color.White(), color.Black(), _T("NG      %4d"), judge.NG);

	DxHelper::DrawOutlineString(JUDGE_X_PX, JUDGE_Y_PX + lineHeight * 8, color.White(), color.Black(), _T("MaxCombo%4d"), judge.maxCombo);

	DxHelper::DrawOutlineString(JUDGE_X_PX, JUDGE_Y_PX + lineHeight * 10, color.White(), color.Black(), _T("ms %d"), static_cast<int>(now));
}

void GameBase::DrawScore(int num)
{
	// numが十進数で何桁になるか調べる
	auto beamWidth = 0;
	for (auto i = 1; num >= i; i *= 10)
	{
		beamWidth++;
	}

	auto x = 0;
	if (SceneManager::g_PlayMode == SelectKeys::KEYS_4)
		x = 928;
	else
		x = 922;

	for (auto i = 0; i < beamWidth; i++)
	{
		DrawGraph(x, 680, m_scoreNumImage[num % 10], TRUE);
		num /= 10;
		x -= 20;
	}
}

void GameBase::DrawDifficultyNum(int num, std::array<int,10> pic) const
{
	// numが十進数で何桁になるか調べる
	auto beamWidth = 0;
	for (auto i = 1; num >= i; i *= 10) beamWidth++;

	// 画面右上に右詰で表示
	// xは描く数字(一桁一桁各々)の左端のX座標
	auto x = 880;
	if (num < 10) x = 868;
	for (auto i = 0; i < beamWidth; i++)
	{
		DrawGraph(x, 42, pic[num % 10], TRUE);
		num /= 10;
		x -= 24;
	}
}