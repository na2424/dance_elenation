#include "header.h"
#include "LoadingWindow.h"
#include <vector>
#include <tchar.h>
//#include <boost/algorithm/string.hpp>

#define IDD_LOADING_DIALOG              116
#define IDC_STATIC_MESSAGE1             1010
#define IDC_STATIC_MESSAGE2             1011
#define IDC_BUTTON_CLOSE                1011
#define IDC_VIEW_LOG                    1012
#define IDC_STATIC_MESSAGE3             1013
#define STM_SETIMAGE        0x0172
#define IDC_SPLASH                      1020
static HBITMAP g_hBitmap = nullptr;

void LoadingWindow::Load() const
{
	//CTestDlg* dlg = new CTestDlg;
	//dlg->Create(IDD_TEST_DIALOG);
}

LoadingWindow::LoadingWindow(HINSTANCE hInstance)
{
	//HINSTANCE hinstance = handle.Get();
	//hwnd = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_LOADING_DIALOG), NULL, WndProc);
	for (unsigned i = 0; i < 3; ++i)
		text[i] = "XXX"; /* always set on first call */
	SetText("Initializing hardware...");
	Paint();
}
LoadingWindow::~LoadingWindow()
{
	if (hwnd)
		DestroyWindow(hwnd);
}
void LoadingWindow::SetText(tstring str)
{
	std::vector<tstring> asMessageLines;
	//vector<tstring> str;
	boost::algorithm::split(asMessageLines, str, boost::is_any_of(_T("\n")), boost::token_compress_on);

	while (asMessageLines.size() < 3)
		asMessageLines.push_back("");

	const int msgs[] = { IDC_STATIC_MESSAGE1, IDC_STATIC_MESSAGE2, IDC_STATIC_MESSAGE3 };
	for (unsigned i = 0; i < 3; ++i)
	{
		if (text[i] == asMessageLines[i])
			continue;
		text[i] = asMessageLines[i];

		SendDlgItemMessage(hwnd, msgs[i], WM_SETTEXT, 0,
			(LPARAM)asMessageLines[i].c_str());
	}
}

BOOL CALLBACK LoadingWindow::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_INITDIALOG:
		g_hBitmap = NULL;//LoadWin32Surface("Data/splash.png");
		if (g_hBitmap == NULL)
			g_hBitmap = NULL;//LoadWin32Surface("Data/splash.bmp");
		SendMessage(
			GetDlgItem(hWnd, IDC_SPLASH),
			STM_SETIMAGE,
			(WPARAM)IMAGE_BITMAP,
			(LPARAM)(HANDLE)g_hBitmap);
		break;

	case WM_DESTROY:
		DeleteObject(g_hBitmap);
		g_hBitmap = NULL;
		break;
	}

	return FALSE;
}

void LoadingWindow::Paint()
{
	SendMessage(hwnd, WM_PAINT, 0, 0);

	/* Process all queued messages since the last paint.  This allows the window to
	* come back if it loses focus during load. */
	MSG msg;
	while (PeekMessage(&msg, hwnd, 0, 0, PM_NOREMOVE)) {
		GetMessage(&msg, hwnd, 0, 0);
		DispatchMessage(&msg);
	}
}