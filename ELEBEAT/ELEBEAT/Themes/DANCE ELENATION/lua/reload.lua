local reloadType = unpack(...)	-- 引数を受け取る
local files = {}				-- リロード対象スクリプトファイル
local reloadFuncs = {}			-- リロード後に実行する関数
local reloadFuncsSet = {}		-- リロード後に実行する関数の一致チェック用
local isReload　= true			-- リロードか通常ロードか

-- 以下のスクリプトファイルがこのゲームのすべてとする
local allFiles = {
	"_file_game.lua",
	"_file_game4keys.lua",
	"_file_key_select.lua",
	"_file_music_select.lua",
	"_file_options.lua",
	"_file_result.lua"
}

--リロード引数による各種設定
if reloadType == "load" then
	--通常ロードをここで処理する
	--全ファイルロード
	files = allFiles
	isReload　= false
else if reloadType == "chars" then
	--キャラクターロード時
	files = {
		"",
	}
else if reloadType == "all" then
	--全リロード時

