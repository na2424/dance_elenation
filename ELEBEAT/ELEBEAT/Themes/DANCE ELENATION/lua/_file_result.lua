--画像
gradeImage=		"grade_ele.png";
fullcomboImage=	"fullcombo.png";
dotImage=		"dot.png";
persentImage=	"percent.png";
resultTopImage=	"result_bar.png";
frameImage=		"result_pic.png";
clearedImage=	"cleared.png"
failedImage=		"failed.png"
--音源
resultInSound=		"eval_in.mp3";
resultLoopSound=	"loop.wav";

RANK_MAX = 100;
RANK_AAA = 99;
RANK_AA = 95;
RANK_A = 90;
RANK_B = 80;
RANK_C = 70;
RANK_D = 60;

--- ランクを返す関数
--- rateは達成率、Difは難易度(0→GSP,1→BSP,2→DSP,3→ESP,4→CSP)
function GetRank(rate,Dif)

	local rank_flag

	--MFC
	if rate==RANK_MAX then
		rank_flag=0
	--AAA
	elseif RANK_AAA<=rate and rate<RANK_MAX then
		rank_flag=1

	--AA
	elseif RANK_AA<=rate and rate<RANK_AAA then
		rank_flag=2

	--A
	elseif RANK_A<=rate and rate<RANK_AA then
		rank_flag=3

	--B
	elseif RANK_B<=rate and rate<RANK_A then
		rank_flag=4

	--C
	elseif RANK_C<=rate and rate<RANK_B then
		rank_flag=5

	--D
	elseif RANK_D<=rate and rate<RANK_C then
		rank_flag=6

	--E
	elseif 0<=rate and rate<RANK_D then
		rank_flag=7
	end
	
	return rank_flag
end