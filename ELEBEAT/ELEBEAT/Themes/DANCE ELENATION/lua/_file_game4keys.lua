
--array cg
judgeAreaImage = "_Fallback Receptor go 2x1.png"
judgeImage = "judge.png"
difImage = "dff.png"
--cg
lifeGageImage1 = "gage_bms.png"
lifeGageImage2 = "norm.png"
shaterImage1 = "shater01.png"
shaterImage2 = "shater02.png"
filterImage1 = "filter01.png"
filterImage2 = "filter02.png"
cursorImage = "cursor.png"
startImage = "start.png"
readyImage = "ready.png"
goImage = "go.png"
comboImage = "combo.png"
failedImage = "failed.png"
back = "game_back.png"
backgroundImage = "game_back.png"
backUnderImage = "game_back_under.png"
elebeatImage = "elebeat.png"
bpmImage = "bpm.png"
--se
readyGoSound = "readyGo.mp3"
handClapSound = "assist tick.ogg"
failedSound = "failed.mp3"
backSound = "Common back.ogg"

function UpdateScore(maxnote,pa,gr,go,OK)
	--基本点 : 1,000,000÷(総ノート数＋総FA数＋総SA数)
	--スコア : 基本点×(PERFECT数＋O.K.数)＋{(基本点÷2)－10}×GREAT数＋{(基本点÷5)－10}×GOOD数
	--10点単位、端数は切り捨て
	local basePoints = 1000000.0 / maxnote
	local score = basePoints*(pa + OK) + ((basePoints/2.0)-10)*gr + ((basePoints/5.0)-10)*go
	return math.floor(score/10) *10;
end