--Image
backoverImage=	"musicselect_bar.png";
nanidoImage=		"selectmusic02.png";
backfrontImage=	"selectmusic01.png";
setumei1Image=	"setumei01.png";
setumei2Image=	"setumei02.png";
blendImage=	"blend.png";
blackImage=	"black.png";
rightImage=	"right.png";
leftImage=	"left.png";
rightClickImage="right_click.png";
leftClickImage=	"left_click.png";
markerWakuImage=	"marker_waku.png";
playerOptionImage=	"player_option_waku.png";
bpmWakuImage=	"sentakugamen-bpm.png";
selecterImage=	"icon.png";
raderImage=	"GrooveRadar.png";
loadingImage=	"loading.png";

--Audio
selectdifsound=	"ScreenSelectMusic_difficulty_harder.ogg";
wheel_sound=	"MusicWheel_change.ogg";
decide_sound=	"decide.mp3";
expand_sound=	"cursor5.ogg";
mrk_sound=	"meka_ta_sui01.mp3";
close_sound=	"cursor1.ogg";
mrk_move_sound=	"b_001.mp3";