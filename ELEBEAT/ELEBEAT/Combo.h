#ifndef COMBO_H
#define COMBO_H

//コンボに関する構造体
class Combo
{
private:
	int m_effect100ComboFrame;

public:
	int m_AnimationFrame;            //コンボが増える際のアニメーションで使う変数
	int m_ComboNum;                //コンボ数を入れるための変数
	int m_ComboColorState;

	Combo(void) :
		m_effect100ComboFrame(25),
		m_AnimationFrame(0),
		m_ComboNum(0),
		m_ComboColorState(0) {}

	void ShowEffect100Combo(int&& filter02);
};

#endif