#ifndef KEYS_SELECT_H
#define KEYS_SELECT_H
#include <array>
#include <vector>
#include <memory>
#include "DxLib.h"
#include "SceneManager.h"

class BlueLightBallList;
class MouseClickObjectList;
class TimerDrawer;
//class VECTOR;

using namespace DxLib;

class KeysSelect : public Scene
{
private:

	int animationFrame;             //キーの選択が変更された時のフラグ
	int m_time;                     //タイマー

	int which = 0;
	int casol_high = 0;        //カーソルの位置を示す(0→4KEYS、1→9KEYS、2→16KEYS)
	int place_x_num = 122;    //カーソルのx座標の位置
	int startTime;
	int flag_a;
	int flag_b;

	std::unique_ptr<VECTOR> offsetModelPosition;

	std::unique_ptr<TimerDrawer> numberDraw;
	std::unique_ptr<BlueLightBallList> m_lightballList;
	std::unique_ptr<MouseClickObjectList> m_mouseClickObjectList;

	//-------------------------
	// 定数.
	//-------------------------
	const int place_x = 100;        //カーソルの座標の基準x座標
	const int place_y = 177;        //カーソルの座標の基準y座標
	const int spacing = 358;        //カーソルの相対的な移動座標
	const int BALL_MAX = 39;

	//-------------------------
	// 画像.
	//-------------------------
	std::array<Image, 3> keysImage;
	std::array<Image, 3> keysModeImage;
	Image selectCursorImage;        //カーソルの画像のハンドル
	Image keysselectOverImage;
	Image informationImage;

	//-------------------------
	// 音声.
	//-------------------------
	Sound keysSelectMusicSound;     //ループ音源
	Sound moveSound;                //移動サウンド
	Sound BackSound;                //バック時のサウンド

	//-------------------------
	// モデル.
	//-------------------------
	std::array<Model, 2> ringModel;

	//-------------------------
	// 関数.
	//-------------------------
	void ReadLua(void);
	void LoadSceneImage();
	void LoadSceneSound();
	void LoadSceneModel();

	void DrawFrame(const int &count) const;
	void DrawBackBox(const int count) const;

public:
	KeysSelect();

	void Initialize(const std::shared_ptr<Music> &musicP) override;
	void Update(const std::shared_ptr<Music> &musicP) override;
	void Finalize(const std::shared_ptr<Music> &musicP) override;
};

#endif