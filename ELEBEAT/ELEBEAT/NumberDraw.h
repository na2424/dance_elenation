#ifndef NUMBER_DRAW_H
#define NUMBER_DRAW_H
#include "Dxlib.h"
#include <array>

//共有タイマー数字描画クラス
class TimerDrawer
{
public:
	//右上のタイマー画像を入れるための配列、各画面共通で使う。
	std::array<int,10> m_TimerImages;
	//関数
	void DrawMenuTimer(int num, int addY = 0);
	//void DrawSelectMusicTimer(int num, int addY);
	TimerDrawer() {
		LoadDivGraph(_T("Themes/DANCE ELENATION/Numbers/timer_num.png"), 10, 10, 1, 80, 80, m_TimerImages.data());
	}
};
#endif