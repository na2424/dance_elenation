#ifndef SELECTMUSIC_4KEYS_H
#define SELECTMUSIC_4KEYS_H

#include "SelectMusicBase.h"
#include "define.h"
#include <atomic>

class BlueLightBall;

//-----------------------------
// 4keys
//-----------------------------
class SelectMusic4Keys : public SelectMusicBase
{
public:
	SelectMusic4Keys();

	void Initialize(const std::shared_ptr<Music> &musicP) override;
	void Update(const std::shared_ptr<Music>& musicP) override;
	void Finalize(const std::shared_ptr<Music> &musicP) override;

	void LoadingAnimation(void) const;
	static int g_SelectFolderIndex;
	static int g_Beforecenter4;                                        //folder->AAの時に保持　前のfolder配置を保持
																	 // チャートのポリゴン座標を収納する配列
	VECTOR m_Points[ITEM_NUM];
	VECTOR m_ViewPoint[ITEM_NUM];

	//4keys用
	static std::vector<int> g_Banner;
	static std::vector<int> g_TempBanner;
	static std::vector<int> g_KindFolder4;

	static std::vector<double> g_BannerSizeRatio;
	static std::vector<double> g_TempBannerSizeRatio;
	static std::vector<double> g_KindFolder4SizeRatio;

private:

	std::shared_ptr<TimerDrawer> m_numberDraw;
	std::vector<BlueLightBall> lightball;

	int m_displayTime;
	int m_internalTime;
	int m_markerY;
	EscapeState m_breakState;
	int m_difRect;
	int m_selectercounter;
	int m_speedIndex;
	int m_optionFlag;

	//bool
	bool m_speedFlag;
	bool m_isBack;

	int m_reverseFlag;
	int m_noteskinFlag;

	int m_musicKind;                //フォルダ数
	int m_musicNum;                 //楽曲数
	std::vector<int> m_musicKind4Max;    //各フォルダ内楽曲数
	int m_selectFolderNum;            //現在選択中のフォルダ番号
	int m_selectMusicDif;             //現在選択中の難易度

	std::atomic<bool> m_isLoadedGraphAll;

	std::array<std::array<int, 3>, 5> m_DifNumber;

	//---------------------
	// 定数.
	//---------------------
	const int BALL_MAX = 39;
	const unsigned int m_difficultBoxColor[5] =
	{
		GetColor(0, 224, 224),
		GetColor(255, 255, 0),
		GetColor(255, 0, 0),
		GetColor(0, 255, 0),
		GetColor(224, 0, 224)
	};

	//---------------------
	// フォント.
	//---------------------
	std::array<int, 4> m_fontHandle;

	//---------------------
	// 画像.
	//---------------------
	std::array<std::array<Image, 10>, 6> difnumberColorImage;
	std::array<Image, 6> m_footImage;
	std::array<Image, 5> m_fullcombo;
	std::array<Image, 8> m_rankImage;
	std::array<Image, 5> m_difImage;
	Image m_aaFrameImage;

	//---------------------
	// 音声.
	//---------------------


	//---------------------
	// 関数.
	//---------------------
	void LoadSceneImage(void);
	void LoadSceneSound(void);
	void GraphCoordinatesSet(void);
	static void MarkerCoordinatesSet(void);
	void DrawGraphAll(int nowcenter);
	KeySelectMusicType CatchKey(int nowcenter, int& selectdifsound, int flag_a, bool *s_flag);
	int ChangeMode(int nowcenter, const std::shared_ptr<Music> &musicP);
	void SetMode(int nowcenter, const std::shared_ptr<Music> &musicP);
	void LoadGraphAll(const std::shared_ptr<Music> &musicP);
	void DrawMusicInfo(int musicNumber4, int fontHandle[], int counter, const std::shared_ptr<Music> &musicP) const;
	int NowFolderNumber(int nowcenter, const std::shared_ptr<Music> &musicP) const;
	int NowAANumber(int nowcenter) const;
	int SetAAGraph(int firstAA, int endAA, int nowcenter);
	void Delselectmusic(void);
	void CutBpm(int num, int nowcenter, const std::shared_ptr<Music> &musicP) const;
	void EscapeSelect(EscapeState escapeState, int nowcenter);
	static int GetRandom(int min, int max, const std::shared_ptr<Music> &musicP);
	void ShowBpm(void);
	void CalculateDifNumber(std::array<std::array<int, 3>, 5> &number, int nowcenter, const std::shared_ptr<Music> &musicP);
	void DrawLR(int rightPic, int leftPic) const;
	void DrawLR_Clicked(int rightClickPic, int leftClickPic);
	void DrawHiScore(int nowcenter, int count, int fullComboImage[], int rankImage[], int fontHandle[], bool& s_flag, int flag_a, const std::shared_ptr<Music> &musicP, VECTOR points[]);
	void LoadHiScore(int nowcenter, const std::shared_ptr<Music> &musicP);
	void LayerStringDraw(int stringX, int stirngY, const tstring& drawstring, int fontHandle, int colorFlag) const;
	void DelSelectMusic(int& loopsound, int& backSound);
	void BuildChart(const double *status, VECTOR *points) const;
	void DrawChart(void) const;
	void RegisterLuaFunctions(void);
	void UpdateLoadingImage(lua_State *L) const;

	void ShowPlayerOption(int FontHandle[], int option_flag, int speed_flag, int rv_flag, int noteSkinType, int marker_y);
	void DrawPlayerOptionList(const std::vector<tstring>& nameList, int fontHandle, int optionFlag, int flag, int markerY, int listLength, int n, int width) const;

	void OnTimeUpFolder(const std::shared_ptr<Music> &musicP);
	void OnTimeUpAA() const;
};

#endif