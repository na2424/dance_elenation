#include <windows.h>
#include "DxLib.h"
#include "define.h"
#include "extern.h"
#include "header.h"
#include "define.h"
#include "savedata_class.h"



int OptionSaveData::g_SongCount = 1;           //現在何曲目かを入れる変数、初期値は１。(またタイトルに戻ったら1を代入する)
int OptionSaveData::g_SongCountSetting = 3;    //1プレイで何曲まで遊べるかを入れる変数。オプションで変更可能
bool OptionSaveData::g_IsEventMode = false;    //EVENTMODEのON,OFF(ON→１プレイで何曲でも遊べる、OFF→１プレイで設定した曲数だけ遊べる)
int OptionSaveData::g_Style = 1;               //4:3(CSstyle)か5:4(ACstyle)かを入れる変数
bool OptionSaveData::g_IsFullScreen = false;   //ウィンドウモードかフルスクリーンモードかを入れる変数
int OptionSaveData::g_ShowFps = 0;

//オプションロード関数
void OptionSaveData::LoadDat(void)
{
	TCHAR *name = _T("Data\\save_data_options.dat");
	FILE *fp1;
	errno_t error;
	/*ファイルの内容からデータをロード*/

	if ((error = _tfopen_s(&fp1, name, _T("rb"))) != 0)
	{
		//ファイルが無かったら.datを新規作成
		error = _tfopen_s(&fp1, name, _T("wb"));

		this->m_isEventMode = OptionSaveData::g_IsEventMode;
		this->m_songCount = OptionSaveData::g_SongCountSetting;
		this->m_style = OptionSaveData::g_Style;
		this->m_isFullScreen = OptionSaveData::g_IsFullScreen;
		this->m_isShowFps = OptionSaveData::g_ShowFps;
		fwrite(this, sizeof(OptionSaveData), 1, fp1);
		fclose(fp1);
	}
	else
	{
		//ファイルがあればデータを読み込む
		fread(this, sizeof(OptionSaveData), 1, fp1);

		OptionSaveData::g_IsEventMode = this->m_isEventMode;
		OptionSaveData::g_SongCountSetting = this->m_songCount;
		OptionSaveData::g_Style = this->m_style;
		OptionSaveData::g_IsFullScreen = this->m_isFullScreen;
		OptionSaveData::g_ShowFps = this->m_isShowFps;
		fclose(fp1);
	}
}

//オプションセーブ関数
void OptionSaveData::SaveDat(void)
{
	TCHAR *name = _T("Data\\save_data_options.dat");
	FILE *fp1;
	errno_t error;

	this->m_isEventMode = OptionSaveData::g_IsEventMode;
	this->m_songCount = OptionSaveData::g_SongCountSetting;
	this->m_style = OptionSaveData::g_Style;
	this->m_isFullScreen = OptionSaveData::g_IsFullScreen;
	this->m_isShowFps = OptionSaveData::g_ShowFps;

	/*データの内容をファイルにセーブ*/
	if ((error = _tfopen_s(&fp1, name, _T("wb"))) != 0)
	{
		printfDx(_T("ファイルオープンエラー\n"));
	}
	else
	{
		fwrite(this, sizeof(OptionSaveData), 1, fp1);
		fclose(fp1);
	}
}

void ScoreData::TranslateToSafeFileName(tstring& strSafeFileName)
{
	//禁止半角を削除
	Replace(strSafeFileName, _T("\\"), _T(""));
	Replace(strSafeFileName, _T("/"), _T(""));
	Replace(strSafeFileName, _T("*"), _T(""));
	Replace(strSafeFileName, _T(":"), _T(""));
	Replace(strSafeFileName, _T("?"), _T(""));
	Replace(strSafeFileName, _T("<"), _T(""));
	Replace(strSafeFileName, _T(">"), _T(""));
	Replace(strSafeFileName, _T("\""), _T(""));
	Replace(strSafeFileName, _T("|"), _T(""));
}

//  文字列を置換する
void ScoreData::Replace(tstring& string1, tstring string2, tstring string3)
{
	auto pos(string1.find(string2));

	while (pos != tstring::npos)
	{
		string1.replace(pos, string2.length(), string3);
		pos = string1.find(string2, pos + string3.length());
	}
}